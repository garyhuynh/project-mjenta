﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Accounts
{
    using CodeFluent.Runtime;
    using CodeFluent.Runtime.Utilities;
    using Kendo.Mvc.UI;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    
    
    // CodeFluent Entities generated (http://www.softfluent.com). Date: Monday, 08 August 2016 12:42.
    // Build:1.0.61214.0840
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CodeFluent Entities", "1.0.61214.0840")]
    public enum AccountType
    {
        
        customer = 0,
        
        club = 1,
    }
}
