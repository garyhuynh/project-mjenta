﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Events
{
    using CodeFluent.Runtime;
    using CodeFluent.Runtime.Utilities;
    using Kendo.Mvc.UI;
    using System.Collections.Generic;
    
    
    // CodeFluent Entities generated (http://www.softfluent.com). Date: Monday, 08 August 2016 12:42.
    // Build:1.0.61214.0840
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CodeFluent Entities", "1.0.61214.0840")]
    [System.SerializableAttribute()]
    [System.ComponentModel.DataObjectAttribute()]
    [System.Diagnostics.DebuggerDisplayAttribute("EK={EntityKey}, Comment={Comment}, Id={Id}")]
    [System.ComponentModel.TypeConverterAttribute(typeof(CodeFluent.Runtime.Design.NameTypeConverter))]
    public partial class EventRoll : System.ICloneable, System.IComparable, System.IComparable<Events.EventRoll>, CodeFluent.Runtime.ICodeFluentCollectionEntity<int>, System.ComponentModel.IDataErrorInfo, CodeFluent.Runtime.ICodeFluentMemberValidator, CodeFluent.Runtime.ICodeFluentSummaryValidator, System.IEquatable<Events.EventRoll>
    {
        
        private bool _raisePropertyChangedEvents = true;
        
        private CodeFluent.Runtime.CodeFluentEntityState _entityState;
        
        private byte[] _rowVersion;
        
        private int _id = -1;
        
        private bool _isAttending = CodeFluentPersistence.DefaultBooleanValue;
        
        private string _comment = default(string);
        
        private int _customerId = -1;
        
        [System.NonSerializedAttribute()]
        private Users.Customer _customer = null;
        
        private int _eventBookingId = -1;
        
        [System.NonSerializedAttribute()]
        private Events.EventBooking _eventBooking = null;
        
        public EventRoll()
        {
            this._entityState = CodeFluent.Runtime.CodeFluentEntityState.Created;
        }
        
        [System.ComponentModel.BrowsableAttribute(false)]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public virtual bool RaisePropertyChangedEvents
        {
            get
            {
                return this._raisePropertyChangedEvents;
            }
            set
            {
                this._raisePropertyChangedEvents = value;
            }
        }
        
        public virtual string EntityKey
        {
            get
            {
                return this.Id.ToString();
            }
            set
            {
                this.Id = ((int)(ConvertUtilities.ChangeType(value, typeof(int), -1)));
            }
        }
        
        public virtual string EntityDisplayName
        {
            get
            {
                return this.Comment;
            }
        }
        
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        [System.ComponentModel.DataObjectFieldAttribute(false, true)]
        [System.ComponentModel.TypeConverterAttribute(typeof(CodeFluent.Runtime.Design.ByteArrayConverter))]
        public byte[] RowVersion
        {
            get
            {
                return this._rowVersion;
            }
            set
            {
                if (((value != null) 
                            && (value.Length == 0)))
                {
                    value = null;
                }
                this._rowVersion = value;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("RowVersion"));
            }
        }
        
        [System.ComponentModel.DefaultValueAttribute(((int)(-1)))]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=false, Type=typeof(int))]
        [System.ComponentModel.DataObjectFieldAttribute(true)]
        public int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                if ((System.Collections.Generic.EqualityComparer<int>.Default.Equals(value, this._id) == true))
                {
                    return;
                }
                int oldKey = this._id;
                this._id = value;
                try
                {
                    this.OnCollectionKeyChanged(oldKey);
                }
                catch (System.ArgumentException )
                {
                    this._id = oldKey;
                    return;
                }
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Id"));
            }
        }
        
        [System.ComponentModel.DefaultValueAttribute(CodeFluentPersistence.DefaultBooleanValue)]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=false, Type=typeof(bool))]
        public bool isAttending
        {
            get
            {
                return this._isAttending;
            }
            set
            {
                this._isAttending = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("isAttending"));
            }
        }
        
        [System.ComponentModel.DefaultValueAttribute(default(string))]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Type=typeof(string))]
        public string Comment
        {
            get
            {
                return this._comment;
            }
            set
            {
                this._comment = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Comment"));
            }
        }
        
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=false)]
        [System.ComponentModel.DataObjectFieldAttribute(true)]
        public int CustomerId
        {
            get
            {
                if (((this._customerId == -1) 
                            && (this._customer != null)))
                {
                    this._customerId = this._customer.Id;
                }
                return this._customerId;
            }
            set
            {
                if ((System.Collections.Generic.EqualityComparer<int>.Default.Equals(value, this.CustomerId) == true))
                {
                    return;
                }
                this._customer = null;
                this._customerId = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Customer"));
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("CustomerId"));
            }
        }
        
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public Users.Customer Customer
        {
            get
            {
                if ((this._customer == null))
                {
                    this._customer = Users.Customer.Load(this._customerId);
                }
                return this._customer;
            }
            set
            {
                this._customerId = -1;
                this._customer = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("Customer"));
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("CustomerId"));
            }
        }
        
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=false)]
        [System.ComponentModel.DataObjectFieldAttribute(true)]
        public int EventBookingId
        {
            get
            {
                if (((this._eventBookingId == -1) 
                            && (this._eventBooking != null)))
                {
                    this._eventBookingId = this._eventBooking.Id;
                }
                return this._eventBookingId;
            }
            set
            {
                if ((System.Collections.Generic.EqualityComparer<int>.Default.Equals(value, this.EventBookingId) == true))
                {
                    return;
                }
                this._eventBooking = null;
                this._eventBookingId = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("EventBooking"));
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("EventBookingId"));
            }
        }
        
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public Events.EventBooking EventBooking
        {
            get
            {
                if ((this._eventBooking == null))
                {
                    this._eventBooking = Events.EventBooking.Load(this._eventBookingId);
                }
                return this._eventBooking;
            }
            set
            {
                this._eventBookingId = -1;
                this._eventBooking = value;
                this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Modified;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("EventBooking"));
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("EventBookingId"));
            }
        }
        
        string System.ComponentModel.IDataErrorInfo.Error
        {
            get
            {
                return this.Validate(System.Globalization.CultureInfo.CurrentCulture);
            }
        }
        
        string System.ComponentModel.IDataErrorInfo.this[string columnName]
        {
            get
            {
                return CodeFluentPersistence.ValidateMember(System.Globalization.CultureInfo.CurrentCulture, this, columnName, null);
            }
        }
        
        int CodeFluent.Runtime.Utilities.IKeyable<System.Int32>.Key
        {
            get
            {
                return this.Id;
            }
        }
        
        public virtual CodeFluent.Runtime.CodeFluentEntityState EntityState
        {
            get
            {
                return this._entityState;
            }
            set
            {
                if ((System.Collections.Generic.EqualityComparer<CodeFluent.Runtime.CodeFluentEntityState>.Default.Equals(value, this.EntityState) == true))
                {
                    return;
                }
                if (((this._entityState == CodeFluent.Runtime.CodeFluentEntityState.ToBeDeleted) 
                            && (value == CodeFluent.Runtime.CodeFluentEntityState.Modified)))
                {
                    return;
                }
                if (((this._entityState == CodeFluent.Runtime.CodeFluentEntityState.Created) 
                            && (value == CodeFluent.Runtime.CodeFluentEntityState.Modified)))
                {
                    return;
                }
                this._entityState = value;
                this.OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("EntityState"));
            }
        }
        
        [field:System.NonSerializedAttribute()]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        [field:System.NonSerializedAttribute()]
        public event CodeFluent.Runtime.CodeFluentEntityActionEventHandler EntityAction;
        
        [field:System.NonSerializedAttribute()]
        public event System.EventHandler<CodeFluent.Runtime.Utilities.KeyChangedEventArgs<int>> KeyChanged;
        
        protected virtual void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((this.RaisePropertyChangedEvents == false))
            {
                return;
            }
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, e);
            }
        }
        
        protected virtual void OnEntityAction(CodeFluent.Runtime.CodeFluentEntityActionEventArgs e)
        {
            if ((this.EntityAction != null))
            {
                this.EntityAction(this, e);
            }
        }
        
        public virtual bool Equals(Events.EventRoll eventRoll)
        {
            if ((eventRoll == null))
            {
                return false;
            }
            if ((this.Id == -1))
            {
                return base.Equals(eventRoll);
            }
            return (this.Id.Equals(eventRoll.Id) == true);
        }
        
        public override int GetHashCode()
        {
            return this._id;
        }
        
        public override bool Equals(object obj)
        {
            Events.EventRoll eventRoll = null;
			eventRoll = obj as Events.EventRoll;
            return this.Equals(eventRoll);
        }
        
        int System.IComparable.CompareTo(object value)
        {
            Events.EventRoll eventRoll = null;
eventRoll = value as Events.EventRoll;
            if ((eventRoll == null))
            {
                throw new System.ArgumentException("value");
            }
            int localCompareTo = this.CompareTo(eventRoll);
            return localCompareTo;
        }
        
        public virtual int CompareTo(Events.EventRoll eventRoll)
        {
            if ((eventRoll == null))
            {
                throw new System.ArgumentNullException("eventRoll");
            }
            int localCompareTo = this.Id.CompareTo(eventRoll.Id);
            return localCompareTo;
        }
        
        public virtual string Validate(System.Globalization.CultureInfo culture)
        {
            return CodeFluentPersistence.Validate(culture, this, null);
        }
        
        public virtual void Validate(System.Globalization.CultureInfo culture, System.Collections.Generic.IList<CodeFluent.Runtime.CodeFluentValidationException> results)
        {
            CodeFluent.Runtime.CodeFluentEntityActionEventArgs evt = new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Validating, true, results);
            evt.Culture = culture;
            this.OnEntityAction(evt);
            if ((evt.Cancel == true))
            {
                string externalValidate;
                if ((evt.Argument != null))
                {
                    externalValidate = evt.Argument.ToString();
                }
                else
                {
                    externalValidate = TennisManager.Resources.Manager.GetStringWithDefault(culture, "Events.EventRoll.ExternalValidate", "Type \'Events.EventRoll\' cannot be validated.", null);
                }
                CodeFluentPersistence.AddValidationError(results, externalValidate);
            }
            CodeFluentPersistence.ValidateMember(culture, results, this, null);
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Validated, false, results));
        }
        
        public void Validate()
        {
            string var = this.Validate(System.Globalization.CultureInfo.CurrentCulture);
            if ((var != null))
            {
                throw new CodeFluent.Runtime.CodeFluentValidationException(var);
            }
        }
        
        string CodeFluent.Runtime.ICodeFluentValidator.Validate(System.Globalization.CultureInfo culture)
        {
            string localValidate = this.Validate(culture);
            return localValidate;
        }
        
        void CodeFluent.Runtime.ICodeFluentMemberValidator.Validate(System.Globalization.CultureInfo culture, string memberName, System.Collections.Generic.IList<CodeFluent.Runtime.CodeFluentValidationException> results)
        {
            this.ValidateMember(culture, memberName, results);
        }
        
        public virtual bool Delete()
        {
            bool ret = false;
            CodeFluent.Runtime.CodeFluentEntityActionEventArgs evt = new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Deleting, true);
            this.OnEntityAction(evt);
            if ((evt.Cancel == true))
            {
                return ret;
            }
            if ((this.EntityState == CodeFluent.Runtime.CodeFluentEntityState.Deleted))
            {
                return ret;
            }
            if ((this.RowVersion == null))
            {
                return ret;
            }
            CodeFluent.Runtime.CodeFluentPersistence persistence = CodeFluentContext.Get(TennisManager.Constants.TennisManagerStoreName).Persistence;
            persistence.CreateStoredProcedureCommand(null, "EventRoll", "Delete");
            persistence.AddRawParameter("@Id", this.Id);
            persistence.AddParameter("@_rowVersion", this.RowVersion);
            persistence.ExecuteNonQuery();
            this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Deleted;
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Deleted, false, false));
            ret = true;
            return ret;
        }
        
        protected virtual void ReadRecord(System.Data.IDataReader reader, CodeFluent.Runtime.CodeFluentReloadOptions options)
        {
            if ((reader == null))
            {
                throw new System.ArgumentNullException("reader");
            }
            if ((((options & CodeFluent.Runtime.CodeFluentReloadOptions.Properties) 
                        == 0) 
                        == false))
            {
                this._id = CodeFluentPersistence.GetReaderValue(reader, "Id", ((int)(-1)));
                this._isAttending = CodeFluentPersistence.GetReaderValue(reader, "isAttending", ((bool)(CodeFluentPersistence.DefaultBooleanValue)));
                this._comment = CodeFluentPersistence.GetReaderValue(reader, "Comment", ((string)(default(string))));
                this.CustomerId = CodeFluentPersistence.GetReaderValue(reader, "Customer_Id", ((int)(-1)));
                this.EventBookingId = CodeFluentPersistence.GetReaderValue(reader, "EventBooking_Id", ((int)(-1)));
            }
            if ((((options & CodeFluent.Runtime.CodeFluentReloadOptions.RowVersion) 
                        == 0) 
                        == false))
            {
                this._rowVersion = CodeFluentPersistence.GetReaderValue(reader, "_rowVersion", ((byte[])(null)));
            }
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.ReadRecord, false, false));
        }
        
        void CodeFluent.Runtime.ICodeFluentEntity.ReadRecord(System.Data.IDataReader reader)
        {
            this.ReadRecord(reader, CodeFluent.Runtime.CodeFluentReloadOptions.Default);
        }
        
        protected virtual void ReadRecordOnSave(System.Data.IDataReader reader)
        {
            if ((reader == null))
            {
                throw new System.ArgumentNullException("reader");
            }
            this._id = CodeFluentPersistence.GetReaderValue(reader, "Id", ((int)(-1)));
            this._rowVersion = CodeFluentPersistence.GetReaderValue(reader, "_rowVersion", ((byte[])(null)));
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.ReadRecordOnSave, false, false));
        }
        
        void CodeFluent.Runtime.ICodeFluentEntity.ReadRecordOnSave(System.Data.IDataReader reader)
        {
            this.ReadRecordOnSave(reader);
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public static Events.EventRoll Load(int id)
        {
            if ((id == -1))
            {
                return null;
            }
            Events.EventRoll eventRoll = new Events.EventRoll();
            CodeFluent.Runtime.CodeFluentPersistence persistence = CodeFluentContext.Get(TennisManager.Constants.TennisManagerStoreName).Persistence;
            persistence.CreateStoredProcedureCommand(null, "EventRoll", "Load");
            persistence.AddRawParameter("@Id", id);
            System.Data.IDataReader reader = null;
            try
            {
                reader = persistence.ExecuteReader();
                if ((reader.Read() == true))
                {
                    eventRoll.ReadRecord(reader, CodeFluent.Runtime.CodeFluentReloadOptions.Default);
                    eventRoll.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Unchanged;
                    return eventRoll;
                }
            }
            finally
            {
                if ((reader != null))
                {
                    reader.Dispose();
                }
                persistence.CompleteCommand();
            }
            return null;
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public static Events.EventRoll LoadById(int id)
        {
            if ((id == -1))
            {
                return null;
            }
            Events.EventRoll eventRoll = new Events.EventRoll();
            CodeFluent.Runtime.CodeFluentPersistence persistence = CodeFluentContext.Get(TennisManager.Constants.TennisManagerStoreName).Persistence;
            persistence.CreateStoredProcedureCommand(null, "EventRoll", "LoadById");
            persistence.AddRawParameter("@Id", id);
            System.Data.IDataReader reader = null;
            try
            {
                reader = persistence.ExecuteReader();
                if ((reader.Read() == true))
                {
                    eventRoll.ReadRecord(reader, CodeFluent.Runtime.CodeFluentReloadOptions.Default);
                    eventRoll.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Unchanged;
                    return eventRoll;
                }
            }
            finally
            {
                if ((reader != null))
                {
                    reader.Dispose();
                }
                persistence.CompleteCommand();
            }
            return null;
        }
        
        public virtual bool Reload(CodeFluent.Runtime.CodeFluentReloadOptions options)
        {
            bool ret = false;
            if ((this.Id == -1))
            {
                return ret;
            }
            CodeFluent.Runtime.CodeFluentPersistence persistence = CodeFluentContext.Get(TennisManager.Constants.TennisManagerStoreName).Persistence;
            persistence.CreateStoredProcedureCommand(null, "EventRoll", "Load");
            persistence.AddRawParameter("@Id", this.Id);
            System.Data.IDataReader reader = null;
            try
            {
                reader = persistence.ExecuteReader();
                if ((reader.Read() == true))
                {
                    this.ReadRecord(reader, options);
                    this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Unchanged;
                    ret = true;
                }
                else
                {
                    this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Deleted;
                }
            }
            finally
            {
                if ((reader != null))
                {
                    reader.Dispose();
                }
                persistence.CompleteCommand();
            }
            return ret;
        }
        
        protected virtual bool BaseSave(bool force)
        {
            if ((this.EntityState == CodeFluent.Runtime.CodeFluentEntityState.ToBeDeleted))
            {
                this.Delete();
                return false;
            }
            CodeFluent.Runtime.CodeFluentEntityActionEventArgs evt = new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Saving, true);
            this.OnEntityAction(evt);
            if ((evt.Cancel == true))
            {
                return false;
            }
            CodeFluentPersistence.ThrowIfDeleted(this);
            this.Validate();
            if (((force == false) 
                        && (this.EntityState == CodeFluent.Runtime.CodeFluentEntityState.Unchanged)))
            {
                return false;
            }
            CodeFluent.Runtime.CodeFluentPersistence persistence = CodeFluentContext.Get(TennisManager.Constants.TennisManagerStoreName).Persistence;
            persistence.CreateStoredProcedureCommand(null, "EventRoll", "Save");
            persistence.AddRawParameter("@Id", this.Id);
            persistence.AddRawParameter("@isAttending", this.isAttending);
            persistence.AddRawParameter("@Comment", this.Comment);
            persistence.AddRawParameter("@Customer_Id", this.CustomerId);
            persistence.AddRawParameter("@EventBooking_Id", this.EventBookingId);
            persistence.AddParameter("@_trackLastWriteUser", persistence.Context.User.Name);
            persistence.AddParameter("@_rowVersion", this.RowVersion);
            System.Data.IDataReader reader = null;
            try
            {
                reader = persistence.ExecuteReader();
                if ((reader.Read() == true))
                {
                    this.ReadRecordOnSave(reader);
                }
                CodeFluentPersistence.NextResults(reader);
            }
            finally
            {
                if ((reader != null))
                {
                    reader.Dispose();
                }
                persistence.CompleteCommand();
            }
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.Saved, false, false));
            this.EntityState = CodeFluent.Runtime.CodeFluentEntityState.Unchanged;
            return true;
        }
        
        public virtual bool Save()
        {
            bool localSave = this.BaseSave(false);
            return localSave;
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public static bool Save(Events.EventRoll eventRoll)
        {
            if ((eventRoll == null))
            {
                return false;
            }
            bool ret = eventRoll.Save();
            return ret;
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Insert, true)]
        public static bool Insert(Events.EventRoll eventRoll)
        {
            bool ret = Events.EventRoll.Save(eventRoll);
            return ret;
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Delete, true)]
        public static bool Delete(Events.EventRoll eventRoll)
        {
            if ((eventRoll == null))
            {
                return false;
            }
            bool ret = eventRoll.Delete();
            return ret;
        }
        
        public string Trace()
        {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            System.IO.StringWriter stringWriter = new System.IO.StringWriter(stringBuilder, System.Globalization.CultureInfo.CurrentCulture);
            System.CodeDom.Compiler.IndentedTextWriter writer = new System.CodeDom.Compiler.IndentedTextWriter(stringWriter);
            this.BaseTrace(writer);
            writer.Flush();
            ((System.IDisposable)(writer)).Dispose();
            ((System.IDisposable)(stringWriter)).Dispose();
            string sr = stringBuilder.ToString();
            return sr;
        }
        
        void CodeFluent.Runtime.ICodeFluentObject.Trace(System.CodeDom.Compiler.IndentedTextWriter writer)
        {
            this.BaseTrace(writer);
        }
        
        protected virtual void BaseTrace(System.CodeDom.Compiler.IndentedTextWriter writer)
        {
            writer.Write("[");
            writer.Write("Id=");
            writer.Write(this.Id);
            writer.Write(",");
            writer.Write("isAttending=");
            writer.Write(this.isAttending);
            writer.Write(",");
            writer.Write("Comment=");
            writer.Write(this.Comment);
            writer.Write(",");
            writer.Write("Customer=");
            if ((this._customer != null))
            {
                ((CodeFluent.Runtime.ICodeFluentObject)(this._customer)).Trace(writer);
            }
            else
            {
                writer.Write("<null>");
            }
            writer.Write(",");
            writer.Write("_customerId=");
            writer.Write(this._customerId);
            writer.Write(",");
            writer.Write("EventBooking=");
            if ((this._eventBooking != null))
            {
                ((CodeFluent.Runtime.ICodeFluentObject)(this._eventBooking)).Trace(writer);
            }
            else
            {
                writer.Write("<null>");
            }
            writer.Write(",");
            writer.Write("_eventBookingId=");
            writer.Write(this._eventBookingId);
            writer.Write(", EntityState=");
            writer.Write(this.EntityState);
            writer.Write("]");
        }
        
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public static Events.EventRoll LoadByEntityKey(string key)
        {
            if ((key == string.Empty))
            {
                return null;
            }
            Events.EventRoll eventRoll;
            int var = ((int)(ConvertUtilities.ChangeType(key, typeof(int), -1)));
            eventRoll = Events.EventRoll.Load(var);
            return eventRoll;
        }
        
        protected virtual void ValidateMember(System.Globalization.CultureInfo culture, string memberName, System.Collections.Generic.IList<CodeFluent.Runtime.CodeFluentValidationException> results)
        {
        }
        
        public Events.EventRoll Clone(bool deep)
        {
            Events.EventRoll eventRoll = new Events.EventRoll();
            this.CopyTo(eventRoll, deep);
            return eventRoll;
        }
        
        public Events.EventRoll Clone()
        {
            Events.EventRoll localClone = this.Clone(true);
            return localClone;
        }
        
        object System.ICloneable.Clone()
        {
            object localClone = this.Clone();
            return localClone;
        }
        
        public virtual void CopyFrom(System.Collections.IDictionary dict, bool deep)
        {
            if ((dict == null))
            {
                throw new System.ArgumentNullException("dict");
            }
            if ((dict.Contains("Id") == true))
            {
                this.Id = ((int)(ConvertUtilities.ChangeType(dict["Id"], typeof(int), -1)));
            }
            if ((dict.Contains("Comment") == true))
            {
                this.Comment = ((string)(ConvertUtilities.ChangeType(dict["Comment"], typeof(string), default(string))));
            }
            if ((dict.Contains("isAttending") == true))
            {
                this.isAttending = ((bool)(ConvertUtilities.ChangeType(dict["isAttending"], typeof(bool), CodeFluentPersistence.DefaultBooleanValue)));
            }
            if ((dict.Contains("EventBookingId") == true))
            {
                this.EventBookingId = ((int)(ConvertUtilities.ChangeType(dict["EventBookingId"], typeof(int), -1)));
            }
            if ((dict.Contains("CustomerId") == true))
            {
                this.CustomerId = ((int)(ConvertUtilities.ChangeType(dict["CustomerId"], typeof(int), -1)));
            }
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.CopyFrom, false, dict));
        }
        
        public virtual void CopyTo(Events.EventRoll eventRoll, bool deep)
        {
            if ((eventRoll == null))
            {
                throw new System.ArgumentNullException("eventRoll");
            }
            eventRoll.Id = this.Id;
            eventRoll.Comment = this.Comment;
            eventRoll.isAttending = this.isAttending;
            eventRoll.EventBookingId = this.EventBookingId;
            eventRoll.CustomerId = this.CustomerId;
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.CopyTo, false, eventRoll));
        }
        
        public virtual void CopyTo(System.Collections.IDictionary dict, bool deep)
        {
            if ((dict == null))
            {
                throw new System.ArgumentNullException("dict");
            }
            dict["Id"] = this.Id;
            dict["Comment"] = this.Comment;
            dict["isAttending"] = this.isAttending;
            dict["EventBookingId"] = this.EventBookingId;
            dict["CustomerId"] = this.CustomerId;
            this.OnEntityAction(new CodeFluent.Runtime.CodeFluentEntityActionEventArgs(this, CodeFluent.Runtime.CodeFluentEntityAction.CopyTo, false, dict));
        }
        
        protected void OnCollectionKeyChanged(int key)
        {
            if ((this.KeyChanged != null))
            {
                this.KeyChanged(this, new CodeFluent.Runtime.Utilities.KeyChangedEventArgs<int>(key));
            }
        }
    }
}
