﻿/* CodeFluent Generated Monday, 08 August 2016 12:51. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-US. UiCulture:en-US. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AccountingPeriod_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AccountingPeriod_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Batch_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Batch_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_DeleteEventBookingBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_DeleteEventBookingBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_SaveEventBookingBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_SaveEventBookingBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CancellationType_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CancellationType_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Checkout_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Checkout_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_DeleteEventBookingCoaches]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_DeleteEventBookingCoaches]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_DeleteEventCoaches]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_DeleteEventCoaches]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_SaveEventBookingCoaches]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_SaveEventBookingCoaches]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_SaveEventCoaches]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_SaveEventCoaches]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_DeleteEventCourts]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_DeleteEventCourts]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_SaveEventCourts]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_SaveEventCourts]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Currency_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Currency_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_DeleteEventDays]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_DeleteEventDays]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_SaveEventDays]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_SaveEventDays]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_DeleteEventGenderAges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_DeleteEventGenderAges]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_SaveEventGenderAges]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_SaveEventGenderAges]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_DeleteInvoiceJournals]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_DeleteInvoiceJournals]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_SaveInvoiceJournals]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_SaveInvoiceJournals]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_DeleteEventBookingLevels]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_DeleteEventBookingLevels]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_DeleteEventLevels]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_DeleteEventLevels]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_SaveEventBookingLevels]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_SaveEventBookingLevels]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_SaveEventLevels]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_SaveEventLevels]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_DeletePlayerLevelPlayerRatings]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_DeletePlayerLevelPlayerRatings]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_SavePlayerLevelPlayerRatings]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_SavePlayerLevelPlayerRatings]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_DeleteOrderItemTransactions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_DeleteOrderItemTransactions]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_SaveOrderItemTransactions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_SaveOrderItemTransactions]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_DeleteRoleUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_DeleteRoleUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_SaveRoleUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_SaveRoleUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_Delete]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_Save]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_Save]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_GetClubAccount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_GetClubAccount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_GetCustomerAccount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_GetCustomerAccount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_LoadByCurrency]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_LoadByCurrency]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Account_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AccountingPeriod_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AccountingPeriod_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[AccountingPeriod_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Admin_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Batch_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Batch_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Batch_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_CountByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_CountByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadBookingByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadBookingByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByAdmin]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByBookingStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByBookingStatus]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByBookingType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByBookingType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByCoach]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByCoach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByCourt]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByCourt]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByMultiCourt]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByMultiCourt]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking_LoadParentMultiCourt]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Booking_LoadParentMultiCourt]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_GetStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_GetStatus]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_LoadByBookingType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_LoadByBookingType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingStatus_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_LoadByBookingType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_LoadByBookingType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingSubType_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[BookingType_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CancellationType_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CancellationType_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CancellationType_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Checkout_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Checkout_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Checkout_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CheckoutItem_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club_LoadOneByRefNo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Club_LoadOneByRefNo]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin_LoadOneByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubAdmin_LoadOneByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_LoadByComponent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_LoadByComponent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubComponents_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel_LoadByPlayerLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ClubPlayerLevel_LoadByPlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadCoachesByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadCoachesByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach_LoadCoachesByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Coach_LoadCoachesByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Components_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadAllByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadAllByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadBySurface]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadBySurface]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadByZone]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadByZone]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadCourtsByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadCourtsByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court_LoadOneByRefNo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Court_LoadOneByRefNo]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByClubAndDayOfRate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByClubAndDayOfRate]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByClubAndIsWeekday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByClubAndIsWeekday]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByMembershipTypeIsWeekdayTimePeriod]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypeIsWeekdayTimePeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByMembershipTypePeriodZone]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypePeriodZone]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByMembershipTypePeriodZoneDayOfWeek]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypePeriodZoneDayOfWeek]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByMembershipTypeTimePeriodIsWeekdayZone]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypeTimePeriodIsWeekdayZone]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByTimePeriod]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByTimePeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate_LoadByZone]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CourtHireRate_LoadByZone]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_LoadByOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_LoadByOrder]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit_LoadCustomerCredits]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Credit_LoadCustomerCredits]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_LoadByCredit]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_LoadByCredit]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust_LoadByInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[CreditAdjust_LoadByInvoice]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Currency_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Currency_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Currency_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_CountByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_CountByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_CountBySearch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_CountBySearch]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadBySearch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadBySearch]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_LoadOneByUserId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_LoadOneByUserId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer_SearchCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Customer_SearchCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek_LoadDaysByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DaysOfWeek_LoadDaysByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadBySupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadBySupplier]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_LoadOneByInvoiceNo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_LoadOneByInvoiceNo]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery_UpdateInstanceById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Delivery_UpdateInstanceById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadAllByDelivery]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadAllByDelivery]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadByDelivery]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadByDelivery]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem_UpdateInstanceById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DeliveryItem_UpdateInstanceById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount_LoadByProductVariant]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Discount_LoadByProductVariant]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_LoadByDiscount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_LoadByDiscount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup_LoadByMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[DiscountGroup_LoadByMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadAllByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadAllByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadAllByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadAllByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHire_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadAllByEquipmentHire]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadAllByEquipmentHire]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadAllByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadAllByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadByEquipmentHire]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadByEquipmentHire]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EquipmentHireItem_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByAllFilters]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByAllFilters]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeCoach]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeCoach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeCoachDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeCoachDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeCoachLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeCoachLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeIds]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeIds]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeIdsCoach]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeIdsCoach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeIdsDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeIdsDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeIdsLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeIdsLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByBookingTypeLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByBookingTypeLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByCoachDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByCoachDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByCoachDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByCoachDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByCoachLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByCoachLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_CountByTitle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_CountByTitle]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByAllFilters]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByAllFilters]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeCoach]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeCoach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeCoachDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeCoachDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeCoachLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeCoachLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeIds]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeIds]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeIdsCoach]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeIdsCoach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeIdsDays]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeIdsDays]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeIdsLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeIdsLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByBookingTypeLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByBookingTypeLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByCoachDay]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByCoachDay]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByCoachDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByCoachDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByCoachLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByCoachLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByDayLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByDayLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_LoadByTitle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Event_LoadByTitle]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadByBookingId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadByBookingId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadByDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadByDateRange]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadByEventBookingCancellation]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadByEventBookingCancellation]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBooking_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_LoadByCancellationType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_LoadByCancellationType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_LoadByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_LoadByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventBookingCancellation_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_LoadByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_LoadByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_LoadByMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_LoadByMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee_LoadOneByEventAndMemberShipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventFee_LoadOneByEventAndMemberShipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_CountByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_CountByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_CountByPaidConfirmed]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_CountByPaidConfirmed]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadByEventBookingId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadByEventBookingId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration_LoadByOrderItem]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRegistration_LoadByOrderItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_LoadByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_LoadByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventRoll_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_LoadByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_LoadByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[EventWaitlist_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility_LoadOneByRefNo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Facility_LoadOneByRefNo]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Gender_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge_LoadGenderAgesByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[GenderAge_LoadGenderAgesByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadByAdmin]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadByAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_LoadByOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Invoice_LoadByOrder]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal_LoadJournalsByInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Journal_LoadJournalsByInvoice]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadByActiveCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadByActiveCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadByMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadByMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member_LoadOneByCustomerId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Member_LoadOneByCustomerId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadAllByClubIsActive]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadAllByClubIsActive]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadDefaultMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadDefaultMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipType_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByClubAndIsWeekday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByClubAndIsWeekday]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByMembershipType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByMembershipTypeAndIsWeekday]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipTypeAndIsWeekday]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByMembershipTypePeriodZoneDayOfRate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipTypePeriodZoneDayOfRate]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod_LoadByTimePeriodGroup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByTimePeriodGroup]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageAccount_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_CountByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_CountByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadByAdmin]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadByAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadByDateRange]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadByDateRange]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog_LoadByMessageNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageLog_LoadByMessageNumber]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_GetNumber]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_GetNumber]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber_LoadByMessageService]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageNumber_LoadByMessageService]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService_LoadByMessageAccount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[MessageService_LoadByMessageAccount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadByAdmin]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadByAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order_LoadByStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Order_LoadByStatus]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByMembership]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByMembership]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByOrder]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByProductVariant]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByProductVariant]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByRegistration]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByRegistration]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_LoadByStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItem_LoadByStatus]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_LoadByOrderItem]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_LoadByOrderItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail_LoadDetailsByOrderItem]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[OrderItemDetail_LoadDetailsByOrderItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadByView]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadByView]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadLevelsByEvent]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadLevelsByEvent]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadLevelsByEventBooking]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadLevelsByEventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_LoadPlayerLevelsPlayerRatingsByPlayerRating]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerLevel_LoadPlayerLevelsPlayerRatingsByPlayerRating]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating_LoadPlayerRatingsPlayerLevelsByPlayerLevel]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[PlayerRating_LoadPlayerRatingsPlayerLevelsByPlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByAllFilters]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByAllFilters]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductBrandId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductBrandId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductClassId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductClassId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductClassProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductClassProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductClassProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductClassProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductClassProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductClassProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductClass]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductClass]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductClassProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductClassProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductClassProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductClassProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductTypeProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductTypeProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductVendorId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductVendorId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_CountByTitle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_CountByTitle]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByAllFilters]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByAllFilters]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductClass]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductClass]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductClassProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductClassProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductClassProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductClassProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductClassProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductClassProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductOption]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductOption]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductClass]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductClass]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductClassProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductClassProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductClassProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductClassProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductTypeProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductTypeProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductVendor]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_LoadByProductVendorProductBrand]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_LoadByProductVendorProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product_SearchByTitle]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Product_SearchByTitle]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand_LoadOneByNameClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductBrand_LoadOneByNameClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass_LoadOneByNameClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductClass_LoadOneByNameClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_CountProductImages]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_CountProductImages]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductImage_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory_LoadByProductVariant]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductInventory_LoadByProductVariant]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption_LoadOneByNameClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductOption_LoadOneByNameClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType_LoadOneByNameClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductType_LoadOneByNameClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_CountByBarcode]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_CountByBarcode]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_CountByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_CountByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_CountDuplicates]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_CountDuplicates]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadByClubId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadByClubId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadBySearch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadBySearch]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_LoadOneByProductId]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_LoadOneByProductId]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant_SearchByBarcode]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVariant_SearchByBarcode]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor_LoadOneByNameClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ProductVendor_LoadOneByNameClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_LoadByName]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_LoadByName]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_LoadRolesUserByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Role_LoadRolesUserByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim_LoadByRole]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[RoleClaim_LoadByRole]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_LoadByAdmin]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_LoadByAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale_LoadByInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Sale_LoadByInvoice]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_LoadByOrder]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_LoadByOrder]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem_LoadBySale]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SaleItem_LoadBySale]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size_LoadByProductType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Size_LoadByProductType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_LoadByCustomer]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_LoadByCustomer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Source_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Stocktake_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_LoadByProduct]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_LoadByProduct]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem_LoadByStocktake]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[StocktakeItem_LoadByStocktake]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadAllByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadAllByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_LoadOneByReference]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_LoadOneByReference]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier_UpdateInstanceById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Supplier_UpdateInstanceById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_LoadAllBySupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_LoadAllBySupplier]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_LoadBySupplier]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_LoadBySupplier]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact_UpdateInstanceById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[SupplierContact_UpdateInstanceById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Surface_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Term_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod_LoadByTimePeriodGroup]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriod_LoadByTimePeriodGroup]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup_LoadOneByIsDefault]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[TimePeriodGroup_LoadOneByIsDefault]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByAccount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByAccount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByAccountingPeriod]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByAccountingPeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByCurrency]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByCurrency]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByDestination]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByDestination]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByInvoice]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByInvoice]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadByJournal]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadByJournal]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadBySource]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadBySource]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction_LoadTransactionsByOrderItem]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Transaction_LoadTransactionsByOrderItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadByEmail]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadByEmail]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadByGender]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadByGender]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadByPlayerRating]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadByPlayerRating]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadByUserLoginInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadByUserLoginInfo]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadByUserName]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadByUserName]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadOneById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User_LoadUserRolesByRole]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[User_LoadUserRolesByRole]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserClaim_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin_LoadByUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[UserLogin_LoadByUser]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_Load]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_Load]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadAll]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadAll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadAllByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadAllByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadByClub]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadByClub]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadByFacility]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadByFacility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadById]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone_LoadOneById]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[Zone_LoadOneById]
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByBookingTypeIds_0' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByBookingTypeIds_0]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByBookingTypeIds_0] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByBookingTypeIdsCoach_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsCoach_1]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsCoach_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByBookingTypeIdsDay_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsDay_1]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsDay_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByBookingTypeIdsLevel_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsLevel_1]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByBookingTypeIdsLevel_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByCoachDay_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByCoachDay_2]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByCoachDay_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByCoachDayLevel_3' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByCoachDayLevel_3]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByCoachDayLevel_3] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByCoachLevel_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByCoachLevel_2]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByCoachLevel_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByDayLevel_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByDayLevel_2]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByDayLevel_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_CountByTitle_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_CountByTitle_1]
GO

CREATE TYPE [dbo].[cf_type_Event_CountByTitle_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByBookingTypeIds_0' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByBookingTypeIds_0]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByBookingTypeIds_0] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByBookingTypeIdsCoach_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsCoach_1]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsCoach_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByBookingTypeIdsDays_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsDays_1]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsDays_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByBookingTypeIdsLevel_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsLevel_1]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByBookingTypeIdsLevel_1] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByCoachDay_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByCoachDay_2]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByCoachDay_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByCoachDayLevel_3' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByCoachDayLevel_3]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByCoachDayLevel_3] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByCoachLevel_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByCoachLevel_2]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByCoachLevel_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByDayLevel_2' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByDayLevel_2]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByDayLevel_2] AS TABLE (
 [Item] [int] NULL)
GO

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'cf_type_Event_LoadByTitle_1' AND ss.name = N'dbo')
DROP TYPE [dbo].[cf_type_Event_LoadByTitle_1]
GO

CREATE TYPE [dbo].[cf_type_Event_LoadByTitle_1] AS TABLE (
 [Item] [int] NULL)
GO

CREATE PROCEDURE [dbo].[Account_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Transaction] SET
 [Transaction].[Account_Id] = NULL
    WHERE ([Transaction].[Account_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Transaction] SET
 [Transaction].[Source_Id] = NULL
    WHERE ([Transaction].[Source_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Transaction] SET
 [Transaction].[Destination_Id] = NULL
    WHERE ([Transaction].[Destination_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Account] FROM [Account] 
    WHERE (([Account].[Id] = @Id) AND ([Account].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Account_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Account_Save]
(
 @Id [int] = NULL,
 @Balance [decimal] (28, 13) = NULL,
 @Currency_Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @AccountType [int] = NULL,
 @Club_Id [int] = NULL,
 @Customer_Id [int] = NULL,
 @Reference [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Account] SET
     [Account].[Balance] = @Balance,
     [Account].[Currency_Id] = @Currency_Id,
     [Account].[Description] = @Description,
     [Account].[AccountType] = @AccountType,
     [Account].[Club_Id] = @Club_Id,
     [Account].[Customer_Id] = @Customer_Id,
     [Account].[Reference] = @Reference,
     [Account].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Account].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Account].[Id] = @Id) AND ([Account].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Account_Save')
        RETURN
    END
    SELECT DISTINCT [Account].[_rowVersion], @Id AS 'Id' 
        FROM [Account] 
        WHERE ([Account].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Account] (
        [Account].[Balance],
        [Account].[Currency_Id],
        [Account].[Description],
        [Account].[AccountType],
        [Account].[Club_Id],
        [Account].[Customer_Id],
        [Account].[Reference],
        [Account].[_trackCreationUser],
        [Account].[_trackLastWriteUser])
    VALUES (
        @Balance,
        @Currency_Id,
        @Description,
        @AccountType,
        @Club_Id,
        @Customer_Id,
        @Reference,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Account].[_rowVersion], @Id AS 'Id' 
        FROM [Account] 
        WHERE ([Account].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[AccountingPeriod_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Transaction] SET
 [Transaction].[AccountingPeriod_Id] = NULL
    WHERE ([Transaction].[AccountingPeriod_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [AccountingPeriod] FROM [AccountingPeriod] 
    WHERE (([AccountingPeriod].[Id] = @Id) AND ([AccountingPeriod].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'AccountingPeriod_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[AccountingPeriod_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Country [nvarchar] (256) = NULL,
 @StartDate [datetime] = NULL,
 @EndDate [datetime] = NULL,
 @isCurrent [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [AccountingPeriod] SET
     [AccountingPeriod].[Name] = @Name,
     [AccountingPeriod].[Country] = @Country,
     [AccountingPeriod].[StartDate] = @StartDate,
     [AccountingPeriod].[EndDate] = @EndDate,
     [AccountingPeriod].[isCurrent] = @isCurrent,
     [AccountingPeriod].[_trackLastWriteUser] = @_trackLastWriteUser,
     [AccountingPeriod].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([AccountingPeriod].[Id] = @Id) AND ([AccountingPeriod].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'AccountingPeriod_Save')
        RETURN
    END
    SELECT DISTINCT [AccountingPeriod].[_rowVersion], @Id AS 'Id' 
        FROM [AccountingPeriod] 
        WHERE ([AccountingPeriod].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [AccountingPeriod] (
        [AccountingPeriod].[Name],
        [AccountingPeriod].[Country],
        [AccountingPeriod].[StartDate],
        [AccountingPeriod].[EndDate],
        [AccountingPeriod].[isCurrent],
        [AccountingPeriod].[_trackCreationUser],
        [AccountingPeriod].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Country,
        @StartDate,
        @EndDate,
        @isCurrent,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [AccountingPeriod].[_rowVersion], @Id AS 'Id' 
        FROM [AccountingPeriod] 
        WHERE ([AccountingPeriod].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Booking] SET
 [Booking].[Admin_Id] = NULL
    WHERE ([Booking].[Admin_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Invoice] SET
 [Invoice].[Admin_Id] = NULL
    WHERE ([Invoice].[Admin_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Sale] SET
 [Sale].[Admin_Id] = NULL
    WHERE ([Sale].[Admin_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MessageLog] SET
 [MessageLog].[Admin_Id] = NULL
    WHERE ([MessageLog].[Admin_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Order] SET
 [Order].[Admin_Id] = NULL
    WHERE ([Order].[Admin_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Admin] FROM [Admin] 
    WHERE (([Admin].[Id] = @Id) AND ([Admin].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Admin_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_Save]
(
 @Id [int] = NULL,
 @User_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @CreationDateUTC [datetime] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Admin] SET
     [Admin].[User_Id] = @User_Id,
     [Admin].[Club_Id] = @Club_Id,
     [Admin].[CreationDateUTC] = @CreationDateUTC,
     [Admin].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Admin].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Admin].[Id] = @Id) AND ([Admin].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Admin_Save')
        RETURN
    END
    SELECT DISTINCT [Admin].[_rowVersion], @Id AS 'Id' 
        FROM [Admin] 
        WHERE ([Admin].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Admin] (
        [Admin].[User_Id],
        [Admin].[Club_Id],
        [Admin].[CreationDateUTC],
        [Admin].[_trackCreationUser],
        [Admin].[_trackLastWriteUser])
    VALUES (
        @User_Id,
        @Club_Id,
        @CreationDateUTC,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Admin].[_rowVersion], @Id AS 'Id' 
        FROM [Admin] 
        WHERE ([Admin].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Batch_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Batch] FROM [Batch] 
    WHERE (([Batch].[Id] = @Id) AND ([Batch].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Batch_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Batch_Save]
(
 @Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Batch] SET
     [Batch].[Description] = @Description,
     [Batch].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Batch].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Batch].[Id] = @Id) AND ([Batch].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Batch_Save')
        RETURN
    END
    SELECT DISTINCT [Batch].[_rowVersion], @Id AS 'Id' 
        FROM [Batch] 
        WHERE ([Batch].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Batch] (
        [Batch].[Description],
        [Batch].[_trackCreationUser],
        [Batch].[_trackLastWriteUser])
    VALUES (
        @Description,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Batch].[_rowVersion], @Id AS 'Id' 
        FROM [Batch] 
        WHERE ([Batch].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBooking_Booking_Booking] FROM [EventBooking_Booking_Booking] 
    WHERE ([EventBooking_Booking_Booking].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [OrderItem] SET
 [OrderItem].[Booking_Id] = NULL
    WHERE ([OrderItem].[Booking_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Booking] FROM [Booking] 
    WHERE (([Booking].[Id] = @Id) AND ([Booking].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Booking_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_DeleteEventBookingBooking]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBooking_Booking_Booking] FROM [EventBooking_Booking_Booking] 
    WHERE (([EventBooking_Booking_Booking].[Id2] = @Id2) AND ([EventBooking_Booking_Booking].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_Save]
(
 @Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @Start [datetime] = NULL,
 @End [datetime] = NULL,
 @StartTimezone [nvarchar] (256) = NULL,
 @EndTimezone [nvarchar] (256) = NULL,
 @IsAllDay [bit] = NULL,
 @Recurrence [int] = NULL,
 @RecurrenceException [nvarchar] (256) = NULL,
 @RecurrenceRule [nvarchar] (256) = NULL,
 @Title [nvarchar] (256) = NULL,
 @ReferenceNo [nvarchar] (256) = NULL,
 @BookingType_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @isActive [bit] = NULL,
 @Price [decimal] (28, 13) = NULL,
 @isPaid [bit] = NULL,
 @NumberOfPlayers [int] = NULL,
 @User_Id [int] = NULL,
 @Facility_Id [int] = NULL,
 @Court_Id [int] = NULL,
 @isMultiCourt [bit] = NULL,
 @MultiCourt [int] = NULL,
 @Customer_Id [int] = NULL,
 @Admin_Id [int] = NULL,
 @Coach_Id [int] = NULL,
 @BookingStatus_Id [int] = NULL,
 @Event_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Booking] SET
     [Booking].[Description] = @Description,
     [Booking].[Start] = @Start,
     [Booking].[End] = @End,
     [Booking].[StartTimezone] = @StartTimezone,
     [Booking].[EndTimezone] = @EndTimezone,
     [Booking].[IsAllDay] = @IsAllDay,
     [Booking].[Recurrence] = @Recurrence,
     [Booking].[RecurrenceException] = @RecurrenceException,
     [Booking].[RecurrenceRule] = @RecurrenceRule,
     [Booking].[Title] = @Title,
     [Booking].[ReferenceNo] = @ReferenceNo,
     [Booking].[BookingType_Id] = @BookingType_Id,
     [Booking].[Club_Id] = @Club_Id,
     [Booking].[isActive] = @isActive,
     [Booking].[Price] = @Price,
     [Booking].[isPaid] = @isPaid,
     [Booking].[NumberOfPlayers] = @NumberOfPlayers,
     [Booking].[User_Id] = @User_Id,
     [Booking].[Facility_Id] = @Facility_Id,
     [Booking].[Court_Id] = @Court_Id,
     [Booking].[isMultiCourt] = @isMultiCourt,
     [Booking].[MultiCourt] = @MultiCourt,
     [Booking].[Customer_Id] = @Customer_Id,
     [Booking].[Admin_Id] = @Admin_Id,
     [Booking].[Coach_Id] = @Coach_Id,
     [Booking].[BookingStatus_Id] = @BookingStatus_Id,
     [Booking].[Event_Id] = @Event_Id,
     [Booking].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Booking].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Booking].[Id] = @Id) AND ([Booking].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Booking_Save')
        RETURN
    END
    SELECT DISTINCT [Booking].[_rowVersion], @Id AS 'Id' 
        FROM [Booking] 
        WHERE ([Booking].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Booking] (
        [Booking].[Description],
        [Booking].[Start],
        [Booking].[End],
        [Booking].[StartTimezone],
        [Booking].[EndTimezone],
        [Booking].[IsAllDay],
        [Booking].[Recurrence],
        [Booking].[RecurrenceException],
        [Booking].[RecurrenceRule],
        [Booking].[Title],
        [Booking].[ReferenceNo],
        [Booking].[BookingType_Id],
        [Booking].[Club_Id],
        [Booking].[isActive],
        [Booking].[Price],
        [Booking].[isPaid],
        [Booking].[NumberOfPlayers],
        [Booking].[User_Id],
        [Booking].[Facility_Id],
        [Booking].[Court_Id],
        [Booking].[isMultiCourt],
        [Booking].[MultiCourt],
        [Booking].[Customer_Id],
        [Booking].[Admin_Id],
        [Booking].[Coach_Id],
        [Booking].[BookingStatus_Id],
        [Booking].[Event_Id],
        [Booking].[_trackCreationUser],
        [Booking].[_trackLastWriteUser])
    VALUES (
        @Description,
        @Start,
        @End,
        @StartTimezone,
        @EndTimezone,
        @IsAllDay,
        @Recurrence,
        @RecurrenceException,
        @RecurrenceRule,
        @Title,
        @ReferenceNo,
        @BookingType_Id,
        @Club_Id,
        @isActive,
        @Price,
        @isPaid,
        @NumberOfPlayers,
        @User_Id,
        @Facility_Id,
        @Court_Id,
        @isMultiCourt,
        @MultiCourt,
        @Customer_Id,
        @Admin_Id,
        @Coach_Id,
        @BookingStatus_Id,
        @Event_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Booking].[_rowVersion], @Id AS 'Id' 
        FROM [Booking] 
        WHERE ([Booking].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_SaveEventBookingBooking]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [EventBooking_Booking_Booking].[Id] 
    FROM [EventBooking_Booking_Booking] 
    WHERE (([EventBooking_Booking_Booking].[Id2] = @Id2) AND ([EventBooking_Booking_Booking].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [EventBooking_Booking_Booking] (
        [EventBooking_Booking_Booking].[Id],
        [EventBooking_Booking_Booking].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Booking] SET
 [Booking].[BookingStatus_Id] = NULL
    WHERE ([Booking].[BookingStatus_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [BookingStatus] FROM [BookingStatus] 
    WHERE (([BookingStatus].[Id] = @Id) AND ([BookingStatus].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingStatus_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_Save]
(
 @Id [int] = NULL,
 @Colour [nvarchar] (256) = NULL,
 @isPaid [bit] = NULL,
 @BookingType_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [BookingStatus] SET
     [BookingStatus].[Colour] = @Colour,
     [BookingStatus].[isPaid] = @isPaid,
     [BookingStatus].[BookingType_Id] = @BookingType_Id,
     [BookingStatus].[Name] = @Name,
     [BookingStatus].[_trackLastWriteUser] = @_trackLastWriteUser,
     [BookingStatus].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([BookingStatus].[Id] = @Id) AND ([BookingStatus].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingStatus_Save')
        RETURN
    END
    SELECT DISTINCT [BookingStatus].[_rowVersion], @Id AS 'Id' 
        FROM [BookingStatus] 
        WHERE ([BookingStatus].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [BookingStatus] (
        [BookingStatus].[Colour],
        [BookingStatus].[isPaid],
        [BookingStatus].[BookingType_Id],
        [BookingStatus].[Name],
        [BookingStatus].[_trackCreationUser],
        [BookingStatus].[_trackLastWriteUser])
    VALUES (
        @Colour,
        @isPaid,
        @BookingType_Id,
        @Name,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [BookingStatus].[_rowVersion], @Id AS 'Id' 
        FROM [BookingStatus] 
        WHERE ([BookingStatus].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [BookingSubType] FROM [BookingSubType] 
    WHERE (([BookingSubType].[Id] = @Id) AND ([BookingSubType].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingSubType_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_Save]
(
 @Id [int] = NULL,
 @BookingType_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [BookingSubType] SET
     [BookingSubType].[BookingType_Id] = @BookingType_Id,
     [BookingSubType].[Name] = @Name,
     [BookingSubType].[Sequence] = @Sequence,
     [BookingSubType].[_trackLastWriteUser] = @_trackLastWriteUser,
     [BookingSubType].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([BookingSubType].[Id] = @Id) AND ([BookingSubType].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingSubType_Save')
        RETURN
    END
    SELECT DISTINCT [BookingSubType].[_rowVersion], @Id AS 'Id' 
        FROM [BookingSubType] 
        WHERE ([BookingSubType].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [BookingSubType] (
        [BookingSubType].[BookingType_Id],
        [BookingSubType].[Name],
        [BookingSubType].[Sequence],
        [BookingSubType].[_trackCreationUser],
        [BookingSubType].[_trackLastWriteUser])
    VALUES (
        @BookingType_Id,
        @Name,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [BookingSubType].[_rowVersion], @Id AS 'Id' 
        FROM [BookingSubType] 
        WHERE ([BookingSubType].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Booking] SET
 [Booking].[BookingType_Id] = NULL
    WHERE ([Booking].[BookingType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [BookingSubType] SET
 [BookingSubType].[BookingType_Id] = NULL
    WHERE ([BookingSubType].[BookingType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Event] SET
 [Event].[BookingType_Id] = NULL
    WHERE ([Event].[BookingType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [BookingStatus] SET
 [BookingStatus].[BookingType_Id] = NULL
    WHERE ([BookingStatus].[BookingType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [BookingType] FROM [BookingType] 
    WHERE (([BookingType].[Id] = @Id) AND ([BookingType].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingType_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @BookingCategory [int] = NULL,
 @Colour [nvarchar] (256) = NULL,
 @BookingRule [int] = NULL,
 @BookingOption [int] = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [BookingType] SET
     [BookingType].[Name] = @Name,
     [BookingType].[Sequence] = @Sequence,
     [BookingType].[BookingCategory] = @BookingCategory,
     [BookingType].[Colour] = @Colour,
     [BookingType].[BookingRule] = @BookingRule,
     [BookingType].[BookingOption] = @BookingOption,
     [BookingType].[Club_Id] = @Club_Id,
     [BookingType].[_trackLastWriteUser] = @_trackLastWriteUser,
     [BookingType].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([BookingType].[Id] = @Id) AND ([BookingType].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'BookingType_Save')
        RETURN
    END
    SELECT DISTINCT [BookingType].[_rowVersion], @Id AS 'Id' 
        FROM [BookingType] 
        WHERE ([BookingType].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [BookingType] (
        [BookingType].[Name],
        [BookingType].[Sequence],
        [BookingType].[BookingCategory],
        [BookingType].[Colour],
        [BookingType].[BookingRule],
        [BookingType].[BookingOption],
        [BookingType].[Club_Id],
        [BookingType].[_trackCreationUser],
        [BookingType].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @BookingCategory,
        @Colour,
        @BookingRule,
        @BookingOption,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [BookingType].[_rowVersion], @Id AS 'Id' 
        FROM [BookingType] 
        WHERE ([BookingType].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CancellationType_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [EventBookingCancellation] SET
 [EventBookingCancellation].[CancellationType_Id] = NULL
    WHERE ([EventBookingCancellation].[CancellationType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [CancellationType] FROM [CancellationType] 
    WHERE (([CancellationType].[Id] = @Id) AND ([CancellationType].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CancellationType_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CancellationType_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Description [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [CancellationType] SET
     [CancellationType].[Name] = @Name,
     [CancellationType].[Description] = @Description,
     [CancellationType].[Sequence] = @Sequence,
     [CancellationType].[_trackLastWriteUser] = @_trackLastWriteUser,
     [CancellationType].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([CancellationType].[Id] = @Id) AND ([CancellationType].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CancellationType_Save')
        RETURN
    END
    SELECT DISTINCT [CancellationType].[_rowVersion], @Id AS 'Id' 
        FROM [CancellationType] 
        WHERE ([CancellationType].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [CancellationType] (
        [CancellationType].[Name],
        [CancellationType].[Description],
        [CancellationType].[Sequence],
        [CancellationType].[_trackCreationUser],
        [CancellationType].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Description,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [CancellationType].[_rowVersion], @Id AS 'Id' 
        FROM [CancellationType] 
        WHERE ([CancellationType].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Checkout_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Checkout] FROM [Checkout] 
    WHERE (([Checkout].[Id] = @Id) AND ([Checkout].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Checkout_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Checkout_Save]
(
 @Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Checkout] SET
     [Checkout].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Checkout].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Checkout].[Id] = @Id) AND ([Checkout].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Checkout_Save')
        RETURN
    END
    SELECT DISTINCT [Checkout].[_rowVersion], @Id AS 'Id' 
        FROM [Checkout] 
        WHERE ([Checkout].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Checkout] (
        [Checkout].[_trackCreationUser],
        [Checkout].[_trackLastWriteUser])
    VALUES (
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Checkout].[_rowVersion], @Id AS 'Id' 
        FROM [Checkout] 
        WHERE ([Checkout].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [CheckoutItem] FROM [CheckoutItem] 
    WHERE (([CheckoutItem].[Id] = @Id) AND ([CheckoutItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CheckoutItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_Save]
(
 @Id [int] = NULL,
 @Product_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [CheckoutItem] SET
     [CheckoutItem].[Product_Id] = @Product_Id,
     [CheckoutItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [CheckoutItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([CheckoutItem].[Id] = @Id) AND ([CheckoutItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CheckoutItem_Save')
        RETURN
    END
    SELECT DISTINCT [CheckoutItem].[_rowVersion], @Id AS 'Id' 
        FROM [CheckoutItem] 
        WHERE ([CheckoutItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [CheckoutItem] (
        [CheckoutItem].[Product_Id],
        [CheckoutItem].[_trackCreationUser],
        [CheckoutItem].[_trackLastWriteUser])
    VALUES (
        @Product_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [CheckoutItem].[_rowVersion], @Id AS 'Id' 
        FROM [CheckoutItem] 
        WHERE ([CheckoutItem].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Club_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Facility] SET
 [Facility].[Club_Id] = NULL
    WHERE ([Facility].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Court] SET
 [Court].[Club_Id] = NULL
    WHERE ([Court].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Booking] SET
 [Booking].[Club_Id] = NULL
    WHERE ([Booking].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [BookingType] SET
 [BookingType].[Club_Id] = NULL
    WHERE ([BookingType].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Product] SET
 [Product].[Club_Id] = NULL
    WHERE ([Product].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductType] SET
 [ProductType].[Club_Id] = NULL
    WHERE ([ProductType].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MembershipType] SET
 [MembershipType].[Club_Id] = NULL
    WHERE ([MembershipType].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [PlayerLevel] SET
 [PlayerLevel].[Club_Id] = NULL
    WHERE ([PlayerLevel].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ClubPlayerLevel] SET
 [ClubPlayerLevel].[Club_Id] = NULL
    WHERE ([ClubPlayerLevel].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Event] SET
 [Event].[Club_Id] = NULL
    WHERE ([Event].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Supplier] SET
 [Supplier].[Club_Id] = NULL
    WHERE ([Supplier].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Delivery] SET
 [Delivery].[Club_Id] = NULL
    WHERE ([Delivery].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EquipmentHire] SET
 [EquipmentHire].[Club_Id] = NULL
    WHERE ([EquipmentHire].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ClubAdmin] SET
 [ClubAdmin].[Club_Id] = NULL
    WHERE ([ClubAdmin].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ClubComponents] SET
 [ClubComponents].[Club_Id] = NULL
    WHERE ([ClubComponents].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [CourtHireRate] SET
 [CourtHireRate].[Club_Id] = NULL
    WHERE ([CourtHireRate].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Zone] SET
 [Zone].[Club_Id] = NULL
    WHERE ([Zone].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [TimePeriod] SET
 [TimePeriod].[Club_Id] = NULL
    WHERE ([TimePeriod].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [TimePeriodGroup] SET
 [TimePeriodGroup].[Club_Id] = NULL
    WHERE ([TimePeriodGroup].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MembershipTypeTimePeriod] SET
 [MembershipTypeTimePeriod].[Club_Id] = NULL
    WHERE ([MembershipTypeTimePeriod].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Account] SET
 [Account].[Club_Id] = NULL
    WHERE ([Account].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Transaction] SET
 [Transaction].[Club_Id] = NULL
    WHERE ([Transaction].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Invoice] SET
 [Invoice].[Club_Id] = NULL
    WHERE ([Invoice].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Customer] SET
 [Customer].[Club_Id] = NULL
    WHERE ([Customer].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Admin] SET
 [Admin].[Club_Id] = NULL
    WHERE ([Admin].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductOption] SET
 [ProductOption].[Club_Id] = NULL
    WHERE ([ProductOption].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductVendor] SET
 [ProductVendor].[Club_Id] = NULL
    WHERE ([ProductVendor].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductBrand] SET
 [ProductBrand].[Club_Id] = NULL
    WHERE ([ProductBrand].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductClass] SET
 [ProductClass].[Club_Id] = NULL
    WHERE ([ProductClass].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Coach] SET
 [Coach].[Club_Id] = NULL
    WHERE ([Coach].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Term] SET
 [Term].[Club_Id] = NULL
    WHERE ([Term].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MessageAccount] SET
 [MessageAccount].[Club_Id] = NULL
    WHERE ([MessageAccount].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Order] SET
 [Order].[Club_Id] = NULL
    WHERE ([Order].[Club_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Club] FROM [Club] 
    WHERE (([Club].[Id] = @Id) AND ([Club].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Club_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Club_Save]
(
 @Id [int] = NULL,
 @FullName [nvarchar] (256) = NULL,
 @ShortName [nvarchar] (256) = NULL,
 @AddressLine1 [nvarchar] (256) = NULL,
 @AddressLine2 [nvarchar] (256) = NULL,
 @Suburb [nvarchar] (256) = NULL,
 @State [nvarchar] (256) = NULL,
 @Postcode [nvarchar] (256) = NULL,
 @Email [nvarchar] (256) = NULL,
 @Website [nvarchar] (256) = NULL,
 @ReferenceNo [nvarchar] (256) = NULL,
 @Phone [nvarchar] (256) = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Club] SET
     [Club].[FullName] = @FullName,
     [Club].[ShortName] = @ShortName,
     [Club].[AddressLine1] = @AddressLine1,
     [Club].[AddressLine2] = @AddressLine2,
     [Club].[Suburb] = @Suburb,
     [Club].[State] = @State,
     [Club].[Postcode] = @Postcode,
     [Club].[Email] = @Email,
     [Club].[Website] = @Website,
     [Club].[ReferenceNo] = @ReferenceNo,
     [Club].[Phone] = @Phone,
     [Club].[isActive] = @isActive,
     [Club].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Club].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Club].[Id] = @Id) AND ([Club].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Club_Save')
        RETURN
    END
    SELECT DISTINCT [Club].[_rowVersion], @Id AS 'Id' 
        FROM [Club] 
        WHERE ([Club].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Club] (
        [Club].[FullName],
        [Club].[ShortName],
        [Club].[AddressLine1],
        [Club].[AddressLine2],
        [Club].[Suburb],
        [Club].[State],
        [Club].[Postcode],
        [Club].[Email],
        [Club].[Website],
        [Club].[ReferenceNo],
        [Club].[Phone],
        [Club].[isActive],
        [Club].[_trackCreationUser],
        [Club].[_trackLastWriteUser])
    VALUES (
        @FullName,
        @ShortName,
        @AddressLine1,
        @AddressLine2,
        @Suburb,
        @State,
        @Postcode,
        @Email,
        @Website,
        @ReferenceNo,
        @Phone,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Club].[_rowVersion], @Id AS 'Id' 
        FROM [Club] 
        WHERE ([Club].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [ClubAdmin] FROM [ClubAdmin] 
    WHERE (([ClubAdmin].[Id] = @Id) AND ([ClubAdmin].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubAdmin_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @User_Id [int] = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ClubAdmin] SET
     [ClubAdmin].[Club_Id] = @Club_Id,
     [ClubAdmin].[User_Id] = @User_Id,
     [ClubAdmin].[isActive] = @isActive,
     [ClubAdmin].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ClubAdmin].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ClubAdmin].[Id] = @Id) AND ([ClubAdmin].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubAdmin_Save')
        RETURN
    END
    SELECT DISTINCT [ClubAdmin].[_rowVersion], @Id AS 'Id' 
        FROM [ClubAdmin] 
        WHERE ([ClubAdmin].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ClubAdmin] (
        [ClubAdmin].[Club_Id],
        [ClubAdmin].[User_Id],
        [ClubAdmin].[isActive],
        [ClubAdmin].[_trackCreationUser],
        [ClubAdmin].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @User_Id,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ClubAdmin].[_rowVersion], @Id AS 'Id' 
        FROM [ClubAdmin] 
        WHERE ([ClubAdmin].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [ClubComponents] FROM [ClubComponents] 
    WHERE (([ClubComponents].[Id] = @Id) AND ([ClubComponents].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubComponents_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @Component_Id [int] = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ClubComponents] SET
     [ClubComponents].[Club_Id] = @Club_Id,
     [ClubComponents].[Component_Id] = @Component_Id,
     [ClubComponents].[isActive] = @isActive,
     [ClubComponents].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ClubComponents].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ClubComponents].[Id] = @Id) AND ([ClubComponents].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubComponents_Save')
        RETURN
    END
    SELECT DISTINCT [ClubComponents].[_rowVersion], @Id AS 'Id' 
        FROM [ClubComponents] 
        WHERE ([ClubComponents].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ClubComponents] (
        [ClubComponents].[Club_Id],
        [ClubComponents].[Component_Id],
        [ClubComponents].[isActive],
        [ClubComponents].[_trackCreationUser],
        [ClubComponents].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @Component_Id,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ClubComponents].[_rowVersion], @Id AS 'Id' 
        FROM [ClubComponents] 
        WHERE ([ClubComponents].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [ClubPlayerLevel] FROM [ClubPlayerLevel] 
    WHERE (([ClubPlayerLevel].[Id] = @Id) AND ([ClubPlayerLevel].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubPlayerLevel_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @PlayerLevel_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ClubPlayerLevel] SET
     [ClubPlayerLevel].[Club_Id] = @Club_Id,
     [ClubPlayerLevel].[PlayerLevel_Id] = @PlayerLevel_Id,
     [ClubPlayerLevel].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ClubPlayerLevel].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ClubPlayerLevel].[Id] = @Id) AND ([ClubPlayerLevel].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ClubPlayerLevel_Save')
        RETURN
    END
    SELECT DISTINCT [ClubPlayerLevel].[_rowVersion], @Id AS 'Id' 
        FROM [ClubPlayerLevel] 
        WHERE ([ClubPlayerLevel].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ClubPlayerLevel] (
        [ClubPlayerLevel].[Club_Id],
        [ClubPlayerLevel].[PlayerLevel_Id],
        [ClubPlayerLevel].[_trackCreationUser],
        [ClubPlayerLevel].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @PlayerLevel_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ClubPlayerLevel].[_rowVersion], @Id AS 'Id' 
        FROM [ClubPlayerLevel] 
        WHERE ([ClubPlayerLevel].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Coaches_Coach] FROM [Event_Coaches_Coach] 
    WHERE ([Event_Coaches_Coach].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [EventBooking_Coaches_Coach] FROM [EventBooking_Coaches_Coach] 
    WHERE ([EventBooking_Coaches_Coach].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Booking] SET
 [Booking].[Coach_Id] = NULL
    WHERE ([Booking].[Coach_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Coach] FROM [Coach] 
    WHERE (([Coach].[Id] = @Id) AND ([Coach].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Coach_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_DeleteEventBookingCoaches]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBooking_Coaches_Coach] FROM [EventBooking_Coaches_Coach] 
    WHERE (([EventBooking_Coaches_Coach].[Id2] = @Id2) AND ([EventBooking_Coaches_Coach].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_DeleteEventCoaches]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Coaches_Coach] FROM [Event_Coaches_Coach] 
    WHERE (([Event_Coaches_Coach].[Id2] = @Id2) AND ([Event_Coaches_Coach].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_Save]
(
 @Id [int] = NULL,
 @CreationDateUTC [datetime] = NULL,
 @isActive [bit] = NULL,
 @User_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Coach] SET
     [Coach].[CreationDateUTC] = @CreationDateUTC,
     [Coach].[isActive] = @isActive,
     [Coach].[User_Id] = @User_Id,
     [Coach].[Club_Id] = @Club_Id,
     [Coach].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Coach].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Coach].[Id] = @Id) AND ([Coach].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Coach_Save')
        RETURN
    END
    SELECT DISTINCT [Coach].[_rowVersion], @Id AS 'Id' 
        FROM [Coach] 
        WHERE ([Coach].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Coach] (
        [Coach].[CreationDateUTC],
        [Coach].[isActive],
        [Coach].[User_Id],
        [Coach].[Club_Id],
        [Coach].[_trackCreationUser],
        [Coach].[_trackLastWriteUser])
    VALUES (
        @CreationDateUTC,
        @isActive,
        @User_Id,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Coach].[_rowVersion], @Id AS 'Id' 
        FROM [Coach] 
        WHERE ([Coach].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_SaveEventBookingCoaches]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [EventBooking_Coaches_Coach].[Id] 
    FROM [EventBooking_Coaches_Coach] 
    WHERE (([EventBooking_Coaches_Coach].[Id2] = @Id2) AND ([EventBooking_Coaches_Coach].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [EventBooking_Coaches_Coach] (
        [EventBooking_Coaches_Coach].[Id],
        [EventBooking_Coaches_Coach].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_SaveEventCoaches]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Event_Coaches_Coach].[Id] 
    FROM [Event_Coaches_Coach] 
    WHERE (([Event_Coaches_Coach].[Id2] = @Id2) AND ([Event_Coaches_Coach].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Event_Coaches_Coach] (
        [Event_Coaches_Coach].[Id],
        [Event_Coaches_Coach].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Components_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [ClubComponents] SET
 [ClubComponents].[Component_Id] = NULL
    WHERE ([ClubComponents].[Component_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Components] FROM [Components] 
    WHERE (([Components].[Id] = @Id) AND ([Components].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Components_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Components_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Components] SET
     [Components].[Name] = @Name,
     [Components].[Sequence] = @Sequence,
     [Components].[isActive] = @isActive,
     [Components].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Components].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Components].[Id] = @Id) AND ([Components].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Components_Save')
        RETURN
    END
    SELECT DISTINCT [Components].[_rowVersion], @Id AS 'Id' 
        FROM [Components] 
        WHERE ([Components].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Components] (
        [Components].[Name],
        [Components].[Sequence],
        [Components].[isActive],
        [Components].[_trackCreationUser],
        [Components].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Components].[_rowVersion], @Id AS 'Id' 
        FROM [Components] 
        WHERE ([Components].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Court_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Courts_Court] FROM [Event_Courts_Court] 
    WHERE ([Event_Courts_Court].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Booking] SET
 [Booking].[Court_Id] = NULL
    WHERE ([Booking].[Court_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Court] FROM [Court] 
    WHERE (([Court].[Id] = @Id) AND ([Court].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Court_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Court_DeleteEventCourts]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Courts_Court] FROM [Event_Courts_Court] 
    WHERE (([Event_Courts_Court].[Id2] = @Id2) AND ([Event_Courts_Court].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Court_Save]
(
 @Id [int] = NULL,
 @Facility_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @isOnline [bit] = NULL,
 @ReferenceNo [nvarchar] (256) = NULL,
 @Surface_Id [int] = NULL,
 @isActive [bit] = NULL,
 @Club_Id [int] = NULL,
 @Colour [nvarchar] (256) = NULL,
 @Title [nvarchar] (256) = NULL,
 @Text [nvarchar] (256) = NULL,
 @Zone_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Court] SET
     [Court].[Facility_Id] = @Facility_Id,
     [Court].[Name] = @Name,
     [Court].[Sequence] = @Sequence,
     [Court].[isOnline] = @isOnline,
     [Court].[ReferenceNo] = @ReferenceNo,
     [Court].[Surface_Id] = @Surface_Id,
     [Court].[isActive] = @isActive,
     [Court].[Club_Id] = @Club_Id,
     [Court].[Colour] = @Colour,
     [Court].[Title] = @Title,
     [Court].[Text] = @Text,
     [Court].[Zone_Id] = @Zone_Id,
     [Court].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Court].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Court].[Id] = @Id) AND ([Court].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Court_Save')
        RETURN
    END
    SELECT DISTINCT [Court].[_rowVersion], @Id AS 'Id' 
        FROM [Court] 
        WHERE ([Court].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Court] (
        [Court].[Facility_Id],
        [Court].[Name],
        [Court].[Sequence],
        [Court].[isOnline],
        [Court].[ReferenceNo],
        [Court].[Surface_Id],
        [Court].[isActive],
        [Court].[Club_Id],
        [Court].[Colour],
        [Court].[Title],
        [Court].[Text],
        [Court].[Zone_Id],
        [Court].[_trackCreationUser],
        [Court].[_trackLastWriteUser])
    VALUES (
        @Facility_Id,
        @Name,
        @Sequence,
        @isOnline,
        @ReferenceNo,
        @Surface_Id,
        @isActive,
        @Club_Id,
        @Colour,
        @Title,
        @Text,
        @Zone_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Court].[_rowVersion], @Id AS 'Id' 
        FROM [Court] 
        WHERE ([Court].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Court_SaveEventCourts]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Event_Courts_Court].[Id] 
    FROM [Event_Courts_Court] 
    WHERE (([Event_Courts_Court].[Id2] = @Id2) AND ([Event_Courts_Court].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Event_Courts_Court] (
        [Event_Courts_Court].[Id],
        [Event_Courts_Court].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [CourtHireRate] FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[Id] = @Id) AND ([CourtHireRate].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CourtHireRate_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_Save]
(
 @Id [int] = NULL,
 @Rate [decimal] (28, 13) = NULL,
 @Zone_Id [int] = NULL,
 @TimePeriod_Id [int] = NULL,
 @IsWeekday [bit] = NULL,
 @Club_Id [int] = NULL,
 @DayOfRate [int] = NULL,
 @MembershipType_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [CourtHireRate] SET
     [CourtHireRate].[Rate] = @Rate,
     [CourtHireRate].[Zone_Id] = @Zone_Id,
     [CourtHireRate].[TimePeriod_Id] = @TimePeriod_Id,
     [CourtHireRate].[IsWeekday] = @IsWeekday,
     [CourtHireRate].[Club_Id] = @Club_Id,
     [CourtHireRate].[DayOfRate] = @DayOfRate,
     [CourtHireRate].[MembershipType_Id] = @MembershipType_Id,
     [CourtHireRate].[_trackLastWriteUser] = @_trackLastWriteUser,
     [CourtHireRate].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([CourtHireRate].[Id] = @Id) AND ([CourtHireRate].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CourtHireRate_Save')
        RETURN
    END
    SELECT DISTINCT [CourtHireRate].[_rowVersion], @Id AS 'Id' 
        FROM [CourtHireRate] 
        WHERE ([CourtHireRate].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [CourtHireRate] (
        [CourtHireRate].[Rate],
        [CourtHireRate].[Zone_Id],
        [CourtHireRate].[TimePeriod_Id],
        [CourtHireRate].[IsWeekday],
        [CourtHireRate].[Club_Id],
        [CourtHireRate].[DayOfRate],
        [CourtHireRate].[MembershipType_Id],
        [CourtHireRate].[_trackCreationUser],
        [CourtHireRate].[_trackLastWriteUser])
    VALUES (
        @Rate,
        @Zone_Id,
        @TimePeriod_Id,
        @IsWeekday,
        @Club_Id,
        @DayOfRate,
        @MembershipType_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [CourtHireRate].[_rowVersion], @Id AS 'Id' 
        FROM [CourtHireRate] 
        WHERE ([CourtHireRate].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [CreditAdjust] SET
 [CreditAdjust].[Credit_Id] = NULL
    WHERE ([CreditAdjust].[Credit_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Credit] FROM [Credit] 
    WHERE (([Credit].[Id] = @Id) AND ([Credit].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Credit_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_Save]
(
 @Id [int] = NULL,
 @DateCreatedUTC [datetime] = NULL,
 @Amount [decimal] (28, 13) = NULL,
 @Transaction [int] = NULL,
 @CreditStatus [int] = NULL,
 @Metadata [nvarchar] (256) = NULL,
 @Customer_Id [int] = NULL,
 @Order_Id [int] = NULL,
 @Balance [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Credit] SET
     [Credit].[DateCreatedUTC] = @DateCreatedUTC,
     [Credit].[Amount] = @Amount,
     [Credit].[Transaction] = @Transaction,
     [Credit].[CreditStatus] = @CreditStatus,
     [Credit].[Metadata] = @Metadata,
     [Credit].[Customer_Id] = @Customer_Id,
     [Credit].[Order_Id] = @Order_Id,
     [Credit].[Balance] = @Balance,
     [Credit].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Credit].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Credit].[Id] = @Id) AND ([Credit].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Credit_Save')
        RETURN
    END
    SELECT DISTINCT [Credit].[_rowVersion], @Id AS 'Id' 
        FROM [Credit] 
        WHERE ([Credit].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Credit] (
        [Credit].[DateCreatedUTC],
        [Credit].[Amount],
        [Credit].[Transaction],
        [Credit].[CreditStatus],
        [Credit].[Metadata],
        [Credit].[Customer_Id],
        [Credit].[Order_Id],
        [Credit].[Balance],
        [Credit].[_trackCreationUser],
        [Credit].[_trackLastWriteUser])
    VALUES (
        @DateCreatedUTC,
        @Amount,
        @Transaction,
        @CreditStatus,
        @Metadata,
        @Customer_Id,
        @Order_Id,
        @Balance,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Credit].[_rowVersion], @Id AS 'Id' 
        FROM [Credit] 
        WHERE ([Credit].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [CreditAdjust] FROM [CreditAdjust] 
    WHERE (([CreditAdjust].[Id] = @Id) AND ([CreditAdjust].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CreditAdjust_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_Save]
(
 @Id [int] = NULL,
 @Credit_Id [int] = NULL,
 @CreditAdjustType [int] = NULL,
 @Amount [decimal] (28, 13) = NULL,
 @AdjustDateTimeUTC [datetime] = NULL,
 @Metadata [nvarchar] (256) = NULL,
 @Balance [decimal] (28, 13) = NULL,
 @Invoice_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [CreditAdjust] SET
     [CreditAdjust].[Credit_Id] = @Credit_Id,
     [CreditAdjust].[CreditAdjustType] = @CreditAdjustType,
     [CreditAdjust].[Amount] = @Amount,
     [CreditAdjust].[AdjustDateTimeUTC] = @AdjustDateTimeUTC,
     [CreditAdjust].[Metadata] = @Metadata,
     [CreditAdjust].[Balance] = @Balance,
     [CreditAdjust].[Invoice_Id] = @Invoice_Id,
     [CreditAdjust].[_trackLastWriteUser] = @_trackLastWriteUser,
     [CreditAdjust].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([CreditAdjust].[Id] = @Id) AND ([CreditAdjust].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'CreditAdjust_Save')
        RETURN
    END
    SELECT DISTINCT [CreditAdjust].[_rowVersion], @Id AS 'Id' 
        FROM [CreditAdjust] 
        WHERE ([CreditAdjust].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [CreditAdjust] (
        [CreditAdjust].[Credit_Id],
        [CreditAdjust].[CreditAdjustType],
        [CreditAdjust].[Amount],
        [CreditAdjust].[AdjustDateTimeUTC],
        [CreditAdjust].[Metadata],
        [CreditAdjust].[Balance],
        [CreditAdjust].[Invoice_Id],
        [CreditAdjust].[_trackCreationUser],
        [CreditAdjust].[_trackLastWriteUser])
    VALUES (
        @Credit_Id,
        @CreditAdjustType,
        @Amount,
        @AdjustDateTimeUTC,
        @Metadata,
        @Balance,
        @Invoice_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [CreditAdjust].[_rowVersion], @Id AS 'Id' 
        FROM [CreditAdjust] 
        WHERE ([CreditAdjust].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Currency_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Account] SET
 [Account].[Currency_Id] = NULL
    WHERE ([Account].[Currency_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Transaction] SET
 [Transaction].[Currency_Id] = NULL
    WHERE ([Transaction].[Currency_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Currency] FROM [Currency] 
    WHERE (([Currency].[Id] = @Id) AND ([Currency].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Currency_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Currency_Save]
(
 @Id [int] = NULL,
 @Code [nvarchar] (256) = NULL,
 @Name [nvarchar] (256) = NULL,
 @DecimalPlaces [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Currency] SET
     [Currency].[Code] = @Code,
     [Currency].[Name] = @Name,
     [Currency].[DecimalPlaces] = @DecimalPlaces,
     [Currency].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Currency].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Currency].[Id] = @Id) AND ([Currency].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Currency_Save')
        RETURN
    END
    SELECT DISTINCT [Currency].[_rowVersion], @Id AS 'Id' 
        FROM [Currency] 
        WHERE ([Currency].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Currency] (
        [Currency].[Code],
        [Currency].[Name],
        [Currency].[DecimalPlaces],
        [Currency].[_trackCreationUser],
        [Currency].[_trackLastWriteUser])
    VALUES (
        @Code,
        @Name,
        @DecimalPlaces,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Currency].[_rowVersion], @Id AS 'Id' 
        FROM [Currency] 
        WHERE ([Currency].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Booking] SET
 [Booking].[Customer_Id] = NULL
    WHERE ([Booking].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Credit] SET
 [Credit].[Customer_Id] = NULL
    WHERE ([Credit].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventWaitlist] SET
 [EventWaitlist].[Customer_Id] = NULL
    WHERE ([EventWaitlist].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventRegistration] SET
 [EventRegistration].[Customer_Id] = NULL
    WHERE ([EventRegistration].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventRoll] SET
 [EventRoll].[Customer_Id] = NULL
    WHERE ([EventRoll].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Account] SET
 [Account].[Customer_Id] = NULL
    WHERE ([Account].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Member] SET
 [Member].[Customer_Id] = NULL
    WHERE ([Member].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Invoice] SET
 [Invoice].[Customer_Id] = NULL
    WHERE ([Invoice].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [OrderItem] SET
 [OrderItem].[Customer_Id] = NULL
    WHERE ([OrderItem].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Sale] SET
 [Sale].[Customer_Id] = NULL
    WHERE ([Sale].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Source] SET
 [Source].[Customer_Id] = NULL
    WHERE ([Source].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MessageLog] SET
 [MessageLog].[Customer_Id] = NULL
    WHERE ([MessageLog].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Order] SET
 [Order].[Customer_Id] = NULL
    WHERE ([Order].[Customer_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Customer] FROM [Customer] 
    WHERE (([Customer].[Id] = @Id) AND ([Customer].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Customer_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_Save]
(
 @Id [int] = NULL,
 @User_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @CreationDateUTC [datetime] = NULL,
 @isMember [bit] = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Customer] SET
     [Customer].[User_Id] = @User_Id,
     [Customer].[Club_Id] = @Club_Id,
     [Customer].[CreationDateUTC] = @CreationDateUTC,
     [Customer].[isMember] = @isMember,
     [Customer].[isActive] = @isActive,
     [Customer].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Customer].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Customer].[Id] = @Id) AND ([Customer].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Customer_Save')
        RETURN
    END
    SELECT DISTINCT [Customer].[_rowVersion], @Id AS 'Id' 
        FROM [Customer] 
        WHERE ([Customer].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Customer] (
        [Customer].[User_Id],
        [Customer].[Club_Id],
        [Customer].[CreationDateUTC],
        [Customer].[isMember],
        [Customer].[isActive],
        [Customer].[_trackCreationUser],
        [Customer].[_trackLastWriteUser])
    VALUES (
        @User_Id,
        @Club_Id,
        @CreationDateUTC,
        @isMember,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Customer].[_rowVersion], @Id AS 'Id' 
        FROM [Customer] 
        WHERE ([Customer].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Days_DaysOfWeek] FROM [Event_Days_DaysOfWeek] 
    WHERE ([Event_Days_DaysOfWeek].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [DaysOfWeek] FROM [DaysOfWeek] 
    WHERE (([DaysOfWeek].[Id] = @Id) AND ([DaysOfWeek].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DaysOfWeek_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_DeleteEventDays]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Days_DaysOfWeek] FROM [Event_Days_DaysOfWeek] 
    WHERE (([Event_Days_DaysOfWeek].[Id2] = @Id2) AND ([Event_Days_DaysOfWeek].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Value [int] = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [DaysOfWeek] SET
     [DaysOfWeek].[Name] = @Name,
     [DaysOfWeek].[Value] = @Value,
     [DaysOfWeek].[Sequence] = @Sequence,
     [DaysOfWeek].[_trackLastWriteUser] = @_trackLastWriteUser,
     [DaysOfWeek].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([DaysOfWeek].[Id] = @Id) AND ([DaysOfWeek].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DaysOfWeek_Save')
        RETURN
    END
    SELECT DISTINCT [DaysOfWeek].[_rowVersion], @Id AS 'Id' 
        FROM [DaysOfWeek] 
        WHERE ([DaysOfWeek].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [DaysOfWeek] (
        [DaysOfWeek].[Name],
        [DaysOfWeek].[Value],
        [DaysOfWeek].[Sequence],
        [DaysOfWeek].[_trackCreationUser],
        [DaysOfWeek].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Value,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [DaysOfWeek].[_rowVersion], @Id AS 'Id' 
        FROM [DaysOfWeek] 
        WHERE ([DaysOfWeek].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_SaveEventDays]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Event_Days_DaysOfWeek].[Id] 
    FROM [Event_Days_DaysOfWeek] 
    WHERE (([Event_Days_DaysOfWeek].[Id2] = @Id2) AND ([Event_Days_DaysOfWeek].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Event_Days_DaysOfWeek] (
        [Event_Days_DaysOfWeek].[Id],
        [Event_Days_DaysOfWeek].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [DeliveryItem] SET
 [DeliveryItem].[Delivery_Id] = NULL
    WHERE ([DeliveryItem].[Delivery_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Delivery] FROM [Delivery] 
    WHERE (([Delivery].[Id] = @Id) AND ([Delivery].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Delivery_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @InvoiceNumber [nvarchar] (256) = NULL,
 @DateReceived [datetime] = NULL,
 @DateRegistered [datetime] = NULL,
 @Supplier_Id [int] = NULL,
 @Facility_Id [int] = NULL,
 @Subtotal [decimal] (28, 13) = NULL,
 @GST [decimal] (28, 13) = NULL,
 @Total [decimal] (28, 13) = NULL,
 @User_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Delivery] SET
     [Delivery].[Club_Id] = @Club_Id,
     [Delivery].[InvoiceNumber] = @InvoiceNumber,
     [Delivery].[DateReceived] = @DateReceived,
     [Delivery].[DateRegistered] = @DateRegistered,
     [Delivery].[Supplier_Id] = @Supplier_Id,
     [Delivery].[Facility_Id] = @Facility_Id,
     [Delivery].[Subtotal] = @Subtotal,
     [Delivery].[GST] = @GST,
     [Delivery].[Total] = @Total,
     [Delivery].[User_Id] = @User_Id,
     [Delivery].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Delivery].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Delivery].[Id] = @Id) AND ([Delivery].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Delivery_Save')
        RETURN
    END
    SELECT DISTINCT [Delivery].[_rowVersion], @Id AS 'Id' 
        FROM [Delivery] 
        WHERE ([Delivery].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Delivery] (
        [Delivery].[Club_Id],
        [Delivery].[InvoiceNumber],
        [Delivery].[DateReceived],
        [Delivery].[DateRegistered],
        [Delivery].[Supplier_Id],
        [Delivery].[Facility_Id],
        [Delivery].[Subtotal],
        [Delivery].[GST],
        [Delivery].[Total],
        [Delivery].[User_Id],
        [Delivery].[_trackCreationUser],
        [Delivery].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @InvoiceNumber,
        @DateReceived,
        @DateRegistered,
        @Supplier_Id,
        @Facility_Id,
        @Subtotal,
        @GST,
        @Total,
        @User_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Delivery].[_rowVersion], @Id AS 'Id' 
        FROM [Delivery] 
        WHERE ([Delivery].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [DeliveryItem] FROM [DeliveryItem] 
    WHERE (([DeliveryItem].[Id] = @Id) AND ([DeliveryItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DeliveryItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_Save]
(
 @Id [int] = NULL,
 @Product_Id [int] = NULL,
 @Quantity [decimal] (28, 13) = NULL,
 @UnitCostPrice [decimal] (28, 13) = NULL,
 @Discount [decimal] (28, 13) = NULL,
 @NetAmount [decimal] (28, 13) = NULL,
 @Total [decimal] (28, 13) = NULL,
 @Delivery_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [DeliveryItem] SET
     [DeliveryItem].[Product_Id] = @Product_Id,
     [DeliveryItem].[Quantity] = @Quantity,
     [DeliveryItem].[UnitCostPrice] = @UnitCostPrice,
     [DeliveryItem].[Discount] = @Discount,
     [DeliveryItem].[NetAmount] = @NetAmount,
     [DeliveryItem].[Total] = @Total,
     [DeliveryItem].[Delivery_Id] = @Delivery_Id,
     [DeliveryItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [DeliveryItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([DeliveryItem].[Id] = @Id) AND ([DeliveryItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DeliveryItem_Save')
        RETURN
    END
    SELECT DISTINCT [DeliveryItem].[_rowVersion], @Id AS 'Id' 
        FROM [DeliveryItem] 
        WHERE ([DeliveryItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [DeliveryItem] (
        [DeliveryItem].[Product_Id],
        [DeliveryItem].[Quantity],
        [DeliveryItem].[UnitCostPrice],
        [DeliveryItem].[Discount],
        [DeliveryItem].[NetAmount],
        [DeliveryItem].[Total],
        [DeliveryItem].[Delivery_Id],
        [DeliveryItem].[_trackCreationUser],
        [DeliveryItem].[_trackLastWriteUser])
    VALUES (
        @Product_Id,
        @Quantity,
        @UnitCostPrice,
        @Discount,
        @NetAmount,
        @Total,
        @Delivery_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [DeliveryItem].[_rowVersion], @Id AS 'Id' 
        FROM [DeliveryItem] 
        WHERE ([DeliveryItem].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Discount_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [DiscountGroup] SET
 [DiscountGroup].[Discount_Id] = NULL
    WHERE ([DiscountGroup].[Discount_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Discount] FROM [Discount] 
    WHERE (([Discount].[Id] = @Id) AND ([Discount].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Discount_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Discount_Save]
(
 @Id [int] = NULL,
 @ProductVariant_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Discount] SET
     [Discount].[ProductVariant_Id] = @ProductVariant_Id,
     [Discount].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Discount].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Discount].[Id] = @Id) AND ([Discount].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Discount_Save')
        RETURN
    END
    SELECT DISTINCT [Discount].[_rowVersion], @Id AS 'Id' 
        FROM [Discount] 
        WHERE ([Discount].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Discount] (
        [Discount].[ProductVariant_Id],
        [Discount].[_trackCreationUser],
        [Discount].[_trackLastWriteUser])
    VALUES (
        @ProductVariant_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Discount].[_rowVersion], @Id AS 'Id' 
        FROM [Discount] 
        WHERE ([Discount].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [DiscountGroup] FROM [DiscountGroup] 
    WHERE (([DiscountGroup].[Id] = @Id) AND ([DiscountGroup].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DiscountGroup_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_Save]
(
 @Id [int] = NULL,
 @Discount_Id [int] = NULL,
 @MembershipType_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [DiscountGroup] SET
     [DiscountGroup].[Discount_Id] = @Discount_Id,
     [DiscountGroup].[MembershipType_Id] = @MembershipType_Id,
     [DiscountGroup].[_trackLastWriteUser] = @_trackLastWriteUser,
     [DiscountGroup].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([DiscountGroup].[Id] = @Id) AND ([DiscountGroup].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'DiscountGroup_Save')
        RETURN
    END
    SELECT DISTINCT [DiscountGroup].[_rowVersion], @Id AS 'Id' 
        FROM [DiscountGroup] 
        WHERE ([DiscountGroup].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [DiscountGroup] (
        [DiscountGroup].[Discount_Id],
        [DiscountGroup].[MembershipType_Id],
        [DiscountGroup].[_trackCreationUser],
        [DiscountGroup].[_trackLastWriteUser])
    VALUES (
        @Discount_Id,
        @MembershipType_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [DiscountGroup].[_rowVersion], @Id AS 'Id' 
        FROM [DiscountGroup] 
        WHERE ([DiscountGroup].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [EquipmentHireItem] SET
 [EquipmentHireItem].[EquipmentHire_Id] = NULL
    WHERE ([EquipmentHireItem].[EquipmentHire_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [EquipmentHire] FROM [EquipmentHire] 
    WHERE (([EquipmentHire].[Id] = @Id) AND ([EquipmentHire].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EquipmentHire_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_Save]
(
 @Id [int] = NULL,
 @DateRequested [datetime] = NULL,
 @Start [datetime] = NULL,
 @End [datetime] = NULL,
 @Price [decimal] (28, 13) = NULL,
 @Club_Id [int] = NULL,
 @Facility_Id [int] = NULL,
 @Status [int] = NULL,
 @NumberOfItems [int] = NULL,
 @User_Id [int] = NULL,
 @Comment [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EquipmentHire] SET
     [EquipmentHire].[DateRequested] = @DateRequested,
     [EquipmentHire].[Start] = @Start,
     [EquipmentHire].[End] = @End,
     [EquipmentHire].[Price] = @Price,
     [EquipmentHire].[Club_Id] = @Club_Id,
     [EquipmentHire].[Facility_Id] = @Facility_Id,
     [EquipmentHire].[Status] = @Status,
     [EquipmentHire].[NumberOfItems] = @NumberOfItems,
     [EquipmentHire].[User_Id] = @User_Id,
     [EquipmentHire].[Comment] = @Comment,
     [EquipmentHire].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EquipmentHire].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EquipmentHire].[Id] = @Id) AND ([EquipmentHire].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EquipmentHire_Save')
        RETURN
    END
    SELECT DISTINCT [EquipmentHire].[_rowVersion], @Id AS 'Id' 
        FROM [EquipmentHire] 
        WHERE ([EquipmentHire].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EquipmentHire] (
        [EquipmentHire].[DateRequested],
        [EquipmentHire].[Start],
        [EquipmentHire].[End],
        [EquipmentHire].[Price],
        [EquipmentHire].[Club_Id],
        [EquipmentHire].[Facility_Id],
        [EquipmentHire].[Status],
        [EquipmentHire].[NumberOfItems],
        [EquipmentHire].[User_Id],
        [EquipmentHire].[Comment],
        [EquipmentHire].[_trackCreationUser],
        [EquipmentHire].[_trackLastWriteUser])
    VALUES (
        @DateRequested,
        @Start,
        @End,
        @Price,
        @Club_Id,
        @Facility_Id,
        @Status,
        @NumberOfItems,
        @User_Id,
        @Comment,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EquipmentHire].[_rowVersion], @Id AS 'Id' 
        FROM [EquipmentHire] 
        WHERE ([EquipmentHire].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EquipmentHireItem] FROM [EquipmentHireItem] 
    WHERE (([EquipmentHireItem].[Id] = @Id) AND ([EquipmentHireItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EquipmentHireItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_Save]
(
 @Id [int] = NULL,
 @Quantity [int] = NULL,
 @EquipmentHire_Id [int] = NULL,
 @Price [decimal] (28, 13) = NULL,
 @Product_Id [int] = NULL,
 @Total [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EquipmentHireItem] SET
     [EquipmentHireItem].[Quantity] = @Quantity,
     [EquipmentHireItem].[EquipmentHire_Id] = @EquipmentHire_Id,
     [EquipmentHireItem].[Price] = @Price,
     [EquipmentHireItem].[Product_Id] = @Product_Id,
     [EquipmentHireItem].[Total] = @Total,
     [EquipmentHireItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EquipmentHireItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EquipmentHireItem].[Id] = @Id) AND ([EquipmentHireItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EquipmentHireItem_Save')
        RETURN
    END
    SELECT DISTINCT [EquipmentHireItem].[_rowVersion], @Id AS 'Id' 
        FROM [EquipmentHireItem] 
        WHERE ([EquipmentHireItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EquipmentHireItem] (
        [EquipmentHireItem].[Quantity],
        [EquipmentHireItem].[EquipmentHire_Id],
        [EquipmentHireItem].[Price],
        [EquipmentHireItem].[Product_Id],
        [EquipmentHireItem].[Total],
        [EquipmentHireItem].[_trackCreationUser],
        [EquipmentHireItem].[_trackLastWriteUser])
    VALUES (
        @Quantity,
        @EquipmentHire_Id,
        @Price,
        @Product_Id,
        @Total,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EquipmentHireItem].[_rowVersion], @Id AS 'Id' 
        FROM [EquipmentHireItem] 
        WHERE ([EquipmentHireItem].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Event_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Coaches_Coach] FROM [Event_Coaches_Coach] 
    WHERE ([Event_Coaches_Coach].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Event_GenderAges_GenderAge] FROM [Event_GenderAges_GenderAge] 
    WHERE ([Event_GenderAges_GenderAge].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Event_Levels_PlayerLevel] FROM [Event_Levels_PlayerLevel] 
    WHERE ([Event_Levels_PlayerLevel].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Event_Courts_Court] FROM [Event_Courts_Court] 
    WHERE ([Event_Courts_Court].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Event_Days_DaysOfWeek] FROM [Event_Days_DaysOfWeek] 
    WHERE ([Event_Days_DaysOfWeek].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Booking] SET
 [Booking].[Event_Id] = NULL
    WHERE ([Booking].[Event_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventFee] SET
 [EventFee].[Event_Id] = NULL
    WHERE ([EventFee].[Event_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventBooking] SET
 [EventBooking].[Event_Id] = NULL
    WHERE ([EventBooking].[Event_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Event] FROM [Event] 
    WHERE (([Event].[Id] = @Id) AND ([Event].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Event_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Event_Save]
(
 @Id [int] = NULL,
 @Title [nvarchar] (256) = NULL,
 @Capacity [int] = NULL,
 @hasWaitlist [bit] = NULL,
 @Description [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @isOnline [bit] = NULL,
 @DailyFee [decimal] (28, 13) = NULL,
 @FullFee [decimal] (28, 13) = NULL,
 @isActive [bit] = NULL,
 @RegistrationDateTime [datetime] = NULL,
 @BookingType_Id [int] = NULL,
 @hasCapacity [bit] = NULL,
 @FeeOption [int] = NULL,
 @FeeStructure [int] = NULL,
 @StartTime [datetime] = NULL,
 @EndTime [datetime] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Event] SET
     [Event].[Title] = @Title,
     [Event].[Capacity] = @Capacity,
     [Event].[hasWaitlist] = @hasWaitlist,
     [Event].[Description] = @Description,
     [Event].[Club_Id] = @Club_Id,
     [Event].[isOnline] = @isOnline,
     [Event].[DailyFee] = @DailyFee,
     [Event].[FullFee] = @FullFee,
     [Event].[isActive] = @isActive,
     [Event].[RegistrationDateTime] = @RegistrationDateTime,
     [Event].[BookingType_Id] = @BookingType_Id,
     [Event].[hasCapacity] = @hasCapacity,
     [Event].[FeeOption] = @FeeOption,
     [Event].[FeeStructure] = @FeeStructure,
     [Event].[StartTime] = @StartTime,
     [Event].[EndTime] = @EndTime,
     [Event].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Event].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Event].[Id] = @Id) AND ([Event].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Event_Save')
        RETURN
    END
    SELECT DISTINCT [Event].[_rowVersion], @Id AS 'Id' 
        FROM [Event] 
        WHERE ([Event].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Event] (
        [Event].[Title],
        [Event].[Capacity],
        [Event].[hasWaitlist],
        [Event].[Description],
        [Event].[Club_Id],
        [Event].[isOnline],
        [Event].[DailyFee],
        [Event].[FullFee],
        [Event].[isActive],
        [Event].[RegistrationDateTime],
        [Event].[BookingType_Id],
        [Event].[hasCapacity],
        [Event].[FeeOption],
        [Event].[FeeStructure],
        [Event].[StartTime],
        [Event].[EndTime],
        [Event].[_trackCreationUser],
        [Event].[_trackLastWriteUser])
    VALUES (
        @Title,
        @Capacity,
        @hasWaitlist,
        @Description,
        @Club_Id,
        @isOnline,
        @DailyFee,
        @FullFee,
        @isActive,
        @RegistrationDateTime,
        @BookingType_Id,
        @hasCapacity,
        @FeeOption,
        @FeeStructure,
        @StartTime,
        @EndTime,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Event].[_rowVersion], @Id AS 'Id' 
        FROM [Event] 
        WHERE ([Event].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBooking_Coaches_Coach] FROM [EventBooking_Coaches_Coach] 
    WHERE ([EventBooking_Coaches_Coach].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [EventBooking_Booking_Booking] FROM [EventBooking_Booking_Booking] 
    WHERE ([EventBooking_Booking_Booking].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [EventBooking_Levels_PlayerLevel] FROM [EventBooking_Levels_PlayerLevel] 
    WHERE ([EventBooking_Levels_PlayerLevel].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [EventWaitlist] SET
 [EventWaitlist].[EventBooking_Id] = NULL
    WHERE ([EventWaitlist].[EventBooking_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventRegistration] SET
 [EventRegistration].[EventBooking_Id] = NULL
    WHERE ([EventRegistration].[EventBooking_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EventRoll] SET
 [EventRoll].[EventBooking_Id] = NULL
    WHERE ([EventRoll].[EventBooking_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [EventBooking] FROM [EventBooking] 
    WHERE (([EventBooking].[Id] = @Id) AND ([EventBooking].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventBooking_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_Save]
(
 @Id [int] = NULL,
 @Event_Id [int] = NULL,
 @Notes [nvarchar] (256) = NULL,
 @EventStatus [int] = NULL,
 @Date [datetime] = NULL,
 @EventBookingCancellation_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventBooking] SET
     [EventBooking].[Event_Id] = @Event_Id,
     [EventBooking].[Notes] = @Notes,
     [EventBooking].[EventStatus] = @EventStatus,
     [EventBooking].[Date] = @Date,
     [EventBooking].[EventBookingCancellation_Id] = @EventBookingCancellation_Id,
     [EventBooking].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventBooking].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventBooking].[Id] = @Id) AND ([EventBooking].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventBooking_Save')
        RETURN
    END
    SELECT DISTINCT [EventBooking].[_rowVersion], @Id AS 'Id' 
        FROM [EventBooking] 
        WHERE ([EventBooking].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventBooking] (
        [EventBooking].[Event_Id],
        [EventBooking].[Notes],
        [EventBooking].[EventStatus],
        [EventBooking].[Date],
        [EventBooking].[EventBookingCancellation_Id],
        [EventBooking].[_trackCreationUser],
        [EventBooking].[_trackLastWriteUser])
    VALUES (
        @Event_Id,
        @Notes,
        @EventStatus,
        @Date,
        @EventBookingCancellation_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventBooking].[_rowVersion], @Id AS 'Id' 
        FROM [EventBooking] 
        WHERE ([EventBooking].[Id] = @Id)
END
IF(@EventBookingCancellation_Id IS NOT NULL)
BEGIN
    UPDATE [EventBookingCancellation] SET
     [EventBookingCancellation].[EventBooking_Id] = @Id
        WHERE ([EventBookingCancellation].[Id] = @EventBookingCancellation_Id)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBookingCancellation] FROM [EventBookingCancellation] 
    WHERE (([EventBookingCancellation].[Id] = @Id) AND ([EventBookingCancellation].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventBookingCancellation_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_Save]
(
 @Id [int] = NULL,
 @CancellationType_Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @EventBooking_Id [int] = NULL,
 @RefundType [int] = NULL,
 @RefundPortion [int] = NULL,
 @isVisible [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventBookingCancellation] SET
     [EventBookingCancellation].[CancellationType_Id] = @CancellationType_Id,
     [EventBookingCancellation].[Description] = @Description,
     [EventBookingCancellation].[EventBooking_Id] = @EventBooking_Id,
     [EventBookingCancellation].[RefundType] = @RefundType,
     [EventBookingCancellation].[RefundPortion] = @RefundPortion,
     [EventBookingCancellation].[isVisible] = @isVisible,
     [EventBookingCancellation].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventBookingCancellation].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventBookingCancellation].[Id] = @Id) AND ([EventBookingCancellation].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventBookingCancellation_Save')
        RETURN
    END
    SELECT DISTINCT [EventBookingCancellation].[_rowVersion], @Id AS 'Id' 
        FROM [EventBookingCancellation] 
        WHERE ([EventBookingCancellation].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventBookingCancellation] (
        [EventBookingCancellation].[CancellationType_Id],
        [EventBookingCancellation].[Description],
        [EventBookingCancellation].[EventBooking_Id],
        [EventBookingCancellation].[RefundType],
        [EventBookingCancellation].[RefundPortion],
        [EventBookingCancellation].[isVisible],
        [EventBookingCancellation].[_trackCreationUser],
        [EventBookingCancellation].[_trackLastWriteUser])
    VALUES (
        @CancellationType_Id,
        @Description,
        @EventBooking_Id,
        @RefundType,
        @RefundPortion,
        @isVisible,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventBookingCancellation].[_rowVersion], @Id AS 'Id' 
        FROM [EventBookingCancellation] 
        WHERE ([EventBookingCancellation].[Id] = @Id)
END
IF(@EventBooking_Id IS NOT NULL)
BEGIN
    UPDATE [EventBooking] SET
     [EventBooking].[EventBookingCancellation_Id] = @Id
        WHERE ([EventBooking].[Id] = @EventBooking_Id)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventFee] FROM [EventFee] 
    WHERE (([EventFee].[Id] = @Id) AND ([EventFee].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventFee_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_Save]
(
 @Id [int] = NULL,
 @Event_Id [int] = NULL,
 @MembershipType_Id [int] = NULL,
 @DailyFee [decimal] (28, 13) = NULL,
 @FullFee [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventFee] SET
     [EventFee].[Event_Id] = @Event_Id,
     [EventFee].[MembershipType_Id] = @MembershipType_Id,
     [EventFee].[DailyFee] = @DailyFee,
     [EventFee].[FullFee] = @FullFee,
     [EventFee].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventFee].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventFee].[Id] = @Id) AND ([EventFee].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventFee_Save')
        RETURN
    END
    SELECT DISTINCT [EventFee].[_rowVersion], @Id AS 'Id' 
        FROM [EventFee] 
        WHERE ([EventFee].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventFee] (
        [EventFee].[Event_Id],
        [EventFee].[MembershipType_Id],
        [EventFee].[DailyFee],
        [EventFee].[FullFee],
        [EventFee].[_trackCreationUser],
        [EventFee].[_trackLastWriteUser])
    VALUES (
        @Event_Id,
        @MembershipType_Id,
        @DailyFee,
        @FullFee,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventFee].[_rowVersion], @Id AS 'Id' 
        FROM [EventFee] 
        WHERE ([EventFee].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventRegistration] FROM [EventRegistration] 
    WHERE (([EventRegistration].[Id] = @Id) AND ([EventRegistration].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventRegistration_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_Save]
(
 @Id [int] = NULL,
 @DateRegistered [datetime] = NULL,
 @Fee [decimal] (28, 13) = NULL,
 @isPaid [bit] = NULL,
 @isAttending [bit] = NULL,
 @Customer_Id [int] = NULL,
 @EventBooking_Id [int] = NULL,
 @Note [nvarchar] (256) = NULL,
 @RegistrationStatus [int] = NULL,
 @OrderItem_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventRegistration] SET
     [EventRegistration].[DateRegistered] = @DateRegistered,
     [EventRegistration].[Fee] = @Fee,
     [EventRegistration].[isPaid] = @isPaid,
     [EventRegistration].[isAttending] = @isAttending,
     [EventRegistration].[Customer_Id] = @Customer_Id,
     [EventRegistration].[EventBooking_Id] = @EventBooking_Id,
     [EventRegistration].[Note] = @Note,
     [EventRegistration].[RegistrationStatus] = @RegistrationStatus,
     [EventRegistration].[OrderItem_Id] = @OrderItem_Id,
     [EventRegistration].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventRegistration].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventRegistration].[Id] = @Id) AND ([EventRegistration].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventRegistration_Save')
        RETURN
    END
    SELECT DISTINCT [EventRegistration].[_rowVersion], @Id AS 'Id' 
        FROM [EventRegistration] 
        WHERE ([EventRegistration].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventRegistration] (
        [EventRegistration].[DateRegistered],
        [EventRegistration].[Fee],
        [EventRegistration].[isPaid],
        [EventRegistration].[isAttending],
        [EventRegistration].[Customer_Id],
        [EventRegistration].[EventBooking_Id],
        [EventRegistration].[Note],
        [EventRegistration].[RegistrationStatus],
        [EventRegistration].[OrderItem_Id],
        [EventRegistration].[_trackCreationUser],
        [EventRegistration].[_trackLastWriteUser])
    VALUES (
        @DateRegistered,
        @Fee,
        @isPaid,
        @isAttending,
        @Customer_Id,
        @EventBooking_Id,
        @Note,
        @RegistrationStatus,
        @OrderItem_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventRegistration].[_rowVersion], @Id AS 'Id' 
        FROM [EventRegistration] 
        WHERE ([EventRegistration].[Id] = @Id)
END
IF(@OrderItem_Id IS NOT NULL)
BEGIN
    UPDATE [OrderItem] SET
     [OrderItem].[Registration_Id] = @Id
        WHERE ([OrderItem].[Id] = @OrderItem_Id)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventRoll] FROM [EventRoll] 
    WHERE (([EventRoll].[Id] = @Id) AND ([EventRoll].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventRoll_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_Save]
(
 @Id [int] = NULL,
 @isAttending [bit] = NULL,
 @Comment [nvarchar] (256) = NULL,
 @Customer_Id [int] = NULL,
 @EventBooking_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventRoll] SET
     [EventRoll].[isAttending] = @isAttending,
     [EventRoll].[Comment] = @Comment,
     [EventRoll].[Customer_Id] = @Customer_Id,
     [EventRoll].[EventBooking_Id] = @EventBooking_Id,
     [EventRoll].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventRoll].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventRoll].[Id] = @Id) AND ([EventRoll].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventRoll_Save')
        RETURN
    END
    SELECT DISTINCT [EventRoll].[_rowVersion], @Id AS 'Id' 
        FROM [EventRoll] 
        WHERE ([EventRoll].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventRoll] (
        [EventRoll].[isAttending],
        [EventRoll].[Comment],
        [EventRoll].[Customer_Id],
        [EventRoll].[EventBooking_Id],
        [EventRoll].[_trackCreationUser],
        [EventRoll].[_trackLastWriteUser])
    VALUES (
        @isAttending,
        @Comment,
        @Customer_Id,
        @EventBooking_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventRoll].[_rowVersion], @Id AS 'Id' 
        FROM [EventRoll] 
        WHERE ([EventRoll].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventWaitlist] FROM [EventWaitlist] 
    WHERE (([EventWaitlist].[Id] = @Id) AND ([EventWaitlist].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventWaitlist_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_Save]
(
 @Id [int] = NULL,
 @isWaiting [bit] = NULL,
 @Comment [nvarchar] (256) = NULL,
 @Customer_Id [int] = NULL,
 @EventBooking_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [EventWaitlist] SET
     [EventWaitlist].[isWaiting] = @isWaiting,
     [EventWaitlist].[Comment] = @Comment,
     [EventWaitlist].[Customer_Id] = @Customer_Id,
     [EventWaitlist].[EventBooking_Id] = @EventBooking_Id,
     [EventWaitlist].[_trackLastWriteUser] = @_trackLastWriteUser,
     [EventWaitlist].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([EventWaitlist].[Id] = @Id) AND ([EventWaitlist].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'EventWaitlist_Save')
        RETURN
    END
    SELECT DISTINCT [EventWaitlist].[_rowVersion], @Id AS 'Id' 
        FROM [EventWaitlist] 
        WHERE ([EventWaitlist].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [EventWaitlist] (
        [EventWaitlist].[isWaiting],
        [EventWaitlist].[Comment],
        [EventWaitlist].[Customer_Id],
        [EventWaitlist].[EventBooking_Id],
        [EventWaitlist].[_trackCreationUser],
        [EventWaitlist].[_trackLastWriteUser])
    VALUES (
        @isWaiting,
        @Comment,
        @Customer_Id,
        @EventBooking_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [EventWaitlist].[_rowVersion], @Id AS 'Id' 
        FROM [EventWaitlist] 
        WHERE ([EventWaitlist].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Court] SET
 [Court].[Facility_Id] = NULL
    WHERE ([Court].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Booking] SET
 [Booking].[Facility_Id] = NULL
    WHERE ([Booking].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Stocktake] SET
 [Stocktake].[Facility_Id] = NULL
    WHERE ([Stocktake].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Delivery] SET
 [Delivery].[Facility_Id] = NULL
    WHERE ([Delivery].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EquipmentHire] SET
 [EquipmentHire].[Facility_Id] = NULL
    WHERE ([EquipmentHire].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Zone] SET
 [Zone].[Facility_Id] = NULL
    WHERE ([Zone].[Facility_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Facility] FROM [Facility] 
    WHERE (([Facility].[Id] = @Id) AND ([Facility].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Facility_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @FullName [nvarchar] (256) = NULL,
 @ShortName [nvarchar] (256) = NULL,
 @AddressLine1 [nvarchar] (256) = NULL,
 @AddressLine2 [nvarchar] (256) = NULL,
 @Email [nvarchar] (256) = NULL,
 @Phone [nvarchar] (256) = NULL,
 @Suburb [nvarchar] (256) = NULL,
 @State [nvarchar] (256) = NULL,
 @Postcode [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @ReferenceNo [nvarchar] (256) = NULL,
 @isOnline [bit] = NULL,
 @ReserveName [nvarchar] (256) = NULL,
 @isActive [bit] = NULL,
 @Colour [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Facility] SET
     [Facility].[Club_Id] = @Club_Id,
     [Facility].[FullName] = @FullName,
     [Facility].[ShortName] = @ShortName,
     [Facility].[AddressLine1] = @AddressLine1,
     [Facility].[AddressLine2] = @AddressLine2,
     [Facility].[Email] = @Email,
     [Facility].[Phone] = @Phone,
     [Facility].[Suburb] = @Suburb,
     [Facility].[State] = @State,
     [Facility].[Postcode] = @Postcode,
     [Facility].[Sequence] = @Sequence,
     [Facility].[ReferenceNo] = @ReferenceNo,
     [Facility].[isOnline] = @isOnline,
     [Facility].[ReserveName] = @ReserveName,
     [Facility].[isActive] = @isActive,
     [Facility].[Colour] = @Colour,
     [Facility].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Facility].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Facility].[Id] = @Id) AND ([Facility].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Facility_Save')
        RETURN
    END
    SELECT DISTINCT [Facility].[_rowVersion], @Id AS 'Id' 
        FROM [Facility] 
        WHERE ([Facility].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Facility] (
        [Facility].[Club_Id],
        [Facility].[FullName],
        [Facility].[ShortName],
        [Facility].[AddressLine1],
        [Facility].[AddressLine2],
        [Facility].[Email],
        [Facility].[Phone],
        [Facility].[Suburb],
        [Facility].[State],
        [Facility].[Postcode],
        [Facility].[Sequence],
        [Facility].[ReferenceNo],
        [Facility].[isOnline],
        [Facility].[ReserveName],
        [Facility].[isActive],
        [Facility].[Colour],
        [Facility].[_trackCreationUser],
        [Facility].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @FullName,
        @ShortName,
        @AddressLine1,
        @AddressLine2,
        @Email,
        @Phone,
        @Suburb,
        @State,
        @Postcode,
        @Sequence,
        @ReferenceNo,
        @isOnline,
        @ReserveName,
        @isActive,
        @Colour,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Facility].[_rowVersion], @Id AS 'Id' 
        FROM [Facility] 
        WHERE ([Facility].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [User] SET
 [User].[Gender_Id] = NULL
    WHERE ([User].[Gender_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Gender] FROM [Gender] 
    WHERE (([Gender].[Id] = @Id) AND ([Gender].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Gender_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Gender] SET
     [Gender].[Name] = @Name,
     [Gender].[Sequence] = @Sequence,
     [Gender].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Gender].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Gender].[Id] = @Id) AND ([Gender].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Gender_Save')
        RETURN
    END
    SELECT DISTINCT [Gender].[_rowVersion], @Id AS 'Id' 
        FROM [Gender] 
        WHERE ([Gender].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Gender] (
        [Gender].[Name],
        [Gender].[Sequence],
        [Gender].[_trackCreationUser],
        [Gender].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Gender].[_rowVersion], @Id AS 'Id' 
        FROM [Gender] 
        WHERE ([Gender].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_GenderAges_GenderAge] FROM [Event_GenderAges_GenderAge] 
    WHERE ([Event_GenderAges_GenderAge].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [GenderAge] FROM [GenderAge] 
    WHERE (([GenderAge].[Id] = @Id) AND ([GenderAge].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'GenderAge_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_DeleteEventGenderAges]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_GenderAges_GenderAge] FROM [Event_GenderAges_GenderAge] 
    WHERE (([Event_GenderAges_GenderAge].[Id2] = @Id2) AND ([Event_GenderAges_GenderAge].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @AgeGroup [int] = NULL,
 @GenderGroup [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [GenderAge] SET
     [GenderAge].[Name] = @Name,
     [GenderAge].[Sequence] = @Sequence,
     [GenderAge].[AgeGroup] = @AgeGroup,
     [GenderAge].[GenderGroup] = @GenderGroup,
     [GenderAge].[_trackLastWriteUser] = @_trackLastWriteUser,
     [GenderAge].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([GenderAge].[Id] = @Id) AND ([GenderAge].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'GenderAge_Save')
        RETURN
    END
    SELECT DISTINCT [GenderAge].[_rowVersion], @Id AS 'Id' 
        FROM [GenderAge] 
        WHERE ([GenderAge].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [GenderAge] (
        [GenderAge].[Name],
        [GenderAge].[Sequence],
        [GenderAge].[AgeGroup],
        [GenderAge].[GenderGroup],
        [GenderAge].[_trackCreationUser],
        [GenderAge].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @AgeGroup,
        @GenderGroup,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [GenderAge].[_rowVersion], @Id AS 'Id' 
        FROM [GenderAge] 
        WHERE ([GenderAge].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_SaveEventGenderAges]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Event_GenderAges_GenderAge].[Id] 
    FROM [Event_GenderAges_GenderAge] 
    WHERE (([Event_GenderAges_GenderAge].[Id2] = @Id2) AND ([Event_GenderAges_GenderAge].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Event_GenderAges_GenderAge] (
        [Event_GenderAges_GenderAge].[Id],
        [Event_GenderAges_GenderAge].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Invoice_Journals_Journal] FROM [Invoice_Journals_Journal] 
    WHERE ([Invoice_Journals_Journal].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Transaction] SET
 [Transaction].[Invoice_Id] = NULL
    WHERE ([Transaction].[Invoice_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Sale] SET
 [Sale].[Invoice_Id] = NULL
    WHERE ([Sale].[Invoice_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [CreditAdjust] SET
 [CreditAdjust].[Invoice_Id] = NULL
    WHERE ([CreditAdjust].[Invoice_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Invoice] FROM [Invoice] 
    WHERE (([Invoice].[Id] = @Id) AND ([Invoice].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Invoice_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_Save]
(
 @Id [int] = NULL,
 @InvoiceNo [nvarchar] (256) = NULL,
 @Customer_Id [int] = NULL,
 @CreationDateUTC [datetime] = NULL,
 @Admin_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @Order_Id [int] = NULL,
 @Balance [decimal] (28, 13) = NULL,
 @Change [decimal] (28, 13) = NULL,
 @AmountPaid [decimal] (28, 13) = NULL,
 @Note [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Invoice] SET
     [Invoice].[InvoiceNo] = @InvoiceNo,
     [Invoice].[Customer_Id] = @Customer_Id,
     [Invoice].[CreationDateUTC] = @CreationDateUTC,
     [Invoice].[Admin_Id] = @Admin_Id,
     [Invoice].[Club_Id] = @Club_Id,
     [Invoice].[Order_Id] = @Order_Id,
     [Invoice].[Balance] = @Balance,
     [Invoice].[Change] = @Change,
     [Invoice].[AmountPaid] = @AmountPaid,
     [Invoice].[Note] = @Note,
     [Invoice].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Invoice].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Invoice].[Id] = @Id) AND ([Invoice].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Invoice_Save')
        RETURN
    END
    SELECT DISTINCT [Invoice].[_rowVersion], @Id AS 'Id' 
        FROM [Invoice] 
        WHERE ([Invoice].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Invoice] (
        [Invoice].[InvoiceNo],
        [Invoice].[Customer_Id],
        [Invoice].[CreationDateUTC],
        [Invoice].[Admin_Id],
        [Invoice].[Club_Id],
        [Invoice].[Order_Id],
        [Invoice].[Balance],
        [Invoice].[Change],
        [Invoice].[AmountPaid],
        [Invoice].[Note],
        [Invoice].[_trackCreationUser],
        [Invoice].[_trackLastWriteUser])
    VALUES (
        @InvoiceNo,
        @Customer_Id,
        @CreationDateUTC,
        @Admin_Id,
        @Club_Id,
        @Order_Id,
        @Balance,
        @Change,
        @AmountPaid,
        @Note,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Invoice].[_rowVersion], @Id AS 'Id' 
        FROM [Invoice] 
        WHERE ([Invoice].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Invoice_Journals_Journal] FROM [Invoice_Journals_Journal] 
    WHERE ([Invoice_Journals_Journal].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Transaction] SET
 [Transaction].[Journal_Id] = NULL
    WHERE ([Transaction].[Journal_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Journal] FROM [Journal] 
    WHERE (([Journal].[Id] = @Id) AND ([Journal].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Journal_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_DeleteInvoiceJournals]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Invoice_Journals_Journal] FROM [Invoice_Journals_Journal] 
    WHERE (([Invoice_Journals_Journal].[Id2] = @Id2) AND ([Invoice_Journals_Journal].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_Save]
(
 @Id [int] = NULL,
 @JournalType [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Journal] SET
     [Journal].[JournalType] = @JournalType,
     [Journal].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Journal].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Journal].[Id] = @Id) AND ([Journal].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Journal_Save')
        RETURN
    END
    SELECT DISTINCT [Journal].[_rowVersion], @Id AS 'Id' 
        FROM [Journal] 
        WHERE ([Journal].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Journal] (
        [Journal].[JournalType],
        [Journal].[_trackCreationUser],
        [Journal].[_trackLastWriteUser])
    VALUES (
        @JournalType,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Journal].[_rowVersion], @Id AS 'Id' 
        FROM [Journal] 
        WHERE ([Journal].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_SaveInvoiceJournals]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Invoice_Journals_Journal].[Id] 
    FROM [Invoice_Journals_Journal] 
    WHERE (([Invoice_Journals_Journal].[Id2] = @Id2) AND ([Invoice_Journals_Journal].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Invoice_Journals_Journal] (
        [Invoice_Journals_Journal].[Id],
        [Invoice_Journals_Journal].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Member_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [OrderItem] SET
 [OrderItem].[Membership_Id] = NULL
    WHERE ([OrderItem].[Membership_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Member] FROM [Member] 
    WHERE (([Member].[Id] = @Id) AND ([Member].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Member_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Member_Save]
(
 @Id [int] = NULL,
 @StartDateUTC [datetime] = NULL,
 @EndDateUTC [datetime] = NULL,
 @MembershipType_Id [int] = NULL,
 @Customer_Id [int] = NULL,
 @isActive [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Member] SET
     [Member].[StartDateUTC] = @StartDateUTC,
     [Member].[EndDateUTC] = @EndDateUTC,
     [Member].[MembershipType_Id] = @MembershipType_Id,
     [Member].[Customer_Id] = @Customer_Id,
     [Member].[isActive] = @isActive,
     [Member].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Member].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Member].[Id] = @Id) AND ([Member].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Member_Save')
        RETURN
    END
    SELECT DISTINCT [Member].[_rowVersion], @Id AS 'Id' 
        FROM [Member] 
        WHERE ([Member].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Member] (
        [Member].[StartDateUTC],
        [Member].[EndDateUTC],
        [Member].[MembershipType_Id],
        [Member].[Customer_Id],
        [Member].[isActive],
        [Member].[_trackCreationUser],
        [Member].[_trackLastWriteUser])
    VALUES (
        @StartDateUTC,
        @EndDateUTC,
        @MembershipType_Id,
        @Customer_Id,
        @isActive,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Member].[_rowVersion], @Id AS 'Id' 
        FROM [Member] 
        WHERE ([Member].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [EventFee] SET
 [EventFee].[MembershipType_Id] = NULL
    WHERE ([EventFee].[MembershipType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [CourtHireRate] SET
 [CourtHireRate].[MembershipType_Id] = NULL
    WHERE ([CourtHireRate].[MembershipType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MembershipTypeTimePeriod] SET
 [MembershipTypeTimePeriod].[MembershipType_Id] = NULL
    WHERE ([MembershipTypeTimePeriod].[MembershipType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Member] SET
 [Member].[MembershipType_Id] = NULL
    WHERE ([Member].[MembershipType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [DiscountGroup] SET
 [DiscountGroup].[MembershipType_Id] = NULL
    WHERE ([DiscountGroup].[MembershipType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [MembershipType] FROM [MembershipType] 
    WHERE (([MembershipType].[Id] = @Id) AND ([MembershipType].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MembershipType_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @ExpiryRule [int] = NULL,
 @Limit [int] = NULL,
 @ProRata [money] = NULL,
 @RenewalDate [datetime] = NULL,
 @Duration [int] = NULL,
 @IsActive [bit] = NULL,
 @Fee [money] = NULL,
 @Description [nvarchar] (256) = NULL,
 @isDefault [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MembershipType] SET
     [MembershipType].[Name] = @Name,
     [MembershipType].[Club_Id] = @Club_Id,
     [MembershipType].[ExpiryRule] = @ExpiryRule,
     [MembershipType].[Limit] = @Limit,
     [MembershipType].[ProRata] = @ProRata,
     [MembershipType].[RenewalDate] = @RenewalDate,
     [MembershipType].[Duration] = @Duration,
     [MembershipType].[IsActive] = @IsActive,
     [MembershipType].[Fee] = @Fee,
     [MembershipType].[Description] = @Description,
     [MembershipType].[isDefault] = @isDefault,
     [MembershipType].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MembershipType].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MembershipType].[Id] = @Id) AND ([MembershipType].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MembershipType_Save')
        RETURN
    END
    SELECT DISTINCT [MembershipType].[_rowVersion], @Id AS 'Id' 
        FROM [MembershipType] 
        WHERE ([MembershipType].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MembershipType] (
        [MembershipType].[Name],
        [MembershipType].[Club_Id],
        [MembershipType].[ExpiryRule],
        [MembershipType].[Limit],
        [MembershipType].[ProRata],
        [MembershipType].[RenewalDate],
        [MembershipType].[Duration],
        [MembershipType].[IsActive],
        [MembershipType].[Fee],
        [MembershipType].[Description],
        [MembershipType].[isDefault],
        [MembershipType].[_trackCreationUser],
        [MembershipType].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Club_Id,
        @ExpiryRule,
        @Limit,
        @ProRata,
        @RenewalDate,
        @Duration,
        @IsActive,
        @Fee,
        @Description,
        @isDefault,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MembershipType].[_rowVersion], @Id AS 'Id' 
        FROM [MembershipType] 
        WHERE ([MembershipType].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [MembershipTypeTimePeriod] FROM [MembershipTypeTimePeriod] 
    WHERE (([MembershipTypeTimePeriod].[Id] = @Id) AND ([MembershipTypeTimePeriod].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MembershipTypeTimePeriod_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_Save]
(
 @Id [int] = NULL,
 @MembershipType_Id [int] = NULL,
 @TimePeriodGroup_Id [int] = NULL,
 @isWeekday [bit] = NULL,
 @Club_Id [int] = NULL,
 @DayOfRate [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MembershipTypeTimePeriod] SET
     [MembershipTypeTimePeriod].[MembershipType_Id] = @MembershipType_Id,
     [MembershipTypeTimePeriod].[TimePeriodGroup_Id] = @TimePeriodGroup_Id,
     [MembershipTypeTimePeriod].[isWeekday] = @isWeekday,
     [MembershipTypeTimePeriod].[Club_Id] = @Club_Id,
     [MembershipTypeTimePeriod].[DayOfRate] = @DayOfRate,
     [MembershipTypeTimePeriod].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MembershipTypeTimePeriod].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MembershipTypeTimePeriod].[Id] = @Id) AND ([MembershipTypeTimePeriod].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MembershipTypeTimePeriod_Save')
        RETURN
    END
    SELECT DISTINCT [MembershipTypeTimePeriod].[_rowVersion], @Id AS 'Id' 
        FROM [MembershipTypeTimePeriod] 
        WHERE ([MembershipTypeTimePeriod].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MembershipTypeTimePeriod] (
        [MembershipTypeTimePeriod].[MembershipType_Id],
        [MembershipTypeTimePeriod].[TimePeriodGroup_Id],
        [MembershipTypeTimePeriod].[isWeekday],
        [MembershipTypeTimePeriod].[Club_Id],
        [MembershipTypeTimePeriod].[DayOfRate],
        [MembershipTypeTimePeriod].[_trackCreationUser],
        [MembershipTypeTimePeriod].[_trackLastWriteUser])
    VALUES (
        @MembershipType_Id,
        @TimePeriodGroup_Id,
        @isWeekday,
        @Club_Id,
        @DayOfRate,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MembershipTypeTimePeriod].[_rowVersion], @Id AS 'Id' 
        FROM [MembershipTypeTimePeriod] 
        WHERE ([MembershipTypeTimePeriod].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [MessageService] SET
 [MessageService].[MessageAccount_Id] = NULL
    WHERE ([MessageService].[MessageAccount_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [MessageAccount] FROM [MessageAccount] 
    WHERE (([MessageAccount].[Id] = @Id) AND ([MessageAccount].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageAccount_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_Save]
(
 @Id [int] = NULL,
 @Club_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Reference [nvarchar] (256) = NULL,
 @Token [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MessageAccount] SET
     [MessageAccount].[Club_Id] = @Club_Id,
     [MessageAccount].[Name] = @Name,
     [MessageAccount].[Reference] = @Reference,
     [MessageAccount].[Token] = @Token,
     [MessageAccount].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MessageAccount].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MessageAccount].[Id] = @Id) AND ([MessageAccount].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageAccount_Save')
        RETURN
    END
    SELECT DISTINCT [MessageAccount].[_rowVersion], @Id AS 'Id' 
        FROM [MessageAccount] 
        WHERE ([MessageAccount].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MessageAccount] (
        [MessageAccount].[Club_Id],
        [MessageAccount].[Name],
        [MessageAccount].[Reference],
        [MessageAccount].[Token],
        [MessageAccount].[_trackCreationUser],
        [MessageAccount].[_trackLastWriteUser])
    VALUES (
        @Club_Id,
        @Name,
        @Reference,
        @Token,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MessageAccount].[_rowVersion], @Id AS 'Id' 
        FROM [MessageAccount] 
        WHERE ([MessageAccount].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [MessageLog] FROM [MessageLog] 
    WHERE (([MessageLog].[Id] = @Id) AND ([MessageLog].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageLog_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_Save]
(
 @Id [int] = NULL,
 @Body [nvarchar] (256) = NULL,
 @Direction [int] = NULL,
 @Status [int] = NULL,
 @ErrorMessage [nvarchar] (256) = NULL,
 @Price [decimal] (28, 13) = NULL,
 @Admin_Id [int] = NULL,
 @Customer_Id [int] = NULL,
 @Reference [nvarchar] (256) = NULL,
 @From [nvarchar] (256) = NULL,
 @To [nvarchar] (256) = NULL,
 @DateSent [datetime] = NULL,
 @MessageNumber_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MessageLog] SET
     [MessageLog].[Body] = @Body,
     [MessageLog].[Direction] = @Direction,
     [MessageLog].[Status] = @Status,
     [MessageLog].[ErrorMessage] = @ErrorMessage,
     [MessageLog].[Price] = @Price,
     [MessageLog].[Admin_Id] = @Admin_Id,
     [MessageLog].[Customer_Id] = @Customer_Id,
     [MessageLog].[Reference] = @Reference,
     [MessageLog].[From] = @From,
     [MessageLog].[To] = @To,
     [MessageLog].[DateSent] = @DateSent,
     [MessageLog].[MessageNumber_Id] = @MessageNumber_Id,
     [MessageLog].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MessageLog].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MessageLog].[Id] = @Id) AND ([MessageLog].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageLog_Save')
        RETURN
    END
    SELECT DISTINCT [MessageLog].[_rowVersion], @Id AS 'Id' 
        FROM [MessageLog] 
        WHERE ([MessageLog].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MessageLog] (
        [MessageLog].[Body],
        [MessageLog].[Direction],
        [MessageLog].[Status],
        [MessageLog].[ErrorMessage],
        [MessageLog].[Price],
        [MessageLog].[Admin_Id],
        [MessageLog].[Customer_Id],
        [MessageLog].[Reference],
        [MessageLog].[From],
        [MessageLog].[To],
        [MessageLog].[DateSent],
        [MessageLog].[MessageNumber_Id],
        [MessageLog].[_trackCreationUser],
        [MessageLog].[_trackLastWriteUser])
    VALUES (
        @Body,
        @Direction,
        @Status,
        @ErrorMessage,
        @Price,
        @Admin_Id,
        @Customer_Id,
        @Reference,
        @From,
        @To,
        @DateSent,
        @MessageNumber_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MessageLog].[_rowVersion], @Id AS 'Id' 
        FROM [MessageLog] 
        WHERE ([MessageLog].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [MessageLog] SET
 [MessageLog].[MessageNumber_Id] = NULL
    WHERE ([MessageLog].[MessageNumber_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [MessageNumber] FROM [MessageNumber] 
    WHERE (([MessageNumber].[Id] = @Id) AND ([MessageNumber].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageNumber_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_Save]
(
 @Id [int] = NULL,
 @PhoneNumber [nvarchar] (256) = NULL,
 @Location [nvarchar] (256) = NULL,
 @Reference [nvarchar] (256) = NULL,
 @MessageService_Id [int] = NULL,
 @IsDefault [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MessageNumber] SET
     [MessageNumber].[PhoneNumber] = @PhoneNumber,
     [MessageNumber].[Location] = @Location,
     [MessageNumber].[Reference] = @Reference,
     [MessageNumber].[MessageService_Id] = @MessageService_Id,
     [MessageNumber].[IsDefault] = @IsDefault,
     [MessageNumber].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MessageNumber].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MessageNumber].[Id] = @Id) AND ([MessageNumber].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageNumber_Save')
        RETURN
    END
    SELECT DISTINCT [MessageNumber].[_rowVersion], @Id AS 'Id' 
        FROM [MessageNumber] 
        WHERE ([MessageNumber].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MessageNumber] (
        [MessageNumber].[PhoneNumber],
        [MessageNumber].[Location],
        [MessageNumber].[Reference],
        [MessageNumber].[MessageService_Id],
        [MessageNumber].[IsDefault],
        [MessageNumber].[_trackCreationUser],
        [MessageNumber].[_trackLastWriteUser])
    VALUES (
        @PhoneNumber,
        @Location,
        @Reference,
        @MessageService_Id,
        @IsDefault,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MessageNumber].[_rowVersion], @Id AS 'Id' 
        FROM [MessageNumber] 
        WHERE ([MessageNumber].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [MessageNumber] SET
 [MessageNumber].[MessageService_Id] = NULL
    WHERE ([MessageNumber].[MessageService_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [MessageService] FROM [MessageService] 
    WHERE (([MessageService].[Id] = @Id) AND ([MessageService].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageService_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Reference [nvarchar] (256) = NULL,
 @MessageAccount_Id [int] = NULL,
 @AlphaId [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [MessageService] SET
     [MessageService].[Name] = @Name,
     [MessageService].[Reference] = @Reference,
     [MessageService].[MessageAccount_Id] = @MessageAccount_Id,
     [MessageService].[AlphaId] = @AlphaId,
     [MessageService].[_trackLastWriteUser] = @_trackLastWriteUser,
     [MessageService].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([MessageService].[Id] = @Id) AND ([MessageService].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'MessageService_Save')
        RETURN
    END
    SELECT DISTINCT [MessageService].[_rowVersion], @Id AS 'Id' 
        FROM [MessageService] 
        WHERE ([MessageService].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [MessageService] (
        [MessageService].[Name],
        [MessageService].[Reference],
        [MessageService].[MessageAccount_Id],
        [MessageService].[AlphaId],
        [MessageService].[_trackCreationUser],
        [MessageService].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Reference,
        @MessageAccount_Id,
        @AlphaId,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [MessageService].[_rowVersion], @Id AS 'Id' 
        FROM [MessageService] 
        WHERE ([MessageService].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Order_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Invoice] SET
 [Invoice].[Order_Id] = NULL
    WHERE ([Invoice].[Order_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [OrderItem] SET
 [OrderItem].[Order_Id] = NULL
    WHERE ([OrderItem].[Order_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Order] FROM [Order] 
    WHERE (([Order].[Id] = @Id) AND ([Order].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Order_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Order_Save]
(
 @Id [int] = NULL,
 @OrderStatus [int] = NULL,
 @Discount [decimal] (28, 13) = NULL,
 @Description [nvarchar] (256) = NULL,
 @Note [nvarchar] (256) = NULL,
 @OrderDateUTC [datetime] = NULL,
 @Subtotal [decimal] (28, 13) = NULL,
 @Tax [decimal] (28, 13) = NULL,
 @Total [decimal] (28, 13) = NULL,
 @Customer_Id [int] = NULL,
 @Admin_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Order] SET
     [Order].[OrderStatus] = @OrderStatus,
     [Order].[Discount] = @Discount,
     [Order].[Description] = @Description,
     [Order].[Note] = @Note,
     [Order].[OrderDateUTC] = @OrderDateUTC,
     [Order].[Subtotal] = @Subtotal,
     [Order].[Tax] = @Tax,
     [Order].[Total] = @Total,
     [Order].[Customer_Id] = @Customer_Id,
     [Order].[Admin_Id] = @Admin_Id,
     [Order].[Club_Id] = @Club_Id,
     [Order].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Order].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Order].[Id] = @Id) AND ([Order].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Order_Save')
        RETURN
    END
    SELECT DISTINCT [Order].[_rowVersion], @Id AS 'Id' 
        FROM [Order] 
        WHERE ([Order].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Order] (
        [Order].[OrderStatus],
        [Order].[Discount],
        [Order].[Description],
        [Order].[Note],
        [Order].[OrderDateUTC],
        [Order].[Subtotal],
        [Order].[Tax],
        [Order].[Total],
        [Order].[Customer_Id],
        [Order].[Admin_Id],
        [Order].[Club_Id],
        [Order].[_trackCreationUser],
        [Order].[_trackLastWriteUser])
    VALUES (
        @OrderStatus,
        @Discount,
        @Description,
        @Note,
        @OrderDateUTC,
        @Subtotal,
        @Tax,
        @Total,
        @Customer_Id,
        @Admin_Id,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Order].[_rowVersion], @Id AS 'Id' 
        FROM [Order] 
        WHERE ([Order].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [OrderItem_Transactions_Transaction] FROM [OrderItem_Transactions_Transaction] 
    WHERE ([OrderItem_Transactions_Transaction].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [OrderItemDetail] SET
 [OrderItemDetail].[OrderItem_Id] = NULL
    WHERE ([OrderItemDetail].[OrderItem_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Credit] SET
 [Credit].[Order_Id] = NULL
    WHERE ([Credit].[Order_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [SaleItem] SET
 [SaleItem].[Order_Id] = NULL
    WHERE ([SaleItem].[Order_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [OrderItem] FROM [OrderItem] 
    WHERE (([OrderItem].[Id] = @Id) AND ([OrderItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'OrderItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_Save]
(
 @Id [int] = NULL,
 @isPaid [bit] = NULL,
 @Quantity [int] = NULL,
 @Booking_Id [int] = NULL,
 @CreationDateUTC [datetime] = NULL,
 @Metadata [nvarchar] (256) = NULL,
 @Amount [decimal] (28, 13) = NULL,
 @Registration_Id [int] = NULL,
 @Membership_Id [int] = NULL,
 @Note [nvarchar] (256) = NULL,
 @ProductVariant_Id [int] = NULL,
 @AdjustType [int] = NULL,
 @AdjustAmount [decimal] (28, 13) = NULL,
 @Order_Id [int] = NULL,
 @OrderItemType [int] = NULL,
 @OrderItemStatus [int] = NULL,
 @Customer_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [OrderItem] SET
     [OrderItem].[isPaid] = @isPaid,
     [OrderItem].[Quantity] = @Quantity,
     [OrderItem].[Booking_Id] = @Booking_Id,
     [OrderItem].[CreationDateUTC] = @CreationDateUTC,
     [OrderItem].[Metadata] = @Metadata,
     [OrderItem].[Amount] = @Amount,
     [OrderItem].[Registration_Id] = @Registration_Id,
     [OrderItem].[Membership_Id] = @Membership_Id,
     [OrderItem].[Note] = @Note,
     [OrderItem].[ProductVariant_Id] = @ProductVariant_Id,
     [OrderItem].[AdjustType] = @AdjustType,
     [OrderItem].[AdjustAmount] = @AdjustAmount,
     [OrderItem].[Order_Id] = @Order_Id,
     [OrderItem].[OrderItemType] = @OrderItemType,
     [OrderItem].[OrderItemStatus] = @OrderItemStatus,
     [OrderItem].[Customer_Id] = @Customer_Id,
     [OrderItem].[Name] = @Name,
     [OrderItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [OrderItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([OrderItem].[Id] = @Id) AND ([OrderItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'OrderItem_Save')
        RETURN
    END
    SELECT DISTINCT [OrderItem].[_rowVersion], @Id AS 'Id' 
        FROM [OrderItem] 
        WHERE ([OrderItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [OrderItem] (
        [OrderItem].[isPaid],
        [OrderItem].[Quantity],
        [OrderItem].[Booking_Id],
        [OrderItem].[CreationDateUTC],
        [OrderItem].[Metadata],
        [OrderItem].[Amount],
        [OrderItem].[Registration_Id],
        [OrderItem].[Membership_Id],
        [OrderItem].[Note],
        [OrderItem].[ProductVariant_Id],
        [OrderItem].[AdjustType],
        [OrderItem].[AdjustAmount],
        [OrderItem].[Order_Id],
        [OrderItem].[OrderItemType],
        [OrderItem].[OrderItemStatus],
        [OrderItem].[Customer_Id],
        [OrderItem].[Name],
        [OrderItem].[_trackCreationUser],
        [OrderItem].[_trackLastWriteUser])
    VALUES (
        @isPaid,
        @Quantity,
        @Booking_Id,
        @CreationDateUTC,
        @Metadata,
        @Amount,
        @Registration_Id,
        @Membership_Id,
        @Note,
        @ProductVariant_Id,
        @AdjustType,
        @AdjustAmount,
        @Order_Id,
        @OrderItemType,
        @OrderItemStatus,
        @Customer_Id,
        @Name,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [OrderItem].[_rowVersion], @Id AS 'Id' 
        FROM [OrderItem] 
        WHERE ([OrderItem].[Id] = @Id)
END
IF(@Registration_Id IS NOT NULL)
BEGIN
    UPDATE [EventRegistration] SET
     [EventRegistration].[OrderItem_Id] = @Id
        WHERE ([EventRegistration].[Id] = @Registration_Id)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [OrderItemDetail] FROM [OrderItemDetail] 
    WHERE (([OrderItemDetail].[Id] = @Id) AND ([OrderItemDetail].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'OrderItemDetail_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @OrderItem_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [OrderItemDetail] SET
     [OrderItemDetail].[Name] = @Name,
     [OrderItemDetail].[Sequence] = @Sequence,
     [OrderItemDetail].[OrderItem_Id] = @OrderItem_Id,
     [OrderItemDetail].[_trackLastWriteUser] = @_trackLastWriteUser,
     [OrderItemDetail].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([OrderItemDetail].[Id] = @Id) AND ([OrderItemDetail].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'OrderItemDetail_Save')
        RETURN
    END
    SELECT DISTINCT [OrderItemDetail].[_rowVersion], @Id AS 'Id' 
        FROM [OrderItemDetail] 
        WHERE ([OrderItemDetail].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [OrderItemDetail] (
        [OrderItemDetail].[Name],
        [OrderItemDetail].[Sequence],
        [OrderItemDetail].[OrderItem_Id],
        [OrderItemDetail].[_trackCreationUser],
        [OrderItemDetail].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @OrderItem_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [OrderItemDetail].[_rowVersion], @Id AS 'Id' 
        FROM [OrderItemDetail] 
        WHERE ([OrderItemDetail].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] FROM [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] 
    WHERE ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Event_Levels_PlayerLevel] FROM [Event_Levels_PlayerLevel] 
    WHERE ([Event_Levels_PlayerLevel].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [EventBooking_Levels_PlayerLevel] FROM [EventBooking_Levels_PlayerLevel] 
    WHERE ([EventBooking_Levels_PlayerLevel].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [ClubPlayerLevel] SET
 [ClubPlayerLevel].[PlayerLevel_Id] = NULL
    WHERE ([ClubPlayerLevel].[PlayerLevel_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [PlayerLevel] FROM [PlayerLevel] 
    WHERE (([PlayerLevel].[Id] = @Id) AND ([PlayerLevel].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'PlayerLevel_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_DeleteEventBookingLevels]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [EventBooking_Levels_PlayerLevel] FROM [EventBooking_Levels_PlayerLevel] 
    WHERE (([EventBooking_Levels_PlayerLevel].[Id2] = @Id2) AND ([EventBooking_Levels_PlayerLevel].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_DeleteEventLevels]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Event_Levels_PlayerLevel] FROM [Event_Levels_PlayerLevel] 
    WHERE (([Event_Levels_PlayerLevel].[Id2] = @Id2) AND ([Event_Levels_PlayerLevel].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @Club_Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [PlayerLevel] SET
     [PlayerLevel].[Name] = @Name,
     [PlayerLevel].[Sequence] = @Sequence,
     [PlayerLevel].[Club_Id] = @Club_Id,
     [PlayerLevel].[Description] = @Description,
     [PlayerLevel].[_trackLastWriteUser] = @_trackLastWriteUser,
     [PlayerLevel].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([PlayerLevel].[Id] = @Id) AND ([PlayerLevel].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'PlayerLevel_Save')
        RETURN
    END
    SELECT DISTINCT [PlayerLevel].[_rowVersion], @Id AS 'Id' 
        FROM [PlayerLevel] 
        WHERE ([PlayerLevel].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [PlayerLevel] (
        [PlayerLevel].[Name],
        [PlayerLevel].[Sequence],
        [PlayerLevel].[Club_Id],
        [PlayerLevel].[Description],
        [PlayerLevel].[_trackCreationUser],
        [PlayerLevel].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @Club_Id,
        @Description,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [PlayerLevel].[_rowVersion], @Id AS 'Id' 
        FROM [PlayerLevel] 
        WHERE ([PlayerLevel].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_SaveEventBookingLevels]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [EventBooking_Levels_PlayerLevel].[Id] 
    FROM [EventBooking_Levels_PlayerLevel] 
    WHERE (([EventBooking_Levels_PlayerLevel].[Id2] = @Id2) AND ([EventBooking_Levels_PlayerLevel].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [EventBooking_Levels_PlayerLevel] (
        [EventBooking_Levels_PlayerLevel].[Id],
        [EventBooking_Levels_PlayerLevel].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_SaveEventLevels]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Event_Levels_PlayerLevel].[Id] 
    FROM [Event_Levels_PlayerLevel] 
    WHERE (([Event_Levels_PlayerLevel].[Id2] = @Id2) AND ([Event_Levels_PlayerLevel].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Event_Levels_PlayerLevel] (
        [Event_Levels_PlayerLevel].[Id],
        [Event_Levels_PlayerLevel].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] FROM [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] 
    WHERE ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [User] SET
 [User].[PlayerRating_Id] = NULL
    WHERE ([User].[PlayerRating_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [PlayerRating] FROM [PlayerRating] 
    WHERE (([PlayerRating].[Id] = @Id) AND ([PlayerRating].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'PlayerRating_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_DeletePlayerLevelPlayerRatings]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] FROM [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] 
    WHERE (([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2] = @Id2) AND ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [PlayerRating] SET
     [PlayerRating].[Name] = @Name,
     [PlayerRating].[Sequence] = @Sequence,
     [PlayerRating].[Description] = @Description,
     [PlayerRating].[_trackLastWriteUser] = @_trackLastWriteUser,
     [PlayerRating].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([PlayerRating].[Id] = @Id) AND ([PlayerRating].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'PlayerRating_Save')
        RETURN
    END
    SELECT DISTINCT [PlayerRating].[_rowVersion], @Id AS 'Id' 
        FROM [PlayerRating] 
        WHERE ([PlayerRating].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [PlayerRating] (
        [PlayerRating].[Name],
        [PlayerRating].[Sequence],
        [PlayerRating].[Description],
        [PlayerRating].[_trackCreationUser],
        [PlayerRating].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @Description,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [PlayerRating].[_rowVersion], @Id AS 'Id' 
        FROM [PlayerRating] 
        WHERE ([PlayerRating].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_SavePlayerLevelPlayerRatings]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] 
    FROM [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] 
    WHERE (([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2] = @Id2) AND ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] (
        [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id],
        [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Product_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [StocktakeItem] SET
 [StocktakeItem].[Product_Id] = NULL
    WHERE ([StocktakeItem].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [DeliveryItem] SET
 [DeliveryItem].[Product_Id] = NULL
    WHERE ([DeliveryItem].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EquipmentHireItem] SET
 [EquipmentHireItem].[Product_Id] = NULL
    WHERE ([EquipmentHireItem].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [CheckoutItem] SET
 [CheckoutItem].[Product_Id] = NULL
    WHERE ([CheckoutItem].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductVariant] SET
 [ProductVariant].[Product_Id] = NULL
    WHERE ([ProductVariant].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductImage] SET
 [ProductImage].[Product_Id] = NULL
    WHERE ([ProductImage].[Product_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Product] FROM [Product] 
    WHERE (([Product].[Id] = @Id) AND ([Product].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Product_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Product_Save]
(
 @Id [int] = NULL,
 @ProductType_Id [int] = NULL,
 @Description [nvarchar] (3840) = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @includeGST [bit] = NULL,
 @hasVariant [bit] = NULL,
 @Title [nvarchar] (256) = NULL,
 @PhotoUrl [nvarchar] (256) = NULL,
 @ProductStatus [int] = NULL,
 @ProductVendor_Id [int] = NULL,
 @Quantity [int] = NULL,
 @hasImages [bit] = NULL,
 @Reference [nvarchar] (256) = NULL,
 @Tags [nvarchar] (256) = NULL,
 @ProductOption_Id [int] = NULL,
 @VariantCount [int] = NULL,
 @hasMultiple [bit] = NULL,
 @ProductBrand_Id [int] = NULL,
 @ProductClass_Id [int] = NULL,
 @InventoryOption [int] = NULL,
 @hasDiscount [bit] = NULL,
 @hasMemberPrice [bit] = NULL,
 @Taxable [bit] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Product] SET
     [Product].[ProductType_Id] = @ProductType_Id,
     [Product].[Description] = @Description,
     [Product].[Name] = @Name,
     [Product].[Club_Id] = @Club_Id,
     [Product].[includeGST] = @includeGST,
     [Product].[hasVariant] = @hasVariant,
     [Product].[Title] = @Title,
     [Product].[PhotoUrl] = @PhotoUrl,
     [Product].[ProductStatus] = @ProductStatus,
     [Product].[ProductVendor_Id] = @ProductVendor_Id,
     [Product].[Quantity] = @Quantity,
     [Product].[hasImages] = @hasImages,
     [Product].[Reference] = @Reference,
     [Product].[Tags] = @Tags,
     [Product].[ProductOption_Id] = @ProductOption_Id,
     [Product].[VariantCount] = @VariantCount,
     [Product].[hasMultiple] = @hasMultiple,
     [Product].[ProductBrand_Id] = @ProductBrand_Id,
     [Product].[ProductClass_Id] = @ProductClass_Id,
     [Product].[InventoryOption] = @InventoryOption,
     [Product].[hasDiscount] = @hasDiscount,
     [Product].[hasMemberPrice] = @hasMemberPrice,
     [Product].[Taxable] = @Taxable,
     [Product].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Product].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Product].[Id] = @Id) AND ([Product].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Product_Save')
        RETURN
    END
    SELECT DISTINCT [Product].[_rowVersion], @Id AS 'Id' 
        FROM [Product] 
        WHERE ([Product].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Product] (
        [Product].[ProductType_Id],
        [Product].[Description],
        [Product].[Name],
        [Product].[Club_Id],
        [Product].[includeGST],
        [Product].[hasVariant],
        [Product].[Title],
        [Product].[PhotoUrl],
        [Product].[ProductStatus],
        [Product].[ProductVendor_Id],
        [Product].[Quantity],
        [Product].[hasImages],
        [Product].[Reference],
        [Product].[Tags],
        [Product].[ProductOption_Id],
        [Product].[VariantCount],
        [Product].[hasMultiple],
        [Product].[ProductBrand_Id],
        [Product].[ProductClass_Id],
        [Product].[InventoryOption],
        [Product].[hasDiscount],
        [Product].[hasMemberPrice],
        [Product].[Taxable],
        [Product].[_trackCreationUser],
        [Product].[_trackLastWriteUser])
    VALUES (
        @ProductType_Id,
        @Description,
        @Name,
        @Club_Id,
        @includeGST,
        @hasVariant,
        @Title,
        @PhotoUrl,
        @ProductStatus,
        @ProductVendor_Id,
        @Quantity,
        @hasImages,
        @Reference,
        @Tags,
        @ProductOption_Id,
        @VariantCount,
        @hasMultiple,
        @ProductBrand_Id,
        @ProductClass_Id,
        @InventoryOption,
        @hasDiscount,
        @hasMemberPrice,
        @Taxable,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Product].[_rowVersion], @Id AS 'Id' 
        FROM [Product] 
        WHERE ([Product].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Product] SET
 [Product].[ProductBrand_Id] = NULL
    WHERE ([Product].[ProductBrand_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductBrand] FROM [ProductBrand] 
    WHERE (([ProductBrand].[Id] = @Id) AND ([ProductBrand].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductBrand_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductBrand] SET
     [ProductBrand].[Name] = @Name,
     [ProductBrand].[Club_Id] = @Club_Id,
     [ProductBrand].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductBrand].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductBrand].[Id] = @Id) AND ([ProductBrand].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductBrand_Save')
        RETURN
    END
    SELECT DISTINCT [ProductBrand].[_rowVersion], @Id AS 'Id' 
        FROM [ProductBrand] 
        WHERE ([ProductBrand].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductBrand] (
        [ProductBrand].[Name],
        [ProductBrand].[Club_Id],
        [ProductBrand].[_trackCreationUser],
        [ProductBrand].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductBrand].[_rowVersion], @Id AS 'Id' 
        FROM [ProductBrand] 
        WHERE ([ProductBrand].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Product] SET
 [Product].[ProductClass_Id] = NULL
    WHERE ([Product].[ProductClass_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductClass] FROM [ProductClass] 
    WHERE (([ProductClass].[Id] = @Id) AND ([ProductClass].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductClass_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductClass] SET
     [ProductClass].[Name] = @Name,
     [ProductClass].[Club_Id] = @Club_Id,
     [ProductClass].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductClass].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductClass].[Id] = @Id) AND ([ProductClass].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductClass_Save')
        RETURN
    END
    SELECT DISTINCT [ProductClass].[_rowVersion], @Id AS 'Id' 
        FROM [ProductClass] 
        WHERE ([ProductClass].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductClass] (
        [ProductClass].[Name],
        [ProductClass].[Club_Id],
        [ProductClass].[_trackCreationUser],
        [ProductClass].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductClass].[_rowVersion], @Id AS 'Id' 
        FROM [ProductClass] 
        WHERE ([ProductClass].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [ProductImage] FROM [ProductImage] 
    WHERE (([ProductImage].[Id] = @Id) AND ([ProductImage].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductImage_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_Save]
(
 @Id [int] = NULL,
 @Reference [nvarchar] (256) = NULL,
 @Name [nvarchar] (256) = NULL,
 @Product_Id [int] = NULL,
 @Url [nvarchar] (256) = NULL,
 @isDefault [bit] = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductImage] SET
     [ProductImage].[Reference] = @Reference,
     [ProductImage].[Name] = @Name,
     [ProductImage].[Product_Id] = @Product_Id,
     [ProductImage].[Url] = @Url,
     [ProductImage].[isDefault] = @isDefault,
     [ProductImage].[Sequence] = @Sequence,
     [ProductImage].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductImage].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductImage].[Id] = @Id) AND ([ProductImage].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductImage_Save')
        RETURN
    END
    SELECT DISTINCT [ProductImage].[_rowVersion], @Id AS 'Id' 
        FROM [ProductImage] 
        WHERE ([ProductImage].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductImage] (
        [ProductImage].[Reference],
        [ProductImage].[Name],
        [ProductImage].[Product_Id],
        [ProductImage].[Url],
        [ProductImage].[isDefault],
        [ProductImage].[Sequence],
        [ProductImage].[_trackCreationUser],
        [ProductImage].[_trackLastWriteUser])
    VALUES (
        @Reference,
        @Name,
        @Product_Id,
        @Url,
        @isDefault,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductImage].[_rowVersion], @Id AS 'Id' 
        FROM [ProductImage] 
        WHERE ([ProductImage].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [ProductInventory] FROM [ProductInventory] 
    WHERE (([ProductInventory].[Id] = @Id) AND ([ProductInventory].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductInventory_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_Save]
(
 @Id [int] = NULL,
 @AdjustAmount [int] = NULL,
 @AdjustDateTimeUTC [datetime] = NULL,
 @InventoryAdjustType [int] = NULL,
 @ProductVariant_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductInventory] SET
     [ProductInventory].[AdjustAmount] = @AdjustAmount,
     [ProductInventory].[AdjustDateTimeUTC] = @AdjustDateTimeUTC,
     [ProductInventory].[InventoryAdjustType] = @InventoryAdjustType,
     [ProductInventory].[ProductVariant_Id] = @ProductVariant_Id,
     [ProductInventory].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductInventory].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductInventory].[Id] = @Id) AND ([ProductInventory].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductInventory_Save')
        RETURN
    END
    SELECT DISTINCT [ProductInventory].[_rowVersion], @Id AS 'Id' 
        FROM [ProductInventory] 
        WHERE ([ProductInventory].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductInventory] (
        [ProductInventory].[AdjustAmount],
        [ProductInventory].[AdjustDateTimeUTC],
        [ProductInventory].[InventoryAdjustType],
        [ProductInventory].[ProductVariant_Id],
        [ProductInventory].[_trackCreationUser],
        [ProductInventory].[_trackLastWriteUser])
    VALUES (
        @AdjustAmount,
        @AdjustDateTimeUTC,
        @InventoryAdjustType,
        @ProductVariant_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductInventory].[_rowVersion], @Id AS 'Id' 
        FROM [ProductInventory] 
        WHERE ([ProductInventory].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Product] SET
 [Product].[ProductOption_Id] = NULL
    WHERE ([Product].[ProductOption_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductOption] FROM [ProductOption] 
    WHERE (([ProductOption].[Id] = @Id) AND ([ProductOption].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductOption_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductOption] SET
     [ProductOption].[Name] = @Name,
     [ProductOption].[Club_Id] = @Club_Id,
     [ProductOption].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductOption].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductOption].[Id] = @Id) AND ([ProductOption].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductOption_Save')
        RETURN
    END
    SELECT DISTINCT [ProductOption].[_rowVersion], @Id AS 'Id' 
        FROM [ProductOption] 
        WHERE ([ProductOption].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductOption] (
        [ProductOption].[Name],
        [ProductOption].[Club_Id],
        [ProductOption].[_trackCreationUser],
        [ProductOption].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductOption].[_rowVersion], @Id AS 'Id' 
        FROM [ProductOption] 
        WHERE ([ProductOption].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Product] SET
 [Product].[ProductType_Id] = NULL
    WHERE ([Product].[ProductType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Size] SET
 [Size].[ProductType_Id] = NULL
    WHERE ([Size].[ProductType_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductType] FROM [ProductType] 
    WHERE (([ProductType].[Id] = @Id) AND ([ProductType].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductType_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductType] SET
     [ProductType].[Name] = @Name,
     [ProductType].[Sequence] = @Sequence,
     [ProductType].[Club_Id] = @Club_Id,
     [ProductType].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductType].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductType].[Id] = @Id) AND ([ProductType].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductType_Save')
        RETURN
    END
    SELECT DISTINCT [ProductType].[_rowVersion], @Id AS 'Id' 
        FROM [ProductType] 
        WHERE ([ProductType].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductType] (
        [ProductType].[Name],
        [ProductType].[Sequence],
        [ProductType].[Club_Id],
        [ProductType].[_trackCreationUser],
        [ProductType].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Sequence,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductType].[_rowVersion], @Id AS 'Id' 
        FROM [ProductType] 
        WHERE ([ProductType].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [OrderItem] SET
 [OrderItem].[ProductVariant_Id] = NULL
    WHERE ([OrderItem].[ProductVariant_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ProductInventory] SET
 [ProductInventory].[ProductVariant_Id] = NULL
    WHERE ([ProductInventory].[ProductVariant_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Discount] SET
 [Discount].[ProductVariant_Id] = NULL
    WHERE ([Discount].[ProductVariant_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductVariant] FROM [ProductVariant] 
    WHERE (([ProductVariant].[Id] = @Id) AND ([ProductVariant].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductVariant_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_Save]
(
 @Id [int] = NULL,
 @Product_Id [int] = NULL,
 @SKU [nvarchar] (256) = NULL,
 @Barcode [nvarchar] (256) = NULL,
 @Quantity [int] = NULL,
 @ProductOptionValue [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @RetailPrice [decimal] (28, 13) = NULL,
 @Taxable [bit] = NULL,
 @isDefault [bit] = NULL,
 @InventoryOption [int] = NULL,
 @Title [nvarchar] (256) = NULL,
 @MemberPrice [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductVariant] SET
     [ProductVariant].[Product_Id] = @Product_Id,
     [ProductVariant].[SKU] = @SKU,
     [ProductVariant].[Barcode] = @Barcode,
     [ProductVariant].[Quantity] = @Quantity,
     [ProductVariant].[ProductOptionValue] = @ProductOptionValue,
     [ProductVariant].[Sequence] = @Sequence,
     [ProductVariant].[RetailPrice] = @RetailPrice,
     [ProductVariant].[Taxable] = @Taxable,
     [ProductVariant].[isDefault] = @isDefault,
     [ProductVariant].[InventoryOption] = @InventoryOption,
     [ProductVariant].[Title] = @Title,
     [ProductVariant].[MemberPrice] = @MemberPrice,
     [ProductVariant].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductVariant].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductVariant].[Id] = @Id) AND ([ProductVariant].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductVariant_Save')
        RETURN
    END
    SELECT DISTINCT [ProductVariant].[_rowVersion], @Id AS 'Id' 
        FROM [ProductVariant] 
        WHERE ([ProductVariant].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductVariant] (
        [ProductVariant].[Product_Id],
        [ProductVariant].[SKU],
        [ProductVariant].[Barcode],
        [ProductVariant].[Quantity],
        [ProductVariant].[ProductOptionValue],
        [ProductVariant].[Sequence],
        [ProductVariant].[RetailPrice],
        [ProductVariant].[Taxable],
        [ProductVariant].[isDefault],
        [ProductVariant].[InventoryOption],
        [ProductVariant].[Title],
        [ProductVariant].[MemberPrice],
        [ProductVariant].[_trackCreationUser],
        [ProductVariant].[_trackLastWriteUser])
    VALUES (
        @Product_Id,
        @SKU,
        @Barcode,
        @Quantity,
        @ProductOptionValue,
        @Sequence,
        @RetailPrice,
        @Taxable,
        @isDefault,
        @InventoryOption,
        @Title,
        @MemberPrice,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductVariant].[_rowVersion], @Id AS 'Id' 
        FROM [ProductVariant] 
        WHERE ([ProductVariant].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Product] SET
 [Product].[ProductVendor_Id] = NULL
    WHERE ([Product].[ProductVendor_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [ProductVendor] FROM [ProductVendor] 
    WHERE (([ProductVendor].[Id] = @Id) AND ([ProductVendor].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductVendor_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [ProductVendor] SET
     [ProductVendor].[Name] = @Name,
     [ProductVendor].[Club_Id] = @Club_Id,
     [ProductVendor].[_trackLastWriteUser] = @_trackLastWriteUser,
     [ProductVendor].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([ProductVendor].[Id] = @Id) AND ([ProductVendor].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'ProductVendor_Save')
        RETURN
    END
    SELECT DISTINCT [ProductVendor].[_rowVersion], @Id AS 'Id' 
        FROM [ProductVendor] 
        WHERE ([ProductVendor].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [ProductVendor] (
        [ProductVendor].[Name],
        [ProductVendor].[Club_Id],
        [ProductVendor].[_trackCreationUser],
        [ProductVendor].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [ProductVendor].[_rowVersion], @Id AS 'Id' 
        FROM [ProductVendor] 
        WHERE ([ProductVendor].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Role_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Role_User_User_Roles] FROM [Role_User_User_Roles] 
    WHERE ([Role_User_User_Roles].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [RoleClaim] FROM [RoleClaim]
    INNER JOIN [Role] ON ([RoleClaim].[Role_Id] = [Role].[Id])
            LEFT OUTER JOIN [RoleClaim] [RoleClaim$1] ON ([Role].[Id] = [RoleClaim$1].[Role_Id]) 
    WHERE ([Role].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Role] FROM [Role] 
    WHERE (([Role].[Id] = @Id) AND ([Role].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Role_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Role_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256),
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Role] SET
     [Role].[Name] = @Name,
     [Role].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Role].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Role].[Id] = @Id) AND ([Role].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Role_Save')
        RETURN
    END
    SELECT DISTINCT [Role].[_rowVersion], @Id AS 'Id' 
        FROM [Role] 
        WHERE ([Role].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Role] (
        [Role].[Name],
        [Role].[_trackCreationUser],
        [Role].[_trackLastWriteUser])
    VALUES (
        @Name,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Role].[_rowVersion], @Id AS 'Id' 
        FROM [Role] 
        WHERE ([Role].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [RoleClaim] FROM [RoleClaim] 
    WHERE (([RoleClaim].[Id] = @Id) AND ([RoleClaim].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'RoleClaim_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_Save]
(
 @Id [int] = NULL,
 @Type [nvarchar] (256),
 @Value [nvarchar] (256) = NULL,
 @ValueType [nvarchar] (256) = NULL,
 @Role_Id [int],
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [RoleClaim] SET
     [RoleClaim].[Type] = @Type,
     [RoleClaim].[Value] = @Value,
     [RoleClaim].[ValueType] = @ValueType,
     [RoleClaim].[Role_Id] = @Role_Id,
     [RoleClaim].[_trackLastWriteUser] = @_trackLastWriteUser,
     [RoleClaim].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([RoleClaim].[Id] = @Id) AND ([RoleClaim].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'RoleClaim_Save')
        RETURN
    END
    SELECT DISTINCT [RoleClaim].[_rowVersion], @Id AS 'Id' 
        FROM [RoleClaim] 
        WHERE ([RoleClaim].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [RoleClaim] (
        [RoleClaim].[Type],
        [RoleClaim].[Value],
        [RoleClaim].[ValueType],
        [RoleClaim].[Role_Id],
        [RoleClaim].[_trackCreationUser],
        [RoleClaim].[_trackLastWriteUser])
    VALUES (
        @Type,
        @Value,
        @ValueType,
        @Role_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [RoleClaim].[_rowVersion], @Id AS 'Id' 
        FROM [RoleClaim] 
        WHERE ([RoleClaim].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [SaleItem] SET
 [SaleItem].[Sale_Id] = NULL
    WHERE ([SaleItem].[Sale_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Sale] FROM [Sale] 
    WHERE (([Sale].[Id] = @Id) AND ([Sale].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Sale_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_Save]
(
 @Id [int] = NULL,
 @Customer_Id [int] = NULL,
 @Admin_Id [int] = NULL,
 @Tax [decimal] (28, 13) = NULL,
 @SubTotal [decimal] (28, 13) = NULL,
 @TotalAmount [decimal] (28, 13) = NULL,
 @Note [nvarchar] (256) = NULL,
 @SaleStatus [int] = NULL,
 @SaleDateTimeUTC [datetime] = NULL,
 @Description [nvarchar] (256) = NULL,
 @Invoice_Id [int] = NULL,
 @DiscountType [int] = NULL,
 @Discount [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Sale] SET
     [Sale].[Customer_Id] = @Customer_Id,
     [Sale].[Admin_Id] = @Admin_Id,
     [Sale].[Tax] = @Tax,
     [Sale].[SubTotal] = @SubTotal,
     [Sale].[TotalAmount] = @TotalAmount,
     [Sale].[Note] = @Note,
     [Sale].[SaleStatus] = @SaleStatus,
     [Sale].[SaleDateTimeUTC] = @SaleDateTimeUTC,
     [Sale].[Description] = @Description,
     [Sale].[Invoice_Id] = @Invoice_Id,
     [Sale].[DiscountType] = @DiscountType,
     [Sale].[Discount] = @Discount,
     [Sale].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Sale].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Sale].[Id] = @Id) AND ([Sale].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Sale_Save')
        RETURN
    END
    SELECT DISTINCT [Sale].[_rowVersion], @Id AS 'Id' 
        FROM [Sale] 
        WHERE ([Sale].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Sale] (
        [Sale].[Customer_Id],
        [Sale].[Admin_Id],
        [Sale].[Tax],
        [Sale].[SubTotal],
        [Sale].[TotalAmount],
        [Sale].[Note],
        [Sale].[SaleStatus],
        [Sale].[SaleDateTimeUTC],
        [Sale].[Description],
        [Sale].[Invoice_Id],
        [Sale].[DiscountType],
        [Sale].[Discount],
        [Sale].[_trackCreationUser],
        [Sale].[_trackLastWriteUser])
    VALUES (
        @Customer_Id,
        @Admin_Id,
        @Tax,
        @SubTotal,
        @TotalAmount,
        @Note,
        @SaleStatus,
        @SaleDateTimeUTC,
        @Description,
        @Invoice_Id,
        @DiscountType,
        @Discount,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Sale].[_rowVersion], @Id AS 'Id' 
        FROM [Sale] 
        WHERE ([Sale].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [SaleItem] FROM [SaleItem] 
    WHERE (([SaleItem].[Id] = @Id) AND ([SaleItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'SaleItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_Save]
(
 @Id [int] = NULL,
 @Sale_Id [int] = NULL,
 @Order_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [SaleItem] SET
     [SaleItem].[Sale_Id] = @Sale_Id,
     [SaleItem].[Order_Id] = @Order_Id,
     [SaleItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [SaleItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([SaleItem].[Id] = @Id) AND ([SaleItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'SaleItem_Save')
        RETURN
    END
    SELECT DISTINCT [SaleItem].[_rowVersion], @Id AS 'Id' 
        FROM [SaleItem] 
        WHERE ([SaleItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [SaleItem] (
        [SaleItem].[Sale_Id],
        [SaleItem].[Order_Id],
        [SaleItem].[_trackCreationUser],
        [SaleItem].[_trackLastWriteUser])
    VALUES (
        @Sale_Id,
        @Order_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [SaleItem].[_rowVersion], @Id AS 'Id' 
        FROM [SaleItem] 
        WHERE ([SaleItem].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Size_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Size] FROM [Size] 
    WHERE (([Size].[Id] = @Id) AND ([Size].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Size_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Size_Save]
(
 @Id [int] = NULL,
 @Value [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @ProductType_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Size] SET
     [Size].[Value] = @Value,
     [Size].[Sequence] = @Sequence,
     [Size].[ProductType_Id] = @ProductType_Id,
     [Size].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Size].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Size].[Id] = @Id) AND ([Size].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Size_Save')
        RETURN
    END
    SELECT DISTINCT [Size].[_rowVersion], @Id AS 'Id' 
        FROM [Size] 
        WHERE ([Size].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Size] (
        [Size].[Value],
        [Size].[Sequence],
        [Size].[ProductType_Id],
        [Size].[_trackCreationUser],
        [Size].[_trackLastWriteUser])
    VALUES (
        @Value,
        @Sequence,
        @ProductType_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Size].[_rowVersion], @Id AS 'Id' 
        FROM [Size] 
        WHERE ([Size].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Source_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Source] FROM [Source] 
    WHERE (([Source].[Id] = @Id) AND ([Source].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Source_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Source_Save]
(
 @Id [int] = NULL,
 @Customer_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Source] SET
     [Source].[Customer_Id] = @Customer_Id,
     [Source].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Source].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Source].[Id] = @Id) AND ([Source].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Source_Save')
        RETURN
    END
    SELECT DISTINCT [Source].[_rowVersion], @Id AS 'Id' 
        FROM [Source] 
        WHERE ([Source].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Source] (
        [Source].[Customer_Id],
        [Source].[_trackCreationUser],
        [Source].[_trackLastWriteUser])
    VALUES (
        @Customer_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Source].[_rowVersion], @Id AS 'Id' 
        FROM [Source] 
        WHERE ([Source].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [StocktakeItem] SET
 [StocktakeItem].[Stocktake_Id] = NULL
    WHERE ([StocktakeItem].[Stocktake_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Stocktake] FROM [Stocktake] 
    WHERE (([Stocktake].[Id] = @Id) AND ([Stocktake].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Stocktake_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_Save]
(
 @Id [int] = NULL,
 @Start [datetime] = NULL,
 @End [datetime] = NULL,
 @NumberOfItems [int] = NULL,
 @User_Id [int] = NULL,
 @Facility_Id [int] = NULL,
 @PreviousCount [decimal] (28, 13) = NULL,
 @PreviousValuation [decimal] (28, 13) = NULL,
 @CurrentValuation [decimal] (28, 13) = NULL,
 @State [int] = NULL,
 @CountResult [decimal] (28, 13) = NULL,
 @CurrentCount [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Stocktake] SET
     [Stocktake].[Start] = @Start,
     [Stocktake].[End] = @End,
     [Stocktake].[NumberOfItems] = @NumberOfItems,
     [Stocktake].[User_Id] = @User_Id,
     [Stocktake].[Facility_Id] = @Facility_Id,
     [Stocktake].[PreviousCount] = @PreviousCount,
     [Stocktake].[PreviousValuation] = @PreviousValuation,
     [Stocktake].[CurrentValuation] = @CurrentValuation,
     [Stocktake].[State] = @State,
     [Stocktake].[CountResult] = @CountResult,
     [Stocktake].[CurrentCount] = @CurrentCount,
     [Stocktake].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Stocktake].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Stocktake].[Id] = @Id) AND ([Stocktake].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Stocktake_Save')
        RETURN
    END
    SELECT DISTINCT [Stocktake].[_rowVersion], @Id AS 'Id' 
        FROM [Stocktake] 
        WHERE ([Stocktake].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Stocktake] (
        [Stocktake].[Start],
        [Stocktake].[End],
        [Stocktake].[NumberOfItems],
        [Stocktake].[User_Id],
        [Stocktake].[Facility_Id],
        [Stocktake].[PreviousCount],
        [Stocktake].[PreviousValuation],
        [Stocktake].[CurrentValuation],
        [Stocktake].[State],
        [Stocktake].[CountResult],
        [Stocktake].[CurrentCount],
        [Stocktake].[_trackCreationUser],
        [Stocktake].[_trackLastWriteUser])
    VALUES (
        @Start,
        @End,
        @NumberOfItems,
        @User_Id,
        @Facility_Id,
        @PreviousCount,
        @PreviousValuation,
        @CurrentValuation,
        @State,
        @CountResult,
        @CurrentCount,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Stocktake].[_rowVersion], @Id AS 'Id' 
        FROM [Stocktake] 
        WHERE ([Stocktake].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [StocktakeItem] FROM [StocktakeItem] 
    WHERE (([StocktakeItem].[Id] = @Id) AND ([StocktakeItem].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'StocktakeItem_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_Save]
(
 @Id [int] = NULL,
 @State [int] = NULL,
 @Stocktake_Id [int] = NULL,
 @Product_Id [int] = NULL,
 @Comment [nvarchar] (256) = NULL,
 @PreviousCount [decimal] (28, 13) = NULL,
 @Result [decimal] (28, 13) = NULL,
 @CurrentCount [decimal] (28, 13) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [StocktakeItem] SET
     [StocktakeItem].[State] = @State,
     [StocktakeItem].[Stocktake_Id] = @Stocktake_Id,
     [StocktakeItem].[Product_Id] = @Product_Id,
     [StocktakeItem].[Comment] = @Comment,
     [StocktakeItem].[PreviousCount] = @PreviousCount,
     [StocktakeItem].[Result] = @Result,
     [StocktakeItem].[CurrentCount] = @CurrentCount,
     [StocktakeItem].[_trackLastWriteUser] = @_trackLastWriteUser,
     [StocktakeItem].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([StocktakeItem].[Id] = @Id) AND ([StocktakeItem].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'StocktakeItem_Save')
        RETURN
    END
    SELECT DISTINCT [StocktakeItem].[_rowVersion], @Id AS 'Id' 
        FROM [StocktakeItem] 
        WHERE ([StocktakeItem].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [StocktakeItem] (
        [StocktakeItem].[State],
        [StocktakeItem].[Stocktake_Id],
        [StocktakeItem].[Product_Id],
        [StocktakeItem].[Comment],
        [StocktakeItem].[PreviousCount],
        [StocktakeItem].[Result],
        [StocktakeItem].[CurrentCount],
        [StocktakeItem].[_trackCreationUser],
        [StocktakeItem].[_trackLastWriteUser])
    VALUES (
        @State,
        @Stocktake_Id,
        @Product_Id,
        @Comment,
        @PreviousCount,
        @Result,
        @CurrentCount,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [StocktakeItem].[_rowVersion], @Id AS 'Id' 
        FROM [StocktakeItem] 
        WHERE ([StocktakeItem].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [SupplierContact] SET
 [SupplierContact].[Supplier_Id] = NULL
    WHERE ([SupplierContact].[Supplier_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Delivery] SET
 [Delivery].[Supplier_Id] = NULL
    WHERE ([Delivery].[Supplier_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Supplier] FROM [Supplier] 
    WHERE (([Supplier].[Id] = @Id) AND ([Supplier].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Supplier_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Email [nvarchar] (256) = NULL,
 @Phone [nvarchar] (256) = NULL,
 @Website [nvarchar] (256) = NULL,
 @ReferenceNo [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Supplier] SET
     [Supplier].[Name] = @Name,
     [Supplier].[Email] = @Email,
     [Supplier].[Phone] = @Phone,
     [Supplier].[Website] = @Website,
     [Supplier].[ReferenceNo] = @ReferenceNo,
     [Supplier].[Club_Id] = @Club_Id,
     [Supplier].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Supplier].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Supplier].[Id] = @Id) AND ([Supplier].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Supplier_Save')
        RETURN
    END
    SELECT DISTINCT [Supplier].[_rowVersion], @Id AS 'Id' 
        FROM [Supplier] 
        WHERE ([Supplier].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Supplier] (
        [Supplier].[Name],
        [Supplier].[Email],
        [Supplier].[Phone],
        [Supplier].[Website],
        [Supplier].[ReferenceNo],
        [Supplier].[Club_Id],
        [Supplier].[_trackCreationUser],
        [Supplier].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Email,
        @Phone,
        @Website,
        @ReferenceNo,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Supplier].[_rowVersion], @Id AS 'Id' 
        FROM [Supplier] 
        WHERE ([Supplier].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [SupplierContact] FROM [SupplierContact] 
    WHERE (([SupplierContact].[Id] = @Id) AND ([SupplierContact].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'SupplierContact_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_Save]
(
 @Id [int] = NULL,
 @Supplier_Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Position [nvarchar] (256) = NULL,
 @Phone [nvarchar] (256) = NULL,
 @Email [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [SupplierContact] SET
     [SupplierContact].[Supplier_Id] = @Supplier_Id,
     [SupplierContact].[Name] = @Name,
     [SupplierContact].[Position] = @Position,
     [SupplierContact].[Phone] = @Phone,
     [SupplierContact].[Email] = @Email,
     [SupplierContact].[_trackLastWriteUser] = @_trackLastWriteUser,
     [SupplierContact].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([SupplierContact].[Id] = @Id) AND ([SupplierContact].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'SupplierContact_Save')
        RETURN
    END
    SELECT DISTINCT [SupplierContact].[_rowVersion], @Id AS 'Id' 
        FROM [SupplierContact] 
        WHERE ([SupplierContact].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [SupplierContact] (
        [SupplierContact].[Supplier_Id],
        [SupplierContact].[Name],
        [SupplierContact].[Position],
        [SupplierContact].[Phone],
        [SupplierContact].[Email],
        [SupplierContact].[_trackCreationUser],
        [SupplierContact].[_trackLastWriteUser])
    VALUES (
        @Supplier_Id,
        @Name,
        @Position,
        @Phone,
        @Email,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [SupplierContact].[_rowVersion], @Id AS 'Id' 
        FROM [SupplierContact] 
        WHERE ([SupplierContact].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Surface_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Court] SET
 [Court].[Surface_Id] = NULL
    WHERE ([Court].[Surface_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Surface] FROM [Surface] 
    WHERE (([Surface].[Id] = @Id) AND ([Surface].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Surface_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Surface_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Description [nvarchar] (256) = NULL,
 @Sequence [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Surface] SET
     [Surface].[Name] = @Name,
     [Surface].[Description] = @Description,
     [Surface].[Sequence] = @Sequence,
     [Surface].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Surface].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Surface].[Id] = @Id) AND ([Surface].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Surface_Save')
        RETURN
    END
    SELECT DISTINCT [Surface].[_rowVersion], @Id AS 'Id' 
        FROM [Surface] 
        WHERE ([Surface].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Surface] (
        [Surface].[Name],
        [Surface].[Description],
        [Surface].[Sequence],
        [Surface].[_trackCreationUser],
        [Surface].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Description,
        @Sequence,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Surface].[_rowVersion], @Id AS 'Id' 
        FROM [Surface] 
        WHERE ([Surface].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Term_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Term] FROM [Term] 
    WHERE (([Term].[Id] = @Id) AND ([Term].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Term_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Term_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @StartDateUTC [datetime] = NULL,
 @EndDateUTC [datetime] = NULL,
 @isActive [bit] = NULL,
 @Club_Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Term] SET
     [Term].[Name] = @Name,
     [Term].[StartDateUTC] = @StartDateUTC,
     [Term].[EndDateUTC] = @EndDateUTC,
     [Term].[isActive] = @isActive,
     [Term].[Club_Id] = @Club_Id,
     [Term].[Description] = @Description,
     [Term].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Term].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Term].[Id] = @Id) AND ([Term].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Term_Save')
        RETURN
    END
    SELECT DISTINCT [Term].[_rowVersion], @Id AS 'Id' 
        FROM [Term] 
        WHERE ([Term].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Term] (
        [Term].[Name],
        [Term].[StartDateUTC],
        [Term].[EndDateUTC],
        [Term].[isActive],
        [Term].[Club_Id],
        [Term].[Description],
        [Term].[_trackCreationUser],
        [Term].[_trackLastWriteUser])
    VALUES (
        @Name,
        @StartDateUTC,
        @EndDateUTC,
        @isActive,
        @Club_Id,
        @Description,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Term].[_rowVersion], @Id AS 'Id' 
        FROM [Term] 
        WHERE ([Term].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [CourtHireRate] SET
 [CourtHireRate].[TimePeriod_Id] = NULL
    WHERE ([CourtHireRate].[TimePeriod_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [TimePeriod] FROM [TimePeriod] 
    WHERE (([TimePeriod].[Id] = @Id) AND ([TimePeriod].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'TimePeriod_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Start [datetime] = NULL,
 @End [datetime] = NULL,
 @Sequence [int] = NULL,
 @Club_Id [int] = NULL,
 @TimePeriodGroup_Id [int] = NULL,
 @StartTime [time] = NULL,
 @EndTime [time] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [TimePeriod] SET
     [TimePeriod].[Name] = @Name,
     [TimePeriod].[Start] = @Start,
     [TimePeriod].[End] = @End,
     [TimePeriod].[Sequence] = @Sequence,
     [TimePeriod].[Club_Id] = @Club_Id,
     [TimePeriod].[TimePeriodGroup_Id] = @TimePeriodGroup_Id,
     [TimePeriod].[StartTime] = @StartTime,
     [TimePeriod].[EndTime] = @EndTime,
     [TimePeriod].[_trackLastWriteUser] = @_trackLastWriteUser,
     [TimePeriod].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([TimePeriod].[Id] = @Id) AND ([TimePeriod].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'TimePeriod_Save')
        RETURN
    END
    SELECT DISTINCT [TimePeriod].[_rowVersion], @Id AS 'Id' 
        FROM [TimePeriod] 
        WHERE ([TimePeriod].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [TimePeriod] (
        [TimePeriod].[Name],
        [TimePeriod].[Start],
        [TimePeriod].[End],
        [TimePeriod].[Sequence],
        [TimePeriod].[Club_Id],
        [TimePeriod].[TimePeriodGroup_Id],
        [TimePeriod].[StartTime],
        [TimePeriod].[EndTime],
        [TimePeriod].[_trackCreationUser],
        [TimePeriod].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Start,
        @End,
        @Sequence,
        @Club_Id,
        @TimePeriodGroup_Id,
        @StartTime,
        @EndTime,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [TimePeriod].[_rowVersion], @Id AS 'Id' 
        FROM [TimePeriod] 
        WHERE ([TimePeriod].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [TimePeriod] SET
 [TimePeriod].[TimePeriodGroup_Id] = NULL
    WHERE ([TimePeriod].[TimePeriodGroup_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [MembershipTypeTimePeriod] SET
 [MembershipTypeTimePeriod].[TimePeriodGroup_Id] = NULL
    WHERE ([MembershipTypeTimePeriod].[TimePeriodGroup_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [TimePeriodGroup] FROM [TimePeriodGroup] 
    WHERE (([TimePeriodGroup].[Id] = @Id) AND ([TimePeriodGroup].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'TimePeriodGroup_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @isDefault [bit] = NULL,
 @Description [nvarchar] (256) = NULL,
 @Club_Id [int] = NULL,
 @NumberOfTimePeriods [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [TimePeriodGroup] SET
     [TimePeriodGroup].[Name] = @Name,
     [TimePeriodGroup].[isDefault] = @isDefault,
     [TimePeriodGroup].[Description] = @Description,
     [TimePeriodGroup].[Club_Id] = @Club_Id,
     [TimePeriodGroup].[NumberOfTimePeriods] = @NumberOfTimePeriods,
     [TimePeriodGroup].[_trackLastWriteUser] = @_trackLastWriteUser,
     [TimePeriodGroup].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([TimePeriodGroup].[Id] = @Id) AND ([TimePeriodGroup].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'TimePeriodGroup_Save')
        RETURN
    END
    SELECT DISTINCT [TimePeriodGroup].[_rowVersion], @Id AS 'Id' 
        FROM [TimePeriodGroup] 
        WHERE ([TimePeriodGroup].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [TimePeriodGroup] (
        [TimePeriodGroup].[Name],
        [TimePeriodGroup].[isDefault],
        [TimePeriodGroup].[Description],
        [TimePeriodGroup].[Club_Id],
        [TimePeriodGroup].[NumberOfTimePeriods],
        [TimePeriodGroup].[_trackCreationUser],
        [TimePeriodGroup].[_trackLastWriteUser])
    VALUES (
        @Name,
        @isDefault,
        @Description,
        @Club_Id,
        @NumberOfTimePeriods,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [TimePeriodGroup].[_rowVersion], @Id AS 'Id' 
        FROM [TimePeriodGroup] 
        WHERE ([TimePeriodGroup].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [OrderItem_Transactions_Transaction] FROM [OrderItem_Transactions_Transaction] 
    WHERE ([OrderItem_Transactions_Transaction].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [Transaction] FROM [Transaction] 
    WHERE (([Transaction].[Id] = @Id) AND ([Transaction].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Transaction_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_DeleteOrderItemTransactions]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [OrderItem_Transactions_Transaction] FROM [OrderItem_Transactions_Transaction] 
    WHERE (([OrderItem_Transactions_Transaction].[Id2] = @Id2) AND ([OrderItem_Transactions_Transaction].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_Save]
(
 @Id [int] = NULL,
 @Account_Id [int] = NULL,
 @Currency_Id [int] = NULL,
 @Journal_Id [int] = NULL,
 @Amount [decimal] (28, 13) = NULL,
 @AccountingPeriod_Id [int] = NULL,
 @Description [nvarchar] (256) = NULL,
 @TransactionStatus [int] = NULL,
 @SourceType [int] = NULL,
 @Invoice_Id [int] = NULL,
 @Club_Id [int] = NULL,
 @DateCreatedUTC [datetime] = NULL,
 @TransactionType [int] = NULL,
 @DestinationType [int] = NULL,
 @Metadata [nvarchar] (256) = NULL,
 @Balance [decimal] (28, 13) = NULL,
 @Reference [nvarchar] (256) = NULL,
 @Source_Id [int] = NULL,
 @Destination_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Transaction] SET
     [Transaction].[Account_Id] = @Account_Id,
     [Transaction].[Currency_Id] = @Currency_Id,
     [Transaction].[Journal_Id] = @Journal_Id,
     [Transaction].[Amount] = @Amount,
     [Transaction].[AccountingPeriod_Id] = @AccountingPeriod_Id,
     [Transaction].[Description] = @Description,
     [Transaction].[TransactionStatus] = @TransactionStatus,
     [Transaction].[SourceType] = @SourceType,
     [Transaction].[Invoice_Id] = @Invoice_Id,
     [Transaction].[Club_Id] = @Club_Id,
     [Transaction].[DateCreatedUTC] = @DateCreatedUTC,
     [Transaction].[TransactionType] = @TransactionType,
     [Transaction].[DestinationType] = @DestinationType,
     [Transaction].[Metadata] = @Metadata,
     [Transaction].[Balance] = @Balance,
     [Transaction].[Reference] = @Reference,
     [Transaction].[Source_Id] = @Source_Id,
     [Transaction].[Destination_Id] = @Destination_Id,
     [Transaction].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Transaction].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Transaction].[Id] = @Id) AND ([Transaction].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Transaction_Save')
        RETURN
    END
    SELECT DISTINCT [Transaction].[_rowVersion], @Id AS 'Id' 
        FROM [Transaction] 
        WHERE ([Transaction].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Transaction] (
        [Transaction].[Account_Id],
        [Transaction].[Currency_Id],
        [Transaction].[Journal_Id],
        [Transaction].[Amount],
        [Transaction].[AccountingPeriod_Id],
        [Transaction].[Description],
        [Transaction].[TransactionStatus],
        [Transaction].[SourceType],
        [Transaction].[Invoice_Id],
        [Transaction].[Club_Id],
        [Transaction].[DateCreatedUTC],
        [Transaction].[TransactionType],
        [Transaction].[DestinationType],
        [Transaction].[Metadata],
        [Transaction].[Balance],
        [Transaction].[Reference],
        [Transaction].[Source_Id],
        [Transaction].[Destination_Id],
        [Transaction].[_trackCreationUser],
        [Transaction].[_trackLastWriteUser])
    VALUES (
        @Account_Id,
        @Currency_Id,
        @Journal_Id,
        @Amount,
        @AccountingPeriod_Id,
        @Description,
        @TransactionStatus,
        @SourceType,
        @Invoice_Id,
        @Club_Id,
        @DateCreatedUTC,
        @TransactionType,
        @DestinationType,
        @Metadata,
        @Balance,
        @Reference,
        @Source_Id,
        @Destination_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Transaction].[_rowVersion], @Id AS 'Id' 
        FROM [Transaction] 
        WHERE ([Transaction].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_SaveOrderItemTransactions]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [OrderItem_Transactions_Transaction].[Id] 
    FROM [OrderItem_Transactions_Transaction] 
    WHERE (([OrderItem_Transactions_Transaction].[Id2] = @Id2) AND ([OrderItem_Transactions_Transaction].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [OrderItem_Transactions_Transaction] (
        [OrderItem_Transactions_Transaction].[Id],
        [OrderItem_Transactions_Transaction].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[User_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Role_User_User_Roles] FROM [Role_User_User_Roles] 
    WHERE ([Role_User_User_Roles].[Id2] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
UPDATE [Booking] SET
 [Booking].[User_Id] = NULL
    WHERE ([Booking].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Stocktake] SET
 [Stocktake].[User_Id] = NULL
    WHERE ([Stocktake].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Delivery] SET
 [Delivery].[User_Id] = NULL
    WHERE ([Delivery].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [EquipmentHire] SET
 [EquipmentHire].[User_Id] = NULL
    WHERE ([EquipmentHire].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [ClubAdmin] SET
 [ClubAdmin].[User_Id] = NULL
    WHERE ([ClubAdmin].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Customer] SET
 [Customer].[User_Id] = NULL
    WHERE ([Customer].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Admin] SET
 [Admin].[User_Id] = NULL
    WHERE ([Admin].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [Coach] SET
 [Coach].[User_Id] = NULL
    WHERE ([Coach].[User_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [UserClaim] FROM [UserClaim]
    INNER JOIN [User] ON ([UserClaim].[User_Id] = [User].[Id])
            LEFT OUTER JOIN [UserClaim] [UserClaim$1] ON ([User].[Id] = [UserClaim$1].[User_Id]) 
    WHERE ([User].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [UserLogin] FROM [UserLogin]
    INNER JOIN [User] ON ([UserLogin].[User_Id] = [User].[Id]) 
    WHERE ([User].[Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
DELETE [User] FROM [User] 
    WHERE (([User].[Id] = @Id) AND ([User].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'User_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[User_DeleteRoleUser]
(
 @Id [int] = NULL,
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [Role_User_User_Roles] FROM [Role_User_User_Roles] 
    WHERE (([Role_User_User_Roles].[Id2] = @Id2) AND ([Role_User_User_Roles].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[User_Save]
(
 @Id [int] = NULL,
 @UserName [nvarchar] (256),
 @CreationDateUTC [datetime],
 @Email [nvarchar] (256) = NULL,
 @EmailConfirmed [bit],
 @PhoneNumber [nvarchar] (256) = NULL,
 @PhoneNumberConfirmed [bit],
 @Password [nvarchar] (256) = NULL,
 @LastPasswordChangeDate [datetime] = NULL,
 @AccessFailedCount [int],
 @AccessFailedWindowStart [datetime] = NULL,
 @LockoutEnabled [bit],
 @LockoutEndDateUtc [datetime] = NULL,
 @LastProfileUpdateDate [datetime] = NULL,
 @SecurityStamp [nvarchar] (256),
 @TwoFactorEnabled [bit],
 @FirstName [nvarchar] (256) = NULL,
 @LastName [nvarchar] (256) = NULL,
 @Gender_Id [int] = NULL,
 @AddressLine1 [nvarchar] (256) = NULL,
 @AddressLine2 [nvarchar] (256) = NULL,
 @Suburb [nvarchar] (256) = NULL,
 @State [nvarchar] (256) = NULL,
 @Postcode [nvarchar] (256) = NULL,
 @TennisId [nvarchar] (256) = NULL,
 @DateOfBirth [datetime] = NULL,
 @Country [nvarchar] (256) = NULL,
 @FullName [nvarchar] (256) = NULL,
 @PlayerRating_Id [int] = NULL,
 @Pin [nvarchar] (256) = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [User] SET
     [User].[UserName] = @UserName,
     [User].[CreationDateUTC] = @CreationDateUTC,
     [User].[Email] = @Email,
     [User].[EmailConfirmed] = @EmailConfirmed,
     [User].[PhoneNumber] = @PhoneNumber,
     [User].[PhoneNumberConfirmed] = @PhoneNumberConfirmed,
     [User].[Password] = @Password,
     [User].[LastPasswordChangeDate] = @LastPasswordChangeDate,
     [User].[AccessFailedCount] = @AccessFailedCount,
     [User].[AccessFailedWindowStart] = @AccessFailedWindowStart,
     [User].[LockoutEnabled] = @LockoutEnabled,
     [User].[LockoutEndDateUtc] = @LockoutEndDateUtc,
     [User].[LastProfileUpdateDate] = @LastProfileUpdateDate,
     [User].[SecurityStamp] = @SecurityStamp,
     [User].[TwoFactorEnabled] = @TwoFactorEnabled,
     [User].[FirstName] = @FirstName,
     [User].[LastName] = @LastName,
     [User].[Gender_Id] = @Gender_Id,
     [User].[AddressLine1] = @AddressLine1,
     [User].[AddressLine2] = @AddressLine2,
     [User].[Suburb] = @Suburb,
     [User].[State] = @State,
     [User].[Postcode] = @Postcode,
     [User].[TennisId] = @TennisId,
     [User].[DateOfBirth] = @DateOfBirth,
     [User].[Country] = @Country,
     [User].[FullName] = @FullName,
     [User].[PlayerRating_Id] = @PlayerRating_Id,
     [User].[Pin] = @Pin,
     [User].[_trackLastWriteUser] = @_trackLastWriteUser,
     [User].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([User].[Id] = @Id) AND ([User].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'User_Save')
        RETURN
    END
    SELECT DISTINCT [User].[_rowVersion], @Id AS 'Id' 
        FROM [User] 
        WHERE ([User].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [User] (
        [User].[UserName],
        [User].[CreationDateUTC],
        [User].[Email],
        [User].[EmailConfirmed],
        [User].[PhoneNumber],
        [User].[PhoneNumberConfirmed],
        [User].[Password],
        [User].[LastPasswordChangeDate],
        [User].[AccessFailedCount],
        [User].[AccessFailedWindowStart],
        [User].[LockoutEnabled],
        [User].[LockoutEndDateUtc],
        [User].[LastProfileUpdateDate],
        [User].[SecurityStamp],
        [User].[TwoFactorEnabled],
        [User].[FirstName],
        [User].[LastName],
        [User].[Gender_Id],
        [User].[AddressLine1],
        [User].[AddressLine2],
        [User].[Suburb],
        [User].[State],
        [User].[Postcode],
        [User].[TennisId],
        [User].[DateOfBirth],
        [User].[Country],
        [User].[FullName],
        [User].[PlayerRating_Id],
        [User].[Pin],
        [User].[_trackCreationUser],
        [User].[_trackLastWriteUser])
    VALUES (
        @UserName,
        @CreationDateUTC,
        @Email,
        @EmailConfirmed,
        @PhoneNumber,
        @PhoneNumberConfirmed,
        @Password,
        @LastPasswordChangeDate,
        @AccessFailedCount,
        @AccessFailedWindowStart,
        @LockoutEnabled,
        @LockoutEndDateUtc,
        @LastProfileUpdateDate,
        @SecurityStamp,
        @TwoFactorEnabled,
        @FirstName,
        @LastName,
        @Gender_Id,
        @AddressLine1,
        @AddressLine2,
        @Suburb,
        @State,
        @Postcode,
        @TennisId,
        @DateOfBirth,
        @Country,
        @FullName,
        @PlayerRating_Id,
        @Pin,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [User].[_rowVersion], @Id AS 'Id' 
        FROM [User] 
        WHERE ([User].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[User_SaveRoleUser]
(
 @Id [int],
 @Id2 [int]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
SELECT DISTINCT TOP 1 [Role_User_User_Roles].[Id] 
    FROM [Role_User_User_Roles] 
    WHERE (([Role_User_User_Roles].[Id2] = @Id2) AND ([Role_User_User_Roles].[Id] = @Id))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
IF(@rowcount = 0)
BEGIN
    INSERT INTO [Role_User_User_Roles] (
        [Role_User_User_Roles].[Id],
        [Role_User_User_Roles].[Id2])
    VALUES (
        @Id,
        @Id2)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [UserClaim] FROM [UserClaim] 
    WHERE (([UserClaim].[Id] = @Id) AND ([UserClaim].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'UserClaim_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_Save]
(
 @Id [int] = NULL,
 @Type [nvarchar] (256),
 @Value [nvarchar] (256) = NULL,
 @ValueType [nvarchar] (256) = NULL,
 @Issuer [nvarchar] (256) = NULL,
 @OriginalIssuer [nvarchar] (256) = NULL,
 @User_Id [int],
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [UserClaim] SET
     [UserClaim].[Type] = @Type,
     [UserClaim].[Value] = @Value,
     [UserClaim].[ValueType] = @ValueType,
     [UserClaim].[Issuer] = @Issuer,
     [UserClaim].[OriginalIssuer] = @OriginalIssuer,
     [UserClaim].[User_Id] = @User_Id,
     [UserClaim].[_trackLastWriteUser] = @_trackLastWriteUser,
     [UserClaim].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([UserClaim].[Id] = @Id) AND ([UserClaim].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'UserClaim_Save')
        RETURN
    END
    SELECT DISTINCT [UserClaim].[_rowVersion], @Id AS 'Id' 
        FROM [UserClaim] 
        WHERE ([UserClaim].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [UserClaim] (
        [UserClaim].[Type],
        [UserClaim].[Value],
        [UserClaim].[ValueType],
        [UserClaim].[Issuer],
        [UserClaim].[OriginalIssuer],
        [UserClaim].[User_Id],
        [UserClaim].[_trackCreationUser],
        [UserClaim].[_trackLastWriteUser])
    VALUES (
        @Type,
        @Value,
        @ValueType,
        @Issuer,
        @OriginalIssuer,
        @User_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [UserClaim].[_rowVersion], @Id AS 'Id' 
        FROM [UserClaim] 
        WHERE ([UserClaim].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
DELETE [UserLogin] FROM [UserLogin] 
    WHERE (([UserLogin].[Id] = @Id) AND ([UserLogin].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'UserLogin_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_Save]
(
 @Id [int] = NULL,
 @ProviderName [nvarchar] (256),
 @ProviderKey [nvarchar] (256),
 @ProviderDisplayName [nvarchar] (256),
 @User_Id [int],
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [UserLogin] SET
     [UserLogin].[ProviderName] = @ProviderName,
     [UserLogin].[ProviderKey] = @ProviderKey,
     [UserLogin].[ProviderDisplayName] = @ProviderDisplayName,
     [UserLogin].[User_Id] = @User_Id,
     [UserLogin].[_trackLastWriteUser] = @_trackLastWriteUser,
     [UserLogin].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([UserLogin].[Id] = @Id) AND ([UserLogin].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'UserLogin_Save')
        RETURN
    END
    SELECT DISTINCT [UserLogin].[_rowVersion], @Id AS 'Id' 
        FROM [UserLogin] 
        WHERE ([UserLogin].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [UserLogin] (
        [UserLogin].[ProviderName],
        [UserLogin].[ProviderKey],
        [UserLogin].[ProviderDisplayName],
        [UserLogin].[User_Id],
        [UserLogin].[_trackCreationUser],
        [UserLogin].[_trackLastWriteUser])
    VALUES (
        @ProviderName,
        @ProviderKey,
        @ProviderDisplayName,
        @User_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [UserLogin].[_rowVersion], @Id AS 'Id' 
        FROM [UserLogin] 
        WHERE ([UserLogin].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_Delete]
(
 @Id [int],
 @_rowVersion [rowversion]
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
UPDATE [Court] SET
 [Court].[Zone_Id] = NULL
    WHERE ([Court].[Zone_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
UPDATE [CourtHireRate] SET
 [CourtHireRate].[Zone_Id] = NULL
    WHERE ([CourtHireRate].[Zone_Id] = @Id)
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@error != 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RETURN
END
DELETE [Zone] FROM [Zone] 
    WHERE (([Zone].[Id] = @Id) AND ([Zone].[_rowVersion] = @_rowVersion))
SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
IF(@rowcount = 0)
BEGIN
    IF @tran = 1 ROLLBACK TRANSACTION
    RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Zone_Delete')
    RETURN
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_Save]
(
 @Id [int] = NULL,
 @Name [nvarchar] (256) = NULL,
 @Description [nvarchar] (256) = NULL,
 @Facility_Id [int] = NULL,
 @Sequence [int] = NULL,
 @Club_Id [int] = NULL,
 @_trackLastWriteUser [nvarchar] (64) = NULL,
 @_rowVersion [rowversion] = NULL
)
AS
SET NOCOUNT ON
DECLARE @error int, @rowcount int
DECLARE @tran bit; SELECT @tran = 0
IF @@TRANCOUNT = 0
BEGIN
 SELECT @tran = 1
 BEGIN TRANSACTION
END
IF(@_trackLastWriteUser IS NULL)
BEGIN
    SELECT DISTINCT @_trackLastWriteUser = SYSTEM_USER  
END
IF(@_rowVersion IS NOT NULL)
BEGIN
    UPDATE [Zone] SET
     [Zone].[Name] = @Name,
     [Zone].[Description] = @Description,
     [Zone].[Facility_Id] = @Facility_Id,
     [Zone].[Sequence] = @Sequence,
     [Zone].[Club_Id] = @Club_Id,
     [Zone].[_trackLastWriteUser] = @_trackLastWriteUser,
     [Zone].[_trackLastWriteTime] = GETUTCDATE()
        WHERE (([Zone].[Id] = @Id) AND ([Zone].[_rowVersion] = @_rowVersion))
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    IF(@rowcount = 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RAISERROR ('Concurrency error in procedure %s', 16, 1, 'Zone_Save')
        RETURN
    END
    SELECT DISTINCT [Zone].[_rowVersion], @Id AS 'Id' 
        FROM [Zone] 
        WHERE ([Zone].[Id] = @Id)
END
ELSE
BEGIN
    INSERT INTO [Zone] (
        [Zone].[Name],
        [Zone].[Description],
        [Zone].[Facility_Id],
        [Zone].[Sequence],
        [Zone].[Club_Id],
        [Zone].[_trackCreationUser],
        [Zone].[_trackLastWriteUser])
    VALUES (
        @Name,
        @Description,
        @Facility_Id,
        @Sequence,
        @Club_Id,
        @_trackLastWriteUser,
        @_trackLastWriteUser)
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    IF(@error != 0)
    BEGIN
        IF @tran = 1 ROLLBACK TRANSACTION
        RETURN
    END
    SELECT DISTINCT @Id = SCOPE_IDENTITY()  
    SELECT DISTINCT [Zone].[_rowVersion], @Id AS 'Id' 
        FROM [Zone] 
        WHERE ([Zone].[Id] = @Id)
END
IF @tran = 1 COMMIT TRANSACTION

RETURN
GO

CREATE PROCEDURE [dbo].[Account_GetClubAccount]
(
 @clubId [int],
 @accountType [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account]
        LEFT OUTER JOIN [Club] ON ([Account].[Club_Id] = [Club].[Id]) 
    WHERE (([Club].[Id] = @clubId) AND ([Account].[AccountType] = @accountType))

RETURN
GO

CREATE PROCEDURE [dbo].[Account_GetCustomerAccount]
(
 @customerId [int],
 @accountType [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account]
        LEFT OUTER JOIN [Customer] ON ([Account].[Customer_Id] = [Customer].[Id]) 
    WHERE (([Customer].[Id] = @customerId) AND ([Account].[AccountType] = @accountType))

RETURN
GO

CREATE PROCEDURE [dbo].[Account_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 
    WHERE ([Account].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Account_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 

RETURN
GO

CREATE PROCEDURE [dbo].[Account_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 
    WHERE ([Account].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Account_LoadByCurrency]
(
 @CurrencyId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 
    WHERE ([Account].[Currency_Id] = @CurrencyId)

RETURN
GO

CREATE PROCEDURE [dbo].[Account_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 
    WHERE ([Account].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Account_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Account].[Id], [Account].[Balance], [Account].[Currency_Id], [Account].[Description], [Account].[AccountType], [Account].[Club_Id], [Account].[Customer_Id], [Account].[Reference], [Account].[_trackLastWriteTime], [Account].[_trackCreationTime], [Account].[_trackLastWriteUser], [Account].[_trackCreationUser], [Account].[_rowVersion] 
    FROM [Account] 
    WHERE ([Account].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[AccountingPeriod_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [AccountingPeriod].[Id], [AccountingPeriod].[Name], [AccountingPeriod].[Country], [AccountingPeriod].[StartDate], [AccountingPeriod].[EndDate], [AccountingPeriod].[isCurrent], [AccountingPeriod].[_trackLastWriteTime], [AccountingPeriod].[_trackCreationTime], [AccountingPeriod].[_trackLastWriteUser], [AccountingPeriod].[_trackCreationUser], [AccountingPeriod].[_rowVersion] 
    FROM [AccountingPeriod] 
    WHERE ([AccountingPeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[AccountingPeriod_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [AccountingPeriod].[Id], [AccountingPeriod].[Name], [AccountingPeriod].[Country], [AccountingPeriod].[StartDate], [AccountingPeriod].[EndDate], [AccountingPeriod].[isCurrent], [AccountingPeriod].[_trackLastWriteTime], [AccountingPeriod].[_trackCreationTime], [AccountingPeriod].[_trackLastWriteUser], [AccountingPeriod].[_trackCreationUser], [AccountingPeriod].[_rowVersion] 
    FROM [AccountingPeriod] 

RETURN
GO

CREATE PROCEDURE [dbo].[AccountingPeriod_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [AccountingPeriod].[Id], [AccountingPeriod].[Name], [AccountingPeriod].[Country], [AccountingPeriod].[StartDate], [AccountingPeriod].[EndDate], [AccountingPeriod].[isCurrent], [AccountingPeriod].[_trackLastWriteTime], [AccountingPeriod].[_trackCreationTime], [AccountingPeriod].[_trackLastWriteUser], [AccountingPeriod].[_trackCreationUser], [AccountingPeriod].[_rowVersion] 
    FROM [AccountingPeriod] 
    WHERE ([AccountingPeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Admin].[Id], [Admin].[User_Id], [Admin].[Club_Id], [Admin].[CreationDateUTC], [Admin].[_trackLastWriteTime], [Admin].[_trackCreationTime], [Admin].[_trackLastWriteUser], [Admin].[_trackCreationUser], [Admin].[_rowVersion] 
    FROM [Admin] 
    WHERE ([Admin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Admin].[Id], [Admin].[User_Id], [Admin].[Club_Id], [Admin].[CreationDateUTC], [Admin].[_trackLastWriteTime], [Admin].[_trackCreationTime], [Admin].[_trackLastWriteUser], [Admin].[_trackCreationUser], [Admin].[_rowVersion] 
    FROM [Admin] 

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Admin].[Id], [Admin].[User_Id], [Admin].[Club_Id], [Admin].[CreationDateUTC], [Admin].[_trackLastWriteTime], [Admin].[_trackCreationTime], [Admin].[_trackLastWriteUser], [Admin].[_trackCreationUser], [Admin].[_rowVersion] 
    FROM [Admin] 
    WHERE ([Admin].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Admin].[Id], [Admin].[User_Id], [Admin].[Club_Id], [Admin].[CreationDateUTC], [Admin].[_trackLastWriteTime], [Admin].[_trackCreationTime], [Admin].[_trackLastWriteUser], [Admin].[_trackCreationUser], [Admin].[_rowVersion] 
    FROM [Admin] 
    WHERE ([Admin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Admin_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Admin].[Id], [Admin].[User_Id], [Admin].[Club_Id], [Admin].[CreationDateUTC], [Admin].[_trackLastWriteTime], [Admin].[_trackCreationTime], [Admin].[_trackLastWriteUser], [Admin].[_trackCreationUser], [Admin].[_rowVersion] 
    FROM [Admin] 
    WHERE ([Admin].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Batch_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Batch].[Id], [Batch].[Description], [Batch].[_trackLastWriteTime], [Batch].[_trackCreationTime], [Batch].[_trackLastWriteUser], [Batch].[_trackCreationUser], [Batch].[_rowVersion] 
    FROM [Batch] 
    WHERE ([Batch].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Batch_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Batch].[Id], [Batch].[Description], [Batch].[_trackLastWriteTime], [Batch].[_trackCreationTime], [Batch].[_trackLastWriteUser], [Batch].[_trackCreationUser], [Batch].[_rowVersion] 
    FROM [Batch] 

RETURN
GO

CREATE PROCEDURE [dbo].[Batch_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Batch].[Id], [Batch].[Description], [Batch].[_trackLastWriteTime], [Batch].[_trackCreationTime], [Batch].[_trackLastWriteUser], [Batch].[_trackCreationUser], [Batch].[_rowVersion] 
    FROM [Batch] 
    WHERE ([Batch].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_CountByCustomer]
(
 @customerId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Booking]
    LEFT OUTER JOIN [Customer] ON ([Booking].[Customer_Id] = [Customer].[Id]) 
    WHERE ([Customer].[Id] = @customerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadAllByClub]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Club_Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadBookingByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking]
        LEFT OUTER JOIN [EventBooking_Booking_Booking] ON ([Booking].[Id] = [EventBooking_Booking_Booking].[Id2])
                LEFT OUTER JOIN [EventBooking] ON ([EventBooking_Booking_Booking].[Id] = [EventBooking].[Id]) 
    WHERE ([EventBooking_Booking_Booking].[Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByAdmin]
(
 @AdminId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Admin_Id] = @AdminId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByBookingStatus]
(
 @BookingStatusId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[BookingStatus_Id] = @BookingStatusId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByBookingType]
(
 @BookingTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[BookingType_Id] = @BookingTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByCoach]
(
 @CoachId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Coach_Id] = @CoachId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByCourt]
(
 @CourtId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Court_Id] = @CourtId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Event_Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByMultiCourt]
(
 @multiCourt [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[MultiCourt] = @multiCourt)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking] 
    WHERE ([Booking].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Booking_LoadParentMultiCourt]
(
 @courtId [int],
 @start [datetime],
 @end [datetime]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[isMultiCourt], [Booking].[MultiCourt], [Booking].[Customer_Id], [Booking].[Admin_Id], [Booking].[Coach_Id], [Booking].[BookingStatus_Id], [Booking].[Event_Id], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteUser], [Booking].[_trackCreationUser], [Booking].[_rowVersion] 
    FROM [Booking]
        LEFT OUTER JOIN [Court] ON ([Booking].[Court_Id] = [Court].[Id]) 
    WHERE (([Court].[Id] = @courtId) AND (([Booking].[Start] = @start) AND ([Booking].[End] = @end)))

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_GetStatus]
(
 @bookingTypeId [int],
 @isPaid [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingStatus].[Id], [BookingStatus].[Colour], [BookingStatus].[isPaid], [BookingStatus].[BookingType_Id], [BookingStatus].[Name], [BookingStatus].[_trackLastWriteTime], [BookingStatus].[_trackCreationTime], [BookingStatus].[_trackLastWriteUser], [BookingStatus].[_trackCreationUser], [BookingStatus].[_rowVersion] 
    FROM [BookingStatus]
        LEFT OUTER JOIN [BookingType] ON ([BookingStatus].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([BookingStatus].[isPaid] = @isPaid))

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingStatus].[Id], [BookingStatus].[Colour], [BookingStatus].[isPaid], [BookingStatus].[BookingType_Id], [BookingStatus].[Name], [BookingStatus].[_trackLastWriteTime], [BookingStatus].[_trackCreationTime], [BookingStatus].[_trackLastWriteUser], [BookingStatus].[_trackCreationUser], [BookingStatus].[_rowVersion] 
    FROM [BookingStatus] 
    WHERE ([BookingStatus].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingStatus].[Id], [BookingStatus].[Colour], [BookingStatus].[isPaid], [BookingStatus].[BookingType_Id], [BookingStatus].[Name], [BookingStatus].[_trackLastWriteTime], [BookingStatus].[_trackCreationTime], [BookingStatus].[_trackLastWriteUser], [BookingStatus].[_trackCreationUser], [BookingStatus].[_rowVersion] 
    FROM [BookingStatus] 

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_LoadByBookingType]
(
 @BookingTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingStatus].[Id], [BookingStatus].[Colour], [BookingStatus].[isPaid], [BookingStatus].[BookingType_Id], [BookingStatus].[Name], [BookingStatus].[_trackLastWriteTime], [BookingStatus].[_trackCreationTime], [BookingStatus].[_trackLastWriteUser], [BookingStatus].[_trackCreationUser], [BookingStatus].[_rowVersion] 
    FROM [BookingStatus] 
    WHERE ([BookingStatus].[BookingType_Id] = @BookingTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingStatus_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingStatus].[Id], [BookingStatus].[Colour], [BookingStatus].[isPaid], [BookingStatus].[BookingType_Id], [BookingStatus].[Name], [BookingStatus].[_trackLastWriteTime], [BookingStatus].[_trackCreationTime], [BookingStatus].[_trackLastWriteUser], [BookingStatus].[_trackCreationUser], [BookingStatus].[_rowVersion] 
    FROM [BookingStatus] 
    WHERE ([BookingStatus].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteUser], [BookingSubType].[_trackCreationUser], [BookingSubType].[_rowVersion] 
    FROM [BookingSubType] 
    WHERE ([BookingSubType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteUser], [BookingSubType].[_trackCreationUser], [BookingSubType].[_rowVersion] 
    FROM [BookingSubType] 

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_LoadByBookingType]
(
 @BookingTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteUser], [BookingSubType].[_trackCreationUser], [BookingSubType].[_rowVersion] 
    FROM [BookingSubType] 
    WHERE ([BookingSubType].[BookingType_Id] = @BookingTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteUser], [BookingSubType].[_trackCreationUser], [BookingSubType].[_rowVersion] 
    FROM [BookingSubType] 
    WHERE ([BookingSubType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingSubType_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteUser], [BookingSubType].[_trackCreationUser], [BookingSubType].[_rowVersion] 
    FROM [BookingSubType] 
    WHERE ([BookingSubType].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory], [BookingType].[Colour], [BookingType].[BookingRule], [BookingType].[BookingOption], [BookingType].[Club_Id], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteUser], [BookingType].[_trackCreationUser], [BookingType].[_rowVersion] 
    FROM [BookingType] 
    WHERE ([BookingType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory], [BookingType].[Colour], [BookingType].[BookingRule], [BookingType].[BookingOption], [BookingType].[Club_Id], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteUser], [BookingType].[_trackCreationUser], [BookingType].[_rowVersion] 
    FROM [BookingType] 
    ORDER BY [BookingType].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory], [BookingType].[Colour], [BookingType].[BookingRule], [BookingType].[BookingOption], [BookingType].[Club_Id], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteUser], [BookingType].[_trackCreationUser], [BookingType].[_rowVersion] 
    FROM [BookingType] 
    WHERE ([BookingType].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory], [BookingType].[Colour], [BookingType].[BookingRule], [BookingType].[BookingOption], [BookingType].[Club_Id], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteUser], [BookingType].[_trackCreationUser], [BookingType].[_rowVersion] 
    FROM [BookingType] 
    WHERE ([BookingType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[BookingType_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory], [BookingType].[Colour], [BookingType].[BookingRule], [BookingType].[BookingOption], [BookingType].[Club_Id], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteUser], [BookingType].[_trackCreationUser], [BookingType].[_rowVersion] 
    FROM [BookingType] 
    WHERE ([BookingType].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[CancellationType_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CancellationType].[Id], [CancellationType].[Name], [CancellationType].[Description], [CancellationType].[Sequence], [CancellationType].[_trackLastWriteTime], [CancellationType].[_trackCreationTime], [CancellationType].[_trackLastWriteUser], [CancellationType].[_trackCreationUser], [CancellationType].[_rowVersion] 
    FROM [CancellationType] 
    WHERE ([CancellationType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CancellationType_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CancellationType].[Id], [CancellationType].[Name], [CancellationType].[Description], [CancellationType].[Sequence], [CancellationType].[_trackLastWriteTime], [CancellationType].[_trackCreationTime], [CancellationType].[_trackLastWriteUser], [CancellationType].[_trackCreationUser], [CancellationType].[_rowVersion] 
    FROM [CancellationType] 

RETURN
GO

CREATE PROCEDURE [dbo].[CancellationType_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CancellationType].[Id], [CancellationType].[Name], [CancellationType].[Description], [CancellationType].[Sequence], [CancellationType].[_trackLastWriteTime], [CancellationType].[_trackCreationTime], [CancellationType].[_trackLastWriteUser], [CancellationType].[_trackCreationUser], [CancellationType].[_rowVersion] 
    FROM [CancellationType] 
    WHERE ([CancellationType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Checkout_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Checkout].[Id], [Checkout].[_trackLastWriteTime], [Checkout].[_trackCreationTime], [Checkout].[_trackLastWriteUser], [Checkout].[_trackCreationUser], [Checkout].[_rowVersion] 
    FROM [Checkout] 
    WHERE ([Checkout].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Checkout_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Checkout].[Id], [Checkout].[_trackLastWriteTime], [Checkout].[_trackCreationTime], [Checkout].[_trackLastWriteUser], [Checkout].[_trackCreationUser], [Checkout].[_rowVersion] 
    FROM [Checkout] 

RETURN
GO

CREATE PROCEDURE [dbo].[Checkout_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Checkout].[Id], [Checkout].[_trackLastWriteTime], [Checkout].[_trackCreationTime], [Checkout].[_trackLastWriteUser], [Checkout].[_trackCreationUser], [Checkout].[_rowVersion] 
    FROM [Checkout] 
    WHERE ([Checkout].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CheckoutItem].[Id], [CheckoutItem].[Product_Id], [CheckoutItem].[_trackLastWriteTime], [CheckoutItem].[_trackCreationTime], [CheckoutItem].[_trackLastWriteUser], [CheckoutItem].[_trackCreationUser], [CheckoutItem].[_rowVersion] 
    FROM [CheckoutItem] 
    WHERE ([CheckoutItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CheckoutItem].[Id], [CheckoutItem].[Product_Id], [CheckoutItem].[_trackLastWriteTime], [CheckoutItem].[_trackCreationTime], [CheckoutItem].[_trackLastWriteUser], [CheckoutItem].[_trackCreationUser], [CheckoutItem].[_rowVersion] 
    FROM [CheckoutItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CheckoutItem].[Id], [CheckoutItem].[Product_Id], [CheckoutItem].[_trackLastWriteTime], [CheckoutItem].[_trackCreationTime], [CheckoutItem].[_trackLastWriteUser], [CheckoutItem].[_trackCreationUser], [CheckoutItem].[_rowVersion] 
    FROM [CheckoutItem] 
    WHERE ([CheckoutItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CheckoutItem_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CheckoutItem].[Id], [CheckoutItem].[Product_Id], [CheckoutItem].[_trackLastWriteTime], [CheckoutItem].[_trackCreationTime], [CheckoutItem].[_trackLastWriteUser], [CheckoutItem].[_trackCreationUser], [CheckoutItem].[_rowVersion] 
    FROM [CheckoutItem] 
    WHERE ([CheckoutItem].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[Club_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_trackLastWriteTime], [Club].[_trackCreationTime], [Club].[_trackLastWriteUser], [Club].[_trackCreationUser], [Club].[_rowVersion] 
    FROM [Club] 
    WHERE ([Club].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Club_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_trackLastWriteTime], [Club].[_trackCreationTime], [Club].[_trackLastWriteUser], [Club].[_trackCreationUser], [Club].[_rowVersion] 
    FROM [Club] 

RETURN
GO

CREATE PROCEDURE [dbo].[Club_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_trackLastWriteTime], [Club].[_trackCreationTime], [Club].[_trackLastWriteUser], [Club].[_trackCreationUser], [Club].[_rowVersion] 
    FROM [Club] 
    WHERE ([Club].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Club_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_trackLastWriteTime], [Club].[_trackCreationTime], [Club].[_trackLastWriteUser], [Club].[_trackCreationUser], [Club].[_rowVersion] 
    FROM [Club] 
    WHERE ([Club].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Club_LoadOneByRefNo]
(
 @ref [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_trackLastWriteTime], [Club].[_trackCreationTime], [Club].[_trackLastWriteUser], [Club].[_trackCreationUser], [Club].[_rowVersion] 
    FROM [Club] 
    WHERE ([Club].[ReferenceNo] = @ref)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadAllByClub]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[Club_Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadOneById]
(
 @id [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubAdmin_LoadOneByUser]
(
 @userId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteUser], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_rowVersion] 
    FROM [ClubAdmin] 
    WHERE ([ClubAdmin].[User_Id] = @userId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 
    WHERE ([ClubComponents].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_LoadAllByClub]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 
    WHERE ([ClubComponents].[Club_Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 
    WHERE ([ClubComponents].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_LoadByComponent]
(
 @ComponentId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 
    WHERE ([ClubComponents].[Component_Id] = @ComponentId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubComponents_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteUser], [ClubComponents].[_trackCreationUser], [ClubComponents].[_rowVersion] 
    FROM [ClubComponents] 
    WHERE ([ClubComponents].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteUser], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_rowVersion] 
    FROM [ClubPlayerLevel] 
    WHERE ([ClubPlayerLevel].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteUser], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_rowVersion] 
    FROM [ClubPlayerLevel] 

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteUser], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_rowVersion] 
    FROM [ClubPlayerLevel] 
    WHERE ([ClubPlayerLevel].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteUser], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_rowVersion] 
    FROM [ClubPlayerLevel] 
    WHERE ([ClubPlayerLevel].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ClubPlayerLevel_LoadByPlayerLevel]
(
 @PlayerLevelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteUser], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_rowVersion] 
    FROM [ClubPlayerLevel] 
    WHERE ([ClubPlayerLevel].[PlayerLevel_Id] = @PlayerLevelId)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach] 
    WHERE ([Coach].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach] 

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach] 
    WHERE ([Coach].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach] 
    WHERE ([Coach].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach] 
    WHERE ([Coach].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadCoachesByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach]
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Coach].[Id] = [Event_Coaches_Coach].[Id2])
                LEFT OUTER JOIN [Event] ON ([Event_Coaches_Coach].[Id] = [Event].[Id]) 
    WHERE ([Event_Coaches_Coach].[Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[Coach_LoadCoachesByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Coach].[Id], [Coach].[CreationDateUTC], [Coach].[isActive], [Coach].[User_Id], [Coach].[Club_Id], [Coach].[_trackLastWriteTime], [Coach].[_trackCreationTime], [Coach].[_trackLastWriteUser], [Coach].[_trackCreationUser], [Coach].[_rowVersion] 
    FROM [Coach]
        LEFT OUTER JOIN [EventBooking_Coaches_Coach] ON ([Coach].[Id] = [EventBooking_Coaches_Coach].[Id2])
                LEFT OUTER JOIN [EventBooking] ON ([EventBooking_Coaches_Coach].[Id] = [EventBooking].[Id]) 
    WHERE ([EventBooking_Coaches_Coach].[Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[Components_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Components].[Id], [Components].[Name], [Components].[Sequence], [Components].[isActive], [Components].[_trackLastWriteTime], [Components].[_trackCreationTime], [Components].[_trackLastWriteUser], [Components].[_trackCreationUser], [Components].[_rowVersion] 
    FROM [Components] 
    WHERE ([Components].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Components_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Components].[Id], [Components].[Name], [Components].[Sequence], [Components].[isActive], [Components].[_trackLastWriteTime], [Components].[_trackCreationTime], [Components].[_trackLastWriteUser], [Components].[_trackCreationUser], [Components].[_rowVersion] 
    FROM [Components] 

RETURN
GO

CREATE PROCEDURE [dbo].[Components_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Components].[Id], [Components].[Name], [Components].[Sequence], [Components].[isActive], [Components].[_trackLastWriteTime], [Components].[_trackCreationTime], [Components].[_trackLastWriteUser], [Components].[_trackCreationUser], [Components].[_rowVersion] 
    FROM [Components] 
    WHERE ([Components].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Components_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Components].[Id], [Components].[Name], [Components].[Sequence], [Components].[isActive], [Components].[_trackLastWriteTime], [Components].[_trackCreationTime], [Components].[_trackLastWriteUser], [Components].[_trackCreationUser], [Components].[_rowVersion] 
    FROM [Components] 
    WHERE ([Components].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadAllByClub]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Club_Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadAllByFacility]
(
 @facilityid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Facility_Id] = @facilityid)
    ORDER BY [Court].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadBySurface]
(
 @SurfaceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Surface_Id] = @SurfaceId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadByZone]
(
 @ZoneId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Zone_Id] = @ZoneId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadCourtsByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court]
        LEFT OUTER JOIN [Event_Courts_Court] ON ([Court].[Id] = [Event_Courts_Court].[Id2])
                LEFT OUTER JOIN [Event] ON ([Event_Courts_Court].[Id] = [Event].[Id]) 
    WHERE ([Event_Courts_Court].[Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Court_LoadOneByRefNo]
(
 @ref [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_trackLastWriteTime], [Court].[_trackCreationTime], [Court].[_trackLastWriteUser], [Court].[_trackCreationUser], [Court].[_rowVersion] 
    FROM [Court] 
    WHERE ([Court].[ReferenceNo] = @ref)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByClubAndDayOfRate]
(
 @clubId [int],
 @dayOfRate [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[Club_Id] = @clubId) AND ([CourtHireRate].[DayOfRate] = @dayOfRate))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByClubAndIsWeekday]
(
 @clubId [int],
 @isWeekday [bit],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[Club_Id] = @clubId) AND ([CourtHireRate].[IsWeekday] = @isWeekday))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByMembershipType]
(
 @MembershipTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[MembershipType_Id] = @MembershipTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypeIsWeekdayTimePeriod]
(
 @membershipTypeId [int],
 @timePeriodId [int],
 @isWeekday [bit],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate]
        LEFT OUTER JOIN [MembershipType] ON ([CourtHireRate].[MembershipType_Id] = [MembershipType].[Id])
        LEFT OUTER JOIN [TimePeriod] ON ([CourtHireRate].[TimePeriod_Id] = [TimePeriod].[Id]) 
    WHERE (([MembershipType].[Id] = @membershipTypeId) AND (([TimePeriod].[Id] = @timePeriodId) AND ([CourtHireRate].[IsWeekday] = @isWeekday)))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypePeriodZone]
(
 @membershipTypeId [int],
 @periodId [int],
 @zoneId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[MembershipType_Id] = @membershipTypeId) AND (([CourtHireRate].[TimePeriod_Id] = @periodId) AND ([CourtHireRate].[Zone_Id] = @zoneId)))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypePeriodZoneDayOfWeek]
(
 @membershipTypeId [int],
 @periodId [int],
 @zoneId [int],
 @dayOfWeek [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[MembershipType_Id] = @membershipTypeId) AND (([CourtHireRate].[TimePeriod_Id] = @periodId) AND (([CourtHireRate].[Zone_Id] = @zoneId) AND ([CourtHireRate].[DayOfRate] = @dayOfWeek))))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByMembershipTypeTimePeriodIsWeekdayZone]
(
 @membershipTypeId [int],
 @timePeriodId [int],
 @zoneId [int],
 @isWeekday [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE (([CourtHireRate].[MembershipType_Id] = @membershipTypeId) AND (([CourtHireRate].[TimePeriod_Id] = @timePeriodId) AND (([CourtHireRate].[Zone_Id] = @zoneId) AND ([CourtHireRate].[IsWeekday] = @isWeekday))))

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByTimePeriod]
(
 @TimePeriodId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[TimePeriod_Id] = @TimePeriodId)

RETURN
GO

CREATE PROCEDURE [dbo].[CourtHireRate_LoadByZone]
(
 @ZoneId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteUser], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_rowVersion] 
    FROM [CourtHireRate] 
    WHERE ([CourtHireRate].[Zone_Id] = @ZoneId)

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit] 
    WHERE ([Credit].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit] 

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit] 
    WHERE ([Credit].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit] 
    WHERE ([Credit].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_LoadByOrder]
(
 @OrderId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit] 
    WHERE ([Credit].[Order_Id] = @OrderId)

RETURN
GO

CREATE PROCEDURE [dbo].[Credit_LoadCustomerCredits]
(
 @customerId [int],
 @creditStatus [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Credit].[Id], [Credit].[DateCreatedUTC], [Credit].[Amount], [Credit].[Transaction], [Credit].[CreditStatus], [Credit].[Metadata], [Credit].[Customer_Id], [Credit].[Order_Id], [Credit].[Balance], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteUser], [Credit].[_trackCreationUser], [Credit].[_rowVersion] 
    FROM [Credit]
        LEFT OUTER JOIN [Customer] ON ([Credit].[Customer_Id] = [Customer].[Id]) 
    WHERE (([Customer].[Id] = @customerId) AND ([Credit].[CreditStatus] = @creditStatus))
    ORDER BY [Credit].[DateCreatedUTC] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CreditAdjust].[Id], [CreditAdjust].[Credit_Id], [CreditAdjust].[CreditAdjustType], [CreditAdjust].[Amount], [CreditAdjust].[AdjustDateTimeUTC], [CreditAdjust].[Metadata], [CreditAdjust].[Balance], [CreditAdjust].[Invoice_Id], [CreditAdjust].[_trackLastWriteTime], [CreditAdjust].[_trackCreationTime], [CreditAdjust].[_trackLastWriteUser], [CreditAdjust].[_trackCreationUser], [CreditAdjust].[_rowVersion] 
    FROM [CreditAdjust] 
    WHERE ([CreditAdjust].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CreditAdjust].[Id], [CreditAdjust].[Credit_Id], [CreditAdjust].[CreditAdjustType], [CreditAdjust].[Amount], [CreditAdjust].[AdjustDateTimeUTC], [CreditAdjust].[Metadata], [CreditAdjust].[Balance], [CreditAdjust].[Invoice_Id], [CreditAdjust].[_trackLastWriteTime], [CreditAdjust].[_trackCreationTime], [CreditAdjust].[_trackLastWriteUser], [CreditAdjust].[_trackCreationUser], [CreditAdjust].[_rowVersion] 
    FROM [CreditAdjust] 

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_LoadByCredit]
(
 @CreditId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CreditAdjust].[Id], [CreditAdjust].[Credit_Id], [CreditAdjust].[CreditAdjustType], [CreditAdjust].[Amount], [CreditAdjust].[AdjustDateTimeUTC], [CreditAdjust].[Metadata], [CreditAdjust].[Balance], [CreditAdjust].[Invoice_Id], [CreditAdjust].[_trackLastWriteTime], [CreditAdjust].[_trackCreationTime], [CreditAdjust].[_trackLastWriteUser], [CreditAdjust].[_trackCreationUser], [CreditAdjust].[_rowVersion] 
    FROM [CreditAdjust] 
    WHERE ([CreditAdjust].[Credit_Id] = @CreditId)

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CreditAdjust].[Id], [CreditAdjust].[Credit_Id], [CreditAdjust].[CreditAdjustType], [CreditAdjust].[Amount], [CreditAdjust].[AdjustDateTimeUTC], [CreditAdjust].[Metadata], [CreditAdjust].[Balance], [CreditAdjust].[Invoice_Id], [CreditAdjust].[_trackLastWriteTime], [CreditAdjust].[_trackCreationTime], [CreditAdjust].[_trackLastWriteUser], [CreditAdjust].[_trackCreationUser], [CreditAdjust].[_rowVersion] 
    FROM [CreditAdjust] 
    WHERE ([CreditAdjust].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[CreditAdjust_LoadByInvoice]
(
 @InvoiceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [CreditAdjust].[Id], [CreditAdjust].[Credit_Id], [CreditAdjust].[CreditAdjustType], [CreditAdjust].[Amount], [CreditAdjust].[AdjustDateTimeUTC], [CreditAdjust].[Metadata], [CreditAdjust].[Balance], [CreditAdjust].[Invoice_Id], [CreditAdjust].[_trackLastWriteTime], [CreditAdjust].[_trackCreationTime], [CreditAdjust].[_trackLastWriteUser], [CreditAdjust].[_trackCreationUser], [CreditAdjust].[_rowVersion] 
    FROM [CreditAdjust] 
    WHERE ([CreditAdjust].[Invoice_Id] = @InvoiceId)

RETURN
GO

CREATE PROCEDURE [dbo].[Currency_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Currency].[Id], [Currency].[Code], [Currency].[Name], [Currency].[DecimalPlaces], [Currency].[_trackLastWriteTime], [Currency].[_trackCreationTime], [Currency].[_trackLastWriteUser], [Currency].[_trackCreationUser], [Currency].[_rowVersion] 
    FROM [Currency] 
    WHERE ([Currency].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Currency_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Currency].[Id], [Currency].[Code], [Currency].[Name], [Currency].[DecimalPlaces], [Currency].[_trackLastWriteTime], [Currency].[_trackCreationTime], [Currency].[_trackLastWriteUser], [Currency].[_trackCreationUser], [Currency].[_rowVersion] 
    FROM [Currency] 

RETURN
GO

CREATE PROCEDURE [dbo].[Currency_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Currency].[Id], [Currency].[Code], [Currency].[Name], [Currency].[DecimalPlaces], [Currency].[_trackLastWriteTime], [Currency].[_trackCreationTime], [Currency].[_trackLastWriteUser], [Currency].[_trackCreationUser], [Currency].[_rowVersion] 
    FROM [Currency] 
    WHERE ([Currency].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_CountByClub]
(
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Customer]
    LEFT OUTER JOIN [Club] ON ([Customer].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_CountBySearch]
(
 @value [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Customer]
    LEFT OUTER JOIN [User] ON ([Customer].[User_Id] = [User].[Id])
    LEFT OUTER JOIN [Club] ON ([Customer].[Club_Id] = [Club].[Id])
    LEFT OUTER JOIN [User] [User$1] ON ([Customer].[User_Id] = [User$1].[Id])
    LEFT OUTER JOIN [Club] [Club$1] ON ([Customer].[Club_Id] = [Club$1].[Id])
    LEFT OUTER JOIN [User] [User$2] ON ([Customer].[User_Id] = [User$2].[Id])
    LEFT OUTER JOIN [Club] [Club$2] ON ([Customer].[Club_Id] = [Club$2].[Id])
    LEFT OUTER JOIN [User] [User$3] ON ([Customer].[User_Id] = [User$3].[Id])
    LEFT OUTER JOIN [Club] [Club$3] ON ([Customer].[Club_Id] = [Club$3].[Id])
    LEFT OUTER JOIN [User] [User$4] ON ([Customer].[User_Id] = [User$4].[Id])
    LEFT OUTER JOIN [Club] [Club$4] ON ([Customer].[Club_Id] = [Club$4].[Id])
    LEFT OUTER JOIN [User] [User$5] ON ([Customer].[User_Id] = [User$5].[Id])
    LEFT OUTER JOIN [Club] [Club$5] ON ([Customer].[Club_Id] = [Club$5].[Id]) 
    WHERE ((([User].[FirstName] LIKE @value) AND ([Club].[Id] = @clubId)) OR ((([User$1].[LastName] LIKE @value) AND ([Club$1].[Id] = @clubId)) OR ((((([User$2].[FirstName] + ' ') + [User].[LastName]) LIKE @value) AND ([Club$2].[Id] = @clubId)) OR ((([User$3].[Email] LIKE @value) AND ([Club$3].[Id] = @clubId)) OR ((([User$4].[PhoneNumber] LIKE @value) AND ([Club$4].[Id] = @clubId)) OR (([User$5].[TennisId] LIKE @value) AND ([Club$5].[Id] = @clubId)))))))

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer] 
    WHERE ([Customer].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer] 

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer] 
    WHERE ([Customer].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer] 
    WHERE ([Customer].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadBySearch]
(
 @value [nvarchar] (256),
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer]
        LEFT OUTER JOIN [User] ON ([Customer].[User_Id] = [User].[Id])
        LEFT OUTER JOIN [Club] ON ([Customer].[Club_Id] = [Club].[Id])
        LEFT OUTER JOIN [User] [User$1] ON ([Customer].[User_Id] = [User$1].[Id])
        LEFT OUTER JOIN [Club] [Club$1] ON ([Customer].[Club_Id] = [Club$1].[Id])
        LEFT OUTER JOIN [User] [User$2] ON ([Customer].[User_Id] = [User$2].[Id])
        LEFT OUTER JOIN [Club] [Club$2] ON ([Customer].[Club_Id] = [Club$2].[Id])
        LEFT OUTER JOIN [User] [User$3] ON ([Customer].[User_Id] = [User$3].[Id])
        LEFT OUTER JOIN [Club] [Club$3] ON ([Customer].[Club_Id] = [Club$3].[Id])
        LEFT OUTER JOIN [User] [User$4] ON ([Customer].[User_Id] = [User$4].[Id])
        LEFT OUTER JOIN [Club] [Club$4] ON ([Customer].[Club_Id] = [Club$4].[Id])
        LEFT OUTER JOIN [User] [User$5] ON ([Customer].[User_Id] = [User$5].[Id])
        LEFT OUTER JOIN [Club] [Club$5] ON ([Customer].[Club_Id] = [Club$5].[Id]) 
    WHERE ((([User].[FirstName] LIKE (@value + '%')) AND ([Club].[Id] = @clubId)) OR ((([User$1].[LastName] LIKE (@value + '%')) AND ([Club$1].[Id] = @clubId)) OR ((((([User$2].[FirstName] + ' ') + [User].[LastName]) LIKE (@value + '%')) AND ([Club$2].[Id] = @clubId)) OR ((([User$3].[Email] LIKE (@value + '%')) AND ([Club$3].[Id] = @clubId)) OR ((([User$4].[PhoneNumber] LIKE (@value + '%')) AND ([Club$4].[Id] = @clubId)) OR (([User$5].[TennisId] LIKE (@value + '%')) AND ([Club$5].[Id] = @clubId)))))))

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer] 
    WHERE ([Customer].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_LoadOneByUserId]
(
 @userId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer]
        LEFT OUTER JOIN [User] ON ([Customer].[User_Id] = [User].[Id]) 
    WHERE ([User].[Id] = @userId)

RETURN
GO

CREATE PROCEDURE [dbo].[Customer_SearchCustomer]
(
 @value [nvarchar] (256),
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Customer].[Id], [Customer].[User_Id], [Customer].[Club_Id], [Customer].[CreationDateUTC], [Customer].[isMember], [Customer].[isActive], [Customer].[_trackLastWriteTime], [Customer].[_trackCreationTime], [Customer].[_trackLastWriteUser], [Customer].[_trackCreationUser], [Customer].[_rowVersion] 
    FROM [Customer]
        LEFT OUTER JOIN [User] ON ([Customer].[User_Id] = [User].[Id])
        LEFT OUTER JOIN [Club] ON ([Customer].[Club_Id] = [Club].[Id])
        LEFT OUTER JOIN [User] [User$1] ON ([Customer].[User_Id] = [User$1].[Id])
        LEFT OUTER JOIN [Club] [Club$1] ON ([Customer].[Club_Id] = [Club$1].[Id])
        LEFT OUTER JOIN [User] [User$2] ON ([Customer].[User_Id] = [User$2].[Id])
        LEFT OUTER JOIN [Club] [Club$2] ON ([Customer].[Club_Id] = [Club$2].[Id])
        LEFT OUTER JOIN [User] [User$3] ON ([Customer].[User_Id] = [User$3].[Id])
        LEFT OUTER JOIN [Club] [Club$3] ON ([Customer].[Club_Id] = [Club$3].[Id])
        LEFT OUTER JOIN [User] [User$4] ON ([Customer].[User_Id] = [User$4].[Id])
        LEFT OUTER JOIN [Club] [Club$4] ON ([Customer].[Club_Id] = [Club$4].[Id])
        LEFT OUTER JOIN [User] [User$5] ON ([Customer].[User_Id] = [User$5].[Id])
        LEFT OUTER JOIN [Club] [Club$5] ON ([Customer].[Club_Id] = [Club$5].[Id]) 
    WHERE ((([User].[FirstName] LIKE @value) AND ([Club].[Id] = @clubId)) OR ((([User$1].[LastName] LIKE @value) AND ([Club$1].[Id] = @clubId)) OR ((((([User$2].[FirstName] + ' ') + [User].[LastName]) LIKE @value) AND ([Club$2].[Id] = @clubId)) OR ((([User$3].[Email] LIKE @value) AND ([Club$3].[Id] = @clubId)) OR ((([User$4].[PhoneNumber] LIKE @value) AND ([Club$4].[Id] = @clubId)) OR (([User$5].[TennisId] LIKE @value) AND ([Club$5].[Id] = @clubId)))))))

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DaysOfWeek].[Id], [DaysOfWeek].[Name], [DaysOfWeek].[Value], [DaysOfWeek].[Sequence], [DaysOfWeek].[_trackLastWriteTime], [DaysOfWeek].[_trackCreationTime], [DaysOfWeek].[_trackLastWriteUser], [DaysOfWeek].[_trackCreationUser], [DaysOfWeek].[_rowVersion] 
    FROM [DaysOfWeek] 
    WHERE ([DaysOfWeek].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DaysOfWeek].[Id], [DaysOfWeek].[Name], [DaysOfWeek].[Value], [DaysOfWeek].[Sequence], [DaysOfWeek].[_trackLastWriteTime], [DaysOfWeek].[_trackCreationTime], [DaysOfWeek].[_trackLastWriteUser], [DaysOfWeek].[_trackCreationUser], [DaysOfWeek].[_rowVersion] 
    FROM [DaysOfWeek] 

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DaysOfWeek].[Id], [DaysOfWeek].[Name], [DaysOfWeek].[Value], [DaysOfWeek].[Sequence], [DaysOfWeek].[_trackLastWriteTime], [DaysOfWeek].[_trackCreationTime], [DaysOfWeek].[_trackLastWriteUser], [DaysOfWeek].[_trackCreationUser], [DaysOfWeek].[_rowVersion] 
    FROM [DaysOfWeek] 
    WHERE ([DaysOfWeek].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DaysOfWeek_LoadDaysByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DaysOfWeek].[Id], [DaysOfWeek].[Name], [DaysOfWeek].[Value], [DaysOfWeek].[Sequence], [DaysOfWeek].[_trackLastWriteTime], [DaysOfWeek].[_trackCreationTime], [DaysOfWeek].[_trackLastWriteUser], [DaysOfWeek].[_trackCreationUser], [DaysOfWeek].[_rowVersion] 
    FROM [DaysOfWeek]
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([DaysOfWeek].[Id] = [Event_Days_DaysOfWeek].[Id2])
                LEFT OUTER JOIN [Event] ON ([Event_Days_DaysOfWeek].[Id] = [Event].[Id]) 
    WHERE ([Event_Days_DaysOfWeek].[Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadAllByClub]
(
 @clubid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Club_Id] = @clubid)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadBySupplier]
(
 @SupplierId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Supplier_Id] = @SupplierId)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_LoadOneByInvoiceNo]
(
 @invoiceno [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteUser], [Delivery].[_trackCreationUser], [Delivery].[_rowVersion] 
    FROM [Delivery] 
    WHERE ([Delivery].[InvoiceNumber] = @invoiceno)

RETURN
GO

CREATE PROCEDURE [dbo].[Delivery_UpdateInstanceById]
(
 @id [int],
 @datereceived [datetime],
 @dateregistered [datetime],
 @facilityid [int],
 @gst [decimal] (28, 13),
 @invoiceno [nvarchar] (256),
 @subtotal [decimal] (28, 13),
 @supplierid [int],
 @total [decimal] (28, 13),
 @userid [int]
)
AS
SET NOCOUNT ON
UPDATE 

[Delivery] 

SET 

DateReceived = @datereceived, 
DateRegistered = @dateregistered, 
Facility_Id = @facilityid, 
GST = @gst, 
InvoiceNumber = @invoiceno, 
Subtotal = @subtotal, 
Supplier_id = @supplierid, 
Total = @total, 
User_Id = @userid 

WHERE 

Id = @id
RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadAllByDelivery]
(
 @deliveryid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Delivery_Id] = @deliveryid)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadByDelivery]
(
 @DeliveryId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Delivery_Id] = @DeliveryId)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteUser], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_rowVersion] 
    FROM [DeliveryItem] 
    WHERE ([DeliveryItem].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[DeliveryItem_UpdateInstanceById]
(
 @id [int],
 @discount [decimal] (28, 13),
 @netamount [decimal] (28, 13),
 @quantity [decimal] (28, 13),
 @total [decimal] (28, 13),
 @unitcostprice [decimal] (28, 13),
 @productid [int]
)
AS
SET NOCOUNT ON
UPDATE 

[DeliveryItem] 

SET 

Discount = @discount, 
NetAmount = @netamount, 
Quantity = @quantity, 
Total = @total, 
UnitCostPrice = @unitcostprice, 
Product_Id = @productid 

WHERE 

Id = @id
RETURN
GO

CREATE PROCEDURE [dbo].[Discount_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Discount].[Id], [Discount].[ProductVariant_Id], [Discount].[_trackLastWriteTime], [Discount].[_trackCreationTime], [Discount].[_trackLastWriteUser], [Discount].[_trackCreationUser], [Discount].[_rowVersion] 
    FROM [Discount] 
    WHERE ([Discount].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Discount_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Discount].[Id], [Discount].[ProductVariant_Id], [Discount].[_trackLastWriteTime], [Discount].[_trackCreationTime], [Discount].[_trackLastWriteUser], [Discount].[_trackCreationUser], [Discount].[_rowVersion] 
    FROM [Discount] 

RETURN
GO

CREATE PROCEDURE [dbo].[Discount_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Discount].[Id], [Discount].[ProductVariant_Id], [Discount].[_trackLastWriteTime], [Discount].[_trackCreationTime], [Discount].[_trackLastWriteUser], [Discount].[_trackCreationUser], [Discount].[_rowVersion] 
    FROM [Discount] 
    WHERE ([Discount].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Discount_LoadByProductVariant]
(
 @ProductVariantId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Discount].[Id], [Discount].[ProductVariant_Id], [Discount].[_trackLastWriteTime], [Discount].[_trackCreationTime], [Discount].[_trackLastWriteUser], [Discount].[_trackCreationUser], [Discount].[_rowVersion] 
    FROM [Discount] 
    WHERE ([Discount].[ProductVariant_Id] = @ProductVariantId)

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DiscountGroup].[Id], [DiscountGroup].[Discount_Id], [DiscountGroup].[MembershipType_Id], [DiscountGroup].[_trackLastWriteTime], [DiscountGroup].[_trackCreationTime], [DiscountGroup].[_trackLastWriteUser], [DiscountGroup].[_trackCreationUser], [DiscountGroup].[_rowVersion] 
    FROM [DiscountGroup] 
    WHERE ([DiscountGroup].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DiscountGroup].[Id], [DiscountGroup].[Discount_Id], [DiscountGroup].[MembershipType_Id], [DiscountGroup].[_trackLastWriteTime], [DiscountGroup].[_trackCreationTime], [DiscountGroup].[_trackLastWriteUser], [DiscountGroup].[_trackCreationUser], [DiscountGroup].[_rowVersion] 
    FROM [DiscountGroup] 

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_LoadByDiscount]
(
 @DiscountId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DiscountGroup].[Id], [DiscountGroup].[Discount_Id], [DiscountGroup].[MembershipType_Id], [DiscountGroup].[_trackLastWriteTime], [DiscountGroup].[_trackCreationTime], [DiscountGroup].[_trackLastWriteUser], [DiscountGroup].[_trackCreationUser], [DiscountGroup].[_rowVersion] 
    FROM [DiscountGroup] 
    WHERE ([DiscountGroup].[Discount_Id] = @DiscountId)

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DiscountGroup].[Id], [DiscountGroup].[Discount_Id], [DiscountGroup].[MembershipType_Id], [DiscountGroup].[_trackLastWriteTime], [DiscountGroup].[_trackCreationTime], [DiscountGroup].[_trackLastWriteUser], [DiscountGroup].[_trackCreationUser], [DiscountGroup].[_rowVersion] 
    FROM [DiscountGroup] 
    WHERE ([DiscountGroup].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[DiscountGroup_LoadByMembershipType]
(
 @MembershipTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [DiscountGroup].[Id], [DiscountGroup].[Discount_Id], [DiscountGroup].[MembershipType_Id], [DiscountGroup].[_trackLastWriteTime], [DiscountGroup].[_trackCreationTime], [DiscountGroup].[_trackLastWriteUser], [DiscountGroup].[_trackCreationUser], [DiscountGroup].[_rowVersion] 
    FROM [DiscountGroup] 
    WHERE ([DiscountGroup].[MembershipType_Id] = @MembershipTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadAllByClub]
(
 @clubid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Club_Id] = @clubid)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadAllByFacility]
(
 @facilityid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Facility_Id] = @facilityid)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadAllByUser]
(
 @userid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[User_Id] = @userid)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHire_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteUser], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_rowVersion] 
    FROM [EquipmentHire] 
    WHERE ([EquipmentHire].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadAllByEquipmentHire]
(
 @equipmenthireid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[EquipmentHire_Id] = @equipmenthireid)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadAllByProduct]
(
 @productid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[Product_Id] = @productid)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadByEquipmentHire]
(
 @EquipmentHireId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[EquipmentHire_Id] = @EquipmentHireId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[EquipmentHireItem_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteUser], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_rowVersion] 
    FROM [EquipmentHireItem] 
    WHERE ([EquipmentHireItem].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByAllFilters]
(
 @bookingTypeId [int],
 @coachId [int],
 @dayId [int],
 @levelId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND ([PlayerLevel].[Id] = @levelId))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeCoach]
(
 @bookingTypeId [int],
 @coachId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([Coach].[Id] = @coachId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeCoachDay]
(
 @bookingTypeId [int],
 @coachId [int],
 @dayId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND ([DaysOfWeek].[Id] = @dayId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeCoachLevel]
(
 @bookingTypeId [int],
 @coachId [int],
 @levelId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND ([PlayerLevel].[Id] = @levelId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeDay]
(
 @bookingTypeId [int],
 @dayId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([DaysOfWeek].[Id] = @dayId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeDayLevel]
(
 @bookingTypeId [int],
 @dayId [int],
 @levelId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([DaysOfWeek].[Id] = @dayId) AND ([PlayerLevel].[Id] = @levelId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeId]
(
 @bookingTypeId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE ([BookingType].[Id] = @bookingTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeIds]
(
 @bookingTypeIds [dbo].[cf_type_Event_CountByBookingTypeIds_0] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeIdsCoach]
(
 @coachId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByBookingTypeIdsCoach_1] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeIdsDay]
(
 @dayId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByBookingTypeIdsDay_1] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([DaysOfWeek].[Id] = @dayId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeIdsLevel]
(
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByBookingTypeIdsLevel_1] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByBookingTypeLevel]
(
 @bookingTypeId [int],
 @levelId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([PlayerLevel].[Id] = @levelId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByCoachDay]
(
 @coachId [int],
 @dayId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByCoachDay_2] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByCoachDayLevel]
(
 @coachId [int],
 @dayId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByCoachDayLevel_3] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByCoachLevel]
(
 @coachId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByCoachLevel_2] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
            LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByDayLevel]
(
 @dayId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_CountByDayLevel_2] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
            LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
    LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
            LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([DaysOfWeek].[Id] = @dayId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_CountByTitle]
(
 @searchValue [nvarchar] (256),
 @bookingTypeIds [dbo].[cf_type_Event_CountByTitle_1] READONLY
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT COUNT(*) FROM [Event]
    LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Event].[Title] LIKE @searchValue) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event] 
    WHERE ([Event].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event] 

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByAllFilters]
(
 @bookingTypeId [int],
 @coachId [int],
 @dayId [int],
 @levelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND ([PlayerLevel].[Id] = @levelId))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingType]
(
 @BookingTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event] 
    WHERE ([Event].[BookingType_Id] = @BookingTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeCoach]
(
 @bookingTypeId [int],
 @coachId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([Coach].[Id] = @coachId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeCoachDay]
(
 @bookingTypeId [int],
 @coachId [int],
 @dayId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND ([DaysOfWeek].[Id] = @dayId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeCoachLevel]
(
 @bookingTypeId [int],
 @coachId [int],
 @levelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([Coach].[Id] = @coachId) AND ([PlayerLevel].[Id] = @levelId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeDay]
(
 @bookingTypeId [int],
 @dayId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([DaysOfWeek].[Id] = @dayId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeDayLevel]
(
 @bookingTypeId [int],
 @dayId [int],
 @levelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND (([DaysOfWeek].[Id] = @dayId) AND ([PlayerLevel].[Id] = @levelId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeIds]
(
 @bookingTypeIds [dbo].[cf_type_Event_LoadByBookingTypeIds_0] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeIdsCoach]
(
 @coachId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByBookingTypeIdsCoach_1] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeIdsDays]
(
 @dayId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByBookingTypeIdsDays_1] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([DaysOfWeek].[Id] = @dayId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeIdsLevel]
(
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByBookingTypeIdsLevel_1] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByBookingTypeLevel]
(
 @bookingTypeId [int],
 @levelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id]) 
    WHERE (([BookingType].[Id] = @bookingTypeId) AND ([PlayerLevel].[Id] = @levelId))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event] 
    WHERE ([Event].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByCoachDay]
(
 @coachId [int],
 @dayId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByCoachDay_2] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByCoachDayLevel]
(
 @coachId [int],
 @dayId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByCoachDayLevel_3] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([DaysOfWeek].[Id] = @dayId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByCoachLevel]
(
 @coachId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByCoachLevel_2] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Coaches_Coach] ON ([Event].[Id] = [Event_Coaches_Coach].[Id])
                LEFT OUTER JOIN [Coach] ON ([Event_Coaches_Coach].[Id2] = [Coach].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Coach].[Id] = @coachId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByDayLevel]
(
 @dayId [int],
 @levelId [int],
 @bookingTypeIds [dbo].[cf_type_Event_LoadByDayLevel_2] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [Event_Days_DaysOfWeek] ON ([Event].[Id] = [Event_Days_DaysOfWeek].[Id])
                LEFT OUTER JOIN [DaysOfWeek] ON ([Event_Days_DaysOfWeek].[Id2] = [DaysOfWeek].[Id])
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([Event].[Id] = [Event_Levels_PlayerLevel].[Id])
                LEFT OUTER JOIN [PlayerLevel] ON ([Event_Levels_PlayerLevel].[Id2] = [PlayerLevel].[Id])
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([DaysOfWeek].[Id] = @dayId) AND (([PlayerLevel].[Id] = @levelId) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds)))))

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event] 
    WHERE ([Event].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Event_LoadByTitle]
(
 @searchValue [nvarchar] (256),
 @bookingTypeIds [dbo].[cf_type_Event_LoadByTitle_1] READONLY,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @_c_bookingTypeIds int; SELECT @_c_bookingTypeIds= COUNT(*) FROM @bookingTypeIds
SELECT DISTINCT [Event].[Id], [Event].[Title], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[Club_Id], [Event].[isOnline], [Event].[DailyFee], [Event].[FullFee], [Event].[isActive], [Event].[RegistrationDateTime], [Event].[BookingType_Id], [Event].[hasCapacity], [Event].[FeeOption], [Event].[FeeStructure], [Event].[StartTime], [Event].[EndTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationTime], [Event].[_trackLastWriteUser], [Event].[_trackCreationUser], [Event].[_rowVersion] 
    FROM [Event]
        LEFT OUTER JOIN [BookingType] ON ([Event].[BookingType_Id] = [BookingType].[Id]) 
    WHERE (([Event].[Title] LIKE @searchValue) AND [BookingType].[Id] IN (((SELECT * FROM @bookingTypeIds))))

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking] 
    WHERE ([EventBooking].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadByBookingId]
(
 @bookingId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking]
        LEFT OUTER JOIN [EventBooking_Booking_Booking] ON ([EventBooking].[Id] = [EventBooking_Booking_Booking].[Id])
                LEFT OUTER JOIN [Booking] ON ([EventBooking_Booking_Booking].[Id2] = [Booking].[Id]) 
    WHERE ([Booking].[Id] = @bookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadByDateRange]
(
 @startDate [datetime],
 @endDate [datetime],
 @eventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking]
        LEFT OUTER JOIN [Event] ON ([EventBooking].[Event_Id] = [Event].[Id]) 
    WHERE (([EventBooking].[Date] >= @startDate) AND (([EventBooking].[Date] <= @endDate) AND ([Event].[Id] = @eventId)))

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking] 
    WHERE ([EventBooking].[Event_Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadByEventBookingCancellation]
(
 @EventBookingCancellationId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking] 
    WHERE ([EventBooking].[EventBookingCancellation_Id] = @EventBookingCancellationId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBooking_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Notes], [EventBooking].[EventStatus], [EventBooking].[Date], [EventBooking].[EventBookingCancellation_Id], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteUser], [EventBooking].[_trackCreationUser], [EventBooking].[_rowVersion] 
    FROM [EventBooking] 
    WHERE ([EventBooking].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBookingCancellation].[Id], [EventBookingCancellation].[CancellationType_Id], [EventBookingCancellation].[Description], [EventBookingCancellation].[EventBooking_Id], [EventBookingCancellation].[RefundType], [EventBookingCancellation].[RefundPortion], [EventBookingCancellation].[isVisible], [EventBookingCancellation].[_trackLastWriteTime], [EventBookingCancellation].[_trackCreationTime], [EventBookingCancellation].[_trackLastWriteUser], [EventBookingCancellation].[_trackCreationUser], [EventBookingCancellation].[_rowVersion] 
    FROM [EventBookingCancellation] 
    WHERE ([EventBookingCancellation].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBookingCancellation].[Id], [EventBookingCancellation].[CancellationType_Id], [EventBookingCancellation].[Description], [EventBookingCancellation].[EventBooking_Id], [EventBookingCancellation].[RefundType], [EventBookingCancellation].[RefundPortion], [EventBookingCancellation].[isVisible], [EventBookingCancellation].[_trackLastWriteTime], [EventBookingCancellation].[_trackCreationTime], [EventBookingCancellation].[_trackLastWriteUser], [EventBookingCancellation].[_trackCreationUser], [EventBookingCancellation].[_rowVersion] 
    FROM [EventBookingCancellation] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_LoadByCancellationType]
(
 @CancellationTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBookingCancellation].[Id], [EventBookingCancellation].[CancellationType_Id], [EventBookingCancellation].[Description], [EventBookingCancellation].[EventBooking_Id], [EventBookingCancellation].[RefundType], [EventBookingCancellation].[RefundPortion], [EventBookingCancellation].[isVisible], [EventBookingCancellation].[_trackLastWriteTime], [EventBookingCancellation].[_trackCreationTime], [EventBookingCancellation].[_trackLastWriteUser], [EventBookingCancellation].[_trackCreationUser], [EventBookingCancellation].[_rowVersion] 
    FROM [EventBookingCancellation] 
    WHERE ([EventBookingCancellation].[CancellationType_Id] = @CancellationTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_LoadByEventBooking]
(
 @EventBookingId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBookingCancellation].[Id], [EventBookingCancellation].[CancellationType_Id], [EventBookingCancellation].[Description], [EventBookingCancellation].[EventBooking_Id], [EventBookingCancellation].[RefundType], [EventBookingCancellation].[RefundPortion], [EventBookingCancellation].[isVisible], [EventBookingCancellation].[_trackLastWriteTime], [EventBookingCancellation].[_trackCreationTime], [EventBookingCancellation].[_trackLastWriteUser], [EventBookingCancellation].[_trackCreationUser], [EventBookingCancellation].[_rowVersion] 
    FROM [EventBookingCancellation] 
    WHERE ([EventBookingCancellation].[EventBooking_Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventBookingCancellation_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventBookingCancellation].[Id], [EventBookingCancellation].[CancellationType_Id], [EventBookingCancellation].[Description], [EventBookingCancellation].[EventBooking_Id], [EventBookingCancellation].[RefundType], [EventBookingCancellation].[RefundPortion], [EventBookingCancellation].[isVisible], [EventBookingCancellation].[_trackLastWriteTime], [EventBookingCancellation].[_trackCreationTime], [EventBookingCancellation].[_trackLastWriteUser], [EventBookingCancellation].[_trackCreationUser], [EventBookingCancellation].[_rowVersion] 
    FROM [EventBookingCancellation] 
    WHERE ([EventBookingCancellation].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 
    WHERE ([EventFee].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_LoadByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 
    WHERE ([EventFee].[Event_Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 
    WHERE ([EventFee].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_LoadByMembershipType]
(
 @MembershipTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 
    WHERE ([EventFee].[MembershipType_Id] = @MembershipTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventFee_LoadOneByEventAndMemberShipType]
(
 @eventId [int],
 @membershipTypeId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteUser], [EventFee].[_trackCreationUser], [EventFee].[_rowVersion] 
    FROM [EventFee] 
    WHERE (([EventFee].[Event_Id] = @eventId) AND ([EventFee].[MembershipType_Id] = @membershipTypeId))

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_CountByEventBooking]
(
 @eventBookingId [int],
 @registrationStatus [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [EventRegistration]
    LEFT OUTER JOIN [EventBooking] ON ([EventRegistration].[EventBooking_Id] = [EventBooking].[Id]) 
    WHERE (([EventBooking].[Id] = @eventBookingId) AND ([EventRegistration].[RegistrationStatus] = @registrationStatus))

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_CountByPaidConfirmed]
(
 @eventBookingId [int],
 @isPaid [bit],
 @registrationStatus [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [EventRegistration]
    LEFT OUTER JOIN [EventBooking] ON ([EventRegistration].[EventBooking_Id] = [EventBooking].[Id]) 
    WHERE (([EventBooking].[Id] = @eventBookingId) AND (([EventRegistration].[isPaid] = @isPaid) AND ([EventRegistration].[RegistrationStatus] = @registrationStatus)))

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 
    WHERE ([EventRegistration].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 
    WHERE ([EventRegistration].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 
    WHERE ([EventRegistration].[EventBooking_Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadByEventBookingId]
(
 @eventBookingId [int],
 @registrationStatus [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration]
        LEFT OUTER JOIN [EventBooking] ON ([EventRegistration].[EventBooking_Id] = [EventBooking].[Id]) 
    WHERE (([EventBooking].[Id] = @eventBookingId) AND ([EventRegistration].[RegistrationStatus] = @registrationStatus))

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 
    WHERE ([EventRegistration].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRegistration_LoadByOrderItem]
(
 @OrderItemId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRegistration].[Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Customer_Id], [EventRegistration].[EventBooking_Id], [EventRegistration].[Note], [EventRegistration].[RegistrationStatus], [EventRegistration].[OrderItem_Id], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteUser], [EventRegistration].[_trackCreationUser], [EventRegistration].[_rowVersion] 
    FROM [EventRegistration] 
    WHERE ([EventRegistration].[OrderItem_Id] = @OrderItemId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRoll].[Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[Customer_Id], [EventRoll].[EventBooking_Id], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteUser], [EventRoll].[_trackCreationUser], [EventRoll].[_rowVersion] 
    FROM [EventRoll] 
    WHERE ([EventRoll].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRoll].[Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[Customer_Id], [EventRoll].[EventBooking_Id], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteUser], [EventRoll].[_trackCreationUser], [EventRoll].[_rowVersion] 
    FROM [EventRoll] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRoll].[Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[Customer_Id], [EventRoll].[EventBooking_Id], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteUser], [EventRoll].[_trackCreationUser], [EventRoll].[_rowVersion] 
    FROM [EventRoll] 
    WHERE ([EventRoll].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_LoadByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRoll].[Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[Customer_Id], [EventRoll].[EventBooking_Id], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteUser], [EventRoll].[_trackCreationUser], [EventRoll].[_rowVersion] 
    FROM [EventRoll] 
    WHERE ([EventRoll].[EventBooking_Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventRoll_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventRoll].[Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[Customer_Id], [EventRoll].[EventBooking_Id], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteUser], [EventRoll].[_trackCreationUser], [EventRoll].[_rowVersion] 
    FROM [EventRoll] 
    WHERE ([EventRoll].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventWaitlist].[Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[Customer_Id], [EventWaitlist].[EventBooking_Id], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteUser], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_rowVersion] 
    FROM [EventWaitlist] 
    WHERE ([EventWaitlist].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventWaitlist].[Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[Customer_Id], [EventWaitlist].[EventBooking_Id], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteUser], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_rowVersion] 
    FROM [EventWaitlist] 

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventWaitlist].[Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[Customer_Id], [EventWaitlist].[EventBooking_Id], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteUser], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_rowVersion] 
    FROM [EventWaitlist] 
    WHERE ([EventWaitlist].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_LoadByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventWaitlist].[Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[Customer_Id], [EventWaitlist].[EventBooking_Id], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteUser], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_rowVersion] 
    FROM [EventWaitlist] 
    WHERE ([EventWaitlist].[EventBooking_Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[EventWaitlist_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [EventWaitlist].[Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[Customer_Id], [EventWaitlist].[EventBooking_Id], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteUser], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_rowVersion] 
    FROM [EventWaitlist] 
    WHERE ([EventWaitlist].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadAllByClub]
(
 @clubid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[Club_Id] = @clubid)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Facility_LoadOneByRefNo]
(
 @ref [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[Colour], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteUser], [Facility].[_trackCreationUser], [Facility].[_rowVersion] 
    FROM [Facility] 
    WHERE ([Facility].[ReferenceNo] = @ref)

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Gender].[Id], [Gender].[Name], [Gender].[Sequence], [Gender].[_trackLastWriteTime], [Gender].[_trackCreationTime], [Gender].[_trackLastWriteUser], [Gender].[_trackCreationUser], [Gender].[_rowVersion] 
    FROM [Gender] 
    WHERE ([Gender].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Gender].[Id], [Gender].[Name], [Gender].[Sequence], [Gender].[_trackLastWriteTime], [Gender].[_trackCreationTime], [Gender].[_trackLastWriteUser], [Gender].[_trackCreationUser], [Gender].[_rowVersion] 
    FROM [Gender] 

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Gender].[Id], [Gender].[Name], [Gender].[Sequence], [Gender].[_trackLastWriteTime], [Gender].[_trackCreationTime], [Gender].[_trackLastWriteUser], [Gender].[_trackCreationUser], [Gender].[_rowVersion] 
    FROM [Gender] 
    WHERE ([Gender].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Gender_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Gender].[Id], [Gender].[Name], [Gender].[Sequence], [Gender].[_trackLastWriteTime], [Gender].[_trackCreationTime], [Gender].[_trackLastWriteUser], [Gender].[_trackCreationUser], [Gender].[_rowVersion] 
    FROM [Gender] 
    WHERE ([Gender].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [GenderAge].[Id], [GenderAge].[Name], [GenderAge].[Sequence], [GenderAge].[AgeGroup], [GenderAge].[GenderGroup], [GenderAge].[_trackLastWriteTime], [GenderAge].[_trackCreationTime], [GenderAge].[_trackLastWriteUser], [GenderAge].[_trackCreationUser], [GenderAge].[_rowVersion] 
    FROM [GenderAge] 
    WHERE ([GenderAge].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [GenderAge].[Id], [GenderAge].[Name], [GenderAge].[Sequence], [GenderAge].[AgeGroup], [GenderAge].[GenderGroup], [GenderAge].[_trackLastWriteTime], [GenderAge].[_trackCreationTime], [GenderAge].[_trackLastWriteUser], [GenderAge].[_trackCreationUser], [GenderAge].[_rowVersion] 
    FROM [GenderAge] 

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [GenderAge].[Id], [GenderAge].[Name], [GenderAge].[Sequence], [GenderAge].[AgeGroup], [GenderAge].[GenderGroup], [GenderAge].[_trackLastWriteTime], [GenderAge].[_trackCreationTime], [GenderAge].[_trackLastWriteUser], [GenderAge].[_trackCreationUser], [GenderAge].[_rowVersion] 
    FROM [GenderAge] 
    WHERE ([GenderAge].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[GenderAge_LoadGenderAgesByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [GenderAge].[Id], [GenderAge].[Name], [GenderAge].[Sequence], [GenderAge].[AgeGroup], [GenderAge].[GenderGroup], [GenderAge].[_trackLastWriteTime], [GenderAge].[_trackCreationTime], [GenderAge].[_trackLastWriteUser], [GenderAge].[_trackCreationUser], [GenderAge].[_rowVersion] 
    FROM [GenderAge]
        LEFT OUTER JOIN [Event_GenderAges_GenderAge] ON ([GenderAge].[Id] = [Event_GenderAges_GenderAge].[Id2])
                LEFT OUTER JOIN [Event] ON ([Event_GenderAges_GenderAge].[Id] = [Event].[Id]) 
    WHERE ([Event_GenderAges_GenderAge].[Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadByAdmin]
(
 @AdminId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Admin_Id] = @AdminId)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Invoice_LoadByOrder]
(
 @OrderId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Invoice].[Id], [Invoice].[InvoiceNo], [Invoice].[Customer_Id], [Invoice].[CreationDateUTC], [Invoice].[Admin_Id], [Invoice].[Club_Id], [Invoice].[Order_Id], [Invoice].[Balance], [Invoice].[Change], [Invoice].[AmountPaid], [Invoice].[Note], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteUser], [Invoice].[_trackCreationUser], [Invoice].[_rowVersion] 
    FROM [Invoice] 
    WHERE ([Invoice].[Order_Id] = @OrderId)

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Journal].[Id], [Journal].[JournalType], [Journal].[_trackLastWriteTime], [Journal].[_trackCreationTime], [Journal].[_trackLastWriteUser], [Journal].[_trackCreationUser], [Journal].[_rowVersion] 
    FROM [Journal] 
    WHERE ([Journal].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Journal].[Id], [Journal].[JournalType], [Journal].[_trackLastWriteTime], [Journal].[_trackCreationTime], [Journal].[_trackLastWriteUser], [Journal].[_trackCreationUser], [Journal].[_rowVersion] 
    FROM [Journal] 

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Journal].[Id], [Journal].[JournalType], [Journal].[_trackLastWriteTime], [Journal].[_trackCreationTime], [Journal].[_trackLastWriteUser], [Journal].[_trackCreationUser], [Journal].[_rowVersion] 
    FROM [Journal] 
    WHERE ([Journal].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Journal_LoadJournalsByInvoice]
(
 @InvoiceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Journal].[Id], [Journal].[JournalType], [Journal].[_trackLastWriteTime], [Journal].[_trackCreationTime], [Journal].[_trackLastWriteUser], [Journal].[_trackCreationUser], [Journal].[_rowVersion] 
    FROM [Journal]
        LEFT OUTER JOIN [Invoice_Journals_Journal] ON ([Journal].[Id] = [Invoice_Journals_Journal].[Id2])
                LEFT OUTER JOIN [Invoice] ON ([Invoice_Journals_Journal].[Id] = [Invoice].[Id]) 
    WHERE ([Invoice_Journals_Journal].[Id] = @InvoiceId)

RETURN
GO

CREATE PROCEDURE [dbo].[Member_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member] 
    WHERE ([Member].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member] 

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadByActiveCustomer]
(
 @customerId [int],
 @isActive [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member]
        LEFT OUTER JOIN [Customer] ON ([Member].[Customer_Id] = [Customer].[Id]) 
    WHERE (([Customer].[Id] = @customerId) AND ([Member].[isActive] = @isActive))

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member] 
    WHERE ([Member].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member] 
    WHERE ([Member].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadByMembershipType]
(
 @MembershipTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member] 
    WHERE ([Member].[MembershipType_Id] = @MembershipTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Member_LoadOneByCustomerId]
(
 @customerId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Member].[Id], [Member].[StartDateUTC], [Member].[EndDateUTC], [Member].[MembershipType_Id], [Member].[Customer_Id], [Member].[isActive], [Member].[_trackLastWriteTime], [Member].[_trackCreationTime], [Member].[_trackLastWriteUser], [Member].[_trackCreationUser], [Member].[_rowVersion] 
    FROM [Member]
        LEFT OUTER JOIN [Customer] ON ([Member].[Customer_Id] = [Customer].[Id]) 
    WHERE ([Customer].[Id] = @customerId)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE ([MembershipType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadAllByClub]
(
 @clubid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE ([MembershipType].[Club_Id] = @clubid)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadAllByClubIsActive]
(
 @clubId [int],
 @isActive [bit],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE (([MembershipType].[Club_Id] = @clubId) AND ([MembershipType].[IsActive] = @isActive))

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE ([MembershipType].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE ([MembershipType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadDefaultMembershipType]
(
 @clubId [int],
 @isDefault [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType]
        LEFT OUTER JOIN [Club] ON ([MembershipType].[Club_Id] = [Club].[Id]) 
    WHERE (([MembershipType].[isDefault] = @isDefault) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipType_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteUser], [MembershipType].[_trackCreationUser], [MembershipType].[_rowVersion] 
    FROM [MembershipType] 
    WHERE ([MembershipType].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE ([MembershipTypeTimePeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE ([MembershipTypeTimePeriod].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByClubAndIsWeekday]
(
 @clubId [int],
 @isWeekday [bit],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE (([MembershipTypeTimePeriod].[Club_Id] = @clubId) AND ([MembershipTypeTimePeriod].[isWeekday] = @isWeekday))

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE ([MembershipTypeTimePeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipType]
(
 @MembershipTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE ([MembershipTypeTimePeriod].[MembershipType_Id] = @MembershipTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipTypeAndIsWeekday]
(
 @membershipTypeId [int],
 @isWeekday [bit]
)
AS
SET NOCOUNT ON
SELECT * FROM [MembershipTypeTimePeriod] 

WHERE MembershipType_Id = @membershipTypeId AND isWeekday = @isWeekday
RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByMembershipTypePeriodZoneDayOfRate]
(
 @membershipTypeId [int],
 @dayOfRate [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE (([MembershipTypeTimePeriod].[MembershipType_Id] = @membershipTypeId) AND ([MembershipTypeTimePeriod].[DayOfRate] = @dayOfRate))

RETURN
GO

CREATE PROCEDURE [dbo].[MembershipTypeTimePeriod_LoadByTimePeriodGroup]
(
 @TimePeriodGroupId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteUser], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_rowVersion] 
    FROM [MembershipTypeTimePeriod] 
    WHERE ([MembershipTypeTimePeriod].[TimePeriodGroup_Id] = @TimePeriodGroupId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageAccount].[Id], [MessageAccount].[Club_Id], [MessageAccount].[Name], [MessageAccount].[Reference], [MessageAccount].[Token], [MessageAccount].[_trackLastWriteTime], [MessageAccount].[_trackCreationTime], [MessageAccount].[_trackLastWriteUser], [MessageAccount].[_trackCreationUser], [MessageAccount].[_rowVersion] 
    FROM [MessageAccount] 
    WHERE ([MessageAccount].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageAccount].[Id], [MessageAccount].[Club_Id], [MessageAccount].[Name], [MessageAccount].[Reference], [MessageAccount].[Token], [MessageAccount].[_trackLastWriteTime], [MessageAccount].[_trackCreationTime], [MessageAccount].[_trackLastWriteUser], [MessageAccount].[_trackCreationUser], [MessageAccount].[_rowVersion] 
    FROM [MessageAccount] 

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageAccount].[Id], [MessageAccount].[Club_Id], [MessageAccount].[Name], [MessageAccount].[Reference], [MessageAccount].[Token], [MessageAccount].[_trackLastWriteTime], [MessageAccount].[_trackCreationTime], [MessageAccount].[_trackLastWriteUser], [MessageAccount].[_trackCreationUser], [MessageAccount].[_rowVersion] 
    FROM [MessageAccount] 
    WHERE ([MessageAccount].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageAccount_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageAccount].[Id], [MessageAccount].[Club_Id], [MessageAccount].[Name], [MessageAccount].[Reference], [MessageAccount].[Token], [MessageAccount].[_trackLastWriteTime], [MessageAccount].[_trackCreationTime], [MessageAccount].[_trackLastWriteUser], [MessageAccount].[_trackCreationUser], [MessageAccount].[_rowVersion] 
    FROM [MessageAccount] 
    WHERE ([MessageAccount].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_CountByClub]
(
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [MessageLog]
    LEFT OUTER JOIN [MessageNumber] ON ([MessageLog].[MessageNumber_Id] = [MessageNumber].[Id])
            LEFT OUTER JOIN [MessageService] ON ([MessageNumber].[MessageService_Id] = [MessageService].[Id])
                    LEFT OUTER JOIN [MessageAccount] ON ([MessageService].[MessageAccount_Id] = [MessageAccount].[Id])
                            LEFT OUTER JOIN [Club] ON ([MessageAccount].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 
    WHERE ([MessageLog].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadByAdmin]
(
 @AdminId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 
    WHERE ([MessageLog].[Admin_Id] = @AdminId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadByClub]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog]
        LEFT OUTER JOIN [MessageNumber] ON ([MessageLog].[MessageNumber_Id] = [MessageNumber].[Id])
                LEFT OUTER JOIN [MessageService] ON ([MessageNumber].[MessageService_Id] = [MessageService].[Id])
                        LEFT OUTER JOIN [MessageAccount] ON ([MessageService].[MessageAccount_Id] = [MessageAccount].[Id])
                                LEFT OUTER JOIN [Club] ON ([MessageAccount].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 
    WHERE ([MessageLog].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadByDateRange]
(
 @startDate [datetime],
 @endDate [datetime],
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog]
        LEFT OUTER JOIN [MessageNumber] ON ([MessageLog].[MessageNumber_Id] = [MessageNumber].[Id])
                LEFT OUTER JOIN [MessageService] ON ([MessageNumber].[MessageService_Id] = [MessageService].[Id])
                        LEFT OUTER JOIN [MessageAccount] ON ([MessageService].[MessageAccount_Id] = [MessageAccount].[Id])
                                LEFT OUTER JOIN [Club] ON ([MessageAccount].[Club_Id] = [Club].[Id]) 
    WHERE (([MessageLog].[DateSent] >= @startDate) AND (([MessageLog].[DateSent] <= @endDate) AND ([Club].[Id] = @clubId)))

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 
    WHERE ([MessageLog].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageLog_LoadByMessageNumber]
(
 @MessageNumberId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageLog].[Id], [MessageLog].[Body], [MessageLog].[Direction], [MessageLog].[Status], [MessageLog].[ErrorMessage], [MessageLog].[Price], [MessageLog].[Admin_Id], [MessageLog].[Customer_Id], [MessageLog].[Reference], [MessageLog].[From], [MessageLog].[To], [MessageLog].[DateSent], [MessageLog].[MessageNumber_Id], [MessageLog].[_trackLastWriteTime], [MessageLog].[_trackCreationTime], [MessageLog].[_trackLastWriteUser], [MessageLog].[_trackCreationUser], [MessageLog].[_rowVersion] 
    FROM [MessageLog] 
    WHERE ([MessageLog].[MessageNumber_Id] = @MessageNumberId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_GetNumber]
(
 @clubId [int],
 @isDefault [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageNumber].[Id], [MessageNumber].[PhoneNumber], [MessageNumber].[Location], [MessageNumber].[Reference], [MessageNumber].[MessageService_Id], [MessageNumber].[IsDefault], [MessageNumber].[_trackLastWriteTime], [MessageNumber].[_trackCreationTime], [MessageNumber].[_trackLastWriteUser], [MessageNumber].[_trackCreationUser], [MessageNumber].[_rowVersion] 
    FROM [MessageNumber]
        LEFT OUTER JOIN [MessageService] ON ([MessageNumber].[MessageService_Id] = [MessageService].[Id])
                LEFT OUTER JOIN [MessageAccount] ON ([MessageService].[MessageAccount_Id] = [MessageAccount].[Id])
                        LEFT OUTER JOIN [Club] ON ([MessageAccount].[Club_Id] = [Club].[Id]) 
    WHERE (([MessageNumber].[IsDefault] = @isDefault) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageNumber].[Id], [MessageNumber].[PhoneNumber], [MessageNumber].[Location], [MessageNumber].[Reference], [MessageNumber].[MessageService_Id], [MessageNumber].[IsDefault], [MessageNumber].[_trackLastWriteTime], [MessageNumber].[_trackCreationTime], [MessageNumber].[_trackLastWriteUser], [MessageNumber].[_trackCreationUser], [MessageNumber].[_rowVersion] 
    FROM [MessageNumber] 
    WHERE ([MessageNumber].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageNumber].[Id], [MessageNumber].[PhoneNumber], [MessageNumber].[Location], [MessageNumber].[Reference], [MessageNumber].[MessageService_Id], [MessageNumber].[IsDefault], [MessageNumber].[_trackLastWriteTime], [MessageNumber].[_trackCreationTime], [MessageNumber].[_trackLastWriteUser], [MessageNumber].[_trackCreationUser], [MessageNumber].[_rowVersion] 
    FROM [MessageNumber] 

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageNumber].[Id], [MessageNumber].[PhoneNumber], [MessageNumber].[Location], [MessageNumber].[Reference], [MessageNumber].[MessageService_Id], [MessageNumber].[IsDefault], [MessageNumber].[_trackLastWriteTime], [MessageNumber].[_trackCreationTime], [MessageNumber].[_trackLastWriteUser], [MessageNumber].[_trackCreationUser], [MessageNumber].[_rowVersion] 
    FROM [MessageNumber] 
    WHERE ([MessageNumber].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageNumber_LoadByMessageService]
(
 @MessageServiceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageNumber].[Id], [MessageNumber].[PhoneNumber], [MessageNumber].[Location], [MessageNumber].[Reference], [MessageNumber].[MessageService_Id], [MessageNumber].[IsDefault], [MessageNumber].[_trackLastWriteTime], [MessageNumber].[_trackCreationTime], [MessageNumber].[_trackLastWriteUser], [MessageNumber].[_trackCreationUser], [MessageNumber].[_rowVersion] 
    FROM [MessageNumber] 
    WHERE ([MessageNumber].[MessageService_Id] = @MessageServiceId)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageService].[Id], [MessageService].[Name], [MessageService].[Reference], [MessageService].[MessageAccount_Id], [MessageService].[AlphaId], [MessageService].[_trackLastWriteTime], [MessageService].[_trackCreationTime], [MessageService].[_trackLastWriteUser], [MessageService].[_trackCreationUser], [MessageService].[_rowVersion] 
    FROM [MessageService] 
    WHERE ([MessageService].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageService].[Id], [MessageService].[Name], [MessageService].[Reference], [MessageService].[MessageAccount_Id], [MessageService].[AlphaId], [MessageService].[_trackLastWriteTime], [MessageService].[_trackCreationTime], [MessageService].[_trackLastWriteUser], [MessageService].[_trackCreationUser], [MessageService].[_rowVersion] 
    FROM [MessageService] 

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageService].[Id], [MessageService].[Name], [MessageService].[Reference], [MessageService].[MessageAccount_Id], [MessageService].[AlphaId], [MessageService].[_trackLastWriteTime], [MessageService].[_trackCreationTime], [MessageService].[_trackLastWriteUser], [MessageService].[_trackCreationUser], [MessageService].[_rowVersion] 
    FROM [MessageService] 
    WHERE ([MessageService].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[MessageService_LoadByMessageAccount]
(
 @MessageAccountId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [MessageService].[Id], [MessageService].[Name], [MessageService].[Reference], [MessageService].[MessageAccount_Id], [MessageService].[AlphaId], [MessageService].[_trackLastWriteTime], [MessageService].[_trackCreationTime], [MessageService].[_trackLastWriteUser], [MessageService].[_trackCreationUser], [MessageService].[_rowVersion] 
    FROM [MessageService] 
    WHERE ([MessageService].[MessageAccount_Id] = @MessageAccountId)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 
    WHERE ([Order].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadByAdmin]
(
 @AdminId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 
    WHERE ([Order].[Admin_Id] = @AdminId)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 
    WHERE ([Order].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 
    WHERE ([Order].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order] 
    WHERE ([Order].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Order_LoadByStatus]
(
 @customerId [int],
 @orderStatus [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Order].[Id], [Order].[OrderStatus], [Order].[Discount], [Order].[Description], [Order].[Note], [Order].[OrderDateUTC], [Order].[Subtotal], [Order].[Tax], [Order].[Total], [Order].[Customer_Id], [Order].[Admin_Id], [Order].[Club_Id], [Order].[_trackLastWriteTime], [Order].[_trackCreationTime], [Order].[_trackLastWriteUser], [Order].[_trackCreationUser], [Order].[_rowVersion] 
    FROM [Order]
        LEFT OUTER JOIN [Customer] ON ([Order].[Customer_Id] = [Customer].[Id]) 
    WHERE (([Customer].[Id] = @customerId) AND ([Order].[OrderStatus] = @orderStatus))

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByBooking]
(
 @BookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Booking_Id] = @BookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByMembership]
(
 @MembershipId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Membership_Id] = @MembershipId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByOrder]
(
 @OrderId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Order_Id] = @OrderId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByProductVariant]
(
 @ProductVariantId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[ProductVariant_Id] = @ProductVariantId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByRegistration]
(
 @RegistrationId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem] 
    WHERE ([OrderItem].[Registration_Id] = @RegistrationId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItem_LoadByStatus]
(
 @customerId [int],
 @orderItemStatus [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItem].[Id], [OrderItem].[isPaid], [OrderItem].[Quantity], [OrderItem].[Booking_Id], [OrderItem].[CreationDateUTC], [OrderItem].[Metadata], [OrderItem].[Amount], [OrderItem].[Registration_Id], [OrderItem].[Membership_Id], [OrderItem].[Note], [OrderItem].[ProductVariant_Id], [OrderItem].[AdjustType], [OrderItem].[AdjustAmount], [OrderItem].[Order_Id], [OrderItem].[OrderItemType], [OrderItem].[OrderItemStatus], [OrderItem].[Customer_Id], [OrderItem].[Name], [OrderItem].[_trackLastWriteTime], [OrderItem].[_trackCreationTime], [OrderItem].[_trackLastWriteUser], [OrderItem].[_trackCreationUser], [OrderItem].[_rowVersion] 
    FROM [OrderItem]
        LEFT OUTER JOIN [Customer] ON ([OrderItem].[Customer_Id] = [Customer].[Id]) 
    WHERE (([Customer].[Id] = @customerId) AND ([OrderItem].[OrderItemStatus] = @orderItemStatus))

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItemDetail].[Id], [OrderItemDetail].[Name], [OrderItemDetail].[Sequence], [OrderItemDetail].[OrderItem_Id], [OrderItemDetail].[_trackLastWriteTime], [OrderItemDetail].[_trackCreationTime], [OrderItemDetail].[_trackLastWriteUser], [OrderItemDetail].[_trackCreationUser], [OrderItemDetail].[_rowVersion] 
    FROM [OrderItemDetail] 
    WHERE ([OrderItemDetail].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItemDetail].[Id], [OrderItemDetail].[Name], [OrderItemDetail].[Sequence], [OrderItemDetail].[OrderItem_Id], [OrderItemDetail].[_trackLastWriteTime], [OrderItemDetail].[_trackCreationTime], [OrderItemDetail].[_trackLastWriteUser], [OrderItemDetail].[_trackCreationUser], [OrderItemDetail].[_rowVersion] 
    FROM [OrderItemDetail] 

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItemDetail].[Id], [OrderItemDetail].[Name], [OrderItemDetail].[Sequence], [OrderItemDetail].[OrderItem_Id], [OrderItemDetail].[_trackLastWriteTime], [OrderItemDetail].[_trackCreationTime], [OrderItemDetail].[_trackLastWriteUser], [OrderItemDetail].[_trackCreationUser], [OrderItemDetail].[_rowVersion] 
    FROM [OrderItemDetail] 
    WHERE ([OrderItemDetail].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_LoadByOrderItem]
(
 @OrderItemId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItemDetail].[Id], [OrderItemDetail].[Name], [OrderItemDetail].[Sequence], [OrderItemDetail].[OrderItem_Id], [OrderItemDetail].[_trackLastWriteTime], [OrderItemDetail].[_trackCreationTime], [OrderItemDetail].[_trackLastWriteUser], [OrderItemDetail].[_trackCreationUser], [OrderItemDetail].[_rowVersion] 
    FROM [OrderItemDetail] 
    WHERE ([OrderItemDetail].[OrderItem_Id] = @OrderItemId)

RETURN
GO

CREATE PROCEDURE [dbo].[OrderItemDetail_LoadDetailsByOrderItem]
(
 @orderItemId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [OrderItemDetail].[Id], [OrderItemDetail].[Name], [OrderItemDetail].[Sequence], [OrderItemDetail].[OrderItem_Id], [OrderItemDetail].[_trackLastWriteTime], [OrderItemDetail].[_trackCreationTime], [OrderItemDetail].[_trackLastWriteUser], [OrderItemDetail].[_trackCreationUser], [OrderItemDetail].[_rowVersion] 
    FROM [OrderItemDetail]
        LEFT OUTER JOIN [OrderItem] ON ([OrderItemDetail].[OrderItem_Id] = [OrderItem].[Id]) 
    WHERE ([OrderItem].[Id] = @orderItemId)
    ORDER BY [OrderItemDetail].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel] 
    WHERE ([PlayerLevel].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel] 

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel] 
    WHERE ([PlayerLevel].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel] 
    WHERE ([PlayerLevel].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadByView]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [vPlayerLevelPlayerLevelView].[Id], [vPlayerLevelPlayerLevelView].[Name], [vPlayerLevelPlayerLevelView].[PlayerRatingName], [vPlayerLevelPlayerLevelView].[_rowVersion], [vPlayerLevelPlayerLevelView].[_trackCreationTime], [vPlayerLevelPlayerLevelView].[_trackLastWriteTime], [vPlayerLevelPlayerLevelView].[_trackCreationUser], [vPlayerLevelPlayerLevelView].[_trackLastWriteUser] 
    FROM [vPlayerLevelPlayerLevelView]
        INNER JOIN [PlayerLevel] ON ([vPlayerLevelPlayerLevelView].[Id] = [PlayerLevel].[Id])
                LEFT OUTER JOIN [Club] ON ([PlayerLevel].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadLevelsByEvent]
(
 @EventId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel]
        LEFT OUTER JOIN [Event_Levels_PlayerLevel] ON ([PlayerLevel].[Id] = [Event_Levels_PlayerLevel].[Id2])
                LEFT OUTER JOIN [Event] ON ([Event_Levels_PlayerLevel].[Id] = [Event].[Id]) 
    WHERE ([Event_Levels_PlayerLevel].[Id] = @EventId)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadLevelsByEventBooking]
(
 @EventBookingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel]
        LEFT OUTER JOIN [EventBooking_Levels_PlayerLevel] ON ([PlayerLevel].[Id] = [EventBooking_Levels_PlayerLevel].[Id2])
                LEFT OUTER JOIN [EventBooking] ON ([EventBooking_Levels_PlayerLevel].[Id] = [EventBooking].[Id]) 
    WHERE ([EventBooking_Levels_PlayerLevel].[Id] = @EventBookingId)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerLevel_LoadPlayerLevelsPlayerRatingsByPlayerRating]
(
 @PlayerRatingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[Club_Id], [PlayerLevel].[Description], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteUser], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_rowVersion] 
    FROM [PlayerLevel]
        LEFT OUTER JOIN [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] ON ([PlayerLevel].[Id] = [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id])
                LEFT OUTER JOIN [PlayerRating] ON ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2] = [PlayerRating].[Id]) 
    WHERE ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2] = @PlayerRatingId)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[Description], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteUser], [PlayerRating].[_trackCreationUser], [PlayerRating].[_rowVersion] 
    FROM [PlayerRating] 
    WHERE ([PlayerRating].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[Description], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteUser], [PlayerRating].[_trackCreationUser], [PlayerRating].[_rowVersion] 
    FROM [PlayerRating] 

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[Description], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteUser], [PlayerRating].[_trackCreationUser], [PlayerRating].[_rowVersion] 
    FROM [PlayerRating] 
    WHERE ([PlayerRating].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[Description], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteUser], [PlayerRating].[_trackCreationUser], [PlayerRating].[_rowVersion] 
    FROM [PlayerRating] 
    WHERE ([PlayerRating].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[PlayerRating_LoadPlayerRatingsPlayerLevelsByPlayerLevel]
(
 @PlayerLevelId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[Description], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteUser], [PlayerRating].[_trackCreationUser], [PlayerRating].[_rowVersion] 
    FROM [PlayerRating]
        LEFT OUTER JOIN [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] ON ([PlayerRating].[Id] = [PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id2])
                LEFT OUTER JOIN [PlayerLevel] ON ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] = [PlayerLevel].[Id]) 
    WHERE ([PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels].[Id] = @PlayerLevelId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByAllFilters]
(
 @productTypeId [int],
 @productVendorId [int],
 @productBrandId [int],
 @productClassId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id])
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductVendor].[Id] = @productVendorId) AND (([ProductBrand].[Id] = @productBrandId) AND ([ProductClass].[Id] = @productClassId))))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByClub]
(
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductBrandId]
(
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE ([ProductBrand].[Id] = @productBrandId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductClassId]
(
 @productClassId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id]) 
    WHERE ([ProductClass].[Id] = @productClassId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductClassProductBrand]
(
 @productClassId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductClassProductVendor]
(
 @productClassId [int],
 @productVendorId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND ([ProductVendor].[Id] = @productVendorId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductClassProductVendorProductBrand]
(
 @productClassId [int],
 @productVendorId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeId]
(
 @productTypeId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id]) 
    WHERE ([ProductType].[Id] = @productTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductBrand]
(
 @productTypeId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductClass]
(
 @productTypeId [int],
 @productClassId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductClass].[Id] = @productClassId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductClassProductBrand]
(
 @productTypeId [int],
 @productClassId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductClass].[Id] = @productClassId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductClassProductVendor]
(
 @productTypeId [int],
 @productClassId [int],
 @productVendorId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductClass].[Id] = @productClassId) AND ([ProductVendor].[Id] = @productVendorId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductVendor]
(
 @productTypeId [int],
 @productVendorId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductVendor].[Id] = @productVendorId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductTypeProductVendorProductBrand]
(
 @productTypeId [int],
 @productVendorId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductVendorId]
(
 @productVendorId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE ([ProductVendor].[Id] = @productVendorId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByProductVendorProductBrand]
(
 @productVendorId [int],
 @productBrandId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
    LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_CountByTitle]
(
 @title [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [Product]
    LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE (([Product].[Title] LIKE @title) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByAllFilters]
(
 @productTypeId [int],
 @productVendorId [int],
 @productBrandId [int],
 @productClassId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id])
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductVendor].[Id] = @productVendorId) AND (([ProductBrand].[Id] = @productBrandId) AND ([ProductClass].[Id] = @productClassId))))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductBrand]
(
 @ProductBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[ProductBrand_Id] = @ProductBrandId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductClass]
(
 @ProductClassId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[ProductClass_Id] = @ProductClassId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductClassProductBrand]
(
 @productClassId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductClassProductVendor]
(
 @productClassId [int],
 @productVendorId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND ([ProductVendor].[Id] = @productVendorId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductClassProductVendorProductBrand]
(
 @productClassId [int],
 @productVendorId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductClass].[Id] = @productClassId) AND (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductOption]
(
 @ProductOptionId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[ProductOption_Id] = @ProductOptionId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductType]
(
 @ProductTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[ProductType_Id] = @ProductTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductBrand]
(
 @productTypeId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductClass]
(
 @productTypeId [int],
 @productClassId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductClass].[Id] = @productClassId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductClassProductBrand]
(
 @productTypeId [int],
 @productClassId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductClass].[Id] = @productClassId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductClassProductVendor]
(
 @productTypeId [int],
 @productClassId [int],
 @productVendorId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductClass] ON ([Product].[ProductClass_Id] = [ProductClass].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductClass].[Id] = @productClassId) AND ([ProductVendor].[Id] = @productVendorId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductVendor]
(
 @productTypeId [int],
 @productVendorId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND ([ProductVendor].[Id] = @productVendorId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductTypeProductVendorProductBrand]
(
 @productTypeId [int],
 @productVendorId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductType] ON ([Product].[ProductType_Id] = [ProductType].[Id])
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductType].[Id] = @productTypeId) AND (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId)))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductVendor]
(
 @ProductVendorId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product] 
    WHERE ([Product].[ProductVendor_Id] = @ProductVendorId)

RETURN
GO

CREATE PROCEDURE [dbo].[Product_LoadByProductVendorProductBrand]
(
 @productVendorId [int],
 @productBrandId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [ProductVendor] ON ([Product].[ProductVendor_Id] = [ProductVendor].[Id])
        LEFT OUTER JOIN [ProductBrand] ON ([Product].[ProductBrand_Id] = [ProductBrand].[Id]) 
    WHERE (([ProductVendor].[Id] = @productVendorId) AND ([ProductBrand].[Id] = @productBrandId))

RETURN
GO

CREATE PROCEDURE [dbo].[Product_SearchByTitle]
(
 @title [nvarchar] (256),
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Product].[Id], [Product].[ProductType_Id], [Product].[Description], [Product].[Name], [Product].[Club_Id], [Product].[includeGST], [Product].[hasVariant], [Product].[Title], [Product].[PhotoUrl], [Product].[ProductStatus], [Product].[ProductVendor_Id], [Product].[Quantity], [Product].[hasImages], [Product].[Reference], [Product].[Tags], [Product].[ProductOption_Id], [Product].[VariantCount], [Product].[hasMultiple], [Product].[ProductBrand_Id], [Product].[ProductClass_Id], [Product].[InventoryOption], [Product].[hasDiscount], [Product].[hasMemberPrice], [Product].[Taxable], [Product].[_trackLastWriteTime], [Product].[_trackCreationTime], [Product].[_trackLastWriteUser], [Product].[_trackCreationUser], [Product].[_rowVersion] 
    FROM [Product]
        LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE (([Product].[Title] LIKE @title) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Club_Id], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteUser], [ProductBrand].[_trackCreationUser], [ProductBrand].[_rowVersion] 
    FROM [ProductBrand] 
    WHERE ([ProductBrand].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Club_Id], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteUser], [ProductBrand].[_trackCreationUser], [ProductBrand].[_rowVersion] 
    FROM [ProductBrand] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Club_Id], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteUser], [ProductBrand].[_trackCreationUser], [ProductBrand].[_rowVersion] 
    FROM [ProductBrand] 
    WHERE ([ProductBrand].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Club_Id], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteUser], [ProductBrand].[_trackCreationUser], [ProductBrand].[_rowVersion] 
    FROM [ProductBrand] 
    WHERE ([ProductBrand].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductBrand_LoadOneByNameClub]
(
 @name [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Club_Id], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteUser], [ProductBrand].[_trackCreationUser], [ProductBrand].[_rowVersion] 
    FROM [ProductBrand]
        LEFT OUTER JOIN [Club] ON ([ProductBrand].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductBrand].[Name] = @name) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductClass].[Id], [ProductClass].[Name], [ProductClass].[Club_Id], [ProductClass].[_trackLastWriteTime], [ProductClass].[_trackCreationTime], [ProductClass].[_trackLastWriteUser], [ProductClass].[_trackCreationUser], [ProductClass].[_rowVersion] 
    FROM [ProductClass] 
    WHERE ([ProductClass].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductClass].[Id], [ProductClass].[Name], [ProductClass].[Club_Id], [ProductClass].[_trackLastWriteTime], [ProductClass].[_trackCreationTime], [ProductClass].[_trackLastWriteUser], [ProductClass].[_trackCreationUser], [ProductClass].[_rowVersion] 
    FROM [ProductClass] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductClass].[Id], [ProductClass].[Name], [ProductClass].[Club_Id], [ProductClass].[_trackLastWriteTime], [ProductClass].[_trackCreationTime], [ProductClass].[_trackLastWriteUser], [ProductClass].[_trackCreationUser], [ProductClass].[_rowVersion] 
    FROM [ProductClass] 
    WHERE ([ProductClass].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductClass].[Id], [ProductClass].[Name], [ProductClass].[Club_Id], [ProductClass].[_trackLastWriteTime], [ProductClass].[_trackCreationTime], [ProductClass].[_trackLastWriteUser], [ProductClass].[_trackCreationUser], [ProductClass].[_rowVersion] 
    FROM [ProductClass] 
    WHERE ([ProductClass].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductClass_LoadOneByNameClub]
(
 @name [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductClass].[Id], [ProductClass].[Name], [ProductClass].[Club_Id], [ProductClass].[_trackLastWriteTime], [ProductClass].[_trackCreationTime], [ProductClass].[_trackLastWriteUser], [ProductClass].[_trackCreationUser], [ProductClass].[_rowVersion] 
    FROM [ProductClass]
        LEFT OUTER JOIN [Club] ON ([ProductClass].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductClass].[Name] = @name) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_CountProductImages]
(
 @productId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [ProductImage]
    LEFT OUTER JOIN [Product] ON ([ProductImage].[Product_Id] = [Product].[Id]) 
    WHERE ([Product].[Id] = @productId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductImage].[Id], [ProductImage].[Reference], [ProductImage].[Name], [ProductImage].[Product_Id], [ProductImage].[Url], [ProductImage].[isDefault], [ProductImage].[Sequence], [ProductImage].[_trackLastWriteTime], [ProductImage].[_trackCreationTime], [ProductImage].[_trackLastWriteUser], [ProductImage].[_trackCreationUser], [ProductImage].[_rowVersion] 
    FROM [ProductImage] 
    WHERE ([ProductImage].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductImage].[Id], [ProductImage].[Reference], [ProductImage].[Name], [ProductImage].[Product_Id], [ProductImage].[Url], [ProductImage].[isDefault], [ProductImage].[Sequence], [ProductImage].[_trackLastWriteTime], [ProductImage].[_trackCreationTime], [ProductImage].[_trackLastWriteUser], [ProductImage].[_trackCreationUser], [ProductImage].[_rowVersion] 
    FROM [ProductImage] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductImage].[Id], [ProductImage].[Reference], [ProductImage].[Name], [ProductImage].[Product_Id], [ProductImage].[Url], [ProductImage].[isDefault], [ProductImage].[Sequence], [ProductImage].[_trackLastWriteTime], [ProductImage].[_trackCreationTime], [ProductImage].[_trackLastWriteUser], [ProductImage].[_trackCreationUser], [ProductImage].[_rowVersion] 
    FROM [ProductImage] 
    WHERE ([ProductImage].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductImage_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductImage].[Id], [ProductImage].[Reference], [ProductImage].[Name], [ProductImage].[Product_Id], [ProductImage].[Url], [ProductImage].[isDefault], [ProductImage].[Sequence], [ProductImage].[_trackLastWriteTime], [ProductImage].[_trackCreationTime], [ProductImage].[_trackLastWriteUser], [ProductImage].[_trackCreationUser], [ProductImage].[_rowVersion] 
    FROM [ProductImage] 
    WHERE ([ProductImage].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductInventory].[Id], [ProductInventory].[AdjustAmount], [ProductInventory].[AdjustDateTimeUTC], [ProductInventory].[InventoryAdjustType], [ProductInventory].[ProductVariant_Id], [ProductInventory].[_trackLastWriteTime], [ProductInventory].[_trackCreationTime], [ProductInventory].[_trackLastWriteUser], [ProductInventory].[_trackCreationUser], [ProductInventory].[_rowVersion] 
    FROM [ProductInventory] 
    WHERE ([ProductInventory].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductInventory].[Id], [ProductInventory].[AdjustAmount], [ProductInventory].[AdjustDateTimeUTC], [ProductInventory].[InventoryAdjustType], [ProductInventory].[ProductVariant_Id], [ProductInventory].[_trackLastWriteTime], [ProductInventory].[_trackCreationTime], [ProductInventory].[_trackLastWriteUser], [ProductInventory].[_trackCreationUser], [ProductInventory].[_rowVersion] 
    FROM [ProductInventory] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductInventory].[Id], [ProductInventory].[AdjustAmount], [ProductInventory].[AdjustDateTimeUTC], [ProductInventory].[InventoryAdjustType], [ProductInventory].[ProductVariant_Id], [ProductInventory].[_trackLastWriteTime], [ProductInventory].[_trackCreationTime], [ProductInventory].[_trackLastWriteUser], [ProductInventory].[_trackCreationUser], [ProductInventory].[_rowVersion] 
    FROM [ProductInventory] 
    WHERE ([ProductInventory].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductInventory_LoadByProductVariant]
(
 @ProductVariantId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductInventory].[Id], [ProductInventory].[AdjustAmount], [ProductInventory].[AdjustDateTimeUTC], [ProductInventory].[InventoryAdjustType], [ProductInventory].[ProductVariant_Id], [ProductInventory].[_trackLastWriteTime], [ProductInventory].[_trackCreationTime], [ProductInventory].[_trackLastWriteUser], [ProductInventory].[_trackCreationUser], [ProductInventory].[_rowVersion] 
    FROM [ProductInventory] 
    WHERE ([ProductInventory].[ProductVariant_Id] = @ProductVariantId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductOption].[Id], [ProductOption].[Name], [ProductOption].[Club_Id], [ProductOption].[_trackLastWriteTime], [ProductOption].[_trackCreationTime], [ProductOption].[_trackLastWriteUser], [ProductOption].[_trackCreationUser], [ProductOption].[_rowVersion] 
    FROM [ProductOption] 
    WHERE ([ProductOption].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductOption].[Id], [ProductOption].[Name], [ProductOption].[Club_Id], [ProductOption].[_trackLastWriteTime], [ProductOption].[_trackCreationTime], [ProductOption].[_trackLastWriteUser], [ProductOption].[_trackCreationUser], [ProductOption].[_rowVersion] 
    FROM [ProductOption] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductOption].[Id], [ProductOption].[Name], [ProductOption].[Club_Id], [ProductOption].[_trackLastWriteTime], [ProductOption].[_trackCreationTime], [ProductOption].[_trackLastWriteUser], [ProductOption].[_trackCreationUser], [ProductOption].[_rowVersion] 
    FROM [ProductOption] 
    WHERE ([ProductOption].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductOption].[Id], [ProductOption].[Name], [ProductOption].[Club_Id], [ProductOption].[_trackLastWriteTime], [ProductOption].[_trackCreationTime], [ProductOption].[_trackLastWriteUser], [ProductOption].[_trackCreationUser], [ProductOption].[_rowVersion] 
    FROM [ProductOption] 
    WHERE ([ProductOption].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductOption_LoadOneByNameClub]
(
 @name [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductOption].[Id], [ProductOption].[Name], [ProductOption].[Club_Id], [ProductOption].[_trackLastWriteTime], [ProductOption].[_trackCreationTime], [ProductOption].[_trackLastWriteUser], [ProductOption].[_trackCreationUser], [ProductOption].[_rowVersion] 
    FROM [ProductOption]
        LEFT OUTER JOIN [Club] ON ([ProductOption].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductOption].[Name] = @name) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType] 
    WHERE ([ProductType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType] 
    ORDER BY [ProductType].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType] 
    WHERE ([ProductType].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType] 
    WHERE ([ProductType].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType] 
    WHERE ([ProductType].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductType_LoadOneByNameClub]
(
 @name [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Club_Id], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteUser], [ProductType].[_trackCreationUser], [ProductType].[_rowVersion] 
    FROM [ProductType]
        LEFT OUTER JOIN [Club] ON ([ProductType].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductType].[Name] = @name) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_CountByBarcode]
(
 @barcode [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [ProductVariant]
    LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id])
            LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductVariant].[Barcode] LIKE @barcode) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_CountByProduct]
(
 @productId [int]
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [ProductVariant]
    LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id]) 
    WHERE ([Product].[Id] = @productId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_CountDuplicates]
(
 @productId [int],
 @productOptionValue [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT COUNT(*) FROM [ProductVariant]
    LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id]) 
    WHERE (([Product].[Id] = @productId) AND ([ProductVariant].[ProductOptionValue] = @productOptionValue))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant] 
    WHERE ([ProductVariant].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadByClubId]
(
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant]
        LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id])
                LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE ([Club].[Id] = @clubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant] 
    WHERE ([ProductVariant].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant] 
    WHERE ([ProductVariant].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadBySearch]
(
 @value [nvarchar] (256) = NULL,
 @clubId [int] = NULL,
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
DECLARE @sql nvarchar(max), @paramlist nvarchar(max)

SELECT @sql=
'SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant]
        LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id])
        LEFT OUTER JOIN [Product] [Product$1] ON ([ProductVariant].[Product_Id] = [Product$1].[Id])
                LEFT OUTER JOIN [Club] ON ([Product$1].[Club_Id] = [Club].[Id])
        LEFT OUTER JOIN [Product] [Product$2] ON ([ProductVariant].[Product_Id] = [Product$2].[Id])
                LEFT OUTER JOIN [Club] [Club$1] ON ([Product$2].[Club_Id] = [Club$1].[Id]) 
    WHERE ((((1 = 1) AND (1 = 1)) OR ((1 = 1) AND (1 = 1))) AND (1 = 1))'
SELECT @paramlist = '@value nvarchar (256), 
    @clubId int, 
    @_orderBy0 nvarchar (64), 
    @_orderByDirection0 bit'
IF @value IS NOT NULL
    SELECT @sql = @sql + ' AND (CONTAINS([Product].[Title],@value) AND CONTAINS([ProductVariant].[Barcode],@value))'
IF @clubId IS NOT NULL
    SELECT @sql = @sql + ' AND (([Club].[Id] = @clubId) AND ([Club$1].[Id] = @clubId))'
EXEC sp_executesql @sql, @paramlist,
    @value, 
    @clubId, 
    @_orderBy0, 
    @_orderByDirection0

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_LoadOneByProductId]
(
 @productId [int],
 @isDefault [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant]
        LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id]) 
    WHERE (([Product].[Id] = @productId) AND ([ProductVariant].[isDefault] = @isDefault))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVariant_SearchByBarcode]
(
 @barcode [nvarchar] (256),
 @clubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVariant].[Id], [ProductVariant].[Product_Id], [ProductVariant].[SKU], [ProductVariant].[Barcode], [ProductVariant].[Quantity], [ProductVariant].[ProductOptionValue], [ProductVariant].[Sequence], [ProductVariant].[RetailPrice], [ProductVariant].[Taxable], [ProductVariant].[isDefault], [ProductVariant].[InventoryOption], [ProductVariant].[Title], [ProductVariant].[MemberPrice], [ProductVariant].[_trackLastWriteTime], [ProductVariant].[_trackCreationTime], [ProductVariant].[_trackLastWriteUser], [ProductVariant].[_trackCreationUser], [ProductVariant].[_rowVersion] 
    FROM [ProductVariant]
        LEFT OUTER JOIN [Product] ON ([ProductVariant].[Product_Id] = [Product].[Id])
                LEFT OUTER JOIN [Club] ON ([Product].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductVariant].[Barcode] LIKE @barcode) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVendor].[Id], [ProductVendor].[Name], [ProductVendor].[Club_Id], [ProductVendor].[_trackLastWriteTime], [ProductVendor].[_trackCreationTime], [ProductVendor].[_trackLastWriteUser], [ProductVendor].[_trackCreationUser], [ProductVendor].[_rowVersion] 
    FROM [ProductVendor] 
    WHERE ([ProductVendor].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVendor].[Id], [ProductVendor].[Name], [ProductVendor].[Club_Id], [ProductVendor].[_trackLastWriteTime], [ProductVendor].[_trackCreationTime], [ProductVendor].[_trackLastWriteUser], [ProductVendor].[_trackCreationUser], [ProductVendor].[_rowVersion] 
    FROM [ProductVendor] 

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVendor].[Id], [ProductVendor].[Name], [ProductVendor].[Club_Id], [ProductVendor].[_trackLastWriteTime], [ProductVendor].[_trackCreationTime], [ProductVendor].[_trackLastWriteUser], [ProductVendor].[_trackCreationUser], [ProductVendor].[_rowVersion] 
    FROM [ProductVendor] 
    WHERE ([ProductVendor].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVendor].[Id], [ProductVendor].[Name], [ProductVendor].[Club_Id], [ProductVendor].[_trackLastWriteTime], [ProductVendor].[_trackCreationTime], [ProductVendor].[_trackLastWriteUser], [ProductVendor].[_trackCreationUser], [ProductVendor].[_rowVersion] 
    FROM [ProductVendor] 
    WHERE ([ProductVendor].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[ProductVendor_LoadOneByNameClub]
(
 @name [nvarchar] (256),
 @clubId [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [ProductVendor].[Id], [ProductVendor].[Name], [ProductVendor].[Club_Id], [ProductVendor].[_trackLastWriteTime], [ProductVendor].[_trackCreationTime], [ProductVendor].[_trackLastWriteUser], [ProductVendor].[_trackCreationUser], [ProductVendor].[_rowVersion] 
    FROM [ProductVendor]
        LEFT OUTER JOIN [Club] ON ([ProductVendor].[Club_Id] = [Club].[Id]) 
    WHERE (([ProductVendor].[Name] = @name) AND ([Club].[Id] = @clubId))

RETURN
GO

CREATE PROCEDURE [dbo].[Role_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Role].[Id], [Role].[Name], [Role].[_trackLastWriteTime], [Role].[_trackCreationTime], [Role].[_trackLastWriteUser], [Role].[_trackCreationUser], [Role].[_rowVersion] 
    FROM [Role] 
    WHERE ([Role].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Role_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Role].[Id], [Role].[Name], [Role].[_trackLastWriteTime], [Role].[_trackCreationTime], [Role].[_trackLastWriteUser], [Role].[_trackCreationUser], [Role].[_rowVersion] 
    FROM [Role] 

RETURN
GO

CREATE PROCEDURE [dbo].[Role_LoadByName]
(
 @Name [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Role].[Id], [Role].[Name], [Role].[_trackLastWriteTime], [Role].[_trackCreationTime], [Role].[_trackLastWriteUser], [Role].[_trackCreationUser], [Role].[_rowVersion] 
    FROM [Role] 
    WHERE ([Role].[Name] = @Name)

RETURN
GO

CREATE PROCEDURE [dbo].[Role_LoadRolesUserByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Role].[Id], [Role].[Name], [Role].[_trackLastWriteTime], [Role].[_trackCreationTime], [Role].[_trackLastWriteUser], [Role].[_trackCreationUser], [Role].[_rowVersion] 
    FROM [Role]
        LEFT OUTER JOIN [Role_User_User_Roles] ON ([Role].[Id] = [Role_User_User_Roles].[Id])
                LEFT OUTER JOIN [User] ON ([Role_User_User_Roles].[Id2] = [User].[Id]) 
    WHERE ([Role_User_User_Roles].[Id2] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [RoleClaim].[Id], [RoleClaim].[Type], [RoleClaim].[Value], [RoleClaim].[ValueType], [RoleClaim].[Role_Id], [RoleClaim].[_trackLastWriteTime], [RoleClaim].[_trackCreationTime], [RoleClaim].[_trackLastWriteUser], [RoleClaim].[_trackCreationUser], [RoleClaim].[_rowVersion] 
    FROM [RoleClaim] 
    WHERE ([RoleClaim].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [RoleClaim].[Id], [RoleClaim].[Type], [RoleClaim].[Value], [RoleClaim].[ValueType], [RoleClaim].[Role_Id], [RoleClaim].[_trackLastWriteTime], [RoleClaim].[_trackCreationTime], [RoleClaim].[_trackLastWriteUser], [RoleClaim].[_trackCreationUser], [RoleClaim].[_rowVersion] 
    FROM [RoleClaim] 

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [RoleClaim].[Id], [RoleClaim].[Type], [RoleClaim].[Value], [RoleClaim].[ValueType], [RoleClaim].[Role_Id], [RoleClaim].[_trackLastWriteTime], [RoleClaim].[_trackCreationTime], [RoleClaim].[_trackLastWriteUser], [RoleClaim].[_trackCreationUser], [RoleClaim].[_rowVersion] 
    FROM [RoleClaim] 
    WHERE ([RoleClaim].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[RoleClaim_LoadByRole]
(
 @RoleId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [RoleClaim].[Id], [RoleClaim].[Type], [RoleClaim].[Value], [RoleClaim].[ValueType], [RoleClaim].[Role_Id], [RoleClaim].[_trackLastWriteTime], [RoleClaim].[_trackCreationTime], [RoleClaim].[_trackLastWriteUser], [RoleClaim].[_trackCreationUser], [RoleClaim].[_rowVersion] 
    FROM [RoleClaim] 
    WHERE ([RoleClaim].[Role_Id] = @RoleId)

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 
    WHERE ([Sale].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_LoadByAdmin]
(
 @AdminId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 
    WHERE ([Sale].[Admin_Id] = @AdminId)

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 
    WHERE ([Sale].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 
    WHERE ([Sale].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Sale_LoadByInvoice]
(
 @InvoiceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Sale].[Id], [Sale].[Customer_Id], [Sale].[Admin_Id], [Sale].[Tax], [Sale].[SubTotal], [Sale].[TotalAmount], [Sale].[Note], [Sale].[SaleStatus], [Sale].[SaleDateTimeUTC], [Sale].[Description], [Sale].[Invoice_Id], [Sale].[DiscountType], [Sale].[Discount], [Sale].[_trackLastWriteTime], [Sale].[_trackCreationTime], [Sale].[_trackLastWriteUser], [Sale].[_trackCreationUser], [Sale].[_rowVersion] 
    FROM [Sale] 
    WHERE ([Sale].[Invoice_Id] = @InvoiceId)

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SaleItem].[Id], [SaleItem].[Sale_Id], [SaleItem].[Order_Id], [SaleItem].[_trackLastWriteTime], [SaleItem].[_trackCreationTime], [SaleItem].[_trackLastWriteUser], [SaleItem].[_trackCreationUser], [SaleItem].[_rowVersion] 
    FROM [SaleItem] 
    WHERE ([SaleItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SaleItem].[Id], [SaleItem].[Sale_Id], [SaleItem].[Order_Id], [SaleItem].[_trackLastWriteTime], [SaleItem].[_trackCreationTime], [SaleItem].[_trackLastWriteUser], [SaleItem].[_trackCreationUser], [SaleItem].[_rowVersion] 
    FROM [SaleItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SaleItem].[Id], [SaleItem].[Sale_Id], [SaleItem].[Order_Id], [SaleItem].[_trackLastWriteTime], [SaleItem].[_trackCreationTime], [SaleItem].[_trackLastWriteUser], [SaleItem].[_trackCreationUser], [SaleItem].[_rowVersion] 
    FROM [SaleItem] 
    WHERE ([SaleItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_LoadByOrder]
(
 @OrderId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SaleItem].[Id], [SaleItem].[Sale_Id], [SaleItem].[Order_Id], [SaleItem].[_trackLastWriteTime], [SaleItem].[_trackCreationTime], [SaleItem].[_trackLastWriteUser], [SaleItem].[_trackCreationUser], [SaleItem].[_rowVersion] 
    FROM [SaleItem] 
    WHERE ([SaleItem].[Order_Id] = @OrderId)

RETURN
GO

CREATE PROCEDURE [dbo].[SaleItem_LoadBySale]
(
 @SaleId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SaleItem].[Id], [SaleItem].[Sale_Id], [SaleItem].[Order_Id], [SaleItem].[_trackLastWriteTime], [SaleItem].[_trackCreationTime], [SaleItem].[_trackLastWriteUser], [SaleItem].[_trackCreationUser], [SaleItem].[_rowVersion] 
    FROM [SaleItem] 
    WHERE ([SaleItem].[Sale_Id] = @SaleId)

RETURN
GO

CREATE PROCEDURE [dbo].[Size_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Size].[Id], [Size].[Value], [Size].[Sequence], [Size].[ProductType_Id], [Size].[_trackLastWriteTime], [Size].[_trackCreationTime], [Size].[_trackLastWriteUser], [Size].[_trackCreationUser], [Size].[_rowVersion] 
    FROM [Size] 
    WHERE ([Size].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Size_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Size].[Id], [Size].[Value], [Size].[Sequence], [Size].[ProductType_Id], [Size].[_trackLastWriteTime], [Size].[_trackCreationTime], [Size].[_trackLastWriteUser], [Size].[_trackCreationUser], [Size].[_rowVersion] 
    FROM [Size] 

RETURN
GO

CREATE PROCEDURE [dbo].[Size_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Size].[Id], [Size].[Value], [Size].[Sequence], [Size].[ProductType_Id], [Size].[_trackLastWriteTime], [Size].[_trackCreationTime], [Size].[_trackLastWriteUser], [Size].[_trackCreationUser], [Size].[_rowVersion] 
    FROM [Size] 
    WHERE ([Size].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Size_LoadByProductType]
(
 @ProductTypeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Size].[Id], [Size].[Value], [Size].[Sequence], [Size].[ProductType_Id], [Size].[_trackLastWriteTime], [Size].[_trackCreationTime], [Size].[_trackLastWriteUser], [Size].[_trackCreationUser], [Size].[_rowVersion] 
    FROM [Size] 
    WHERE ([Size].[ProductType_Id] = @ProductTypeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Source_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Source].[Id], [Source].[Customer_Id], [Source].[_trackLastWriteTime], [Source].[_trackCreationTime], [Source].[_trackLastWriteUser], [Source].[_trackCreationUser], [Source].[_rowVersion] 
    FROM [Source] 
    WHERE ([Source].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Source_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Source].[Id], [Source].[Customer_Id], [Source].[_trackLastWriteTime], [Source].[_trackCreationTime], [Source].[_trackLastWriteUser], [Source].[_trackCreationUser], [Source].[_rowVersion] 
    FROM [Source] 

RETURN
GO

CREATE PROCEDURE [dbo].[Source_LoadByCustomer]
(
 @CustomerId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Source].[Id], [Source].[Customer_Id], [Source].[_trackLastWriteTime], [Source].[_trackCreationTime], [Source].[_trackLastWriteUser], [Source].[_trackCreationUser], [Source].[_rowVersion] 
    FROM [Source] 
    WHERE ([Source].[Customer_Id] = @CustomerId)

RETURN
GO

CREATE PROCEDURE [dbo].[Source_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Source].[Id], [Source].[Customer_Id], [Source].[_trackLastWriteTime], [Source].[_trackCreationTime], [Source].[_trackLastWriteUser], [Source].[_trackCreationUser], [Source].[_rowVersion] 
    FROM [Source] 
    WHERE ([Source].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteUser], [Stocktake].[_trackCreationUser], [Stocktake].[_rowVersion] 
    FROM [Stocktake] 
    WHERE ([Stocktake].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteUser], [Stocktake].[_trackCreationUser], [Stocktake].[_rowVersion] 
    FROM [Stocktake] 

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteUser], [Stocktake].[_trackCreationUser], [Stocktake].[_rowVersion] 
    FROM [Stocktake] 
    WHERE ([Stocktake].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteUser], [Stocktake].[_trackCreationUser], [Stocktake].[_rowVersion] 
    FROM [Stocktake] 
    WHERE ([Stocktake].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Stocktake_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteUser], [Stocktake].[_trackCreationUser], [Stocktake].[_rowVersion] 
    FROM [Stocktake] 
    WHERE ([Stocktake].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteUser], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_rowVersion] 
    FROM [StocktakeItem] 
    WHERE ([StocktakeItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteUser], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_rowVersion] 
    FROM [StocktakeItem] 

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteUser], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_rowVersion] 
    FROM [StocktakeItem] 
    WHERE ([StocktakeItem].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_LoadByProduct]
(
 @ProductId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteUser], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_rowVersion] 
    FROM [StocktakeItem] 
    WHERE ([StocktakeItem].[Product_Id] = @ProductId)

RETURN
GO

CREATE PROCEDURE [dbo].[StocktakeItem_LoadByStocktake]
(
 @StocktakeId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteUser], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_rowVersion] 
    FROM [StocktakeItem] 
    WHERE ([StocktakeItem].[Stocktake_Id] = @StocktakeId)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadAllByClub]
(
 @clubid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[Club_Id] = @clubid)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_LoadOneByReference]
(
 @ref [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteUser], [Supplier].[_trackCreationUser], [Supplier].[_rowVersion] 
    FROM [Supplier] 
    WHERE ([Supplier].[ReferenceNo] = @ref)

RETURN
GO

CREATE PROCEDURE [dbo].[Supplier_UpdateInstanceById]
(
 @id [int],
 @email [nvarchar] (256),
 @name [nvarchar] (256),
 @phone [nvarchar] (256),
 @ref [nvarchar] (256)
)
AS
SET NOCOUNT ON
UPDATE 

[Supplier]

SET 

Email = @email, 
Name = @name, 
Phone = @phone, 
ReferenceNo = @ref 

WHERE 

Id = @id
RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 
    WHERE ([SupplierContact].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_LoadAllBySupplier]
(
 @supplierid [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 
    WHERE ([SupplierContact].[Supplier_Id] = @supplierid)

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 
    WHERE ([SupplierContact].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_LoadBySupplier]
(
 @SupplierId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 
    WHERE ([SupplierContact].[Supplier_Id] = @SupplierId)

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteUser], [SupplierContact].[_trackCreationUser], [SupplierContact].[_rowVersion] 
    FROM [SupplierContact] 
    WHERE ([SupplierContact].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[SupplierContact_UpdateInstanceById]
(
 @id [int],
 @email [nvarchar] (256),
 @name [nvarchar] (256),
 @phone [nvarchar] (256),
 @position [nvarchar] (256)
)
AS
SET NOCOUNT ON
UPDATE 

[SupplierContact] 

SET 

Email = @email, 
Name = @name, 
Phone = @phone, 
Position = @position 

WHERE 

Id = @id
RETURN
GO

CREATE PROCEDURE [dbo].[Surface_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Surface].[Id], [Surface].[Name], [Surface].[Description], [Surface].[Sequence], [Surface].[_trackLastWriteTime], [Surface].[_trackCreationTime], [Surface].[_trackLastWriteUser], [Surface].[_trackCreationUser], [Surface].[_rowVersion] 
    FROM [Surface] 
    WHERE ([Surface].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Surface_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Surface].[Id], [Surface].[Name], [Surface].[Description], [Surface].[Sequence], [Surface].[_trackLastWriteTime], [Surface].[_trackCreationTime], [Surface].[_trackLastWriteUser], [Surface].[_trackCreationUser], [Surface].[_rowVersion] 
    FROM [Surface] 
    ORDER BY [Surface].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[Surface_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Surface].[Id], [Surface].[Name], [Surface].[Description], [Surface].[Sequence], [Surface].[_trackLastWriteTime], [Surface].[_trackCreationTime], [Surface].[_trackLastWriteUser], [Surface].[_trackCreationUser], [Surface].[_rowVersion] 
    FROM [Surface] 
    WHERE ([Surface].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Surface_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Surface].[Id], [Surface].[Name], [Surface].[Description], [Surface].[Sequence], [Surface].[_trackLastWriteTime], [Surface].[_trackCreationTime], [Surface].[_trackLastWriteUser], [Surface].[_trackCreationUser], [Surface].[_rowVersion] 
    FROM [Surface] 
    WHERE ([Surface].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[Term_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Term].[Id], [Term].[Name], [Term].[StartDateUTC], [Term].[EndDateUTC], [Term].[isActive], [Term].[Club_Id], [Term].[Description], [Term].[_trackLastWriteTime], [Term].[_trackCreationTime], [Term].[_trackLastWriteUser], [Term].[_trackCreationUser], [Term].[_rowVersion] 
    FROM [Term] 
    WHERE ([Term].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Term_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Term].[Id], [Term].[Name], [Term].[StartDateUTC], [Term].[EndDateUTC], [Term].[isActive], [Term].[Club_Id], [Term].[Description], [Term].[_trackLastWriteTime], [Term].[_trackCreationTime], [Term].[_trackLastWriteUser], [Term].[_trackCreationUser], [Term].[_rowVersion] 
    FROM [Term] 

RETURN
GO

CREATE PROCEDURE [dbo].[Term_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Term].[Id], [Term].[Name], [Term].[StartDateUTC], [Term].[EndDateUTC], [Term].[isActive], [Term].[Club_Id], [Term].[Description], [Term].[_trackLastWriteTime], [Term].[_trackCreationTime], [Term].[_trackLastWriteUser], [Term].[_trackCreationUser], [Term].[_rowVersion] 
    FROM [Term] 
    WHERE ([Term].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Term_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Term].[Id], [Term].[Name], [Term].[StartDateUTC], [Term].[EndDateUTC], [Term].[isActive], [Term].[Club_Id], [Term].[Description], [Term].[_trackLastWriteTime], [Term].[_trackCreationTime], [Term].[_trackLastWriteUser], [Term].[_trackCreationUser], [Term].[_rowVersion] 
    FROM [Term] 
    WHERE ([Term].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteUser], [TimePeriod].[_trackCreationUser], [TimePeriod].[_rowVersion] 
    FROM [TimePeriod] 
    WHERE ([TimePeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteUser], [TimePeriod].[_trackCreationUser], [TimePeriod].[_rowVersion] 
    FROM [TimePeriod] 

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteUser], [TimePeriod].[_trackCreationUser], [TimePeriod].[_rowVersion] 
    FROM [TimePeriod] 
    WHERE ([TimePeriod].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteUser], [TimePeriod].[_trackCreationUser], [TimePeriod].[_rowVersion] 
    FROM [TimePeriod] 
    WHERE ([TimePeriod].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriod_LoadByTimePeriodGroup]
(
 @TimePeriodGroupId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteUser], [TimePeriod].[_trackCreationUser], [TimePeriod].[_rowVersion] 
    FROM [TimePeriod] 
    WHERE ([TimePeriod].[TimePeriodGroup_Id] = @TimePeriodGroupId)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteUser], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_rowVersion] 
    FROM [TimePeriodGroup] 
    WHERE ([TimePeriodGroup].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteUser], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_rowVersion] 
    FROM [TimePeriodGroup] 

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteUser], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_rowVersion] 
    FROM [TimePeriodGroup] 
    WHERE ([TimePeriodGroup].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteUser], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_rowVersion] 
    FROM [TimePeriodGroup] 
    WHERE ([TimePeriodGroup].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[TimePeriodGroup_LoadOneByIsDefault]
(
 @isDefault [bit]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteUser], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_rowVersion] 
    FROM [TimePeriodGroup] 
    WHERE ([TimePeriodGroup].[isDefault] = @isDefault)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByAccount]
(
 @AccountId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Account_Id] = @AccountId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByAccountingPeriod]
(
 @AccountingPeriodId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[AccountingPeriod_Id] = @AccountingPeriodId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByCurrency]
(
 @CurrencyId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Currency_Id] = @CurrencyId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByDestination]
(
 @DestinationId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Destination_Id] = @DestinationId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByInvoice]
(
 @InvoiceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Invoice_Id] = @InvoiceId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadByJournal]
(
 @JournalId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Journal_Id] = @JournalId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadBySource]
(
 @SourceId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction] 
    WHERE ([Transaction].[Source_Id] = @SourceId)

RETURN
GO

CREATE PROCEDURE [dbo].[Transaction_LoadTransactionsByOrderItem]
(
 @OrderItemId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Transaction].[Id], [Transaction].[Account_Id], [Transaction].[Currency_Id], [Transaction].[Journal_Id], [Transaction].[Amount], [Transaction].[AccountingPeriod_Id], [Transaction].[Description], [Transaction].[TransactionStatus], [Transaction].[SourceType], [Transaction].[Invoice_Id], [Transaction].[Club_Id], [Transaction].[DateCreatedUTC], [Transaction].[TransactionType], [Transaction].[DestinationType], [Transaction].[Metadata], [Transaction].[Balance], [Transaction].[Reference], [Transaction].[Source_Id], [Transaction].[Destination_Id], [Transaction].[_trackLastWriteTime], [Transaction].[_trackCreationTime], [Transaction].[_trackLastWriteUser], [Transaction].[_trackCreationUser], [Transaction].[_rowVersion] 
    FROM [Transaction]
        LEFT OUTER JOIN [OrderItem_Transactions_Transaction] ON ([Transaction].[Id] = [OrderItem_Transactions_Transaction].[Id2])
                LEFT OUTER JOIN [OrderItem] ON ([OrderItem_Transactions_Transaction].[Id] = [OrderItem].[Id]) 
    WHERE ([OrderItem_Transactions_Transaction].[Id] = @OrderItemId)

RETURN
GO

CREATE PROCEDURE [dbo].[User_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadByEmail]
(
 @Email [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[Email] = @Email)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadByGender]
(
 @GenderId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[Gender_Id] = @GenderId)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadByPlayerRating]
(
 @PlayerRatingId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[PlayerRating_Id] = @PlayerRatingId)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadByUserLoginInfo]
(
 @providerKey [nvarchar] (256),
 @providerName [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User]
        LEFT OUTER JOIN [UserLogin] ON ([User].[Id] = [UserLogin].[User_Id]) 
    WHERE (([UserLogin].[ProviderKey] = @providerKey) AND ([UserLogin].[ProviderName] = @providerName))

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadByUserName]
(
 @UserName [nvarchar] (256)
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[UserName] = @UserName)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User] 
    WHERE ([User].[Id] = @id)

RETURN
GO

CREATE PROCEDURE [dbo].[User_LoadUserRolesByRole]
(
 @RoleId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_trackLastWriteTime], [User].[_trackCreationTime], [User].[_trackLastWriteUser], [User].[_trackCreationUser], [User].[_rowVersion] 
    FROM [User]
        LEFT OUTER JOIN [Role_User_User_Roles] ON ([User].[Id] = [Role_User_User_Roles].[Id2])
                LEFT OUTER JOIN [Role] ON ([Role_User_User_Roles].[Id] = [Role].[Id]) 
    WHERE ([Role_User_User_Roles].[Id] = @RoleId)

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserClaim].[Id], [UserClaim].[Type], [UserClaim].[Value], [UserClaim].[ValueType], [UserClaim].[Issuer], [UserClaim].[OriginalIssuer], [UserClaim].[User_Id], [UserClaim].[_trackLastWriteTime], [UserClaim].[_trackCreationTime], [UserClaim].[_trackLastWriteUser], [UserClaim].[_trackCreationUser], [UserClaim].[_rowVersion] 
    FROM [UserClaim] 
    WHERE ([UserClaim].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserClaim].[Id], [UserClaim].[Type], [UserClaim].[Value], [UserClaim].[ValueType], [UserClaim].[Issuer], [UserClaim].[OriginalIssuer], [UserClaim].[User_Id], [UserClaim].[_trackLastWriteTime], [UserClaim].[_trackCreationTime], [UserClaim].[_trackLastWriteUser], [UserClaim].[_trackCreationUser], [UserClaim].[_rowVersion] 
    FROM [UserClaim] 

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserClaim].[Id], [UserClaim].[Type], [UserClaim].[Value], [UserClaim].[ValueType], [UserClaim].[Issuer], [UserClaim].[OriginalIssuer], [UserClaim].[User_Id], [UserClaim].[_trackLastWriteTime], [UserClaim].[_trackCreationTime], [UserClaim].[_trackLastWriteUser], [UserClaim].[_trackCreationUser], [UserClaim].[_rowVersion] 
    FROM [UserClaim] 
    WHERE ([UserClaim].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[UserClaim_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserClaim].[Id], [UserClaim].[Type], [UserClaim].[Value], [UserClaim].[ValueType], [UserClaim].[Issuer], [UserClaim].[OriginalIssuer], [UserClaim].[User_Id], [UserClaim].[_trackLastWriteTime], [UserClaim].[_trackCreationTime], [UserClaim].[_trackLastWriteUser], [UserClaim].[_trackCreationUser], [UserClaim].[_rowVersion] 
    FROM [UserClaim] 
    WHERE ([UserClaim].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserLogin].[Id], [UserLogin].[ProviderName], [UserLogin].[ProviderKey], [UserLogin].[ProviderDisplayName], [UserLogin].[User_Id], [UserLogin].[_trackLastWriteTime], [UserLogin].[_trackCreationTime], [UserLogin].[_trackLastWriteUser], [UserLogin].[_trackCreationUser], [UserLogin].[_rowVersion] 
    FROM [UserLogin] 
    WHERE ([UserLogin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserLogin].[Id], [UserLogin].[ProviderName], [UserLogin].[ProviderKey], [UserLogin].[ProviderDisplayName], [UserLogin].[User_Id], [UserLogin].[_trackLastWriteTime], [UserLogin].[_trackCreationTime], [UserLogin].[_trackLastWriteUser], [UserLogin].[_trackCreationUser], [UserLogin].[_rowVersion] 
    FROM [UserLogin] 

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserLogin].[Id], [UserLogin].[ProviderName], [UserLogin].[ProviderKey], [UserLogin].[ProviderDisplayName], [UserLogin].[User_Id], [UserLogin].[_trackLastWriteTime], [UserLogin].[_trackCreationTime], [UserLogin].[_trackLastWriteUser], [UserLogin].[_trackCreationUser], [UserLogin].[_rowVersion] 
    FROM [UserLogin] 
    WHERE ([UserLogin].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[UserLogin_LoadByUser]
(
 @UserId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [UserLogin].[Id], [UserLogin].[ProviderName], [UserLogin].[ProviderKey], [UserLogin].[ProviderDisplayName], [UserLogin].[User_Id], [UserLogin].[_trackLastWriteTime], [UserLogin].[_trackCreationTime], [UserLogin].[_trackLastWriteUser], [UserLogin].[_trackCreationUser], [UserLogin].[_rowVersion] 
    FROM [UserLogin] 
    WHERE ([UserLogin].[User_Id] = @UserId)

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_Load]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadAll]
(
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadAllByFacility]
(
 @facilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Facility_Id] = @facilityId)
    ORDER BY [Zone].[Sequence] ASC

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadByClub]
(
 @ClubId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Club_Id] = @ClubId)

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadByFacility]
(
 @FacilityId [int],
 @_orderBy0 [nvarchar] (64) = NULL,
 @_orderByDirection0 [bit] = 0
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Facility_Id] = @FacilityId)

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadById]
(
 @Id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Id] = @Id)

RETURN
GO

CREATE PROCEDURE [dbo].[Zone_LoadOneById]
(
 @id [int]
)
AS
SET NOCOUNT ON
SELECT DISTINCT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteUser], [Zone].[_trackCreationUser], [Zone].[_rowVersion] 
    FROM [Zone] 
    WHERE ([Zone].[Id] = @id)

RETURN
GO

