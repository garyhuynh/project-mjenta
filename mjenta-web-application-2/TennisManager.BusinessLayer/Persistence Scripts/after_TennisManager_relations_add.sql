USE tennismanagerdb;

GO
IF  EXISTS (SELECT * FROM sys.fulltext_indexes WHERE object_id = object_id('Product'))
DROP FULLTEXT INDEX ON Product;

IF  EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'product_catalog')
DROP FULLTEXT CATALOG product_catalog


IF  EXISTS (SELECT * FROM sys.fulltext_indexes WHERE object_id = object_id('ProductVariant'))
DROP FULLTEXT INDEX ON ProductVariant;

IF  EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = 'productvariant_catalog')
DROP FULLTEXT CATALOG productvariant_catalog

GO
CREATE FULLTEXT CATALOG product_catalog
GO
CREATE FULLTEXT INDEX ON Product
(
	Title
		Language 1033
	)
	KEY INDEX PK_Pro_Id_Pro
		ON product_catalog; 
GO

CREATE FULLTEXT CATALOG productvariant_catalog
GO
CREATE FULLTEXT INDEX ON ProductVariant
(
	Barcode
		Language 1033
	)
	KEY INDEX PK_PrV_Id_PrV
		ON productvariant_catalog; 
GO

ALTER FULLTEXT INDEX ON Product ENABLE; 
GO 
ALTER FULLTEXT INDEX ON Product START FULL POPULATION;
GO
ALTER FULLTEXT INDEX ON ProductVariant ENABLE; 
GO 
ALTER FULLTEXT INDEX ON ProductVariant START FULL POPULATION;
GO