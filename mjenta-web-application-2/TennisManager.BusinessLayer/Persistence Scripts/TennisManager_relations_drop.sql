﻿/* CodeFluent Generated Monday, 08 August 2016 12:51. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-AU. UiCulture:en-GB. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cur_Id_Cur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Bok_Id_Bok]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Bok_Id_Bok]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
GO
/* no fk for 'FK_Boi_Boo_Id_Bon', tableName='BookingSubType' table='BookingSubType' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
GO
/* no fk for 'FK_Chc_Pro_Id_Pro', tableName='CheckoutItem' table='CheckoutItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
GO
/* no fk for 'FK_Clb_Clu_Id_Clu', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
GO
/* no fk for 'FK_Clb_Use_Id_Use', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
GO
/* no fk for 'FK_ClC_Clu_Id_Clu', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
GO
/* no fk for 'FK_ClC_Com_Id_Com', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
GO
/* no fk for 'FK_ClP_Clu_Id_Clu', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
GO
/* no fk for 'FK_ClP_Pla_Id_Pla', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
GO
/* no fk for 'FK_Cor_Zon_Id_Zon', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
GO
/* no fk for 'FK_Cor_Tim_Id_Tim', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
GO
/* no fk for 'FK_Cor_Clu_Id_Clu', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
GO
/* no fk for 'FK_Cor_Mem_Id_Meb', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
GO
/* no fk for 'FK_Crd_Crd_Id_Cre', tableName='CreditAdjust' table='CreditAdjust' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
GO
/* no fk for 'FK_Crd_Inv_Id_Inv', tableName='CreditAdjust' table='CreditAdjust' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
GO
/* no fk for 'FK_Dei_Pro_Id_Pro', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
GO
/* no fk for 'FK_Dei_Del_Id_Del', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Dis_Id_Dis]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dis_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [FK_Dis_Pro_Id_PrV]
GO
/* no fk for 'FK_Dic_Dis_Id_Dis', tableName='DiscountGroup' table='DiscountGroup' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Dis_Id_Dis]
GO
/* no fk for 'FK_Dic_Mem_Id_Meb', tableName='DiscountGroup' table='DiscountGroup' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
GO
/* no fk for 'FK_Eqi_Equ_Id_Equ', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
GO
/* no fk for 'FK_Eqi_Pro_Id_Pro', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Can_Id_Can]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Can_Id_Can]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
GO
/* no fk for 'FK_EvF_Eve_Id_Eve', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
GO
/* no fk for 'FK_EvF_Mem_Id_Meb', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
GO
/* no fk for 'FK_Evo_Cus_Id_Cus', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
GO
/* no fk for 'FK_Evo_Eve_Id_Evn', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
GO
/* no fk for 'FK_EvW_Cus_Id_Cus', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
GO
/* no fk for 'FK_EvW_Eve_Id_Evn', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
GO
/* no fk for 'FK_Mee_Mem_Id_Meb', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
GO
/* no fk for 'FK_Mee_Tim_Id_Tie', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
GO
/* no fk for 'FK_Mee_Clu_Id_Clu', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mer_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [FK_Mer_Mes_Id_Mes]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
GO
/* no fk for 'FK_Mea_Adm_Id_Adm', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
GO
/* no fk for 'FK_Mea_Cus_Id_Cus', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
GO
/* no fk for 'FK_Mea_Mes_Id_Meg', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Mes_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Mes_Id_Meg]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Mes_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Mes_Id_Meg]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meg_Mes_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [FK_Meg_Mes_Id_Mer]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meg_Mes_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [FK_Meg_Mes_Id_Mer]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mer_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [FK_Mer_Mes_Id_Mes]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Pro_Id_PrV]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
GO
/* no fk for 'FK_Orr_Ord_Id_Ore', tableName='OrderItemDetail' table='OrderItemDetail' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Pru_Id_Pre]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Pru_Id_Pre]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prt_Id_Prd]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prt_Id_Prd]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prt_Id_Prd]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prt_Id_Prd]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
GO
/* no fk for 'FK_Prc_Pro_Id_Pro', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
GO
/* no fk for 'FK_Prt_Pro_Id_PrV', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prt_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [FK_Prt_Pro_Id_PrV]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dis_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [FK_Dis_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prt_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [FK_Prt_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Pru_Id_Pre]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Pru_Id_Pre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
GO
/* no fk for 'FK_Roe_Rol_Id_Rol', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
GO
/* no fk for 'FK_Sae_Sal_Id_Sal', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
GO
/* no fk for 'FK_Sae_Ord_Id_Ore', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
GO
/* no fk for 'FK_Siz_Pro_Id_Pry', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
GO
/* no fk for 'FK_Sou_Cus_Id_Cus', tableName='Source' table='Source' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
GO
/* no fk for 'FK_Stc_Sto_Id_Sto', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
GO
/* no fk for 'FK_Stc_Pro_Id_Pro', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
GO
/* no fk for 'FK_Sul_Sup_Id_Sup', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
GO
/* no fk for 'FK_Ter_Clu_Id_Clu', tableName='Term' table='Term' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Cur_Id_Cur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Jou_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Jou_Id_Jou]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Aco_Id_Aco]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Aco_Id_Aco]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
GO
/* no fk for 'FK_Usr_Use_Id_Use', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
GO
/* no fk for 'FK_UsL_Use_Id_Use', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
GO
/* no fk for 'FK_Ev__Id_Id_Eve', tableName='Event_Coaches_Coach' table='Event_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
GO
/* no fk for 'FK_Ev__Id2_Id_Coa', tableName='Event_Coaches_Coach' table='Event_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
GO
/* no fk for 'FK_EvG_Id_Id_Eve', tableName='Event_GenderAges_GenderAge' table='Event_GenderAges_GenderAge' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
GO
/* no fk for 'FK_EvG_Id2_Id_Ged', tableName='Event_GenderAges_GenderAge' table='Event_GenderAges_GenderAge' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
GO
/* no fk for 'FK_EvL_Id_Id_Eve', tableName='Event_Levels_PlayerLevel' table='Event_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
GO
/* no fk for 'FK_EvL_Id2_Id_Pla', tableName='Event_Levels_PlayerLevel' table='Event_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
GO
/* no fk for 'FK_EvC_Id_Id_Eve', tableName='Event_Courts_Court' table='Event_Courts_Court' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
GO
/* no fk for 'FK_EvC_Id2_Id_Cou', tableName='Event_Courts_Court' table='Event_Courts_Court' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
GO
/* no fk for 'FK_EvD_Id_Id_Eve', tableName='Event_Days_DaysOfWeek' table='Event_Days_DaysOfWeek' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
GO
/* no fk for 'FK_EvD_Id2_Id_Day', tableName='Event_Days_DaysOfWeek' table='Event_Days_DaysOfWeek' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id2_Id_Day]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id2_Id_Day]
GO
/* no fk for 'FK_EvB_Id_Id_Evn', tableName='EventBooking_Coaches_Coach' table='EventBooking_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
GO
/* no fk for 'FK_EvB_Id2_Id_Coa', tableName='EventBooking_Coaches_Coach' table='EventBooking_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
GO
/* no fk for 'FK_Evk_Id_Id_Evn', tableName='EventBooking_Booking_Booking' table='EventBooking_Booking_Booking' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
GO
/* no fk for 'FK_Evk_Id2_Id_Boo', tableName='EventBooking_Booking_Booking' table='EventBooking_Booking_Booking' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
GO
/* no fk for 'FK_Evi_Id_Id_Evn', tableName='EventBooking_Levels_PlayerLevel' table='EventBooking_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
GO
/* no fk for 'FK_Evi_Id2_Id_Pla', tableName='EventBooking_Levels_PlayerLevel' table='EventBooking_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
GO
/* no fk for 'FK_Ino_Id_Id_Inv', tableName='Invoice_Journals_Journal' table='Invoice_Journals_Journal' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
GO
/* no fk for 'FK_Ino_Id2_Id_Jou', tableName='Invoice_Journals_Journal' table='Invoice_Journals_Journal' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id2_Id_Jou]
GO
/* no fk for 'FK_OrI_Id_Id_Ore', tableName='OrderItem_Transactions_Transaction' table='OrderItem_Transactions_Transaction' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
GO
/* no fk for 'FK_OrI_Id2_Id_Tra', tableName='OrderItem_Transactions_Transaction' table='OrderItem_Transactions_Transaction' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
GO
/* no fk for 'FK_Ple_Id_Id_Pla', tableName='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' table='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
GO
/* no fk for 'FK_Ple_Id2_Id_Ply', tableName='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' table='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
GO
/* no fk for 'FK_Ro__Id_Id_Rol', tableName='Role_User_User_Roles' table='Role_User_User_Roles' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
GO
/* no fk for 'FK_Ro__Id2_Id_Use', tableName='Role_User_User_Roles' table='Role_User_User_Roles' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
GO
