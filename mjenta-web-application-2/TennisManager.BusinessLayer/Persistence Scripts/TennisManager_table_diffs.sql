﻿/* CodeFluent Generated Monday, 08 August 2016 12:46. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-AU. UiCulture:en-GB. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [PK_Acc_Id_Acc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [PK_Acc_Id_Acc]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK_Acc_Id_Acc] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[AccountingPeriod]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Admin]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Batch]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Booking]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[BookingStatus]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[BookingSubType]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Bon_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [PK_Bon_Id_Bon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Bon_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [PK_Bon_Id_Bon]
GO
ALTER TABLE [dbo].[BookingType] ADD CONSTRAINT [PK_Bon_Id_Bon] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[CancellationType]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* no fk for 'PK_Che_Id_Che', tableName='Checkout' table='Checkout' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Che_Id_Che]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [PK_Che_Id_Che]
GO
/* no fk for 'PK_Che_Id_Che', tableName='Checkout' table='Checkout' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Che_Id_Che]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [PK_Che_Id_Che]
GO
ALTER TABLE [dbo].[Checkout] ADD CONSTRAINT [PK_Che_Id_Che] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[CheckoutItem]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [PK_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [PK_Clu_Id_Clu]
GO
ALTER TABLE [dbo].[Club] ADD CONSTRAINT [PK_Clu_Id_Clu] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Clb_Id_Clb', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clb_Id_Clb]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [PK_Clb_Id_Clb]
GO
/* no fk for 'PK_Clb_Id_Clb', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clb_Id_Clb]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [PK_Clb_Id_Clb]
GO
ALTER TABLE [dbo].[ClubAdmin] ADD CONSTRAINT [PK_Clb_Id_Clb] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_ClC_Id_ClC', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClC_Id_ClC]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [PK_ClC_Id_ClC]
GO
/* no fk for 'PK_ClC_Id_ClC', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClC_Id_ClC]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [PK_ClC_Id_ClC]
GO
ALTER TABLE [dbo].[ClubComponents] ADD CONSTRAINT [PK_ClC_Id_ClC] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_ClP_Id_ClP', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClP_Id_ClP]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [PK_ClP_Id_ClP]
GO
/* no fk for 'PK_ClP_Id_ClP', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClP_Id_ClP]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [PK_ClP_Id_ClP]
GO
ALTER TABLE [dbo].[ClubPlayerLevel] ADD CONSTRAINT [PK_ClP_Id_ClP] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Coach]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [PK_Com_Id_Com]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [PK_Com_Id_Com]
GO
ALTER TABLE [dbo].[Components] ADD CONSTRAINT [PK_Com_Id_Com] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [PK_Cou_Id_Cou]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [PK_Cou_Id_Cou]
GO
ALTER TABLE [dbo].[Court] ADD CONSTRAINT [PK_Cou_Id_Cou] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Cor_Id_Cor', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cor_Id_Cor]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [PK_Cor_Id_Cor]
GO
/* no fk for 'PK_Cor_Id_Cor', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cor_Id_Cor]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [PK_Cor_Id_Cor]
GO
ALTER TABLE [dbo].[CourtHireRate] ADD CONSTRAINT [PK_Cor_Id_Cor] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cre_Id_Cre]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [PK_Cre_Id_Cre]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cre_Id_Cre]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [PK_Cre_Id_Cre]
GO
ALTER TABLE [dbo].[Credit] ADD CONSTRAINT [PK_Cre_Id_Cre] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[CreditAdjust]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Currency]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Customer]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[DaysOfWeek]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [PK_Del_Id_Del]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [PK_Del_Id_Del]
GO
ALTER TABLE [dbo].[Delivery] ADD CONSTRAINT [PK_Del_Id_Del] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Dei_Id_Dei', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Dei_Id_Dei]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [PK_Dei_Id_Dei]
GO
/* no fk for 'PK_Dei_Id_Dei', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Dei_Id_Dei]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [PK_Dei_Id_Dei]
GO
ALTER TABLE [dbo].[DeliveryItem] ADD CONSTRAINT [PK_Dei_Id_Dei] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Discount]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[DiscountGroup]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [PK_Equ_Id_Equ]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [PK_Equ_Id_Equ]
GO
ALTER TABLE [dbo].[EquipmentHire] ADD CONSTRAINT [PK_Equ_Id_Equ] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Eqi_Id_Eqi', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eqi_Id_Eqi]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [PK_Eqi_Id_Eqi]
GO
/* no fk for 'PK_Eqi_Id_Eqi', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eqi_Id_Eqi]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [PK_Eqi_Id_Eqi]
GO
ALTER TABLE [dbo].[EquipmentHireItem] ADD CONSTRAINT [PK_Eqi_Id_Eqi] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [PK_Eve_Id_Eve]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [PK_Eve_Id_Eve]
GO
ALTER TABLE [dbo].[Event] ADD CONSTRAINT [PK_Eve_Id_Eve] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evn_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [PK_Evn_Id_Evn]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evn_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [PK_Evn_Id_Evn]
GO
ALTER TABLE [dbo].[EventBooking] ADD CONSTRAINT [PK_Evn_Id_Evn] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[EventBookingCancellation]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* no fk for 'PK_EvF_Id_EvF', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvF_Id_EvF]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [PK_EvF_Id_EvF]
GO
/* no fk for 'PK_EvF_Id_EvF', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvF_Id_EvF]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [PK_EvF_Id_EvF]
GO
ALTER TABLE [dbo].[EventFee] ADD CONSTRAINT [PK_EvF_Id_EvF] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvR_Id_EvR]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [PK_EvR_Id_EvR]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvR_Id_EvR]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [PK_EvR_Id_EvR]
GO
ALTER TABLE [dbo].[EventRegistration] ADD CONSTRAINT [PK_EvR_Id_EvR] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Evo_Id_Evo', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evo_Id_Evo]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [PK_Evo_Id_Evo]
GO
/* no fk for 'PK_Evo_Id_Evo', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evo_Id_Evo]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [PK_Evo_Id_Evo]
GO
ALTER TABLE [dbo].[EventRoll] ADD CONSTRAINT [PK_Evo_Id_Evo] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_EvW_Id_EvW', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvW_Id_EvW]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [PK_EvW_Id_EvW]
GO
/* no fk for 'PK_EvW_Id_EvW', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvW_Id_EvW]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [PK_EvW_Id_EvW]
GO
ALTER TABLE [dbo].[EventWaitlist] ADD CONSTRAINT [PK_EvW_Id_EvW] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [PK_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [PK_Fac_Id_Fac]
GO
ALTER TABLE [dbo].[Facility] ADD CONSTRAINT [PK_Fac_Id_Fac] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [PK_Gen_Id_Gen]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [PK_Gen_Id_Gen]
GO
ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [PK_Gen_Id_Gen] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ged_Id_Ged]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [PK_Ged_Id_Ged]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ged_Id_Ged]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [PK_Ged_Id_Ged]
GO
ALTER TABLE [dbo].[GenderAge] ADD CONSTRAINT [PK_Ged_Id_Ged] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [PK_Inv_Id_Inv]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [PK_Inv_Id_Inv]
GO
ALTER TABLE [dbo].[Invoice] ADD CONSTRAINT [PK_Inv_Id_Inv] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Journal]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [PK_Mem_Id_Mem]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [PK_Mem_Id_Mem]
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [PK_Mem_Id_Mem] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Meb_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [PK_Meb_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Meb_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [PK_Meb_Id_Meb]
GO
ALTER TABLE [dbo].[MembershipType] ADD CONSTRAINT [PK_Meb_Id_Meb] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Mee_Id_Mee', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mee_Id_Mee]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [PK_Mee_Id_Mee]
GO
/* no fk for 'PK_Mee_Id_Mee', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mee_Id_Mee]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [PK_Mee_Id_Mee]
GO
ALTER TABLE [dbo].[MembershipTypeTimePeriod] ADD CONSTRAINT [PK_Mee_Id_Mee] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[MessageAccount]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[MessageLog]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[MessageNumber]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[MessageService]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[Order]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[OrderItem]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[OrderItemDetail]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [PK_Pla_Id_Pla]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [PK_Pla_Id_Pla]
GO
ALTER TABLE [dbo].[PlayerLevel] ADD CONSTRAINT [PK_Pla_Id_Pla] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ply_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [PK_Ply_Id_Ply]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ply_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [PK_Ply_Id_Ply]
GO
ALTER TABLE [dbo].[PlayerRating] ADD CONSTRAINT [PK_Ply_Id_Ply] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* WARNING: cannot drop constraint 'PK_Pro_Id_Pro' because it is used as the unique index for full text */
/* WARNING: cannot drop constraint 'PK_Pro_Id_Pro' because it is used as the unique index for full text */
/* table '[dbo].[ProductBrand]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pru_Id_Pru]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [PK_Pru_Id_Pru]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pru_Id_Pru]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [PK_Pru_Id_Pru]
GO
ALTER TABLE [dbo].[ProductClass] ADD CONSTRAINT [PK_Pru_Id_Pru] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Prc_Id_Prc', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prc_Id_Prc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [PK_Prc_Id_Prc]
GO
/* no fk for 'PK_Prc_Id_Prc', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prc_Id_Prc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [PK_Prc_Id_Prc]
GO
ALTER TABLE [dbo].[ProductImage] ADD CONSTRAINT [PK_Prc_Id_Prc] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Prt_Id_Prt', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prt_Id_Prt]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [PK_Prt_Id_Prt]
GO
/* no fk for 'PK_Prt_Id_Prt', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prt_Id_Prt]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [PK_Prt_Id_Prt]
GO
ALTER TABLE [dbo].[ProductInventory] ADD CONSTRAINT [PK_Prt_Id_Prt] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prp_Id_Prp]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [PK_Prp_Id_Prp]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prp_Id_Prp]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [PK_Prp_Id_Prp]
GO
ALTER TABLE [dbo].[ProductOption] ADD CONSTRAINT [PK_Prp_Id_Prp] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pry_Id_Pry]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [PK_Pry_Id_Pry]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pry_Id_Pry]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [PK_Pry_Id_Pry]
GO
ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [PK_Pry_Id_Pry] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* WARNING: cannot drop constraint 'PK_PrV_Id_PrV' because it is used as the unique index for full text */
/* WARNING: cannot drop constraint 'PK_PrV_Id_PrV' because it is used as the unique index for full text */
/* table '[dbo].[ProductVendor]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[IX_Rol_Nam_Rol]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [IX_Rol_Nam_Rol]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [PK_Rol_Id_Rol]
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [PK_Rol_Id_Rol] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[IX_Rol_Nam_Rol]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [IX_Rol_Nam_Rol]
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [IX_Rol_Nam_Rol] UNIQUE
(

  [Name] )
/* no fk for 'PK_Roe_Id_Roe', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Roe_Id_Roe]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [PK_Roe_Id_Roe]
GO
/* no fk for 'PK_Roe_Id_Roe', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Roe_Id_Roe]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [PK_Roe_Id_Roe]
GO
ALTER TABLE [dbo].[RoleClaim] ADD CONSTRAINT [PK_Roe_Id_Roe] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Sale]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* column 'Id', old identity:False, new identity: True*/
/* WARNING: Existing database column 'Id' is not identity while model column '[SaleItem].[Id/Id/DbType.Int32]' is. Cannot drop or create IDENTITY on existing columns. You need to do it manually using temp tables or deleting the table. */
/* no fk for 'PK_Sae_Id_Sae', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sae_Id_Sae]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [PK_Sae_Id_Sae]
GO
/* no fk for 'PK_Sae_Id_Sae', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sae_Id_Sae]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [PK_Sae_Id_Sae]
GO
ALTER TABLE [dbo].[SaleItem] ADD CONSTRAINT [PK_Sae_Id_Sae] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Siz_Id_Siz', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Siz_Id_Siz]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [PK_Siz_Id_Siz]
GO
/* no fk for 'PK_Siz_Id_Siz', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Siz_Id_Siz]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [PK_Siz_Id_Siz]
GO
ALTER TABLE [dbo].[Size] ADD CONSTRAINT [PK_Siz_Id_Siz] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Source]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [PK_Sto_Id_Sto]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [PK_Sto_Id_Sto]
GO
ALTER TABLE [dbo].[Stocktake] ADD CONSTRAINT [PK_Sto_Id_Sto] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Stc_Id_Stc', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Stc_Id_Stc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [PK_Stc_Id_Stc]
GO
/* no fk for 'PK_Stc_Id_Stc', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Stc_Id_Stc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [PK_Stc_Id_Stc]
GO
ALTER TABLE [dbo].[StocktakeItem] ADD CONSTRAINT [PK_Stc_Id_Stc] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [PK_Sup_Id_Sup]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [PK_Sup_Id_Sup]
GO
ALTER TABLE [dbo].[Supplier] ADD CONSTRAINT [PK_Sup_Id_Sup] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Sul_Id_Sul', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sul_Id_Sul]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [PK_Sul_Id_Sul]
GO
/* no fk for 'PK_Sul_Id_Sul', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sul_Id_Sul]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [PK_Sul_Id_Sul]
GO
ALTER TABLE [dbo].[SupplierContact] ADD CONSTRAINT [PK_Sul_Id_Sul] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [PK_Sur_Id_Sur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [PK_Sur_Id_Sur]
GO
ALTER TABLE [dbo].[Surface] ADD CONSTRAINT [PK_Sur_Id_Sur] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Term]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [PK_Tim_Id_Tim]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [PK_Tim_Id_Tim]
GO
ALTER TABLE [dbo].[TimePeriod] ADD CONSTRAINT [PK_Tim_Id_Tim] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tie_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [PK_Tie_Id_Tie]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tie_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [PK_Tie_Id_Tie]
GO
ALTER TABLE [dbo].[TimePeriodGroup] ADD CONSTRAINT [PK_Tie_Id_Tie] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* table '[dbo].[Transaction]' primary key is related to a clustered index and cannot be changed on SQL Azure */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[IX_Use_Ema_Use]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [IX_Use_Ema_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [PK_Use_Id_Use]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [PK_Use_Id_Use] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[IX_Use_Ema_Use]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [IX_Use_Ema_Use]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [IX_Use_Ema_Use] UNIQUE
(

  [Email] )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[IX_Use_Use_Use]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [IX_Use_Use_Use]
GO
ALTER TABLE [dbo].[User] ADD CONSTRAINT [IX_Use_Use_Use] UNIQUE
(

  [UserName] )
/* no fk for 'PK_Usr_Id_Usr', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Usr_Id_Usr]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [PK_Usr_Id_Usr]
GO
/* no fk for 'PK_Usr_Id_Usr', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Usr_Id_Usr]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [PK_Usr_Id_Usr]
GO
ALTER TABLE [dbo].[UserClaim] ADD CONSTRAINT [PK_Usr_Id_Usr] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_UsL_Id_UsL', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_UsL_Id_UsL]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [PK_UsL_Id_UsL]
GO
/* no fk for 'PK_UsL_Id_UsL', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_UsL_Id_UsL]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [PK_UsL_Id_UsL]
GO
ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [PK_UsL_Id_UsL] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [PK_Zon_Id_Zon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [PK_Zon_Id_Zon]
GO
ALTER TABLE [dbo].[Zone] ADD CONSTRAINT [PK_Zon_Id_Zon] PRIMARY KEY NONCLUSTERED
(

 [Id]
 )
/* no fk for 'PK_Ev__Id_Id2_Ev_', tableName='Event_Coaches_Coach' table='Event_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ev__Id_Id2_Ev_]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [PK_Ev__Id_Id2_Ev_]
GO
/* no fk for 'PK_Ev__Id_Id2_Ev_', tableName='Event_Coaches_Coach' table='Event_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ev__Id_Id2_Ev_]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [PK_Ev__Id_Id2_Ev_]
GO
ALTER TABLE [dbo].[Event_Coaches_Coach] ADD CONSTRAINT [PK_Ev__Id_Id2_Ev_] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_EvG_Id_Id2_EvG', tableName='Event_GenderAges_GenderAge' table='Event_GenderAges_GenderAge' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvG_Id_Id2_EvG]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [PK_EvG_Id_Id2_EvG]
GO
/* no fk for 'PK_EvG_Id_Id2_EvG', tableName='Event_GenderAges_GenderAge' table='Event_GenderAges_GenderAge' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvG_Id_Id2_EvG]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [PK_EvG_Id_Id2_EvG]
GO
ALTER TABLE [dbo].[Event_GenderAges_GenderAge] ADD CONSTRAINT [PK_EvG_Id_Id2_EvG] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_EvL_Id_Id2_EvL', tableName='Event_Levels_PlayerLevel' table='Event_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvL_Id_Id2_EvL]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [PK_EvL_Id_Id2_EvL]
GO
/* no fk for 'PK_EvL_Id_Id2_EvL', tableName='Event_Levels_PlayerLevel' table='Event_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvL_Id_Id2_EvL]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [PK_EvL_Id_Id2_EvL]
GO
ALTER TABLE [dbo].[Event_Levels_PlayerLevel] ADD CONSTRAINT [PK_EvL_Id_Id2_EvL] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_EvC_Id_Id2_EvC', tableName='Event_Courts_Court' table='Event_Courts_Court' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvC_Id_Id2_EvC]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [PK_EvC_Id_Id2_EvC]
GO
/* no fk for 'PK_EvC_Id_Id2_EvC', tableName='Event_Courts_Court' table='Event_Courts_Court' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvC_Id_Id2_EvC]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [PK_EvC_Id_Id2_EvC]
GO
ALTER TABLE [dbo].[Event_Courts_Court] ADD CONSTRAINT [PK_EvC_Id_Id2_EvC] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_EvD_Id_Id2_EvD', tableName='Event_Days_DaysOfWeek' table='Event_Days_DaysOfWeek' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvD_Id_Id2_EvD]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [PK_EvD_Id_Id2_EvD]
GO
/* no fk for 'PK_EvD_Id_Id2_EvD', tableName='Event_Days_DaysOfWeek' table='Event_Days_DaysOfWeek' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvD_Id_Id2_EvD]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [PK_EvD_Id_Id2_EvD]
GO
ALTER TABLE [dbo].[Event_Days_DaysOfWeek] ADD CONSTRAINT [PK_EvD_Id_Id2_EvD] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_EvB_Id_Id2_EvB', tableName='EventBooking_Coaches_Coach' table='EventBooking_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvB_Id_Id2_EvB]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [PK_EvB_Id_Id2_EvB]
GO
/* no fk for 'PK_EvB_Id_Id2_EvB', tableName='EventBooking_Coaches_Coach' table='EventBooking_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvB_Id_Id2_EvB]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [PK_EvB_Id_Id2_EvB]
GO
ALTER TABLE [dbo].[EventBooking_Coaches_Coach] ADD CONSTRAINT [PK_EvB_Id_Id2_EvB] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_Evk_Id_Id2_Evk', tableName='EventBooking_Booking_Booking' table='EventBooking_Booking_Booking' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evk_Id_Id2_Evk]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [PK_Evk_Id_Id2_Evk]
GO
/* no fk for 'PK_Evk_Id_Id2_Evk', tableName='EventBooking_Booking_Booking' table='EventBooking_Booking_Booking' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evk_Id_Id2_Evk]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [PK_Evk_Id_Id2_Evk]
GO
ALTER TABLE [dbo].[EventBooking_Booking_Booking] ADD CONSTRAINT [PK_Evk_Id_Id2_Evk] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_Evi_Id_Id2_Evi', tableName='EventBooking_Levels_PlayerLevel' table='EventBooking_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evi_Id_Id2_Evi]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [PK_Evi_Id_Id2_Evi]
GO
/* no fk for 'PK_Evi_Id_Id2_Evi', tableName='EventBooking_Levels_PlayerLevel' table='EventBooking_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evi_Id_Id2_Evi]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [PK_Evi_Id_Id2_Evi]
GO
ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] ADD CONSTRAINT [PK_Evi_Id_Id2_Evi] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* table '[dbo].[Invoice_Journals_Journal]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* table '[dbo].[OrderItem_Transactions_Transaction]' primary key is related to a clustered index and cannot be changed on SQL Azure */
/* no fk for 'PK_Ple_Id_Id2_Ple', tableName='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' table='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ple_Id_Id2_Ple]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [PK_Ple_Id_Id2_Ple]
GO
/* no fk for 'PK_Ple_Id_Id2_Ple', tableName='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' table='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ple_Id_Id2_Ple]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [PK_Ple_Id_Id2_Ple]
GO
ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] ADD CONSTRAINT [PK_Ple_Id_Id2_Ple] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
/* no fk for 'PK_Ro__Id_Id2_Ro_', tableName='Role_User_User_Roles' table='Role_User_User_Roles' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ro__Id_Id2_Ro_]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [PK_Ro__Id_Id2_Ro_]
GO
/* no fk for 'PK_Ro__Id_Id2_Ro_', tableName='Role_User_User_Roles' table='Role_User_User_Roles' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ro__Id_Id2_Ro_]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [PK_Ro__Id_Id2_Ro_]
GO
ALTER TABLE [dbo].[Role_User_User_Roles] ADD CONSTRAINT [PK_Ro__Id_Id2_Ro_] PRIMARY KEY NONCLUSTERED
(

 [Id],
 [Id2]
 )
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tc]') AND parent_obj = object_id(N'[dbo].[Account]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tc]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Acc__tc]

 ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Acc__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tk]') AND parent_obj = object_id(N'[dbo].[Account]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tk]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Acc__tk]

 ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Acc__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tc]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tc]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
 ALTER TABLE [dbo].[AccountingPeriod] DROP CONSTRAINT [DF_Aco__tc]

 ALTER TABLE [dbo].[AccountingPeriod] ADD CONSTRAINT [DF_Aco__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tk]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tk]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
 ALTER TABLE [dbo].[AccountingPeriod] DROP CONSTRAINT [DF_Aco__tk]

 ALTER TABLE [dbo].[AccountingPeriod] ADD CONSTRAINT [DF_Aco__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tc]') AND parent_obj = object_id(N'[dbo].[Admin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tc]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [DF_Adm__tc]

 ALTER TABLE [dbo].[Admin] ADD CONSTRAINT [DF_Adm__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tk]') AND parent_obj = object_id(N'[dbo].[Admin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tk]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [DF_Adm__tk]

 ALTER TABLE [dbo].[Admin] ADD CONSTRAINT [DF_Adm__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tc]') AND parent_obj = object_id(N'[dbo].[Batch]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tc]') AND parent_obj = object_id(N'[dbo].[Batch]'))
 ALTER TABLE [dbo].[Batch] DROP CONSTRAINT [DF_Bat__tc]

 ALTER TABLE [dbo].[Batch] ADD CONSTRAINT [DF_Bat__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tk]') AND parent_obj = object_id(N'[dbo].[Batch]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tk]') AND parent_obj = object_id(N'[dbo].[Batch]'))
 ALTER TABLE [dbo].[Batch] DROP CONSTRAINT [DF_Bat__tk]

 ALTER TABLE [dbo].[Batch] ADD CONSTRAINT [DF_Bat__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tc]') AND parent_obj = object_id(N'[dbo].[Booking]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tc]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [DF_Boo__tc]

 ALTER TABLE [dbo].[Booking] ADD CONSTRAINT [DF_Boo__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tk]') AND parent_obj = object_id(N'[dbo].[Booking]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tk]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [DF_Boo__tk]

 ALTER TABLE [dbo].[Booking] ADD CONSTRAINT [DF_Boo__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tc]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tc]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [DF_Bok__tc]

 ALTER TABLE [dbo].[BookingStatus] ADD CONSTRAINT [DF_Bok__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tk]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tk]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [DF_Bok__tk]

 ALTER TABLE [dbo].[BookingStatus] ADD CONSTRAINT [DF_Bok__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tc]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tc]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [DF_Boi__tc]

 ALTER TABLE [dbo].[BookingSubType] ADD CONSTRAINT [DF_Boi__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tk]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tk]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [DF_Boi__tk]

 ALTER TABLE [dbo].[BookingSubType] ADD CONSTRAINT [DF_Boi__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tc]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tc]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [DF_Bon__tc]

 ALTER TABLE [dbo].[BookingType] ADD CONSTRAINT [DF_Bon__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tk]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tk]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [DF_Bon__tk]

 ALTER TABLE [dbo].[BookingType] ADD CONSTRAINT [DF_Bon__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tc]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tc]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
 ALTER TABLE [dbo].[CancellationType] DROP CONSTRAINT [DF_Can__tc]

 ALTER TABLE [dbo].[CancellationType] ADD CONSTRAINT [DF_Can__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tk]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tk]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
 ALTER TABLE [dbo].[CancellationType] DROP CONSTRAINT [DF_Can__tk]

 ALTER TABLE [dbo].[CancellationType] ADD CONSTRAINT [DF_Can__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tc]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tc]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [DF_Che__tc]

 ALTER TABLE [dbo].[Checkout] ADD CONSTRAINT [DF_Che__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tk]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tk]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [DF_Che__tk]

 ALTER TABLE [dbo].[Checkout] ADD CONSTRAINT [DF_Che__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tc]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tc]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [DF_Chc__tc]

 ALTER TABLE [dbo].[CheckoutItem] ADD CONSTRAINT [DF_Chc__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tk]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tk]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [DF_Chc__tk]

 ALTER TABLE [dbo].[CheckoutItem] ADD CONSTRAINT [DF_Chc__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tc]') AND parent_obj = object_id(N'[dbo].[Club]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tc]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [DF_Clu__tc]

 ALTER TABLE [dbo].[Club] ADD CONSTRAINT [DF_Clu__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tk]') AND parent_obj = object_id(N'[dbo].[Club]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tk]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [DF_Clu__tk]

 ALTER TABLE [dbo].[Club] ADD CONSTRAINT [DF_Clu__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tc]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tc]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [DF_Clb__tc]

 ALTER TABLE [dbo].[ClubAdmin] ADD CONSTRAINT [DF_Clb__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tk]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tk]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [DF_Clb__tk]

 ALTER TABLE [dbo].[ClubAdmin] ADD CONSTRAINT [DF_Clb__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tc]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tc]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [DF_ClC__tc]

 ALTER TABLE [dbo].[ClubComponents] ADD CONSTRAINT [DF_ClC__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tk]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tk]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [DF_ClC__tk]

 ALTER TABLE [dbo].[ClubComponents] ADD CONSTRAINT [DF_ClC__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tc]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tc]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [DF_ClP__tc]

 ALTER TABLE [dbo].[ClubPlayerLevel] ADD CONSTRAINT [DF_ClP__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tk]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tk]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [DF_ClP__tk]

 ALTER TABLE [dbo].[ClubPlayerLevel] ADD CONSTRAINT [DF_ClP__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tc]') AND parent_obj = object_id(N'[dbo].[Coach]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tc]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [DF_Coa__tc]

 ALTER TABLE [dbo].[Coach] ADD CONSTRAINT [DF_Coa__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tk]') AND parent_obj = object_id(N'[dbo].[Coach]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tk]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [DF_Coa__tk]

 ALTER TABLE [dbo].[Coach] ADD CONSTRAINT [DF_Coa__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tc]') AND parent_obj = object_id(N'[dbo].[Components]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tc]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [DF_Com__tc]

 ALTER TABLE [dbo].[Components] ADD CONSTRAINT [DF_Com__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tk]') AND parent_obj = object_id(N'[dbo].[Components]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tk]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [DF_Com__tk]

 ALTER TABLE [dbo].[Components] ADD CONSTRAINT [DF_Com__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tc]') AND parent_obj = object_id(N'[dbo].[Court]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tc]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [DF_Cou__tc]

 ALTER TABLE [dbo].[Court] ADD CONSTRAINT [DF_Cou__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tk]') AND parent_obj = object_id(N'[dbo].[Court]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tk]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [DF_Cou__tk]

 ALTER TABLE [dbo].[Court] ADD CONSTRAINT [DF_Cou__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tc]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tc]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [DF_Cor__tc]

 ALTER TABLE [dbo].[CourtHireRate] ADD CONSTRAINT [DF_Cor__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tk]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tk]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [DF_Cor__tk]

 ALTER TABLE [dbo].[CourtHireRate] ADD CONSTRAINT [DF_Cor__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tc]') AND parent_obj = object_id(N'[dbo].[Credit]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tc]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [DF_Cre__tc]

 ALTER TABLE [dbo].[Credit] ADD CONSTRAINT [DF_Cre__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tk]') AND parent_obj = object_id(N'[dbo].[Credit]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tk]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [DF_Cre__tk]

 ALTER TABLE [dbo].[Credit] ADD CONSTRAINT [DF_Cre__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tc]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tc]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [DF_Crd__tc]

 ALTER TABLE [dbo].[CreditAdjust] ADD CONSTRAINT [DF_Crd__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tk]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tk]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [DF_Crd__tk]

 ALTER TABLE [dbo].[CreditAdjust] ADD CONSTRAINT [DF_Crd__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tc]') AND parent_obj = object_id(N'[dbo].[Currency]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tc]') AND parent_obj = object_id(N'[dbo].[Currency]'))
 ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Cur__tc]

 ALTER TABLE [dbo].[Currency] ADD CONSTRAINT [DF_Cur__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tk]') AND parent_obj = object_id(N'[dbo].[Currency]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tk]') AND parent_obj = object_id(N'[dbo].[Currency]'))
 ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Cur__tk]

 ALTER TABLE [dbo].[Currency] ADD CONSTRAINT [DF_Cur__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tc]') AND parent_obj = object_id(N'[dbo].[Customer]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tc]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Cus__tc]

 ALTER TABLE [dbo].[Customer] ADD CONSTRAINT [DF_Cus__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tk]') AND parent_obj = object_id(N'[dbo].[Customer]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tk]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Cus__tk]

 ALTER TABLE [dbo].[Customer] ADD CONSTRAINT [DF_Cus__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tc]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tc]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
 ALTER TABLE [dbo].[DaysOfWeek] DROP CONSTRAINT [DF_Day__tc]

 ALTER TABLE [dbo].[DaysOfWeek] ADD CONSTRAINT [DF_Day__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tk]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tk]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
 ALTER TABLE [dbo].[DaysOfWeek] DROP CONSTRAINT [DF_Day__tk]

 ALTER TABLE [dbo].[DaysOfWeek] ADD CONSTRAINT [DF_Day__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tc]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tc]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [DF_Del__tc]

 ALTER TABLE [dbo].[Delivery] ADD CONSTRAINT [DF_Del__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tk]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tk]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [DF_Del__tk]

 ALTER TABLE [dbo].[Delivery] ADD CONSTRAINT [DF_Del__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tc]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tc]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [DF_Dei__tc]

 ALTER TABLE [dbo].[DeliveryItem] ADD CONSTRAINT [DF_Dei__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tk]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tk]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [DF_Dei__tk]

 ALTER TABLE [dbo].[DeliveryItem] ADD CONSTRAINT [DF_Dei__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tc]') AND parent_obj = object_id(N'[dbo].[Discount]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tc]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [DF_Dis__tc]

 ALTER TABLE [dbo].[Discount] ADD CONSTRAINT [DF_Dis__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tk]') AND parent_obj = object_id(N'[dbo].[Discount]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tk]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [DF_Dis__tk]

 ALTER TABLE [dbo].[Discount] ADD CONSTRAINT [DF_Dis__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tc]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tc]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [DF_Dic__tc]

 ALTER TABLE [dbo].[DiscountGroup] ADD CONSTRAINT [DF_Dic__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tk]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tk]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [DF_Dic__tk]

 ALTER TABLE [dbo].[DiscountGroup] ADD CONSTRAINT [DF_Dic__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [DF_Equ__tc]

 ALTER TABLE [dbo].[EquipmentHire] ADD CONSTRAINT [DF_Equ__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [DF_Equ__tk]

 ALTER TABLE [dbo].[EquipmentHire] ADD CONSTRAINT [DF_Equ__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [DF_Eqi__tc]

 ALTER TABLE [dbo].[EquipmentHireItem] ADD CONSTRAINT [DF_Eqi__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [DF_Eqi__tk]

 ALTER TABLE [dbo].[EquipmentHireItem] ADD CONSTRAINT [DF_Eqi__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tc]') AND parent_obj = object_id(N'[dbo].[Event]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tc]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [DF_Eve__tc]

 ALTER TABLE [dbo].[Event] ADD CONSTRAINT [DF_Eve__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tk]') AND parent_obj = object_id(N'[dbo].[Event]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tk]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [DF_Eve__tk]

 ALTER TABLE [dbo].[Event] ADD CONSTRAINT [DF_Eve__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tc]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tc]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [DF_Evn__tc]

 ALTER TABLE [dbo].[EventBooking] ADD CONSTRAINT [DF_Evn__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tk]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tk]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [DF_Evn__tk]

 ALTER TABLE [dbo].[EventBooking] ADD CONSTRAINT [DF_Evn__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tc]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tc]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [DF_Evt__tc]

 ALTER TABLE [dbo].[EventBookingCancellation] ADD CONSTRAINT [DF_Evt__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tk]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tk]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [DF_Evt__tk]

 ALTER TABLE [dbo].[EventBookingCancellation] ADD CONSTRAINT [DF_Evt__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tc]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tc]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [DF_EvF__tc]

 ALTER TABLE [dbo].[EventFee] ADD CONSTRAINT [DF_EvF__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tk]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tk]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [DF_EvF__tk]

 ALTER TABLE [dbo].[EventFee] ADD CONSTRAINT [DF_EvF__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tc]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tc]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [DF_EvR__tc]

 ALTER TABLE [dbo].[EventRegistration] ADD CONSTRAINT [DF_EvR__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tk]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tk]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [DF_EvR__tk]

 ALTER TABLE [dbo].[EventRegistration] ADD CONSTRAINT [DF_EvR__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tc]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tc]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [DF_Evo__tc]

 ALTER TABLE [dbo].[EventRoll] ADD CONSTRAINT [DF_Evo__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tk]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tk]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [DF_Evo__tk]

 ALTER TABLE [dbo].[EventRoll] ADD CONSTRAINT [DF_Evo__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tc]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tc]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [DF_EvW__tc]

 ALTER TABLE [dbo].[EventWaitlist] ADD CONSTRAINT [DF_EvW__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tk]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tk]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [DF_EvW__tk]

 ALTER TABLE [dbo].[EventWaitlist] ADD CONSTRAINT [DF_EvW__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tc]') AND parent_obj = object_id(N'[dbo].[Facility]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tc]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [DF_Fac__tc]

 ALTER TABLE [dbo].[Facility] ADD CONSTRAINT [DF_Fac__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tk]') AND parent_obj = object_id(N'[dbo].[Facility]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tk]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [DF_Fac__tk]

 ALTER TABLE [dbo].[Facility] ADD CONSTRAINT [DF_Fac__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tc]') AND parent_obj = object_id(N'[dbo].[Gender]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tc]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [DF_Gen__tc]

 ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [DF_Gen__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tk]') AND parent_obj = object_id(N'[dbo].[Gender]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tk]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [DF_Gen__tk]

 ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [DF_Gen__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tc]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tc]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [DF_Ged__tc]

 ALTER TABLE [dbo].[GenderAge] ADD CONSTRAINT [DF_Ged__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tk]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tk]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [DF_Ged__tk]

 ALTER TABLE [dbo].[GenderAge] ADD CONSTRAINT [DF_Ged__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tc]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tc]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [DF_Inv__tc]

 ALTER TABLE [dbo].[Invoice] ADD CONSTRAINT [DF_Inv__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tk]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tk]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [DF_Inv__tk]

 ALTER TABLE [dbo].[Invoice] ADD CONSTRAINT [DF_Inv__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tc]') AND parent_obj = object_id(N'[dbo].[Journal]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tc]') AND parent_obj = object_id(N'[dbo].[Journal]'))
 ALTER TABLE [dbo].[Journal] DROP CONSTRAINT [DF_Jou__tc]

 ALTER TABLE [dbo].[Journal] ADD CONSTRAINT [DF_Jou__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tk]') AND parent_obj = object_id(N'[dbo].[Journal]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tk]') AND parent_obj = object_id(N'[dbo].[Journal]'))
 ALTER TABLE [dbo].[Journal] DROP CONSTRAINT [DF_Jou__tk]

 ALTER TABLE [dbo].[Journal] ADD CONSTRAINT [DF_Jou__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tc]') AND parent_obj = object_id(N'[dbo].[Member]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tc]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [DF_Mem__tc]

 ALTER TABLE [dbo].[Member] ADD CONSTRAINT [DF_Mem__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tk]') AND parent_obj = object_id(N'[dbo].[Member]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tk]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [DF_Mem__tk]

 ALTER TABLE [dbo].[Member] ADD CONSTRAINT [DF_Mem__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tc]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tc]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [DF_Meb__tc]

 ALTER TABLE [dbo].[MembershipType] ADD CONSTRAINT [DF_Meb__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tk]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tk]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [DF_Meb__tk]

 ALTER TABLE [dbo].[MembershipType] ADD CONSTRAINT [DF_Meb__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tc]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tc]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [DF_Mee__tc]

 ALTER TABLE [dbo].[MembershipTypeTimePeriod] ADD CONSTRAINT [DF_Mee__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tk]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tk]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [DF_Mee__tk]

 ALTER TABLE [dbo].[MembershipTypeTimePeriod] ADD CONSTRAINT [DF_Mee__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tc]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tc]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [DF_Mes__tc]

 ALTER TABLE [dbo].[MessageAccount] ADD CONSTRAINT [DF_Mes__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tk]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tk]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [DF_Mes__tk]

 ALTER TABLE [dbo].[MessageAccount] ADD CONSTRAINT [DF_Mes__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tc]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tc]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [DF_Mea__tc]

 ALTER TABLE [dbo].[MessageLog] ADD CONSTRAINT [DF_Mea__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tk]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tk]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [DF_Mea__tk]

 ALTER TABLE [dbo].[MessageLog] ADD CONSTRAINT [DF_Mea__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tc]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tc]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [DF_Meg__tc]

 ALTER TABLE [dbo].[MessageNumber] ADD CONSTRAINT [DF_Meg__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tk]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tk]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [DF_Meg__tk]

 ALTER TABLE [dbo].[MessageNumber] ADD CONSTRAINT [DF_Meg__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tc]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tc]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [DF_Mer__tc]

 ALTER TABLE [dbo].[MessageService] ADD CONSTRAINT [DF_Mer__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tk]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tk]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [DF_Mer__tk]

 ALTER TABLE [dbo].[MessageService] ADD CONSTRAINT [DF_Mer__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tc]') AND parent_obj = object_id(N'[dbo].[Order]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tc]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [DF_Ord__tc]

 ALTER TABLE [dbo].[Order] ADD CONSTRAINT [DF_Ord__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tk]') AND parent_obj = object_id(N'[dbo].[Order]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tk]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [DF_Ord__tk]

 ALTER TABLE [dbo].[Order] ADD CONSTRAINT [DF_Ord__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tc]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tc]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [DF_Ore__tc]

 ALTER TABLE [dbo].[OrderItem] ADD CONSTRAINT [DF_Ore__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tk]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tk]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [DF_Ore__tk]

 ALTER TABLE [dbo].[OrderItem] ADD CONSTRAINT [DF_Ore__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tc]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tc]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [DF_Orr__tc]

 ALTER TABLE [dbo].[OrderItemDetail] ADD CONSTRAINT [DF_Orr__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tk]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tk]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [DF_Orr__tk]

 ALTER TABLE [dbo].[OrderItemDetail] ADD CONSTRAINT [DF_Orr__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tc]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tc]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [DF_Pla__tc]

 ALTER TABLE [dbo].[PlayerLevel] ADD CONSTRAINT [DF_Pla__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tk]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tk]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [DF_Pla__tk]

 ALTER TABLE [dbo].[PlayerLevel] ADD CONSTRAINT [DF_Pla__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tc]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tc]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [DF_Ply__tc]

 ALTER TABLE [dbo].[PlayerRating] ADD CONSTRAINT [DF_Ply__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tk]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tk]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [DF_Ply__tk]

 ALTER TABLE [dbo].[PlayerRating] ADD CONSTRAINT [DF_Ply__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tc]') AND parent_obj = object_id(N'[dbo].[Product]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tc]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [DF_Pro__tc]

 ALTER TABLE [dbo].[Product] ADD CONSTRAINT [DF_Pro__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tk]') AND parent_obj = object_id(N'[dbo].[Product]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tk]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [DF_Pro__tk]

 ALTER TABLE [dbo].[Product] ADD CONSTRAINT [DF_Pro__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tc]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tc]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [DF_Prd__tc]

 ALTER TABLE [dbo].[ProductBrand] ADD CONSTRAINT [DF_Prd__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tk]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tk]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [DF_Prd__tk]

 ALTER TABLE [dbo].[ProductBrand] ADD CONSTRAINT [DF_Prd__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tc]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tc]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [DF_Pru__tc]

 ALTER TABLE [dbo].[ProductClass] ADD CONSTRAINT [DF_Pru__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tk]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tk]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [DF_Pru__tk]

 ALTER TABLE [dbo].[ProductClass] ADD CONSTRAINT [DF_Pru__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [DF_Prc__tc]

 ALTER TABLE [dbo].[ProductImage] ADD CONSTRAINT [DF_Prc__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tk]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tk]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [DF_Prc__tk]

 ALTER TABLE [dbo].[ProductImage] ADD CONSTRAINT [DF_Prc__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tc]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tc]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [DF_Prt__tc]

 ALTER TABLE [dbo].[ProductInventory] ADD CONSTRAINT [DF_Prt__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tk]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tk]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [DF_Prt__tk]

 ALTER TABLE [dbo].[ProductInventory] ADD CONSTRAINT [DF_Prt__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tc]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tc]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [DF_Prp__tc]

 ALTER TABLE [dbo].[ProductOption] ADD CONSTRAINT [DF_Prp__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tk]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tk]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [DF_Prp__tk]

 ALTER TABLE [dbo].[ProductOption] ADD CONSTRAINT [DF_Prp__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tc]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tc]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [DF_Pry__tc]

 ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [DF_Pry__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tk]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tk]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [DF_Pry__tk]

 ALTER TABLE [dbo].[ProductType] ADD CONSTRAINT [DF_Pry__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tc]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tc]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [DF_PrV__tc]

 ALTER TABLE [dbo].[ProductVariant] ADD CONSTRAINT [DF_PrV__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tk]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tk]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [DF_PrV__tk]

 ALTER TABLE [dbo].[ProductVariant] ADD CONSTRAINT [DF_PrV__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tc]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tc]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [DF_Pre__tc]

 ALTER TABLE [dbo].[ProductVendor] ADD CONSTRAINT [DF_Pre__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tk]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tk]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [DF_Pre__tk]

 ALTER TABLE [dbo].[ProductVendor] ADD CONSTRAINT [DF_Pre__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tc]') AND parent_obj = object_id(N'[dbo].[Role]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tc]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [DF_Rol__tc]

 ALTER TABLE [dbo].[Role] ADD CONSTRAINT [DF_Rol__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tk]') AND parent_obj = object_id(N'[dbo].[Role]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tk]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [DF_Rol__tk]

 ALTER TABLE [dbo].[Role] ADD CONSTRAINT [DF_Rol__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tc]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tc]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [DF_Roe__tc]

 ALTER TABLE [dbo].[RoleClaim] ADD CONSTRAINT [DF_Roe__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tk]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tk]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [DF_Roe__tk]

 ALTER TABLE [dbo].[RoleClaim] ADD CONSTRAINT [DF_Roe__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tc]') AND parent_obj = object_id(N'[dbo].[Sale]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tc]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [DF_Sal__tc]

 ALTER TABLE [dbo].[Sale] ADD CONSTRAINT [DF_Sal__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tk]') AND parent_obj = object_id(N'[dbo].[Sale]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tk]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [DF_Sal__tk]

 ALTER TABLE [dbo].[Sale] ADD CONSTRAINT [DF_Sal__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tc]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tc]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [DF_Sae__tc]

 ALTER TABLE [dbo].[SaleItem] ADD CONSTRAINT [DF_Sae__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tk]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tk]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [DF_Sae__tk]

 ALTER TABLE [dbo].[SaleItem] ADD CONSTRAINT [DF_Sae__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tc]') AND parent_obj = object_id(N'[dbo].[Size]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tc]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [DF_Siz__tc]

 ALTER TABLE [dbo].[Size] ADD CONSTRAINT [DF_Siz__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tk]') AND parent_obj = object_id(N'[dbo].[Size]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tk]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [DF_Siz__tk]

 ALTER TABLE [dbo].[Size] ADD CONSTRAINT [DF_Siz__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tc]') AND parent_obj = object_id(N'[dbo].[Source]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tc]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [DF_Sou__tc]

 ALTER TABLE [dbo].[Source] ADD CONSTRAINT [DF_Sou__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tk]') AND parent_obj = object_id(N'[dbo].[Source]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tk]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [DF_Sou__tk]

 ALTER TABLE [dbo].[Source] ADD CONSTRAINT [DF_Sou__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tc]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tc]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [DF_Sto__tc]

 ALTER TABLE [dbo].[Stocktake] ADD CONSTRAINT [DF_Sto__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tk]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tk]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [DF_Sto__tk]

 ALTER TABLE [dbo].[Stocktake] ADD CONSTRAINT [DF_Sto__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [DF_Stc__tc]

 ALTER TABLE [dbo].[StocktakeItem] ADD CONSTRAINT [DF_Stc__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tk]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tk]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [DF_Stc__tk]

 ALTER TABLE [dbo].[StocktakeItem] ADD CONSTRAINT [DF_Stc__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tc]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tc]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [DF_Sup__tc]

 ALTER TABLE [dbo].[Supplier] ADD CONSTRAINT [DF_Sup__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tk]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tk]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [DF_Sup__tk]

 ALTER TABLE [dbo].[Supplier] ADD CONSTRAINT [DF_Sup__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tc]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tc]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [DF_Sul__tc]

 ALTER TABLE [dbo].[SupplierContact] ADD CONSTRAINT [DF_Sul__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tk]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tk]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [DF_Sul__tk]

 ALTER TABLE [dbo].[SupplierContact] ADD CONSTRAINT [DF_Sul__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tc]') AND parent_obj = object_id(N'[dbo].[Surface]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tc]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [DF_Sur__tc]

 ALTER TABLE [dbo].[Surface] ADD CONSTRAINT [DF_Sur__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tk]') AND parent_obj = object_id(N'[dbo].[Surface]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tk]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [DF_Sur__tk]

 ALTER TABLE [dbo].[Surface] ADD CONSTRAINT [DF_Sur__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tc]') AND parent_obj = object_id(N'[dbo].[Term]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tc]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [DF_Ter__tc]

 ALTER TABLE [dbo].[Term] ADD CONSTRAINT [DF_Ter__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tk]') AND parent_obj = object_id(N'[dbo].[Term]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tk]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [DF_Ter__tk]

 ALTER TABLE [dbo].[Term] ADD CONSTRAINT [DF_Ter__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [DF_Tim__tc]

 ALTER TABLE [dbo].[TimePeriod] ADD CONSTRAINT [DF_Tim__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [DF_Tim__tk]

 ALTER TABLE [dbo].[TimePeriod] ADD CONSTRAINT [DF_Tim__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [DF_Tie__tc]

 ALTER TABLE [dbo].[TimePeriodGroup] ADD CONSTRAINT [DF_Tie__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [DF_Tie__tk]

 ALTER TABLE [dbo].[TimePeriodGroup] ADD CONSTRAINT [DF_Tie__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Tra__tc]

 ALTER TABLE [dbo].[Transaction] ADD CONSTRAINT [DF_Tra__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tk]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tk]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Tra__tk]

 ALTER TABLE [dbo].[Transaction] ADD CONSTRAINT [DF_Tra__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tc]') AND parent_obj = object_id(N'[dbo].[User]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tc]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF_Use__tc]

 ALTER TABLE [dbo].[User] ADD CONSTRAINT [DF_Use__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tk]') AND parent_obj = object_id(N'[dbo].[User]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tk]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF_Use__tk]

 ALTER TABLE [dbo].[User] ADD CONSTRAINT [DF_Use__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tc]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tc]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [DF_Usr__tc]

 ALTER TABLE [dbo].[UserClaim] ADD CONSTRAINT [DF_Usr__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tk]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tk]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [DF_Usr__tk]

 ALTER TABLE [dbo].[UserClaim] ADD CONSTRAINT [DF_Usr__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tc]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tc]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [DF_UsL__tc]

 ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [DF_UsL__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tk]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tk]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [DF_UsL__tk]

 ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [DF_UsL__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tc]') AND parent_obj = object_id(N'[dbo].[Zone]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tc]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [DF_Zon__tc]

 ALTER TABLE [dbo].[Zone] ADD CONSTRAINT [DF_Zon__tc] DEFAULT (GETUTCDATE()) FOR [_trackLastWriteTime]
END
GO
IF NOT EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tk]') AND parent_obj = object_id(N'[dbo].[Zone]'))
BEGIN
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tk]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [DF_Zon__tk]

 ALTER TABLE [dbo].[Zone] ADD CONSTRAINT [DF_Zon__tk] DEFAULT (GETUTCDATE()) FOR [_trackCreationTime]
END
GO
