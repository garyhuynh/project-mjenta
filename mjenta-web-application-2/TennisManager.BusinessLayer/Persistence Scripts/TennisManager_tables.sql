﻿/* CodeFluent Generated Monday, 08 August 2016 12:43. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-AU. UiCulture:en-GB. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Account]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Account]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[AccountingPeriod]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[AccountingPeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Admin]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Admin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Batch]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Batch]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Booking]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Booking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingStatus]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BookingStatus]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingSubType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BookingSubType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[BookingType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BookingType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CancellationType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[CancellationType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Checkout]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Checkout]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CheckoutItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[CheckoutItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Club]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Club]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubAdmin]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ClubAdmin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubComponents]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ClubComponents]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ClubPlayerLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ClubPlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Coach]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Coach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Components]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Components]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Court]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Court]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CourtHireRate]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[CourtHireRate]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Credit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Credit]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[CreditAdjust]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[CreditAdjust]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Currency]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Currency]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Customer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Customer]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DaysOfWeek]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[DaysOfWeek]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Delivery]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Delivery]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DeliveryItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[DeliveryItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Discount]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Discount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DiscountGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[DiscountGroup]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHire]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EquipmentHire]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EquipmentHireItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EquipmentHireItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventBooking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBookingCancellation]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventBookingCancellation]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventFee]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventFee]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRegistration]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventRegistration]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventRoll]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventRoll]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventWaitlist]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventWaitlist]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Facility]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Facility]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Gender]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Gender]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[GenderAge]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[GenderAge]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Invoice]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Journal]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Journal]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Member]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Member]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MembershipType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MembershipTypeTimePeriod]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MembershipTypeTimePeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageAccount]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MessageAccount]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MessageLog]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageNumber]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MessageNumber]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[MessageService]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[MessageService]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Order]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Order]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[OrderItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItemDetail]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[OrderItemDetail]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[PlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerRating]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[PlayerRating]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Product]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Product]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductBrand]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductBrand]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductClass]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductClass]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductImage]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductImage]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductInventory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductInventory]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductOption]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductOption]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductType]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVariant]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductVariant]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[ProductVendor]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[ProductVendor]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Role]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[RoleClaim]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RoleClaim]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Sale]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Sale]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SaleItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[SaleItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Size]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Size]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Source]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Source]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Stocktake]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Stocktake]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[StocktakeItem]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[StocktakeItem]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Supplier]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Supplier]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[SupplierContact]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[SupplierContact]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Surface]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Surface]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Term]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Term]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriod]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TimePeriod]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[TimePeriodGroup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[TimePeriodGroup]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Transaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Transaction]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[User]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[User]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserClaim]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[UserClaim]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[UserLogin]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[UserLogin]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Zone]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Zone]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Coaches_Coach]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event_Coaches_Coach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_GenderAges_GenderAge]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event_GenderAges_GenderAge]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Levels_PlayerLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event_Levels_PlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Courts_Court]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event_Courts_Court]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Event_Days_DaysOfWeek]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Event_Days_DaysOfWeek]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Coaches_Coach]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventBooking_Coaches_Coach]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Booking_Booking]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventBooking_Booking_Booking]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[EventBooking_Levels_PlayerLevel]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Invoice_Journals_Journal]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Invoice_Journals_Journal]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[OrderItem_Transactions_Transaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[OrderItem_Transactions_Transaction]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[Role_User_User_Roles]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Role_User_User_Roles]
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [PK_Acc_Id_Acc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tc]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Acc__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Acc_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Acc_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Dei_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Dei_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Sor_Id_Acc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Sor_Id_Acc]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Acc__tk]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Acc__tk]
GO
CREATE TABLE [dbo].[Account] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Balance] [decimal] (28,13) NULL,
 [Currency_Id] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [AccountType] [int] NULL,
 [Club_Id] [int] NULL,
 [Customer_Id] [int] NULL,
 [Reference] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Acc__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Acc__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Acc_Id_Acc] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Account] ON [dbo].[Account] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Aco_Id_Aco]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Aco_Id_Aco]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Aco_Id_Aco]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
 ALTER TABLE [dbo].[AccountingPeriod] DROP CONSTRAINT [PK_Aco_Id_Aco]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Aco_Id_Aco]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Aco_Id_Aco]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tc]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
 ALTER TABLE [dbo].[AccountingPeriod] DROP CONSTRAINT [DF_Aco__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Aco_Id_Aco]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Aco_Id_Aco]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Aco__tk]') AND parent_obj = object_id(N'[dbo].[AccountingPeriod]'))
 ALTER TABLE [dbo].[AccountingPeriod] DROP CONSTRAINT [DF_Aco__tk]
GO
CREATE TABLE [dbo].[AccountingPeriod] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Country] [nvarchar] (256) NULL,
 [StartDate] [datetime] NULL,
 [EndDate] [datetime] NULL,
 [isCurrent] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Aco__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Aco__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Aco_Id_Aco] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [PK_Adm_Id_Adm]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tc]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [DF_Adm__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Adm_Id_Adm]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Adm_Id_Adm]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Adm__tk]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [DF_Adm__tk]
GO
CREATE TABLE [dbo].[Admin] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [User_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [CreationDateUTC] [datetime] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Adm__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Adm__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Adm_Id_Adm] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Bat_Id_Bat', tableName='Batch' table='Batch' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Bat_Id_Bat]') AND parent_obj = object_id(N'[dbo].[Batch]'))
 ALTER TABLE [dbo].[Batch] DROP CONSTRAINT [PK_Bat_Id_Bat]
GO
/* no fk for 'DF_Bat__tc', tableName='Batch' table='Batch' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tc]') AND parent_obj = object_id(N'[dbo].[Batch]'))
 ALTER TABLE [dbo].[Batch] DROP CONSTRAINT [DF_Bat__tc]
GO
/* no fk for 'DF_Bat__tk', tableName='Batch' table='Batch' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bat__tk]') AND parent_obj = object_id(N'[dbo].[Batch]'))
 ALTER TABLE [dbo].[Batch] DROP CONSTRAINT [DF_Bat__tk]
GO
CREATE TABLE [dbo].[Batch] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Description] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Bat__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Bat__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Bat_Id_Bat] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bo__Id_Id_Boo]') AND parent_obj = object_id(N'[dbo].[Book_Authors_Author]'))
 ALTER TABLE [dbo].[Book_Authors_Author] DROP CONSTRAINT [FK_Bo__Id_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[Book]'))
 ALTER TABLE [dbo].[Book] DROP CONSTRAINT [PK_Boo_Id_Boo]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tc]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [DF_Boo__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boo__tk]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [DF_Boo__tk]
GO
CREATE TABLE [dbo].[Booking] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Description] [nvarchar] (256) NULL,
 [Start] [datetime] NULL,
 [End] [datetime] NULL,
 [StartTimezone] [nvarchar] (256) NULL,
 [EndTimezone] [nvarchar] (256) NULL,
 [IsAllDay] [bit] NULL,
 [Recurrence] [int] NULL,
 [RecurrenceException] [nvarchar] (256) NULL,
 [RecurrenceRule] [nvarchar] (256) NULL,
 [Title] [nvarchar] (256) NULL,
 [ReferenceNo] [nvarchar] (256) NULL,
 [BookingType_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [isActive] [bit] NULL,
 [Price] [decimal] (28,13) NULL,
 [isPaid] [bit] NULL,
 [NumberOfPlayers] [int] NULL,
 [User_Id] [int] NULL,
 [Facility_Id] [int] NULL,
 [Court_Id] [int] NULL,
 [isMultiCourt] [bit] NULL,
 [MultiCourt] [int] NULL,
 [Customer_Id] [int] NULL,
 [Admin_Id] [int] NULL,
 [Coach_Id] [int] NULL,
 [BookingStatus_Id] [int] NULL,
 [Event_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Boo__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Boo__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Boo_Id_Boo] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Booking] ON [dbo].[Booking] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id2_Id_Boo]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id2_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Boo_Id_Boo]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Boo_Id_Boo]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [PK_Bok_Id_Bok]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Bok_Id_Bok]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tc]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [DF_Bok__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Bok_Id_Bok]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bok__tk]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [DF_Bok__tk]
GO
CREATE TABLE [dbo].[BookingStatus] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Colour] [nvarchar] (256) NULL,
 [isPaid] [bit] NULL,
 [BookingType_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Bok__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Bok__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Bok_Id_Bok] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Bok_Id_Bok]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Bok_Id_Bok]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Boi_Id_Boi]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [PK_Boi_Id_Boi]
GO
/* no fk for 'DF_Boi__tc', tableName='BookingSubType' table='BookingSubType' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tc]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [DF_Boi__tc]
GO
/* no fk for 'DF_Boi__tk', tableName='BookingSubType' table='BookingSubType' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Boi__tk]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [DF_Boi__tk]
GO
CREATE TABLE [dbo].[BookingSubType] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [BookingType_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Boi__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Boi__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Boi_Id_Boi] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_BookingSubType] ON [dbo].[BookingSubType] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Bon_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [PK_Bon_Id_Bon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tc]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [DF_Bon__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bok_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingStatus]'))
 ALTER TABLE [dbo].[BookingStatus] DROP CONSTRAINT [FK_Bok_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boi_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[BookingSubType]'))
 ALTER TABLE [dbo].[BookingSubType] DROP CONSTRAINT [FK_Boi_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Boo_Id_Bon]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Boo_Id_Bon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Bon__tk]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [DF_Bon__tk]
GO
CREATE TABLE [dbo].[BookingType] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [BookingCategory] [int] NULL,
 [Colour] [nvarchar] (256) NULL,
 [BookingRule] [int] NULL,
 [BookingOption] [int] NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Bon__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Bon__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Bon_Id_Bon] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_BookingType] ON [dbo].[BookingType] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Can_Id_Can]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Can_Id_Can]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Can_Id_Can]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
 ALTER TABLE [dbo].[CancellationType] DROP CONSTRAINT [PK_Can_Id_Can]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Can_Id_Can]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Can_Id_Can]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tc]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
 ALTER TABLE [dbo].[CancellationType] DROP CONSTRAINT [DF_Can__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Can_Id_Can]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Can_Id_Can]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Can__tk]') AND parent_obj = object_id(N'[dbo].[CancellationType]'))
 ALTER TABLE [dbo].[CancellationType] DROP CONSTRAINT [DF_Can__tk]
GO
CREATE TABLE [dbo].[CancellationType] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Description] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Can__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Can__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Can_Id_Can] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Che_Id_Che', tableName='Checkout' table='Checkout' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Che_Id_Che]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [PK_Che_Id_Che]
GO
/* no fk for 'DF_Che__tc', tableName='Checkout' table='Checkout' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tc]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [DF_Che__tc]
GO
/* no fk for 'DF_Che__tk', tableName='Checkout' table='Checkout' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Che__tk]') AND parent_obj = object_id(N'[dbo].[Checkout]'))
 ALTER TABLE [dbo].[Checkout] DROP CONSTRAINT [DF_Che__tk]
GO
CREATE TABLE [dbo].[Checkout] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Che__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Che__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Che_Id_Che] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Chc_Id_Chc', tableName='CheckoutItem' table='CheckoutItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Chc_Id_Chc]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [PK_Chc_Id_Chc]
GO
/* no fk for 'DF_Chc__tc', tableName='CheckoutItem' table='CheckoutItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tc]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [DF_Chc__tc]
GO
/* no fk for 'DF_Chc__tk', tableName='CheckoutItem' table='CheckoutItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Chc__tk]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [DF_Chc__tk]
GO
CREATE TABLE [dbo].[CheckoutItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Product_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Chc__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Chc__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Chc_Id_Chc] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [PK_Clu_Id_Clu]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tc]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [DF_Clu__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Fac_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [FK_Fac_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prd_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [FK_Prd_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pre_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [FK_Pre_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [FK_Meb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pla_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [FK_Pla_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pry_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [FK_Pry_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sup_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [FK_Sup_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pru_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [FK_Pru_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tie_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [FK_Tie_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ter_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [FK_Ter_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Bon_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[BookingType]'))
 ALTER TABLE [dbo].[BookingType] DROP CONSTRAINT [FK_Bon_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mes_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [FK_Mes_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eve_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [FK_Eve_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prp_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [FK_Prp_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Clu_Id_Clu]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Clu_Id_Clu]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clu__tk]') AND parent_obj = object_id(N'[dbo].[Club]'))
 ALTER TABLE [dbo].[Club] DROP CONSTRAINT [DF_Clu__tk]
GO
CREATE TABLE [dbo].[Club] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [FullName] [nvarchar] (256) NULL,
 [ShortName] [nvarchar] (256) NULL,
 [AddressLine1] [nvarchar] (256) NULL,
 [AddressLine2] [nvarchar] (256) NULL,
 [Suburb] [nvarchar] (256) NULL,
 [State] [nvarchar] (256) NULL,
 [Postcode] [nvarchar] (256) NULL,
 [Email] [nvarchar] (256) NULL,
 [Website] [nvarchar] (256) NULL,
 [ReferenceNo] [nvarchar] (256) NULL,
 [Phone] [nvarchar] (256) NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Clu__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Clu__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Clu_Id_Clu] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Club] ON [dbo].[Club] ( [Id]);
GO

/* no fk for 'PK_Clb_Id_Clb', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Clb_Id_Clb]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [PK_Clb_Id_Clb]
GO
/* no fk for 'DF_Clb__tc', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tc]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [DF_Clb__tc]
GO
/* no fk for 'DF_Clb__tk', tableName='ClubAdmin' table='ClubAdmin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Clb__tk]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [DF_Clb__tk]
GO
CREATE TABLE [dbo].[ClubAdmin] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [User_Id] [int] NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Clb__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Clb__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Clb_Id_Clb] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ClubAdmin] ON [dbo].[ClubAdmin] ( [Id]);
GO

/* no fk for 'PK_ClC_Id_ClC', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClC_Id_ClC]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [PK_ClC_Id_ClC]
GO
/* no fk for 'DF_ClC__tc', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tc]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [DF_ClC__tc]
GO
/* no fk for 'DF_ClC__tk', tableName='ClubComponents' table='ClubComponents' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClC__tk]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [DF_ClC__tk]
GO
CREATE TABLE [dbo].[ClubComponents] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [Component_Id] [int] NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_ClC__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_ClC__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_ClC_Id_ClC] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ClubComponents] ON [dbo].[ClubComponents] ( [Id]);
GO

/* no fk for 'PK_ClP_Id_ClP', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_ClP_Id_ClP]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [PK_ClP_Id_ClP]
GO
/* no fk for 'DF_ClP__tc', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tc]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [DF_ClP__tc]
GO
/* no fk for 'DF_ClP__tk', tableName='ClubPlayerLevel' table='ClubPlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_ClP__tk]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [DF_ClP__tk]
GO
CREATE TABLE [dbo].[ClubPlayerLevel] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [PlayerLevel_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_ClP__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_ClP__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_ClP_Id_ClP] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ClubPlayerLevel] ON [dbo].[ClubPlayerLevel] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [PK_Coa_Id_Coa]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tc]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [DF_Coa__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Coa_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Coa_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id2_Id_Coa]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id2_Id_Coa]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Coa__tk]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [DF_Coa__tk]
GO
CREATE TABLE [dbo].[Coach] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [CreationDateUTC] [datetime] NULL,
 [isActive] [bit] NULL,
 [User_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Coa__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Coa__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Coa_Id_Coa] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [PK_Com_Id_Com]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tc]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [DF_Com__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClC_Com_Id_Com]') AND parent_obj = object_id(N'[dbo].[ClubComponents]'))
 ALTER TABLE [dbo].[ClubComponents] DROP CONSTRAINT [FK_ClC_Com_Id_Com]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Com__tk]') AND parent_obj = object_id(N'[dbo].[Components]'))
 ALTER TABLE [dbo].[Components] DROP CONSTRAINT [DF_Com__tk]
GO
CREATE TABLE [dbo].[Components] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Com__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Com__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Com_Id_Com] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Components] ON [dbo].[Components] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [PK_Cou_Id_Cou]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tc]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [DF_Cou__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id2_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id2_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cou_Id_Cou]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cou_Id_Cou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cou__tk]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [DF_Cou__tk]
GO
CREATE TABLE [dbo].[Court] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Facility_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [isOnline] [bit] NULL,
 [ReferenceNo] [nvarchar] (256) NULL,
 [Surface_Id] [int] NULL,
 [isActive] [bit] NULL,
 [Club_Id] [int] NULL,
 [Colour] [nvarchar] (256) NULL,
 [Title] [nvarchar] (256) NULL,
 [Text] [nvarchar] (256) NULL,
 [Zone_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Cou__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Cou__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Cou_Id_Cou] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Court] ON [dbo].[Court] ( [Id]);
GO

/* no fk for 'PK_Cor_Id_Cor', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cor_Id_Cor]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [PK_Cor_Id_Cor]
GO
/* no fk for 'DF_Cor__tc', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tc]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [DF_Cor__tc]
GO
/* no fk for 'DF_Cor__tk', tableName='CourtHireRate' table='CourtHireRate' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cor__tk]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [DF_Cor__tk]
GO
CREATE TABLE [dbo].[CourtHireRate] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Rate] [decimal] (28,13) NULL,
 [Zone_Id] [int] NULL,
 [TimePeriod_Id] [int] NULL,
 [IsWeekday] [bit] NULL,
 [Club_Id] [int] NULL,
 [DayOfRate] [int] NULL,
 [MembershipType_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Cor__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Cor__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Cor_Id_Cor] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_CourtHireRate] ON [dbo].[CourtHireRate] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cre_Id_Cre]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [PK_Cre_Id_Cre]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tc]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [DF_Cre__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Crd_Id_Cre]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Crd_Id_Cre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cre__tk]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [DF_Cre__tk]
GO
CREATE TABLE [dbo].[Credit] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [DateCreatedUTC] [datetime] NULL,
 [Amount] [decimal] (28,13) NULL,
 [Transaction] [int] NULL,
 [CreditStatus] [int] NULL,
 [Metadata] [nvarchar] (256) NULL,
 [Customer_Id] [int] NULL,
 [Order_Id] [int] NULL,
 [Balance] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Cre__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Cre__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Cre_Id_Cre] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Credit] ON [dbo].[Credit] ( [Id]);
GO

/* no fk for 'PK_Crd_Id_Crd', tableName='CreditAdjust' table='CreditAdjust' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Crd_Id_Crd]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [PK_Crd_Id_Crd]
GO
/* no fk for 'DF_Crd__tc', tableName='CreditAdjust' table='CreditAdjust' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tc]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [DF_Crd__tc]
GO
/* no fk for 'DF_Crd__tk', tableName='CreditAdjust' table='CreditAdjust' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Crd__tk]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [DF_Crd__tk]
GO
CREATE TABLE [dbo].[CreditAdjust] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Credit_Id] [int] NULL,
 [CreditAdjustType] [int] NULL,
 [Amount] [decimal] (28,13) NULL,
 [AdjustDateTimeUTC] [datetime] NULL,
 [Metadata] [nvarchar] (256) NULL,
 [Balance] [decimal] (28,13) NULL,
 [Invoice_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Crd__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Crd__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Crd_Id_Crd] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Currency]'))
 ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [PK_Cur_Id_Cur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tc]') AND parent_obj = object_id(N'[dbo].[Currency]'))
 ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Cur__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cur_Id_Cur]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cur_Id_Cur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cur__tk]') AND parent_obj = object_id(N'[dbo].[Currency]'))
 ALTER TABLE [dbo].[Currency] DROP CONSTRAINT [DF_Cur__tk]
GO
CREATE TABLE [dbo].[Currency] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Code] [nvarchar] (256) NULL,
 [Name] [nvarchar] (256) NULL,
 [DecimalPlaces] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Cur__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Cur__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Cur_Id_Cur] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [PK_Cus_Id_Cus]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tc]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Cus__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ord_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [FK_Ord_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Acc_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Account]'))
 ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Acc_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sou_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [FK_Sou_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Cus_Id_Cus]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Cus_Id_Cus]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Cus__tk]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Cus__tk]
GO
CREATE TABLE [dbo].[Customer] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [User_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [CreationDateUTC] [datetime] NULL,
 [isMember] [bit] NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Cus__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Cus__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Cus_Id_Cus] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id2_Id_Day]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id2_Id_Day]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Day_Id_Day]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
 ALTER TABLE [dbo].[DaysOfWeek] DROP CONSTRAINT [PK_Day_Id_Day]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id2_Id_Day]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id2_Id_Day]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tc]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
 ALTER TABLE [dbo].[DaysOfWeek] DROP CONSTRAINT [DF_Day__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id2_Id_Day]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id2_Id_Day]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Day__tk]') AND parent_obj = object_id(N'[dbo].[DaysOfWeek]'))
 ALTER TABLE [dbo].[DaysOfWeek] DROP CONSTRAINT [DF_Day__tk]
GO
CREATE TABLE [dbo].[DaysOfWeek] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Value] [int] NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Day__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Day__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Day_Id_Day] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [PK_Del_Id_Del]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tc]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [DF_Del__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Del_Id_Del]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Del_Id_Del]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Del__tk]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [DF_Del__tk]
GO
CREATE TABLE [dbo].[Delivery] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [InvoiceNumber] [nvarchar] (256) NULL,
 [DateReceived] [datetime] NULL,
 [DateRegistered] [datetime] NULL,
 [Supplier_Id] [int] NULL,
 [Facility_Id] [int] NULL,
 [Subtotal] [decimal] (28,13) NULL,
 [GST] [decimal] (28,13) NULL,
 [Total] [decimal] (28,13) NULL,
 [User_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Del__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Del__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Del_Id_Del] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Delivery] ON [dbo].[Delivery] ( [Id]);
GO

/* no fk for 'PK_Dei_Id_Dei', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Dei_Id_Dei]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [PK_Dei_Id_Dei]
GO
/* no fk for 'DF_Dei__tc', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tc]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [DF_Dei__tc]
GO
/* no fk for 'DF_Dei__tk', tableName='DeliveryItem' table='DeliveryItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dei__tk]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [DF_Dei__tk]
GO
CREATE TABLE [dbo].[DeliveryItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Product_Id] [int] NULL,
 [Quantity] [decimal] (28,13) NULL,
 [UnitCostPrice] [decimal] (28,13) NULL,
 [Discount] [decimal] (28,13) NULL,
 [NetAmount] [decimal] (28,13) NULL,
 [Total] [decimal] (28,13) NULL,
 [Delivery_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Dei__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Dei__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Dei_Id_Dei] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_DeliveryItem] ON [dbo].[DeliveryItem] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Dis_Id_Dis]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [PK_Dis_Id_Dis]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Dis_Id_Dis]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tc]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [DF_Dis__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Dis_Id_Dis]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Dis_Id_Dis]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dis__tk]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [DF_Dis__tk]
GO
CREATE TABLE [dbo].[Discount] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [ProductVariant_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Dis__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Dis__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Dis_Id_Dis] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Dic_Id_Dic', tableName='DiscountGroup' table='DiscountGroup' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Dic_Id_Dic]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [PK_Dic_Id_Dic]
GO
/* no fk for 'DF_Dic__tc', tableName='DiscountGroup' table='DiscountGroup' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tc]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [DF_Dic__tc]
GO
/* no fk for 'DF_Dic__tk', tableName='DiscountGroup' table='DiscountGroup' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Dic__tk]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [DF_Dic__tk]
GO
CREATE TABLE [dbo].[DiscountGroup] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Discount_Id] [int] NULL,
 [MembershipType_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Dic__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Dic__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Dic_Id_Dic] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [PK_Equ_Id_Equ]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [DF_Equ__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Equ_Id_Equ]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Equ_Id_Equ]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Equ__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [DF_Equ__tk]
GO
CREATE TABLE [dbo].[EquipmentHire] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [DateRequested] [datetime] NULL,
 [Start] [datetime] NULL,
 [End] [datetime] NULL,
 [Price] [decimal] (28,13) NULL,
 [Club_Id] [int] NULL,
 [Facility_Id] [int] NULL,
 [Status] [int] NULL,
 [NumberOfItems] [int] NULL,
 [User_Id] [int] NULL,
 [Comment] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Equ__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Equ__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Equ_Id_Equ] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EquipmentHire] ON [dbo].[EquipmentHire] ( [Id]);
GO

/* no fk for 'PK_Eqi_Id_Eqi', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eqi_Id_Eqi]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [PK_Eqi_Id_Eqi]
GO
/* no fk for 'DF_Eqi__tc', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tc]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [DF_Eqi__tc]
GO
/* no fk for 'DF_Eqi__tk', tableName='EquipmentHireItem' table='EquipmentHireItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eqi__tk]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [DF_Eqi__tk]
GO
CREATE TABLE [dbo].[EquipmentHireItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Quantity] [int] NULL,
 [EquipmentHire_Id] [int] NULL,
 [Price] [decimal] (28,13) NULL,
 [Product_Id] [int] NULL,
 [Total] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Eqi__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Eqi__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Eqi_Id_Eqi] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EquipmentHireItem] ON [dbo].[EquipmentHireItem] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [PK_Eve_Id_Eve]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tc]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [DF_Eve__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvD_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [FK_EvD_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvC_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [FK_EvC_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ev__Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [FK_Ev__Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id_Id_Eve]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evn_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evn_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Eve_Id_Eve]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Eve_Id_Eve]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Eve__tk]') AND parent_obj = object_id(N'[dbo].[Event]'))
 ALTER TABLE [dbo].[Event] DROP CONSTRAINT [DF_Eve__tk]
GO
CREATE TABLE [dbo].[Event] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Title] [nvarchar] (256) NULL,
 [Capacity] [int] NULL,
 [hasWaitlist] [bit] NULL,
 [Description] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [isOnline] [bit] NULL,
 [DailyFee] [decimal] (28,13) NULL,
 [FullFee] [decimal] (28,13) NULL,
 [isActive] [bit] NULL,
 [RegistrationDateTime] [datetime] NULL,
 [BookingType_Id] [int] NULL,
 [hasCapacity] [bit] NULL,
 [FeeOption] [int] NULL,
 [FeeStructure] [int] NULL,
 [StartTime] [datetime] NULL,
 [EndTime] [datetime] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Eve__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Eve__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Eve_Id_Eve] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event] ON [dbo].[Event] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evn_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [PK_Evn_Id_Evn]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tc]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [DF_Evn__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evo_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [FK_Evo_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvW_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [FK_EvW_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evk_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [FK_Evk_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvB_Id_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [FK_EvB_Id_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evt_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [FK_Evt_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Eve_Id_Evn]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Eve_Id_Evn]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evn__tk]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [DF_Evn__tk]
GO
CREATE TABLE [dbo].[EventBooking] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Event_Id] [int] NULL,
 [Notes] [nvarchar] (256) NULL,
 [EventStatus] [int] NULL,
 [Date] [datetime] NULL,
 [EventBookingCancellation_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Evn__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Evn__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Evn_Id_Evn] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventBooking] ON [dbo].[EventBooking] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [PK_Evt_Id_Evt]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tc]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [DF_Evt__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evn_Evt_Id_Evt]') AND parent_obj = object_id(N'[dbo].[EventBooking]'))
 ALTER TABLE [dbo].[EventBooking] DROP CONSTRAINT [FK_Evn_Evt_Id_Evt]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evt__tk]') AND parent_obj = object_id(N'[dbo].[EventBookingCancellation]'))
 ALTER TABLE [dbo].[EventBookingCancellation] DROP CONSTRAINT [DF_Evt__tk]
GO
CREATE TABLE [dbo].[EventBookingCancellation] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [CancellationType_Id] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [EventBooking_Id] [int] NULL,
 [RefundType] [int] NULL,
 [RefundPortion] [int] NULL,
 [isVisible] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Evt__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Evt__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Evt_Id_Evt] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_EvF_Id_EvF', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvF_Id_EvF]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [PK_EvF_Id_EvF]
GO
/* no fk for 'DF_EvF__tc', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tc]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [DF_EvF__tc]
GO
/* no fk for 'DF_EvF__tk', tableName='EventFee' table='EventFee' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvF__tk]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [DF_EvF__tk]
GO
CREATE TABLE [dbo].[EventFee] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Event_Id] [int] NULL,
 [MembershipType_Id] [int] NULL,
 [DailyFee] [decimal] (28,13) NULL,
 [FullFee] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_EvF__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_EvF__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_EvF_Id_EvF] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventFee] ON [dbo].[EventFee] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvR_Id_EvR]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [PK_EvR_Id_EvR]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tc]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [DF_EvR__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Reg_Id_EvR]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Reg_Id_EvR]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvR__tk]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [DF_EvR__tk]
GO
CREATE TABLE [dbo].[EventRegistration] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [DateRegistered] [datetime] NULL,
 [Fee] [decimal] (28,13) NULL,
 [isPaid] [bit] NULL,
 [isAttending] [bit] NULL,
 [Customer_Id] [int] NULL,
 [EventBooking_Id] [int] NULL,
 [Note] [nvarchar] (256) NULL,
 [RegistrationStatus] [int] NULL,
 [OrderItem_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_EvR__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_EvR__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_EvR_Id_EvR] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventRegistration] ON [dbo].[EventRegistration] ( [Id]);
GO

/* no fk for 'PK_Evo_Id_Evo', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evo_Id_Evo]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [PK_Evo_Id_Evo]
GO
/* no fk for 'DF_Evo__tc', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tc]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [DF_Evo__tc]
GO
/* no fk for 'DF_Evo__tk', tableName='EventRoll' table='EventRoll' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Evo__tk]') AND parent_obj = object_id(N'[dbo].[EventRoll]'))
 ALTER TABLE [dbo].[EventRoll] DROP CONSTRAINT [DF_Evo__tk]
GO
CREATE TABLE [dbo].[EventRoll] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [isAttending] [bit] NULL,
 [Comment] [nvarchar] (256) NULL,
 [Customer_Id] [int] NULL,
 [EventBooking_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Evo__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Evo__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Evo_Id_Evo] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventRoll] ON [dbo].[EventRoll] ( [Id]);
GO

/* no fk for 'PK_EvW_Id_EvW', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvW_Id_EvW]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [PK_EvW_Id_EvW]
GO
/* no fk for 'DF_EvW__tc', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tc]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [DF_EvW__tc]
GO
/* no fk for 'DF_EvW__tk', tableName='EventWaitlist' table='EventWaitlist' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_EvW__tk]') AND parent_obj = object_id(N'[dbo].[EventWaitlist]'))
 ALTER TABLE [dbo].[EventWaitlist] DROP CONSTRAINT [DF_EvW__tk]
GO
CREATE TABLE [dbo].[EventWaitlist] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [isWaiting] [bit] NULL,
 [Comment] [nvarchar] (256) NULL,
 [Customer_Id] [int] NULL,
 [EventBooking_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_EvW__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_EvW__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_EvW_Id_EvW] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventWaitlist] ON [dbo].[EventWaitlist] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [PK_Fac_Id_Fac]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tc]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [DF_Fac__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Zon_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [FK_Zon_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Fac_Id_Fac]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Fac_Id_Fac]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Fac__tk]') AND parent_obj = object_id(N'[dbo].[Facility]'))
 ALTER TABLE [dbo].[Facility] DROP CONSTRAINT [DF_Fac__tk]
GO
CREATE TABLE [dbo].[Facility] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [FullName] [nvarchar] (256) NULL,
 [ShortName] [nvarchar] (256) NULL,
 [AddressLine1] [nvarchar] (256) NULL,
 [AddressLine2] [nvarchar] (256) NULL,
 [Email] [nvarchar] (256) NULL,
 [Phone] [nvarchar] (256) NULL,
 [Suburb] [nvarchar] (256) NULL,
 [State] [nvarchar] (256) NULL,
 [Postcode] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [ReferenceNo] [nvarchar] (256) NULL,
 [isOnline] [bit] NULL,
 [ReserveName] [nvarchar] (256) NULL,
 [isActive] [bit] NULL,
 [Colour] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Fac__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Fac__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Fac_Id_Fac] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Facility] ON [dbo].[Facility] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [PK_Gen_Id_Gen]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tc]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [DF_Gen__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Gen_Id_Gen]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Gen_Id_Gen]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Gen__tk]') AND parent_obj = object_id(N'[dbo].[Gender]'))
 ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [DF_Gen__tk]
GO
CREATE TABLE [dbo].[Gender] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Gen__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Gen__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Gen_Id_Gen] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Gender] ON [dbo].[Gender] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ged_Id_Ged]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [PK_Ged_Id_Ged]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tc]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [DF_Ged__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvG_Id2_Id_Ged]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [FK_EvG_Id2_Id_Ged]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ged__tk]') AND parent_obj = object_id(N'[dbo].[GenderAge]'))
 ALTER TABLE [dbo].[GenderAge] DROP CONSTRAINT [DF_Ged__tk]
GO
CREATE TABLE [dbo].[GenderAge] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [AgeGroup] [int] NULL,
 [GenderGroup] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Ged__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Ged__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Ged_Id_Ged] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_GenderAge] ON [dbo].[GenderAge] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [PK_Inv_Id_Inv]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tc]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [DF_Inv__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Crd_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[CreditAdjust]'))
 ALTER TABLE [dbo].[CreditAdjust] DROP CONSTRAINT [FK_Crd_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sal_Inv_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [FK_Sal_Inv_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id_Id_Inv]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id_Id_Inv]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Inv__tk]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [DF_Inv__tk]
GO
CREATE TABLE [dbo].[Invoice] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [InvoiceNo] [nvarchar] (256) NULL,
 [Customer_Id] [int] NULL,
 [CreationDateUTC] [datetime] NULL,
 [Admin_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [Order_Id] [int] NULL,
 [Balance] [decimal] (28,13) NULL,
 [Change] [decimal] (28,13) NULL,
 [AmountPaid] [decimal] (28,13) NULL,
 [Note] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Inv__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Inv__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Inv_Id_Inv] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Jou_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Jou_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inc_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Inc_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Jou_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Journal]'))
 ALTER TABLE [dbo].[Journal] DROP CONSTRAINT [PK_Jou_Id_Jou]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Jou_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Jou_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inc_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Inc_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tc]') AND parent_obj = object_id(N'[dbo].[Journal]'))
 ALTER TABLE [dbo].[Journal] DROP CONSTRAINT [DF_Jou__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tra_Jou_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [FK_Tra_Jou_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inc_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Inc_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ino_Id2_Id_Jou]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [FK_Ino_Id2_Id_Jou]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Jou__tk]') AND parent_obj = object_id(N'[dbo].[Journal]'))
 ALTER TABLE [dbo].[Journal] DROP CONSTRAINT [DF_Jou__tk]
GO
CREATE TABLE [dbo].[Journal] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [JournalType] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Jou__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Jou__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Jou_Id_Jou] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [PK_Mem_Id_Mem]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tc]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [DF_Mem__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Mem_Id_Mem]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Mem_Id_Mem]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mem__tk]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [DF_Mem__tk]
GO
CREATE TABLE [dbo].[Member] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [StartDateUTC] [datetime] NULL,
 [EndDateUTC] [datetime] NULL,
 [MembershipType_Id] [int] NULL,
 [Customer_Id] [int] NULL,
 [isActive] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Mem__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Mem__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Mem_Id_Mem] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Member] ON [dbo].[Member] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Meb_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [PK_Meb_Id_Meb]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tc]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [DF_Meb__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dic_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[DiscountGroup]'))
 ALTER TABLE [dbo].[DiscountGroup] DROP CONSTRAINT [FK_Dic_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mem_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[Member]'))
 ALTER TABLE [dbo].[Member] DROP CONSTRAINT [FK_Mem_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvF_Mem_Id_Meb]') AND parent_obj = object_id(N'[dbo].[EventFee]'))
 ALTER TABLE [dbo].[EventFee] DROP CONSTRAINT [FK_EvF_Mem_Id_Meb]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meb__tk]') AND parent_obj = object_id(N'[dbo].[MembershipType]'))
 ALTER TABLE [dbo].[MembershipType] DROP CONSTRAINT [DF_Meb__tk]
GO
CREATE TABLE [dbo].[MembershipType] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [ExpiryRule] [int] NULL,
 [Limit] [int] NULL,
 [ProRata] [money] NULL,
 [RenewalDate] [datetime] NULL,
 [Duration] [int] NULL,
 [IsActive] [bit] NULL,
 [Fee] [money] NULL,
 [Description] [nvarchar] (256) NULL,
 [isDefault] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Meb__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Meb__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Meb_Id_Meb] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_MembershipType] ON [dbo].[MembershipType] ( [Id]);
GO

/* no fk for 'PK_Mee_Id_Mee', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mee_Id_Mee]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [PK_Mee_Id_Mee]
GO
/* no fk for 'DF_Mee__tc', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tc]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [DF_Mee__tc]
GO
/* no fk for 'DF_Mee__tk', tableName='MembershipTypeTimePeriod' table='MembershipTypeTimePeriod' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mee__tk]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [DF_Mee__tk]
GO
CREATE TABLE [dbo].[MembershipTypeTimePeriod] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [MembershipType_Id] [int] NULL,
 [TimePeriodGroup_Id] [int] NULL,
 [isWeekday] [bit] NULL,
 [Club_Id] [int] NULL,
 [DayOfRate] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Mee__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Mee__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Mee_Id_Mee] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_MembershipTypeTimePeriod] ON [dbo].[MembershipTypeTimePeriod] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mer_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [FK_Mer_Mes_Id_Mes]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [PK_Mes_Id_Mes]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mer_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [FK_Mer_Mes_Id_Mes]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tc]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [DF_Mes__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mer_Mes_Id_Mes]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [FK_Mer_Mes_Id_Mes]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mes__tk]') AND parent_obj = object_id(N'[dbo].[MessageAccount]'))
 ALTER TABLE [dbo].[MessageAccount] DROP CONSTRAINT [DF_Mes__tk]
GO
CREATE TABLE [dbo].[MessageAccount] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Club_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [Reference] [nvarchar] (256) NULL,
 [Token] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Mes__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Mes__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Mes_Id_Mes] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Mea_Id_Mea', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mea_Id_Mea]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [PK_Mea_Id_Mea]
GO
/* no fk for 'DF_Mea__tc', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tc]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [DF_Mea__tc]
GO
/* no fk for 'DF_Mea__tk', tableName='MessageLog' table='MessageLog' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mea__tk]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [DF_Mea__tk]
GO
CREATE TABLE [dbo].[MessageLog] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Body] [nvarchar] (256) NULL,
 [Direction] [int] NULL,
 [Status] [int] NULL,
 [ErrorMessage] [nvarchar] (256) NULL,
 [Price] [decimal] (28,13) NULL,
 [Admin_Id] [int] NULL,
 [Customer_Id] [int] NULL,
 [Reference] [nvarchar] (256) NULL,
 [From] [nvarchar] (256) NULL,
 [To] [nvarchar] (256) NULL,
 [DateSent] [datetime] NULL,
 [MessageNumber_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Mea__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Mea__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Mea_Id_Mea] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Mes_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Mes_Id_Meg]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Meg_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [PK_Meg_Id_Meg]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Mes_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Mes_Id_Meg]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tc]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [DF_Meg__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mea_Mes_Id_Meg]') AND parent_obj = object_id(N'[dbo].[MessageLog]'))
 ALTER TABLE [dbo].[MessageLog] DROP CONSTRAINT [FK_Mea_Mes_Id_Meg]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Meg__tk]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [DF_Meg__tk]
GO
CREATE TABLE [dbo].[MessageNumber] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [PhoneNumber] [nvarchar] (256) NULL,
 [Location] [nvarchar] (256) NULL,
 [Reference] [nvarchar] (256) NULL,
 [MessageService_Id] [int] NULL,
 [IsDefault] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Meg__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Meg__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Meg_Id_Meg] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meg_Mes_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [FK_Meg_Mes_Id_Mer]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Mer_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [PK_Mer_Id_Mer]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meg_Mes_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [FK_Meg_Mes_Id_Mer]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tc]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [DF_Mer__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Meg_Mes_Id_Mer]') AND parent_obj = object_id(N'[dbo].[MessageNumber]'))
 ALTER TABLE [dbo].[MessageNumber] DROP CONSTRAINT [FK_Meg_Mes_Id_Mer]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Mer__tk]') AND parent_obj = object_id(N'[dbo].[MessageService]'))
 ALTER TABLE [dbo].[MessageService] DROP CONSTRAINT [DF_Mer__tk]
GO
CREATE TABLE [dbo].[MessageService] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Reference] [nvarchar] (256) NULL,
 [MessageAccount_Id] [int] NULL,
 [AlphaId] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Mer__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Mer__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Mer_Id_Mer] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [PK_Ord_Id_Ord]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tc]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [DF_Ord__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Inv_Ord_Id_Ord]') AND parent_obj = object_id(N'[dbo].[Invoice]'))
 ALTER TABLE [dbo].[Invoice] DROP CONSTRAINT [FK_Inv_Ord_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Orr_Id_Ord]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Orr_Id_Ord]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ord__tk]') AND parent_obj = object_id(N'[dbo].[Order]'))
 ALTER TABLE [dbo].[Order] DROP CONSTRAINT [DF_Ord__tk]
GO
CREATE TABLE [dbo].[Order] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [OrderStatus] [int] NULL,
 [Discount] [decimal] (28,13) NULL,
 [Description] [nvarchar] (256) NULL,
 [Note] [nvarchar] (256) NULL,
 [OrderDateUTC] [datetime] NULL,
 [Subtotal] [decimal] (28,13) NULL,
 [Tax] [decimal] (28,13) NULL,
 [Total] [decimal] (28,13) NULL,
 [Customer_Id] [int] NULL,
 [Admin_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Ord__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Ord__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Ord_Id_Ord] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ore_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [PK_Ore_Id_Ore]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tc]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [DF_Ore__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cre_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[Credit]'))
 ALTER TABLE [dbo].[Credit] DROP CONSTRAINT [FK_Cre_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Orr_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [FK_Orr_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvR_Ord_Id_Ore]') AND parent_obj = object_id(N'[dbo].[EventRegistration]'))
 ALTER TABLE [dbo].[EventRegistration] DROP CONSTRAINT [FK_EvR_Ord_Id_Ore]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ore__tk]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [DF_Ore__tk]
GO
CREATE TABLE [dbo].[OrderItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [isPaid] [bit] NULL,
 [Quantity] [int] NULL,
 [Booking_Id] [int] NULL,
 [CreationDateUTC] [datetime] NULL,
 [Metadata] [nvarchar] (256) NULL,
 [Amount] [decimal] (28,13) NULL,
 [Registration_Id] [int] NULL,
 [Membership_Id] [int] NULL,
 [Note] [nvarchar] (256) NULL,
 [ProductVariant_Id] [int] NULL,
 [AdjustType] [int] NULL,
 [AdjustAmount] [decimal] (28,13) NULL,
 [Order_Id] [int] NULL,
 [OrderItemType] [int] NULL,
 [OrderItemStatus] [int] NULL,
 [Customer_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Ore__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Ore__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Ore_Id_Ore] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Orr_Id_Orr', tableName='OrderItemDetail' table='OrderItemDetail' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Orr_Id_Orr]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [PK_Orr_Id_Orr]
GO
/* no fk for 'DF_Orr__tc', tableName='OrderItemDetail' table='OrderItemDetail' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tc]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [DF_Orr__tc]
GO
/* no fk for 'DF_Orr__tk', tableName='OrderItemDetail' table='OrderItemDetail' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Orr__tk]') AND parent_obj = object_id(N'[dbo].[OrderItemDetail]'))
 ALTER TABLE [dbo].[OrderItemDetail] DROP CONSTRAINT [DF_Orr__tk]
GO
CREATE TABLE [dbo].[OrderItemDetail] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [OrderItem_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Orr__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Orr__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Orr_Id_Orr] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [PK_Pla_Id_Pla]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tc]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [DF_Pla__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Evi_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [FK_Evi_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id_Id_Pla]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_ClP_Pla_Id_Pla]') AND parent_obj = object_id(N'[dbo].[ClubPlayerLevel]'))
 ALTER TABLE [dbo].[ClubPlayerLevel] DROP CONSTRAINT [FK_ClP_Pla_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_EvL_Id2_Id_Pla]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [FK_EvL_Id2_Id_Pla]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pla__tk]') AND parent_obj = object_id(N'[dbo].[PlayerLevel]'))
 ALTER TABLE [dbo].[PlayerLevel] DROP CONSTRAINT [DF_Pla__tk]
GO
CREATE TABLE [dbo].[PlayerLevel] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [Club_Id] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Pla__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Pla__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Pla_Id_Pla] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_PlayerLevel] ON [dbo].[PlayerLevel] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ply_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [PK_Ply_Id_Ply]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tc]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [DF_Ply__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ple_Id2_Id_Ply]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [FK_Ple_Id2_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Use_Pla_Id_Ply]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_Use_Pla_Id_Ply]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ply__tk]') AND parent_obj = object_id(N'[dbo].[PlayerRating]'))
 ALTER TABLE [dbo].[PlayerRating] DROP CONSTRAINT [DF_Ply__tk]
GO
CREATE TABLE [dbo].[PlayerRating] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Ply__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Ply__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Ply_Id_Ply] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_PlayerRating] ON [dbo].[PlayerRating] ( [Id]);
GO

/* WARNING: cannot drop constraint 'PK_Pro_Id_Pro' because it is used as the unique index for full text */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tc]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [DF_Pro__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_PrV_Prd_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [FK_PrV_Prd_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [FK_Prc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Chc_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[CheckoutItem]'))
 ALTER TABLE [dbo].[CheckoutItem] DROP CONSTRAINT [FK_Chc_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dei_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[DeliveryItem]'))
 ALTER TABLE [dbo].[DeliveryItem] DROP CONSTRAINT [FK_Dei_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Eqi_Pro_Id_Pro]') AND parent_obj = object_id(N'[dbo].[EquipmentHireItem]'))
 ALTER TABLE [dbo].[EquipmentHireItem] DROP CONSTRAINT [FK_Eqi_Pro_Id_Pro]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pro__tk]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [DF_Pro__tk]
GO
CREATE TABLE [dbo].[Product] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [ProductType_Id] [int] NULL,
 [Description] [nvarchar] (3840) NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [includeGST] [bit] NULL,
 [hasVariant] [bit] NULL,
 [Title] [nvarchar] (256) NULL,
 [PhotoUrl] [nvarchar] (256) NULL,
 [ProductStatus] [int] NULL,
 [ProductVendor_Id] [int] NULL,
 [Quantity] [int] NULL,
 [hasImages] [bit] NULL,
 [Reference] [nvarchar] (256) NULL,
 [Tags] [nvarchar] (256) NULL,
 [ProductOption_Id] [int] NULL,
 [VariantCount] [int] NULL,
 [hasMultiple] [bit] NULL,
 [ProductBrand_Id] [int] NULL,
 [ProductClass_Id] [int] NULL,
 [InventoryOption] [int] NULL,
 [hasDiscount] [bit] NULL,
 [hasMemberPrice] [bit] NULL,
 [Taxable] [bit] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Pro__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Pro__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Pro_Id_Pro] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Product] ON [dbo].[Product] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prt_Id_Prd]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prt_Id_Prd]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prd_Id_Prd]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [PK_Prd_Id_Prd]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prt_Id_Prd]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prt_Id_Prd]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tc]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [DF_Prd__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prt_Id_Prd]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prt_Id_Prd]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prd__tk]') AND parent_obj = object_id(N'[dbo].[ProductBrand]'))
 ALTER TABLE [dbo].[ProductBrand] DROP CONSTRAINT [DF_Prd__tk]
GO
CREATE TABLE [dbo].[ProductBrand] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Prd__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Prd__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Prd_Id_Prd] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pru_Id_Pru]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [PK_Pru_Id_Pru]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tc]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [DF_Pru__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prl_Id_Pru]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prl_Id_Pru]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pru__tk]') AND parent_obj = object_id(N'[dbo].[ProductClass]'))
 ALTER TABLE [dbo].[ProductClass] DROP CONSTRAINT [DF_Pru__tk]
GO
CREATE TABLE [dbo].[ProductClass] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Pru__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Pru__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Pru_Id_Pru] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductClass] ON [dbo].[ProductClass] ( [Id]);
GO

/* no fk for 'PK_Prc_Id_Prc', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prc_Id_Prc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [PK_Prc_Id_Prc]
GO
/* no fk for 'DF_Prc__tc', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tc]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [DF_Prc__tc]
GO
/* no fk for 'DF_Prc__tk', tableName='ProductImage' table='ProductImage' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prc__tk]') AND parent_obj = object_id(N'[dbo].[ProductImage]'))
 ALTER TABLE [dbo].[ProductImage] DROP CONSTRAINT [DF_Prc__tk]
GO
CREATE TABLE [dbo].[ProductImage] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Reference] [nvarchar] (256) NULL,
 [Name] [nvarchar] (256) NULL,
 [Product_Id] [int] NULL,
 [Url] [nvarchar] (256) NULL,
 [isDefault] [bit] NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Prc__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Prc__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Prc_Id_Prc] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductImage] ON [dbo].[ProductImage] ( [Id]);
GO

/* no fk for 'PK_Prt_Id_Prt', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prt_Id_Prt]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [PK_Prt_Id_Prt]
GO
/* no fk for 'DF_Prt__tc', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tc]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [DF_Prt__tc]
GO
/* no fk for 'DF_Prt__tk', tableName='ProductInventory' table='ProductInventory' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prt__tk]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [DF_Prt__tk]
GO
CREATE TABLE [dbo].[ProductInventory] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [AdjustAmount] [int] NULL,
 [AdjustDateTimeUTC] [datetime] NULL,
 [InventoryAdjustType] [int] NULL,
 [ProductVariant_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Prt__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Prt__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Prt_Id_Prt] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductInventory] ON [dbo].[ProductInventory] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Prp_Id_Prp]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [PK_Prp_Id_Prp]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tc]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [DF_Prp__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prc_Id_Prp]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prc_Id_Prp]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Prp__tk]') AND parent_obj = object_id(N'[dbo].[ProductOption]'))
 ALTER TABLE [dbo].[ProductOption] DROP CONSTRAINT [DF_Prp__tk]
GO
CREATE TABLE [dbo].[ProductOption] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Prp__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Prp__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Prp_Id_Prp] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductOption] ON [dbo].[ProductOption] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pry_Id_Pry]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [PK_Pry_Id_Pry]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tc]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [DF_Pry__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Siz_Pro_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [FK_Siz_Pro_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Prd_Id_Pry]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Prd_Id_Pry]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pry__tk]') AND parent_obj = object_id(N'[dbo].[ProductType]'))
 ALTER TABLE [dbo].[ProductType] DROP CONSTRAINT [DF_Pry__tk]
GO
CREATE TABLE [dbo].[ProductType] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Pry__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Pry__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Pry_Id_Pry] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductType] ON [dbo].[ProductType] ( [Id]);
GO

/* WARNING: cannot drop constraint 'PK_PrV_Id_PrV' because it is used as the unique index for full text */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dis_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [FK_Dis_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prt_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [FK_Prt_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tc]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [DF_PrV__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Dis_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[Discount]'))
 ALTER TABLE [dbo].[Discount] DROP CONSTRAINT [FK_Dis_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ore_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[OrderItem]'))
 ALTER TABLE [dbo].[OrderItem] DROP CONSTRAINT [FK_Ore_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Prt_Pro_Id_PrV]') AND parent_obj = object_id(N'[dbo].[ProductInventory]'))
 ALTER TABLE [dbo].[ProductInventory] DROP CONSTRAINT [FK_Prt_Pro_Id_PrV]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_PrV__tk]') AND parent_obj = object_id(N'[dbo].[ProductVariant]'))
 ALTER TABLE [dbo].[ProductVariant] DROP CONSTRAINT [DF_PrV__tk]
GO
CREATE TABLE [dbo].[ProductVariant] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Product_Id] [int] NULL,
 [SKU] [nvarchar] (256) NULL,
 [Barcode] [nvarchar] (256) NULL,
 [Quantity] [int] NULL,
 [ProductOptionValue] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [RetailPrice] [decimal] (28,13) NULL,
 [Taxable] [bit] NULL,
 [isDefault] [bit] NULL,
 [InventoryOption] [int] NULL,
 [Title] [nvarchar] (256) NULL,
 [MemberPrice] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_PrV__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_PrV__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_PrV_Id_PrV] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductVariant] ON [dbo].[ProductVariant] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Pru_Id_Pre]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Pru_Id_Pre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Pre_Id_Pre]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [PK_Pre_Id_Pre]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Pru_Id_Pre]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Pru_Id_Pre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tc]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [DF_Pre__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Pro_Pru_Id_Pre]') AND parent_obj = object_id(N'[dbo].[Product]'))
 ALTER TABLE [dbo].[Product] DROP CONSTRAINT [FK_Pro_Pru_Id_Pre]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Pre__tk]') AND parent_obj = object_id(N'[dbo].[ProductVendor]'))
 ALTER TABLE [dbo].[ProductVendor] DROP CONSTRAINT [DF_Pre__tk]
GO
CREATE TABLE [dbo].[ProductVendor] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Pre__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Pre__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Pre_Id_Pre] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_ProductVendor] ON [dbo].[ProductVendor] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [PK_Rol_Id_Rol]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tc]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [DF_Rol__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id_Id_Rol]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Roe_Rol_Id_Rol]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [FK_Roe_Rol_Id_Rol]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Rol__tk]') AND parent_obj = object_id(N'[dbo].[Role]'))
 ALTER TABLE [dbo].[Role] DROP CONSTRAINT [DF_Rol__tk]
GO
CREATE TABLE [dbo].[Role] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NOT NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Rol__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Rol__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Rol_Id_Rol] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 ),
 CONSTRAINT [IX_Rol_Nam_Rol] UNIQUE
 (

  [Name] )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Role] ON [dbo].[Role] ( [Id]);
GO

/* no fk for 'PK_Roe_Id_Roe', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Roe_Id_Roe]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [PK_Roe_Id_Roe]
GO
/* no fk for 'DF_Roe__tc', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tc]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [DF_Roe__tc]
GO
/* no fk for 'DF_Roe__tk', tableName='RoleClaim' table='RoleClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Roe__tk]') AND parent_obj = object_id(N'[dbo].[RoleClaim]'))
 ALTER TABLE [dbo].[RoleClaim] DROP CONSTRAINT [DF_Roe__tk]
GO
CREATE TABLE [dbo].[RoleClaim] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Type] [nvarchar] (256) NOT NULL,
 [Value] [nvarchar] (256) NULL,
 [ValueType] [nvarchar] (256) NULL,
 [Role_Id] [int] NOT NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Roe__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Roe__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Roe_Id_Roe] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_RoleClaim] ON [dbo].[RoleClaim] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [PK_Sal_Id_Sal]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tc]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [DF_Sal__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sae_Sal_Id_Sal]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [FK_Sae_Sal_Id_Sal]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sal__tk]') AND parent_obj = object_id(N'[dbo].[Sale]'))
 ALTER TABLE [dbo].[Sale] DROP CONSTRAINT [DF_Sal__tk]
GO
CREATE TABLE [dbo].[Sale] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Customer_Id] [int] NULL,
 [Admin_Id] [int] NULL,
 [Tax] [decimal] (28,13) NULL,
 [SubTotal] [decimal] (28,13) NULL,
 [TotalAmount] [decimal] (28,13) NULL,
 [Note] [nvarchar] (256) NULL,
 [SaleStatus] [int] NULL,
 [SaleDateTimeUTC] [datetime] NULL,
 [Description] [nvarchar] (256) NULL,
 [Invoice_Id] [int] NULL,
 [DiscountType] [int] NULL,
 [Discount] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sal__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sal__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sal_Id_Sal] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Sae_Id_Sae', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sae_Id_Sae]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [PK_Sae_Id_Sae]
GO
/* no fk for 'DF_Sae__tc', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tc]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [DF_Sae__tc]
GO
/* no fk for 'DF_Sae__tk', tableName='SaleItem' table='SaleItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sae__tk]') AND parent_obj = object_id(N'[dbo].[SaleItem]'))
 ALTER TABLE [dbo].[SaleItem] DROP CONSTRAINT [DF_Sae__tk]
GO
CREATE TABLE [dbo].[SaleItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Sale_Id] [int] NULL,
 [Order_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sae__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sae__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sae_Id_Sae] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

/* no fk for 'PK_Siz_Id_Siz', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Siz_Id_Siz]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [PK_Siz_Id_Siz]
GO
/* no fk for 'DF_Siz__tc', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tc]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [DF_Siz__tc]
GO
/* no fk for 'DF_Siz__tk', tableName='Size' table='Size' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Siz__tk]') AND parent_obj = object_id(N'[dbo].[Size]'))
 ALTER TABLE [dbo].[Size] DROP CONSTRAINT [DF_Siz__tk]
GO
CREATE TABLE [dbo].[Size] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Value] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [ProductType_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Siz__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Siz__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Siz_Id_Siz] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Size] ON [dbo].[Size] ( [Id]);
GO

/* no fk for 'PK_Sou_Id_Sou', tableName='Source' table='Source' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sou_Id_Sou]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [PK_Sou_Id_Sou]
GO
/* no fk for 'DF_Sou__tc', tableName='Source' table='Source' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tc]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [DF_Sou__tc]
GO
/* no fk for 'DF_Sou__tk', tableName='Source' table='Source' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sou__tk]') AND parent_obj = object_id(N'[dbo].[Source]'))
 ALTER TABLE [dbo].[Source] DROP CONSTRAINT [DF_Sou__tk]
GO
CREATE TABLE [dbo].[Source] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Customer_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sou__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sou__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sou_Id_Sou] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [PK_Sto_Id_Sto]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tc]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [DF_Sto__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Stc_Sto_Id_Sto]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [FK_Stc_Sto_Id_Sto]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sto__tk]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [DF_Sto__tk]
GO
CREATE TABLE [dbo].[Stocktake] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Start] [datetime] NULL,
 [End] [datetime] NULL,
 [NumberOfItems] [int] NULL,
 [User_Id] [int] NULL,
 [Facility_Id] [int] NULL,
 [PreviousCount] [decimal] (28,13) NULL,
 [PreviousValuation] [decimal] (28,13) NULL,
 [CurrentValuation] [decimal] (28,13) NULL,
 [State] [int] NULL,
 [CountResult] [decimal] (28,13) NULL,
 [CurrentCount] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sto__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sto__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sto_Id_Sto] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Stocktake] ON [dbo].[Stocktake] ( [Id]);
GO

/* no fk for 'PK_Stc_Id_Stc', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Stc_Id_Stc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [PK_Stc_Id_Stc]
GO
/* no fk for 'DF_Stc__tc', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tc]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [DF_Stc__tc]
GO
/* no fk for 'DF_Stc__tk', tableName='StocktakeItem' table='StocktakeItem' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Stc__tk]') AND parent_obj = object_id(N'[dbo].[StocktakeItem]'))
 ALTER TABLE [dbo].[StocktakeItem] DROP CONSTRAINT [DF_Stc__tk]
GO
CREATE TABLE [dbo].[StocktakeItem] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [State] [int] NULL,
 [Stocktake_Id] [int] NULL,
 [Product_Id] [int] NULL,
 [Comment] [nvarchar] (256) NULL,
 [PreviousCount] [decimal] (28,13) NULL,
 [Result] [decimal] (28,13) NULL,
 [CurrentCount] [decimal] (28,13) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Stc__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Stc__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Stc_Id_Stc] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_StocktakeItem] ON [dbo].[StocktakeItem] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [PK_Sup_Id_Sup]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tc]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [DF_Sup__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sul_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [FK_Sul_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Sup_Id_Sup]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Sup_Id_Sup]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sup__tk]') AND parent_obj = object_id(N'[dbo].[Supplier]'))
 ALTER TABLE [dbo].[Supplier] DROP CONSTRAINT [DF_Sup__tk]
GO
CREATE TABLE [dbo].[Supplier] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Email] [nvarchar] (256) NULL,
 [Phone] [nvarchar] (256) NULL,
 [Website] [nvarchar] (256) NULL,
 [ReferenceNo] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sup__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sup__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sup_Id_Sup] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Supplier] ON [dbo].[Supplier] ( [Id]);
GO

/* no fk for 'PK_Sul_Id_Sul', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sul_Id_Sul]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [PK_Sul_Id_Sul]
GO
/* no fk for 'DF_Sul__tc', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tc]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [DF_Sul__tc]
GO
/* no fk for 'DF_Sul__tk', tableName='SupplierContact' table='SupplierContact' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sul__tk]') AND parent_obj = object_id(N'[dbo].[SupplierContact]'))
 ALTER TABLE [dbo].[SupplierContact] DROP CONSTRAINT [DF_Sul__tk]
GO
CREATE TABLE [dbo].[SupplierContact] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Supplier_Id] [int] NULL,
 [Name] [nvarchar] (256) NULL,
 [Position] [nvarchar] (256) NULL,
 [Phone] [nvarchar] (256) NULL,
 [Email] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sul__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sul__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sul_Id_Sul] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_SupplierContact] ON [dbo].[SupplierContact] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [PK_Sur_Id_Sur]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tc]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [DF_Sur__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Sur_Id_Sur]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Sur_Id_Sur]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Sur__tk]') AND parent_obj = object_id(N'[dbo].[Surface]'))
 ALTER TABLE [dbo].[Surface] DROP CONSTRAINT [DF_Sur__tk]
GO
CREATE TABLE [dbo].[Surface] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Description] [nvarchar] (256) NULL,
 [Sequence] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Sur__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Sur__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Sur_Id_Sur] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Surface] ON [dbo].[Surface] ( [Id]);
GO

/* no fk for 'PK_Ter_Id_Ter', tableName='Term' table='Term' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ter_Id_Ter]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [PK_Ter_Id_Ter]
GO
/* no fk for 'DF_Ter__tc', tableName='Term' table='Term' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tc]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [DF_Ter__tc]
GO
/* no fk for 'DF_Ter__tk', tableName='Term' table='Term' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Ter__tk]') AND parent_obj = object_id(N'[dbo].[Term]'))
 ALTER TABLE [dbo].[Term] DROP CONSTRAINT [DF_Ter__tk]
GO
CREATE TABLE [dbo].[Term] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [StartDateUTC] [datetime] NULL,
 [EndDateUTC] [datetime] NULL,
 [isActive] [bit] NULL,
 [Club_Id] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Ter__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Ter__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Ter_Id_Ter] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [PK_Tim_Id_Tim]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [DF_Tim__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Tim_Id_Tim]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Tim_Id_Tim]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tim__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [DF_Tim__tk]
GO
CREATE TABLE [dbo].[TimePeriod] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Start] [datetime] NULL,
 [End] [datetime] NULL,
 [Sequence] [int] NULL,
 [Club_Id] [int] NULL,
 [TimePeriodGroup_Id] [int] NULL,
 [StartTime] [time] NULL,
 [EndTime] [time] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Tim__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Tim__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Tim_Id_Tim] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_TimePeriod] ON [dbo].[TimePeriod] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tie_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [PK_Tie_Id_Tie]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tc]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [DF_Tie__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Mee_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[MembershipTypeTimePeriod]'))
 ALTER TABLE [dbo].[MembershipTypeTimePeriod] DROP CONSTRAINT [FK_Mee_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Tim_Tim_Id_Tie]') AND parent_obj = object_id(N'[dbo].[TimePeriod]'))
 ALTER TABLE [dbo].[TimePeriod] DROP CONSTRAINT [FK_Tim_Tim_Id_Tie]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tie__tk]') AND parent_obj = object_id(N'[dbo].[TimePeriodGroup]'))
 ALTER TABLE [dbo].[TimePeriodGroup] DROP CONSTRAINT [DF_Tie__tk]
GO
CREATE TABLE [dbo].[TimePeriodGroup] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [isDefault] [bit] NULL,
 [Description] [nvarchar] (256) NULL,
 [Club_Id] [int] NULL,
 [NumberOfTimePeriods] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Tie__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Tie__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Tie_Id_Tie] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_TimePeriodGroup] ON [dbo].[TimePeriodGroup] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Tra_Id_Tra]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [PK_Tra_Id_Tra]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tc]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Tra__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_OrI_Id2_Id_Tra]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [FK_OrI_Id2_Id_Tra]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Tra__tk]') AND parent_obj = object_id(N'[dbo].[Transaction]'))
 ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT [DF_Tra__tk]
GO
CREATE TABLE [dbo].[Transaction] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Account_Id] [int] NULL,
 [Currency_Id] [int] NULL,
 [Journal_Id] [int] NULL,
 [Amount] [decimal] (28,13) NULL,
 [AccountingPeriod_Id] [int] NULL,
 [Description] [nvarchar] (256) NULL,
 [TransactionStatus] [int] NULL,
 [SourceType] [int] NULL,
 [Invoice_Id] [int] NULL,
 [Club_Id] [int] NULL,
 [DateCreatedUTC] [datetime] NULL,
 [TransactionType] [int] NULL,
 [DestinationType] [int] NULL,
 [Metadata] [nvarchar] (256) NULL,
 [Balance] [decimal] (28,13) NULL,
 [Reference] [nvarchar] (256) NULL,
 [Source_Id] [int] NULL,
 [Destination_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Tra__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Tra__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Tra_Id_Tra] PRIMARY KEY CLUSTERED
 (

  [Id]
 )
)
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [PK_Use_Id_Use]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tc]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF_Use__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Coa_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Coach]'))
 ALTER TABLE [dbo].[Coach] DROP CONSTRAINT [FK_Coa_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Ro__Id2_Id_Use]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [FK_Ro__Id2_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Sto_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Stocktake]'))
 ALTER TABLE [dbo].[Stocktake] DROP CONSTRAINT [FK_Sto_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Boo_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Booking]'))
 ALTER TABLE [dbo].[Booking] DROP CONSTRAINT [FK_Boo_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Usr_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [FK_Usr_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_UsL_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [FK_UsL_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Adm_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Admin]'))
 ALTER TABLE [dbo].[Admin] DROP CONSTRAINT [FK_Adm_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Clb_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[ClubAdmin]'))
 ALTER TABLE [dbo].[ClubAdmin] DROP CONSTRAINT [FK_Clb_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cus_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Customer]'))
 ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Cus_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Del_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[Delivery]'))
 ALTER TABLE [dbo].[Delivery] DROP CONSTRAINT [FK_Del_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Equ_Use_Id_Use]') AND parent_obj = object_id(N'[dbo].[EquipmentHire]'))
 ALTER TABLE [dbo].[EquipmentHire] DROP CONSTRAINT [FK_Equ_Use_Id_Use]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Use__tk]') AND parent_obj = object_id(N'[dbo].[User]'))
 ALTER TABLE [dbo].[User] DROP CONSTRAINT [DF_Use__tk]
GO
CREATE TABLE [dbo].[User] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [UserName] [nvarchar] (256) NOT NULL,
 [CreationDateUTC] [datetime] NOT NULL,
 [Email] [nvarchar] (256) NULL,
 [EmailConfirmed] [bit] NOT NULL,
 [PhoneNumber] [nvarchar] (256) NULL,
 [PhoneNumberConfirmed] [bit] NOT NULL,
 [Password] [nvarchar] (256) NULL,
 [LastPasswordChangeDate] [datetime] NULL,
 [AccessFailedCount] [int] NOT NULL,
 [AccessFailedWindowStart] [datetime] NULL,
 [LockoutEnabled] [bit] NOT NULL,
 [LockoutEndDateUtc] [datetime] NULL,
 [LastProfileUpdateDate] [datetime] NULL,
 [SecurityStamp] [nvarchar] (256) NOT NULL,
 [TwoFactorEnabled] [bit] NOT NULL,
 [FirstName] [nvarchar] (256) NULL,
 [LastName] [nvarchar] (256) NULL,
 [Gender_Id] [int] NULL,
 [AddressLine1] [nvarchar] (256) NULL,
 [AddressLine2] [nvarchar] (256) NULL,
 [Suburb] [nvarchar] (256) NULL,
 [State] [nvarchar] (256) NULL,
 [Postcode] [nvarchar] (256) NULL,
 [TennisId] [nvarchar] (256) NULL,
 [DateOfBirth] [datetime] NULL,
 [Country] [nvarchar] (256) NULL,
 [FullName] [nvarchar] (256) NULL,
 [PlayerRating_Id] [int] NULL,
 [Pin] [nvarchar] (256) NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Use__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Use__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Use_Id_Use] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 ),
 CONSTRAINT [IX_Use_Ema_Use] UNIQUE
 (

  [Email] ),
 CONSTRAINT [IX_Use_Use_Use] UNIQUE
 (

  [UserName] )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_User] ON [dbo].[User] ( [Id]);
GO

/* no fk for 'PK_Usr_Id_Usr', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Usr_Id_Usr]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [PK_Usr_Id_Usr]
GO
/* no fk for 'DF_Usr__tc', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tc]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [DF_Usr__tc]
GO
/* no fk for 'DF_Usr__tk', tableName='UserClaim' table='UserClaim' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Usr__tk]') AND parent_obj = object_id(N'[dbo].[UserClaim]'))
 ALTER TABLE [dbo].[UserClaim] DROP CONSTRAINT [DF_Usr__tk]
GO
CREATE TABLE [dbo].[UserClaim] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Type] [nvarchar] (256) NOT NULL,
 [Value] [nvarchar] (256) NULL,
 [ValueType] [nvarchar] (256) NULL,
 [Issuer] [nvarchar] (256) NULL,
 [OriginalIssuer] [nvarchar] (256) NULL,
 [User_Id] [int] NOT NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Usr__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Usr__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Usr_Id_Usr] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_UserClaim] ON [dbo].[UserClaim] ( [Id]);
GO

/* no fk for 'PK_UsL_Id_UsL', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_UsL_Id_UsL]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [PK_UsL_Id_UsL]
GO
/* no fk for 'DF_UsL__tc', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tc]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [DF_UsL__tc]
GO
/* no fk for 'DF_UsL__tk', tableName='UserLogin' table='UserLogin' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_UsL__tk]') AND parent_obj = object_id(N'[dbo].[UserLogin]'))
 ALTER TABLE [dbo].[UserLogin] DROP CONSTRAINT [DF_UsL__tk]
GO
CREATE TABLE [dbo].[UserLogin] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [ProviderName] [nvarchar] (256) NOT NULL,
 [ProviderKey] [nvarchar] (256) NOT NULL,
 [ProviderDisplayName] [nvarchar] (256) NOT NULL,
 [User_Id] [int] NOT NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_UsL__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_UsL__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_UsL_Id_UsL] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_UserLogin] ON [dbo].[UserLogin] ( [Id]);
GO

IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [PK_Zon_Id_Zon]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tc]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [DF_Zon__tc]
GO
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cou_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[Court]'))
 ALTER TABLE [dbo].[Court] DROP CONSTRAINT [FK_Cou_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[FK_Cor_Zon_Id_Zon]') AND parent_obj = object_id(N'[dbo].[CourtHireRate]'))
 ALTER TABLE [dbo].[CourtHireRate] DROP CONSTRAINT [FK_Cor_Zon_Id_Zon]
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DF_Zon__tk]') AND parent_obj = object_id(N'[dbo].[Zone]'))
 ALTER TABLE [dbo].[Zone] DROP CONSTRAINT [DF_Zon__tk]
GO
CREATE TABLE [dbo].[Zone] (
 [Id] [int] IDENTITY (1, 1) NOT NULL,
 [Name] [nvarchar] (256) NULL,
 [Description] [nvarchar] (256) NULL,
 [Facility_Id] [int] NULL,
 [Sequence] [int] NULL,
 [Club_Id] [int] NULL,
 [_trackLastWriteTime] [datetime] NOT NULL CONSTRAINT [DF_Zon__tc] DEFAULT (GETUTCDATE()),
 [_trackCreationTime] [datetime] NOT NULL CONSTRAINT [DF_Zon__tk] DEFAULT (GETUTCDATE()),
 [_trackLastWriteUser] [nvarchar] (64) NOT NULL,
 [_trackCreationUser] [nvarchar] (64) NOT NULL,
 [_rowVersion] [rowversion] NOT NULL,
 CONSTRAINT [PK_Zon_Id_Zon] PRIMARY KEY NONCLUSTERED
 (

  [Id]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Zone] ON [dbo].[Zone] ( [Id]);
GO

/* no fk for 'PK_Ev__Id_Id2_Ev_', tableName='Event_Coaches_Coach' table='Event_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ev__Id_Id2_Ev_]') AND parent_obj = object_id(N'[dbo].[Event_Coaches_Coach]'))
 ALTER TABLE [dbo].[Event_Coaches_Coach] DROP CONSTRAINT [PK_Ev__Id_Id2_Ev_]
GO
CREATE TABLE [dbo].[Event_Coaches_Coach] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Ev__Id_Id2_Ev_] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event_Coaches_Coach] ON [dbo].[Event_Coaches_Coach] ( [Id]);
GO

/* no fk for 'PK_EvG_Id_Id2_EvG', tableName='Event_GenderAges_GenderAge' table='Event_GenderAges_GenderAge' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvG_Id_Id2_EvG]') AND parent_obj = object_id(N'[dbo].[Event_GenderAges_GenderAge]'))
 ALTER TABLE [dbo].[Event_GenderAges_GenderAge] DROP CONSTRAINT [PK_EvG_Id_Id2_EvG]
GO
CREATE TABLE [dbo].[Event_GenderAges_GenderAge] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_EvG_Id_Id2_EvG] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event_GenderAges_GenderAge] ON [dbo].[Event_GenderAges_GenderAge] ( [Id], [Id2]);
GO

/* no fk for 'PK_EvL_Id_Id2_EvL', tableName='Event_Levels_PlayerLevel' table='Event_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvL_Id_Id2_EvL]') AND parent_obj = object_id(N'[dbo].[Event_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[Event_Levels_PlayerLevel] DROP CONSTRAINT [PK_EvL_Id_Id2_EvL]
GO
CREATE TABLE [dbo].[Event_Levels_PlayerLevel] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_EvL_Id_Id2_EvL] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event_Levels_PlayerLevel] ON [dbo].[Event_Levels_PlayerLevel] ( [Id], [Id2]);
GO

/* no fk for 'PK_EvC_Id_Id2_EvC', tableName='Event_Courts_Court' table='Event_Courts_Court' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvC_Id_Id2_EvC]') AND parent_obj = object_id(N'[dbo].[Event_Courts_Court]'))
 ALTER TABLE [dbo].[Event_Courts_Court] DROP CONSTRAINT [PK_EvC_Id_Id2_EvC]
GO
CREATE TABLE [dbo].[Event_Courts_Court] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_EvC_Id_Id2_EvC] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event_Courts_Court] ON [dbo].[Event_Courts_Court] ( [Id], [Id2]);
GO

/* no fk for 'PK_EvD_Id_Id2_EvD', tableName='Event_Days_DaysOfWeek' table='Event_Days_DaysOfWeek' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvD_Id_Id2_EvD]') AND parent_obj = object_id(N'[dbo].[Event_Days_DaysOfWeek]'))
 ALTER TABLE [dbo].[Event_Days_DaysOfWeek] DROP CONSTRAINT [PK_EvD_Id_Id2_EvD]
GO
CREATE TABLE [dbo].[Event_Days_DaysOfWeek] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_EvD_Id_Id2_EvD] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Event_Days_DaysOfWeek] ON [dbo].[Event_Days_DaysOfWeek] ( [Id]);
GO

/* no fk for 'PK_EvB_Id_Id2_EvB', tableName='EventBooking_Coaches_Coach' table='EventBooking_Coaches_Coach' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_EvB_Id_Id2_EvB]') AND parent_obj = object_id(N'[dbo].[EventBooking_Coaches_Coach]'))
 ALTER TABLE [dbo].[EventBooking_Coaches_Coach] DROP CONSTRAINT [PK_EvB_Id_Id2_EvB]
GO
CREATE TABLE [dbo].[EventBooking_Coaches_Coach] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_EvB_Id_Id2_EvB] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventBooking_Coaches_Coach] ON [dbo].[EventBooking_Coaches_Coach] ( [Id]);
GO

/* no fk for 'PK_Evk_Id_Id2_Evk', tableName='EventBooking_Booking_Booking' table='EventBooking_Booking_Booking' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evk_Id_Id2_Evk]') AND parent_obj = object_id(N'[dbo].[EventBooking_Booking_Booking]'))
 ALTER TABLE [dbo].[EventBooking_Booking_Booking] DROP CONSTRAINT [PK_Evk_Id_Id2_Evk]
GO
CREATE TABLE [dbo].[EventBooking_Booking_Booking] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Evk_Id_Id2_Evk] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventBooking_Booking_Booking] ON [dbo].[EventBooking_Booking_Booking] ( [Id], [Id2]);
GO

/* no fk for 'PK_Evi_Id_Id2_Evi', tableName='EventBooking_Levels_PlayerLevel' table='EventBooking_Levels_PlayerLevel' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Evi_Id_Id2_Evi]') AND parent_obj = object_id(N'[dbo].[EventBooking_Levels_PlayerLevel]'))
 ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] DROP CONSTRAINT [PK_Evi_Id_Id2_Evi]
GO
CREATE TABLE [dbo].[EventBooking_Levels_PlayerLevel] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Evi_Id_Id2_Evi] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_EventBooking_Levels_PlayerLevel] ON [dbo].[EventBooking_Levels_PlayerLevel] ( [Id], [Id2]);
GO

/* no fk for 'PK_Ino_Id_Id2_Ino', tableName='Invoice_Journals_Journal' table='Invoice_Journals_Journal' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ino_Id_Id2_Ino]') AND parent_obj = object_id(N'[dbo].[Invoice_Journals_Journal]'))
 ALTER TABLE [dbo].[Invoice_Journals_Journal] DROP CONSTRAINT [PK_Ino_Id_Id2_Ino]
GO
CREATE TABLE [dbo].[Invoice_Journals_Journal] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Ino_Id_Id2_Ino] PRIMARY KEY CLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

/* no fk for 'PK_OrI_Id_Id2_OrI', tableName='OrderItem_Transactions_Transaction' table='OrderItem_Transactions_Transaction' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_OrI_Id_Id2_OrI]') AND parent_obj = object_id(N'[dbo].[OrderItem_Transactions_Transaction]'))
 ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] DROP CONSTRAINT [PK_OrI_Id_Id2_OrI]
GO
CREATE TABLE [dbo].[OrderItem_Transactions_Transaction] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_OrI_Id_Id2_OrI] PRIMARY KEY CLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

/* no fk for 'PK_Ple_Id_Id2_Ple', tableName='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' table='PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ple_Id_Id2_Ple]') AND parent_obj = object_id(N'[dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels]'))
 ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] DROP CONSTRAINT [PK_Ple_Id_Id2_Ple]
GO
CREATE TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Ple_Id_Id2_Ple] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] ON [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] ( [Id], [Id2]);
GO

/* no fk for 'PK_Ro__Id_Id2_Ro_', tableName='Role_User_User_Roles' table='Role_User_User_Roles' */
IF EXISTS (SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[PK_Ro__Id_Id2_Ro_]') AND parent_obj = object_id(N'[dbo].[Role_User_User_Roles]'))
 ALTER TABLE [dbo].[Role_User_User_Roles] DROP CONSTRAINT [PK_Ro__Id_Id2_Ro_]
GO
CREATE TABLE [dbo].[Role_User_User_Roles] (
 [Id] [int] NOT NULL,
 [Id2] [int] NOT NULL,
 CONSTRAINT [PK_Ro__Id_Id2_Ro_] PRIMARY KEY NONCLUSTERED
 (

  [Id],
  [Id2]
 )
)
GO

CREATE CLUSTERED INDEX [CL_dbo_Role_User_User_Roles] ON [dbo].[Role_User_User_Roles] ( [Id], [Id2]);
GO

