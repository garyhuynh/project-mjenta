﻿/* CodeFluent Generated Monday, 08 August 2016 12:52. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-AU. UiCulture:en-GB. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
ALTER TABLE [dbo].[Account] WITH NOCHECK ADD CONSTRAINT [FK_Acc_Cur_Id_Cur] FOREIGN KEY (
 [Currency_Id]
) REFERENCES [dbo].[Currency](

 [Id]
)
ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT [FK_Acc_Cur_Id_Cur]
ALTER TABLE [dbo].[Account] WITH NOCHECK ADD CONSTRAINT [FK_Acc_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT [FK_Acc_Clu_Id_Clu]
ALTER TABLE [dbo].[Account] WITH NOCHECK ADD CONSTRAINT [FK_Acc_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT [FK_Acc_Cus_Id_Cus]
ALTER TABLE [dbo].[Admin] WITH NOCHECK ADD CONSTRAINT [FK_Adm_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Admin] NOCHECK CONSTRAINT [FK_Adm_Use_Id_Use]
ALTER TABLE [dbo].[Admin] WITH NOCHECK ADD CONSTRAINT [FK_Adm_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Admin] NOCHECK CONSTRAINT [FK_Adm_Clu_Id_Clu]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Boo_Id_Bon] FOREIGN KEY (
 [BookingType_Id]
) REFERENCES [dbo].[BookingType](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Boo_Id_Bon]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Clu_Id_Clu]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Use_Id_Use]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Fac_Id_Fac]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Cou_Id_Cou] FOREIGN KEY (
 [Court_Id]
) REFERENCES [dbo].[Court](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Cou_Id_Cou]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Cus_Id_Cus]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Adm_Id_Adm] FOREIGN KEY (
 [Admin_Id]
) REFERENCES [dbo].[Admin](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Adm_Id_Adm]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Coa_Id_Coa] FOREIGN KEY (
 [Coach_Id]
) REFERENCES [dbo].[Coach](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Coa_Id_Coa]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Bok_Id_Bok] FOREIGN KEY (
 [BookingStatus_Id]
) REFERENCES [dbo].[BookingStatus](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Bok_Id_Bok]
ALTER TABLE [dbo].[Booking] WITH NOCHECK ADD CONSTRAINT [FK_Boo_Eve_Id_Eve] FOREIGN KEY (
 [Event_Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Booking] NOCHECK CONSTRAINT [FK_Boo_Eve_Id_Eve]
ALTER TABLE [dbo].[BookingStatus] WITH NOCHECK ADD CONSTRAINT [FK_Bok_Boo_Id_Bon] FOREIGN KEY (
 [BookingType_Id]
) REFERENCES [dbo].[BookingType](

 [Id]
)
ALTER TABLE [dbo].[BookingStatus] NOCHECK CONSTRAINT [FK_Bok_Boo_Id_Bon]
ALTER TABLE [dbo].[BookingSubType] WITH NOCHECK ADD CONSTRAINT [FK_Boi_Boo_Id_Bon] FOREIGN KEY (
 [BookingType_Id]
) REFERENCES [dbo].[BookingType](

 [Id]
)
ALTER TABLE [dbo].[BookingSubType] NOCHECK CONSTRAINT [FK_Boi_Boo_Id_Bon]
ALTER TABLE [dbo].[BookingType] WITH NOCHECK ADD CONSTRAINT [FK_Bon_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[BookingType] NOCHECK CONSTRAINT [FK_Bon_Clu_Id_Clu]
ALTER TABLE [dbo].[CheckoutItem] WITH NOCHECK ADD CONSTRAINT [FK_Chc_Pro_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[CheckoutItem] NOCHECK CONSTRAINT [FK_Chc_Pro_Id_Pro]
ALTER TABLE [dbo].[ClubAdmin] WITH NOCHECK ADD CONSTRAINT [FK_Clb_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ClubAdmin] NOCHECK CONSTRAINT [FK_Clb_Clu_Id_Clu]
ALTER TABLE [dbo].[ClubAdmin] WITH NOCHECK ADD CONSTRAINT [FK_Clb_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[ClubAdmin] NOCHECK CONSTRAINT [FK_Clb_Use_Id_Use]
ALTER TABLE [dbo].[ClubComponents] WITH NOCHECK ADD CONSTRAINT [FK_ClC_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ClubComponents] NOCHECK CONSTRAINT [FK_ClC_Clu_Id_Clu]
ALTER TABLE [dbo].[ClubComponents] WITH NOCHECK ADD CONSTRAINT [FK_ClC_Com_Id_Com] FOREIGN KEY (
 [Component_Id]
) REFERENCES [dbo].[Components](

 [Id]
)
ALTER TABLE [dbo].[ClubComponents] NOCHECK CONSTRAINT [FK_ClC_Com_Id_Com]
ALTER TABLE [dbo].[ClubPlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_ClP_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ClubPlayerLevel] NOCHECK CONSTRAINT [FK_ClP_Clu_Id_Clu]
ALTER TABLE [dbo].[ClubPlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_ClP_Pla_Id_Pla] FOREIGN KEY (
 [PlayerLevel_Id]
) REFERENCES [dbo].[PlayerLevel](

 [Id]
)
ALTER TABLE [dbo].[ClubPlayerLevel] NOCHECK CONSTRAINT [FK_ClP_Pla_Id_Pla]
ALTER TABLE [dbo].[Coach] WITH NOCHECK ADD CONSTRAINT [FK_Coa_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Coach] NOCHECK CONSTRAINT [FK_Coa_Use_Id_Use]
ALTER TABLE [dbo].[Coach] WITH NOCHECK ADD CONSTRAINT [FK_Coa_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Coach] NOCHECK CONSTRAINT [FK_Coa_Clu_Id_Clu]
ALTER TABLE [dbo].[Court] WITH NOCHECK ADD CONSTRAINT [FK_Cou_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[Court] NOCHECK CONSTRAINT [FK_Cou_Fac_Id_Fac]
ALTER TABLE [dbo].[Court] WITH NOCHECK ADD CONSTRAINT [FK_Cou_Sur_Id_Sur] FOREIGN KEY (
 [Surface_Id]
) REFERENCES [dbo].[Surface](

 [Id]
)
ALTER TABLE [dbo].[Court] NOCHECK CONSTRAINT [FK_Cou_Sur_Id_Sur]
ALTER TABLE [dbo].[Court] WITH NOCHECK ADD CONSTRAINT [FK_Cou_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Court] NOCHECK CONSTRAINT [FK_Cou_Clu_Id_Clu]
ALTER TABLE [dbo].[Court] WITH NOCHECK ADD CONSTRAINT [FK_Cou_Zon_Id_Zon] FOREIGN KEY (
 [Zone_Id]
) REFERENCES [dbo].[Zone](

 [Id]
)
ALTER TABLE [dbo].[Court] NOCHECK CONSTRAINT [FK_Cou_Zon_Id_Zon]
ALTER TABLE [dbo].[CourtHireRate] WITH NOCHECK ADD CONSTRAINT [FK_Cor_Zon_Id_Zon] FOREIGN KEY (
 [Zone_Id]
) REFERENCES [dbo].[Zone](

 [Id]
)
ALTER TABLE [dbo].[CourtHireRate] NOCHECK CONSTRAINT [FK_Cor_Zon_Id_Zon]
ALTER TABLE [dbo].[CourtHireRate] WITH NOCHECK ADD CONSTRAINT [FK_Cor_Tim_Id_Tim] FOREIGN KEY (
 [TimePeriod_Id]
) REFERENCES [dbo].[TimePeriod](

 [Id]
)
ALTER TABLE [dbo].[CourtHireRate] NOCHECK CONSTRAINT [FK_Cor_Tim_Id_Tim]
ALTER TABLE [dbo].[CourtHireRate] WITH NOCHECK ADD CONSTRAINT [FK_Cor_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[CourtHireRate] NOCHECK CONSTRAINT [FK_Cor_Clu_Id_Clu]
ALTER TABLE [dbo].[CourtHireRate] WITH NOCHECK ADD CONSTRAINT [FK_Cor_Mem_Id_Meb] FOREIGN KEY (
 [MembershipType_Id]
) REFERENCES [dbo].[MembershipType](

 [Id]
)
ALTER TABLE [dbo].[CourtHireRate] NOCHECK CONSTRAINT [FK_Cor_Mem_Id_Meb]
ALTER TABLE [dbo].[Credit] WITH NOCHECK ADD CONSTRAINT [FK_Cre_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Credit] NOCHECK CONSTRAINT [FK_Cre_Cus_Id_Cus]
ALTER TABLE [dbo].[Credit] WITH NOCHECK ADD CONSTRAINT [FK_Cre_Ord_Id_Ore] FOREIGN KEY (
 [Order_Id]
) REFERENCES [dbo].[OrderItem](

 [Id]
)
ALTER TABLE [dbo].[Credit] NOCHECK CONSTRAINT [FK_Cre_Ord_Id_Ore]
ALTER TABLE [dbo].[CreditAdjust] WITH NOCHECK ADD CONSTRAINT [FK_Crd_Crd_Id_Cre] FOREIGN KEY (
 [Credit_Id]
) REFERENCES [dbo].[Credit](

 [Id]
)
ALTER TABLE [dbo].[CreditAdjust] NOCHECK CONSTRAINT [FK_Crd_Crd_Id_Cre]
ALTER TABLE [dbo].[CreditAdjust] WITH NOCHECK ADD CONSTRAINT [FK_Crd_Inv_Id_Inv] FOREIGN KEY (
 [Invoice_Id]
) REFERENCES [dbo].[Invoice](

 [Id]
)
ALTER TABLE [dbo].[CreditAdjust] NOCHECK CONSTRAINT [FK_Crd_Inv_Id_Inv]
ALTER TABLE [dbo].[Customer] WITH NOCHECK ADD CONSTRAINT [FK_Cus_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Customer] NOCHECK CONSTRAINT [FK_Cus_Use_Id_Use]
ALTER TABLE [dbo].[Customer] WITH NOCHECK ADD CONSTRAINT [FK_Cus_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Customer] NOCHECK CONSTRAINT [FK_Cus_Clu_Id_Clu]
ALTER TABLE [dbo].[Delivery] WITH NOCHECK ADD CONSTRAINT [FK_Del_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Delivery] NOCHECK CONSTRAINT [FK_Del_Clu_Id_Clu]
ALTER TABLE [dbo].[Delivery] WITH NOCHECK ADD CONSTRAINT [FK_Del_Sup_Id_Sup] FOREIGN KEY (
 [Supplier_Id]
) REFERENCES [dbo].[Supplier](

 [Id]
)
ALTER TABLE [dbo].[Delivery] NOCHECK CONSTRAINT [FK_Del_Sup_Id_Sup]
ALTER TABLE [dbo].[Delivery] WITH NOCHECK ADD CONSTRAINT [FK_Del_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[Delivery] NOCHECK CONSTRAINT [FK_Del_Fac_Id_Fac]
ALTER TABLE [dbo].[Delivery] WITH NOCHECK ADD CONSTRAINT [FK_Del_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Delivery] NOCHECK CONSTRAINT [FK_Del_Use_Id_Use]
ALTER TABLE [dbo].[DeliveryItem] WITH NOCHECK ADD CONSTRAINT [FK_Dei_Pro_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[DeliveryItem] NOCHECK CONSTRAINT [FK_Dei_Pro_Id_Pro]
ALTER TABLE [dbo].[DeliveryItem] WITH NOCHECK ADD CONSTRAINT [FK_Dei_Del_Id_Del] FOREIGN KEY (
 [Delivery_Id]
) REFERENCES [dbo].[Delivery](

 [Id]
)
ALTER TABLE [dbo].[DeliveryItem] NOCHECK CONSTRAINT [FK_Dei_Del_Id_Del]
ALTER TABLE [dbo].[Discount] WITH NOCHECK ADD CONSTRAINT [FK_Dis_Pro_Id_PrV] FOREIGN KEY (
 [ProductVariant_Id]
) REFERENCES [dbo].[ProductVariant](

 [Id]
)
ALTER TABLE [dbo].[Discount] NOCHECK CONSTRAINT [FK_Dis_Pro_Id_PrV]
ALTER TABLE [dbo].[DiscountGroup] WITH NOCHECK ADD CONSTRAINT [FK_Dic_Dis_Id_Dis] FOREIGN KEY (
 [Discount_Id]
) REFERENCES [dbo].[Discount](

 [Id]
)
ALTER TABLE [dbo].[DiscountGroup] NOCHECK CONSTRAINT [FK_Dic_Dis_Id_Dis]
ALTER TABLE [dbo].[DiscountGroup] WITH NOCHECK ADD CONSTRAINT [FK_Dic_Mem_Id_Meb] FOREIGN KEY (
 [MembershipType_Id]
) REFERENCES [dbo].[MembershipType](

 [Id]
)
ALTER TABLE [dbo].[DiscountGroup] NOCHECK CONSTRAINT [FK_Dic_Mem_Id_Meb]
ALTER TABLE [dbo].[EquipmentHire] WITH NOCHECK ADD CONSTRAINT [FK_Equ_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[EquipmentHire] NOCHECK CONSTRAINT [FK_Equ_Clu_Id_Clu]
ALTER TABLE [dbo].[EquipmentHire] WITH NOCHECK ADD CONSTRAINT [FK_Equ_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[EquipmentHire] NOCHECK CONSTRAINT [FK_Equ_Fac_Id_Fac]
ALTER TABLE [dbo].[EquipmentHire] WITH NOCHECK ADD CONSTRAINT [FK_Equ_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[EquipmentHire] NOCHECK CONSTRAINT [FK_Equ_Use_Id_Use]
ALTER TABLE [dbo].[EquipmentHireItem] WITH NOCHECK ADD CONSTRAINT [FK_Eqi_Equ_Id_Equ] FOREIGN KEY (
 [EquipmentHire_Id]
) REFERENCES [dbo].[EquipmentHire](

 [Id]
)
ALTER TABLE [dbo].[EquipmentHireItem] NOCHECK CONSTRAINT [FK_Eqi_Equ_Id_Equ]
ALTER TABLE [dbo].[EquipmentHireItem] WITH NOCHECK ADD CONSTRAINT [FK_Eqi_Pro_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[EquipmentHireItem] NOCHECK CONSTRAINT [FK_Eqi_Pro_Id_Pro]
ALTER TABLE [dbo].[Event] WITH NOCHECK ADD CONSTRAINT [FK_Eve_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Event] NOCHECK CONSTRAINT [FK_Eve_Clu_Id_Clu]
ALTER TABLE [dbo].[Event] WITH NOCHECK ADD CONSTRAINT [FK_Eve_Boo_Id_Bon] FOREIGN KEY (
 [BookingType_Id]
) REFERENCES [dbo].[BookingType](

 [Id]
)
ALTER TABLE [dbo].[Event] NOCHECK CONSTRAINT [FK_Eve_Boo_Id_Bon]
ALTER TABLE [dbo].[EventBooking] WITH NOCHECK ADD CONSTRAINT [FK_Evn_Evn_Id_Eve] FOREIGN KEY (
 [Event_Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[EventBooking] NOCHECK CONSTRAINT [FK_Evn_Evn_Id_Eve]
ALTER TABLE [dbo].[EventBooking] WITH NOCHECK ADD CONSTRAINT [FK_Evn_Evt_Id_Evt] FOREIGN KEY (
 [EventBookingCancellation_Id]
) REFERENCES [dbo].[EventBookingCancellation](

 [Id]
)
ALTER TABLE [dbo].[EventBooking] NOCHECK CONSTRAINT [FK_Evn_Evt_Id_Evt]
ALTER TABLE [dbo].[EventBookingCancellation] WITH NOCHECK ADD CONSTRAINT [FK_Evt_Can_Id_Can] FOREIGN KEY (
 [CancellationType_Id]
) REFERENCES [dbo].[CancellationType](

 [Id]
)
ALTER TABLE [dbo].[EventBookingCancellation] NOCHECK CONSTRAINT [FK_Evt_Can_Id_Can]
ALTER TABLE [dbo].[EventBookingCancellation] WITH NOCHECK ADD CONSTRAINT [FK_Evt_Eve_Id_Evn] FOREIGN KEY (
 [EventBooking_Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventBookingCancellation] NOCHECK CONSTRAINT [FK_Evt_Eve_Id_Evn]
ALTER TABLE [dbo].[EventFee] WITH NOCHECK ADD CONSTRAINT [FK_EvF_Eve_Id_Eve] FOREIGN KEY (
 [Event_Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[EventFee] NOCHECK CONSTRAINT [FK_EvF_Eve_Id_Eve]
ALTER TABLE [dbo].[EventFee] WITH NOCHECK ADD CONSTRAINT [FK_EvF_Mem_Id_Meb] FOREIGN KEY (
 [MembershipType_Id]
) REFERENCES [dbo].[MembershipType](

 [Id]
)
ALTER TABLE [dbo].[EventFee] NOCHECK CONSTRAINT [FK_EvF_Mem_Id_Meb]
ALTER TABLE [dbo].[EventRegistration] WITH NOCHECK ADD CONSTRAINT [FK_EvR_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[EventRegistration] NOCHECK CONSTRAINT [FK_EvR_Cus_Id_Cus]
ALTER TABLE [dbo].[EventRegistration] WITH NOCHECK ADD CONSTRAINT [FK_EvR_Eve_Id_Evn] FOREIGN KEY (
 [EventBooking_Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventRegistration] NOCHECK CONSTRAINT [FK_EvR_Eve_Id_Evn]
ALTER TABLE [dbo].[EventRegistration] WITH NOCHECK ADD CONSTRAINT [FK_EvR_Ord_Id_Ore] FOREIGN KEY (
 [OrderItem_Id]
) REFERENCES [dbo].[OrderItem](

 [Id]
)
ALTER TABLE [dbo].[EventRegistration] NOCHECK CONSTRAINT [FK_EvR_Ord_Id_Ore]
ALTER TABLE [dbo].[EventRoll] WITH NOCHECK ADD CONSTRAINT [FK_Evo_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[EventRoll] NOCHECK CONSTRAINT [FK_Evo_Cus_Id_Cus]
ALTER TABLE [dbo].[EventRoll] WITH NOCHECK ADD CONSTRAINT [FK_Evo_Eve_Id_Evn] FOREIGN KEY (
 [EventBooking_Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventRoll] NOCHECK CONSTRAINT [FK_Evo_Eve_Id_Evn]
ALTER TABLE [dbo].[EventWaitlist] WITH NOCHECK ADD CONSTRAINT [FK_EvW_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[EventWaitlist] NOCHECK CONSTRAINT [FK_EvW_Cus_Id_Cus]
ALTER TABLE [dbo].[EventWaitlist] WITH NOCHECK ADD CONSTRAINT [FK_EvW_Eve_Id_Evn] FOREIGN KEY (
 [EventBooking_Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventWaitlist] NOCHECK CONSTRAINT [FK_EvW_Eve_Id_Evn]
ALTER TABLE [dbo].[Facility] WITH NOCHECK ADD CONSTRAINT [FK_Fac_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Facility] NOCHECK CONSTRAINT [FK_Fac_Clu_Id_Clu]
ALTER TABLE [dbo].[Invoice] WITH NOCHECK ADD CONSTRAINT [FK_Inv_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Invoice] NOCHECK CONSTRAINT [FK_Inv_Cus_Id_Cus]
ALTER TABLE [dbo].[Invoice] WITH NOCHECK ADD CONSTRAINT [FK_Inv_Adm_Id_Adm] FOREIGN KEY (
 [Admin_Id]
) REFERENCES [dbo].[Admin](

 [Id]
)
ALTER TABLE [dbo].[Invoice] NOCHECK CONSTRAINT [FK_Inv_Adm_Id_Adm]
ALTER TABLE [dbo].[Invoice] WITH NOCHECK ADD CONSTRAINT [FK_Inv_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Invoice] NOCHECK CONSTRAINT [FK_Inv_Clu_Id_Clu]
ALTER TABLE [dbo].[Invoice] WITH NOCHECK ADD CONSTRAINT [FK_Inv_Ord_Id_Ord] FOREIGN KEY (
 [Order_Id]
) REFERENCES [dbo].[Order](

 [Id]
)
ALTER TABLE [dbo].[Invoice] NOCHECK CONSTRAINT [FK_Inv_Ord_Id_Ord]
ALTER TABLE [dbo].[Member] WITH NOCHECK ADD CONSTRAINT [FK_Mem_Mem_Id_Meb] FOREIGN KEY (
 [MembershipType_Id]
) REFERENCES [dbo].[MembershipType](

 [Id]
)
ALTER TABLE [dbo].[Member] NOCHECK CONSTRAINT [FK_Mem_Mem_Id_Meb]
ALTER TABLE [dbo].[Member] WITH NOCHECK ADD CONSTRAINT [FK_Mem_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Member] NOCHECK CONSTRAINT [FK_Mem_Cus_Id_Cus]
ALTER TABLE [dbo].[MembershipType] WITH NOCHECK ADD CONSTRAINT [FK_Meb_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[MembershipType] NOCHECK CONSTRAINT [FK_Meb_Clu_Id_Clu]
ALTER TABLE [dbo].[MembershipTypeTimePeriod] WITH NOCHECK ADD CONSTRAINT [FK_Mee_Mem_Id_Meb] FOREIGN KEY (
 [MembershipType_Id]
) REFERENCES [dbo].[MembershipType](

 [Id]
)
ALTER TABLE [dbo].[MembershipTypeTimePeriod] NOCHECK CONSTRAINT [FK_Mee_Mem_Id_Meb]
ALTER TABLE [dbo].[MembershipTypeTimePeriod] WITH NOCHECK ADD CONSTRAINT [FK_Mee_Tim_Id_Tie] FOREIGN KEY (
 [TimePeriodGroup_Id]
) REFERENCES [dbo].[TimePeriodGroup](

 [Id]
)
ALTER TABLE [dbo].[MembershipTypeTimePeriod] NOCHECK CONSTRAINT [FK_Mee_Tim_Id_Tie]
ALTER TABLE [dbo].[MembershipTypeTimePeriod] WITH NOCHECK ADD CONSTRAINT [FK_Mee_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[MembershipTypeTimePeriod] NOCHECK CONSTRAINT [FK_Mee_Clu_Id_Clu]
ALTER TABLE [dbo].[MessageAccount] WITH NOCHECK ADD CONSTRAINT [FK_Mes_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[MessageAccount] NOCHECK CONSTRAINT [FK_Mes_Clu_Id_Clu]
ALTER TABLE [dbo].[MessageLog] WITH NOCHECK ADD CONSTRAINT [FK_Mea_Adm_Id_Adm] FOREIGN KEY (
 [Admin_Id]
) REFERENCES [dbo].[Admin](

 [Id]
)
ALTER TABLE [dbo].[MessageLog] NOCHECK CONSTRAINT [FK_Mea_Adm_Id_Adm]
ALTER TABLE [dbo].[MessageLog] WITH NOCHECK ADD CONSTRAINT [FK_Mea_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[MessageLog] NOCHECK CONSTRAINT [FK_Mea_Cus_Id_Cus]
ALTER TABLE [dbo].[MessageLog] WITH NOCHECK ADD CONSTRAINT [FK_Mea_Mes_Id_Meg] FOREIGN KEY (
 [MessageNumber_Id]
) REFERENCES [dbo].[MessageNumber](

 [Id]
)
ALTER TABLE [dbo].[MessageLog] NOCHECK CONSTRAINT [FK_Mea_Mes_Id_Meg]
ALTER TABLE [dbo].[MessageNumber] WITH NOCHECK ADD CONSTRAINT [FK_Meg_Mes_Id_Mer] FOREIGN KEY (
 [MessageService_Id]
) REFERENCES [dbo].[MessageService](

 [Id]
)
ALTER TABLE [dbo].[MessageNumber] NOCHECK CONSTRAINT [FK_Meg_Mes_Id_Mer]
ALTER TABLE [dbo].[MessageService] WITH NOCHECK ADD CONSTRAINT [FK_Mer_Mes_Id_Mes] FOREIGN KEY (
 [MessageAccount_Id]
) REFERENCES [dbo].[MessageAccount](

 [Id]
)
ALTER TABLE [dbo].[MessageService] NOCHECK CONSTRAINT [FK_Mer_Mes_Id_Mes]
ALTER TABLE [dbo].[Order] WITH NOCHECK ADD CONSTRAINT [FK_Ord_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Order] NOCHECK CONSTRAINT [FK_Ord_Cus_Id_Cus]
ALTER TABLE [dbo].[Order] WITH NOCHECK ADD CONSTRAINT [FK_Ord_Adm_Id_Adm] FOREIGN KEY (
 [Admin_Id]
) REFERENCES [dbo].[Admin](

 [Id]
)
ALTER TABLE [dbo].[Order] NOCHECK CONSTRAINT [FK_Ord_Adm_Id_Adm]
ALTER TABLE [dbo].[Order] WITH NOCHECK ADD CONSTRAINT [FK_Ord_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Order] NOCHECK CONSTRAINT [FK_Ord_Clu_Id_Clu]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Boo_Id_Boo] FOREIGN KEY (
 [Booking_Id]
) REFERENCES [dbo].[Booking](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Boo_Id_Boo]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Reg_Id_EvR] FOREIGN KEY (
 [Registration_Id]
) REFERENCES [dbo].[EventRegistration](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Reg_Id_EvR]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Mem_Id_Mem] FOREIGN KEY (
 [Membership_Id]
) REFERENCES [dbo].[Member](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Mem_Id_Mem]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Pro_Id_PrV] FOREIGN KEY (
 [ProductVariant_Id]
) REFERENCES [dbo].[ProductVariant](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Pro_Id_PrV]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Orr_Id_Ord] FOREIGN KEY (
 [Order_Id]
) REFERENCES [dbo].[Order](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Orr_Id_Ord]
ALTER TABLE [dbo].[OrderItem] WITH NOCHECK ADD CONSTRAINT [FK_Ore_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[OrderItem] NOCHECK CONSTRAINT [FK_Ore_Cus_Id_Cus]
ALTER TABLE [dbo].[OrderItemDetail] WITH NOCHECK ADD CONSTRAINT [FK_Orr_Ord_Id_Ore] FOREIGN KEY (
 [OrderItem_Id]
) REFERENCES [dbo].[OrderItem](

 [Id]
)
ALTER TABLE [dbo].[OrderItemDetail] NOCHECK CONSTRAINT [FK_Orr_Ord_Id_Ore]
ALTER TABLE [dbo].[PlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_Pla_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[PlayerLevel] NOCHECK CONSTRAINT [FK_Pla_Clu_Id_Clu]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Prd_Id_Pry] FOREIGN KEY (
 [ProductType_Id]
) REFERENCES [dbo].[ProductType](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Prd_Id_Pry]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Clu_Id_Clu]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Pru_Id_Pre] FOREIGN KEY (
 [ProductVendor_Id]
) REFERENCES [dbo].[ProductVendor](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Pru_Id_Pre]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Prc_Id_Prp] FOREIGN KEY (
 [ProductOption_Id]
) REFERENCES [dbo].[ProductOption](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Prc_Id_Prp]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Prt_Id_Prd] FOREIGN KEY (
 [ProductBrand_Id]
) REFERENCES [dbo].[ProductBrand](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Prt_Id_Prd]
ALTER TABLE [dbo].[Product] WITH NOCHECK ADD CONSTRAINT [FK_Pro_Prl_Id_Pru] FOREIGN KEY (
 [ProductClass_Id]
) REFERENCES [dbo].[ProductClass](

 [Id]
)
ALTER TABLE [dbo].[Product] NOCHECK CONSTRAINT [FK_Pro_Prl_Id_Pru]
ALTER TABLE [dbo].[ProductBrand] WITH NOCHECK ADD CONSTRAINT [FK_Prd_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ProductBrand] NOCHECK CONSTRAINT [FK_Prd_Clu_Id_Clu]
ALTER TABLE [dbo].[ProductClass] WITH NOCHECK ADD CONSTRAINT [FK_Pru_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ProductClass] NOCHECK CONSTRAINT [FK_Pru_Clu_Id_Clu]
ALTER TABLE [dbo].[ProductImage] WITH NOCHECK ADD CONSTRAINT [FK_Prc_Pro_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[ProductImage] NOCHECK CONSTRAINT [FK_Prc_Pro_Id_Pro]
ALTER TABLE [dbo].[ProductInventory] WITH NOCHECK ADD CONSTRAINT [FK_Prt_Pro_Id_PrV] FOREIGN KEY (
 [ProductVariant_Id]
) REFERENCES [dbo].[ProductVariant](

 [Id]
)
ALTER TABLE [dbo].[ProductInventory] NOCHECK CONSTRAINT [FK_Prt_Pro_Id_PrV]
ALTER TABLE [dbo].[ProductOption] WITH NOCHECK ADD CONSTRAINT [FK_Prp_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ProductOption] NOCHECK CONSTRAINT [FK_Prp_Clu_Id_Clu]
ALTER TABLE [dbo].[ProductType] WITH NOCHECK ADD CONSTRAINT [FK_Pry_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ProductType] NOCHECK CONSTRAINT [FK_Pry_Clu_Id_Clu]
ALTER TABLE [dbo].[ProductVariant] WITH NOCHECK ADD CONSTRAINT [FK_PrV_Prd_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[ProductVariant] NOCHECK CONSTRAINT [FK_PrV_Prd_Id_Pro]
ALTER TABLE [dbo].[ProductVendor] WITH NOCHECK ADD CONSTRAINT [FK_Pre_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[ProductVendor] NOCHECK CONSTRAINT [FK_Pre_Clu_Id_Clu]
ALTER TABLE [dbo].[RoleClaim] WITH NOCHECK ADD CONSTRAINT [FK_Roe_Rol_Id_Rol] FOREIGN KEY (
 [Role_Id]
) REFERENCES [dbo].[Role](

 [Id]
)
ALTER TABLE [dbo].[RoleClaim] NOCHECK CONSTRAINT [FK_Roe_Rol_Id_Rol]
ALTER TABLE [dbo].[Sale] WITH NOCHECK ADD CONSTRAINT [FK_Sal_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Sale] NOCHECK CONSTRAINT [FK_Sal_Cus_Id_Cus]
ALTER TABLE [dbo].[Sale] WITH NOCHECK ADD CONSTRAINT [FK_Sal_Adm_Id_Adm] FOREIGN KEY (
 [Admin_Id]
) REFERENCES [dbo].[Admin](

 [Id]
)
ALTER TABLE [dbo].[Sale] NOCHECK CONSTRAINT [FK_Sal_Adm_Id_Adm]
ALTER TABLE [dbo].[Sale] WITH NOCHECK ADD CONSTRAINT [FK_Sal_Inv_Id_Inv] FOREIGN KEY (
 [Invoice_Id]
) REFERENCES [dbo].[Invoice](

 [Id]
)
ALTER TABLE [dbo].[Sale] NOCHECK CONSTRAINT [FK_Sal_Inv_Id_Inv]
ALTER TABLE [dbo].[SaleItem] WITH NOCHECK ADD CONSTRAINT [FK_Sae_Sal_Id_Sal] FOREIGN KEY (
 [Sale_Id]
) REFERENCES [dbo].[Sale](

 [Id]
)
ALTER TABLE [dbo].[SaleItem] NOCHECK CONSTRAINT [FK_Sae_Sal_Id_Sal]
ALTER TABLE [dbo].[SaleItem] WITH NOCHECK ADD CONSTRAINT [FK_Sae_Ord_Id_Ore] FOREIGN KEY (
 [Order_Id]
) REFERENCES [dbo].[OrderItem](

 [Id]
)
ALTER TABLE [dbo].[SaleItem] NOCHECK CONSTRAINT [FK_Sae_Ord_Id_Ore]
ALTER TABLE [dbo].[Size] WITH NOCHECK ADD CONSTRAINT [FK_Siz_Pro_Id_Pry] FOREIGN KEY (
 [ProductType_Id]
) REFERENCES [dbo].[ProductType](

 [Id]
)
ALTER TABLE [dbo].[Size] NOCHECK CONSTRAINT [FK_Siz_Pro_Id_Pry]
ALTER TABLE [dbo].[Source] WITH NOCHECK ADD CONSTRAINT [FK_Sou_Cus_Id_Cus] FOREIGN KEY (
 [Customer_Id]
) REFERENCES [dbo].[Customer](

 [Id]
)
ALTER TABLE [dbo].[Source] NOCHECK CONSTRAINT [FK_Sou_Cus_Id_Cus]
ALTER TABLE [dbo].[Stocktake] WITH NOCHECK ADD CONSTRAINT [FK_Sto_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Stocktake] NOCHECK CONSTRAINT [FK_Sto_Use_Id_Use]
ALTER TABLE [dbo].[Stocktake] WITH NOCHECK ADD CONSTRAINT [FK_Sto_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[Stocktake] NOCHECK CONSTRAINT [FK_Sto_Fac_Id_Fac]
ALTER TABLE [dbo].[StocktakeItem] WITH NOCHECK ADD CONSTRAINT [FK_Stc_Sto_Id_Sto] FOREIGN KEY (
 [Stocktake_Id]
) REFERENCES [dbo].[Stocktake](

 [Id]
)
ALTER TABLE [dbo].[StocktakeItem] NOCHECK CONSTRAINT [FK_Stc_Sto_Id_Sto]
ALTER TABLE [dbo].[StocktakeItem] WITH NOCHECK ADD CONSTRAINT [FK_Stc_Pro_Id_Pro] FOREIGN KEY (
 [Product_Id]
) REFERENCES [dbo].[Product](

 [Id]
)
ALTER TABLE [dbo].[StocktakeItem] NOCHECK CONSTRAINT [FK_Stc_Pro_Id_Pro]
ALTER TABLE [dbo].[Supplier] WITH NOCHECK ADD CONSTRAINT [FK_Sup_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Supplier] NOCHECK CONSTRAINT [FK_Sup_Clu_Id_Clu]
ALTER TABLE [dbo].[SupplierContact] WITH NOCHECK ADD CONSTRAINT [FK_Sul_Sup_Id_Sup] FOREIGN KEY (
 [Supplier_Id]
) REFERENCES [dbo].[Supplier](

 [Id]
)
ALTER TABLE [dbo].[SupplierContact] NOCHECK CONSTRAINT [FK_Sul_Sup_Id_Sup]
ALTER TABLE [dbo].[Term] WITH NOCHECK ADD CONSTRAINT [FK_Ter_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Term] NOCHECK CONSTRAINT [FK_Ter_Clu_Id_Clu]
ALTER TABLE [dbo].[TimePeriod] WITH NOCHECK ADD CONSTRAINT [FK_Tim_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[TimePeriod] NOCHECK CONSTRAINT [FK_Tim_Clu_Id_Clu]
ALTER TABLE [dbo].[TimePeriod] WITH NOCHECK ADD CONSTRAINT [FK_Tim_Tim_Id_Tie] FOREIGN KEY (
 [TimePeriodGroup_Id]
) REFERENCES [dbo].[TimePeriodGroup](

 [Id]
)
ALTER TABLE [dbo].[TimePeriod] NOCHECK CONSTRAINT [FK_Tim_Tim_Id_Tie]
ALTER TABLE [dbo].[TimePeriodGroup] WITH NOCHECK ADD CONSTRAINT [FK_Tie_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[TimePeriodGroup] NOCHECK CONSTRAINT [FK_Tie_Clu_Id_Clu]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Acc_Id_Acc] FOREIGN KEY (
 [Account_Id]
) REFERENCES [dbo].[Account](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Acc_Id_Acc]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Cur_Id_Cur] FOREIGN KEY (
 [Currency_Id]
) REFERENCES [dbo].[Currency](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Cur_Id_Cur]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Jou_Id_Jou] FOREIGN KEY (
 [Journal_Id]
) REFERENCES [dbo].[Journal](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Jou_Id_Jou]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Aco_Id_Aco] FOREIGN KEY (
 [AccountingPeriod_Id]
) REFERENCES [dbo].[AccountingPeriod](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Aco_Id_Aco]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Inv_Id_Inv] FOREIGN KEY (
 [Invoice_Id]
) REFERENCES [dbo].[Invoice](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Inv_Id_Inv]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Clu_Id_Clu]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Sor_Id_Acc] FOREIGN KEY (
 [Source_Id]
) REFERENCES [dbo].[Account](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Sor_Id_Acc]
ALTER TABLE [dbo].[Transaction] WITH NOCHECK ADD CONSTRAINT [FK_Tra_Dei_Id_Acc] FOREIGN KEY (
 [Destination_Id]
) REFERENCES [dbo].[Account](

 [Id]
)
ALTER TABLE [dbo].[Transaction] NOCHECK CONSTRAINT [FK_Tra_Dei_Id_Acc]
ALTER TABLE [dbo].[User] WITH NOCHECK ADD CONSTRAINT [FK_Use_Gen_Id_Gen] FOREIGN KEY (
 [Gender_Id]
) REFERENCES [dbo].[Gender](

 [Id]
)
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_Use_Gen_Id_Gen]
ALTER TABLE [dbo].[User] WITH NOCHECK ADD CONSTRAINT [FK_Use_Pla_Id_Ply] FOREIGN KEY (
 [PlayerRating_Id]
) REFERENCES [dbo].[PlayerRating](

 [Id]
)
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_Use_Pla_Id_Ply]
ALTER TABLE [dbo].[UserClaim] WITH NOCHECK ADD CONSTRAINT [FK_Usr_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[UserClaim] NOCHECK CONSTRAINT [FK_Usr_Use_Id_Use]
ALTER TABLE [dbo].[UserLogin] WITH NOCHECK ADD CONSTRAINT [FK_UsL_Use_Id_Use] FOREIGN KEY (
 [User_Id]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[UserLogin] NOCHECK CONSTRAINT [FK_UsL_Use_Id_Use]
ALTER TABLE [dbo].[Zone] WITH NOCHECK ADD CONSTRAINT [FK_Zon_Fac_Id_Fac] FOREIGN KEY (
 [Facility_Id]
) REFERENCES [dbo].[Facility](

 [Id]
)
ALTER TABLE [dbo].[Zone] NOCHECK CONSTRAINT [FK_Zon_Fac_Id_Fac]
ALTER TABLE [dbo].[Zone] WITH NOCHECK ADD CONSTRAINT [FK_Zon_Clu_Id_Clu] FOREIGN KEY (
 [Club_Id]
) REFERENCES [dbo].[Club](

 [Id]
)
ALTER TABLE [dbo].[Zone] NOCHECK CONSTRAINT [FK_Zon_Clu_Id_Clu]
ALTER TABLE [dbo].[Event_Coaches_Coach] WITH NOCHECK ADD CONSTRAINT [FK_Ev__Id_Id_Eve] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Event_Coaches_Coach] NOCHECK CONSTRAINT [FK_Ev__Id_Id_Eve]
ALTER TABLE [dbo].[Event_Coaches_Coach] WITH NOCHECK ADD CONSTRAINT [FK_Ev__Id2_Id_Coa] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Coach](

 [Id]
)
ALTER TABLE [dbo].[Event_Coaches_Coach] NOCHECK CONSTRAINT [FK_Ev__Id2_Id_Coa]
ALTER TABLE [dbo].[Event_GenderAges_GenderAge] WITH NOCHECK ADD CONSTRAINT [FK_EvG_Id_Id_Eve] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Event_GenderAges_GenderAge] NOCHECK CONSTRAINT [FK_EvG_Id_Id_Eve]
ALTER TABLE [dbo].[Event_GenderAges_GenderAge] WITH NOCHECK ADD CONSTRAINT [FK_EvG_Id2_Id_Ged] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[GenderAge](

 [Id]
)
ALTER TABLE [dbo].[Event_GenderAges_GenderAge] NOCHECK CONSTRAINT [FK_EvG_Id2_Id_Ged]
ALTER TABLE [dbo].[Event_Levels_PlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_EvL_Id_Id_Eve] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Event_Levels_PlayerLevel] NOCHECK CONSTRAINT [FK_EvL_Id_Id_Eve]
ALTER TABLE [dbo].[Event_Levels_PlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_EvL_Id2_Id_Pla] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[PlayerLevel](

 [Id]
)
ALTER TABLE [dbo].[Event_Levels_PlayerLevel] NOCHECK CONSTRAINT [FK_EvL_Id2_Id_Pla]
ALTER TABLE [dbo].[Event_Courts_Court] WITH NOCHECK ADD CONSTRAINT [FK_EvC_Id_Id_Eve] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Event_Courts_Court] NOCHECK CONSTRAINT [FK_EvC_Id_Id_Eve]
ALTER TABLE [dbo].[Event_Courts_Court] WITH NOCHECK ADD CONSTRAINT [FK_EvC_Id2_Id_Cou] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Court](

 [Id]
)
ALTER TABLE [dbo].[Event_Courts_Court] NOCHECK CONSTRAINT [FK_EvC_Id2_Id_Cou]
ALTER TABLE [dbo].[Event_Days_DaysOfWeek] WITH NOCHECK ADD CONSTRAINT [FK_EvD_Id_Id_Eve] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Event](

 [Id]
)
ALTER TABLE [dbo].[Event_Days_DaysOfWeek] NOCHECK CONSTRAINT [FK_EvD_Id_Id_Eve]
ALTER TABLE [dbo].[Event_Days_DaysOfWeek] WITH NOCHECK ADD CONSTRAINT [FK_EvD_Id2_Id_Day] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[DaysOfWeek](

 [Id]
)
ALTER TABLE [dbo].[Event_Days_DaysOfWeek] NOCHECK CONSTRAINT [FK_EvD_Id2_Id_Day]
ALTER TABLE [dbo].[EventBooking_Coaches_Coach] WITH NOCHECK ADD CONSTRAINT [FK_EvB_Id_Id_Evn] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Coaches_Coach] NOCHECK CONSTRAINT [FK_EvB_Id_Id_Evn]
ALTER TABLE [dbo].[EventBooking_Coaches_Coach] WITH NOCHECK ADD CONSTRAINT [FK_EvB_Id2_Id_Coa] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Coach](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Coaches_Coach] NOCHECK CONSTRAINT [FK_EvB_Id2_Id_Coa]
ALTER TABLE [dbo].[EventBooking_Booking_Booking] WITH NOCHECK ADD CONSTRAINT [FK_Evk_Id_Id_Evn] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Booking_Booking] NOCHECK CONSTRAINT [FK_Evk_Id_Id_Evn]
ALTER TABLE [dbo].[EventBooking_Booking_Booking] WITH NOCHECK ADD CONSTRAINT [FK_Evk_Id2_Id_Boo] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Booking](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Booking_Booking] NOCHECK CONSTRAINT [FK_Evk_Id2_Id_Boo]
ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_Evi_Id_Id_Evn] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[EventBooking](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] NOCHECK CONSTRAINT [FK_Evi_Id_Id_Evn]
ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] WITH NOCHECK ADD CONSTRAINT [FK_Evi_Id2_Id_Pla] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[PlayerLevel](

 [Id]
)
ALTER TABLE [dbo].[EventBooking_Levels_PlayerLevel] NOCHECK CONSTRAINT [FK_Evi_Id2_Id_Pla]
ALTER TABLE [dbo].[Invoice_Journals_Journal] WITH NOCHECK ADD CONSTRAINT [FK_Ino_Id_Id_Inv] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Invoice](

 [Id]
)
ALTER TABLE [dbo].[Invoice_Journals_Journal] NOCHECK CONSTRAINT [FK_Ino_Id_Id_Inv]
ALTER TABLE [dbo].[Invoice_Journals_Journal] WITH NOCHECK ADD CONSTRAINT [FK_Ino_Id2_Id_Jou] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Journal](

 [Id]
)
ALTER TABLE [dbo].[Invoice_Journals_Journal] NOCHECK CONSTRAINT [FK_Ino_Id2_Id_Jou]
ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] WITH NOCHECK ADD CONSTRAINT [FK_OrI_Id_Id_Ore] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[OrderItem](

 [Id]
)
ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] NOCHECK CONSTRAINT [FK_OrI_Id_Id_Ore]
ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] WITH NOCHECK ADD CONSTRAINT [FK_OrI_Id2_Id_Tra] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[Transaction](

 [Id]
)
ALTER TABLE [dbo].[OrderItem_Transactions_Transaction] NOCHECK CONSTRAINT [FK_OrI_Id2_Id_Tra]
ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] WITH NOCHECK ADD CONSTRAINT [FK_Ple_Id_Id_Pla] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[PlayerLevel](

 [Id]
)
ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] NOCHECK CONSTRAINT [FK_Ple_Id_Id_Pla]
ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] WITH NOCHECK ADD CONSTRAINT [FK_Ple_Id2_Id_Ply] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[PlayerRating](

 [Id]
)
ALTER TABLE [dbo].[PlayerLevel_PlayerRatings_PlayerRating_PlayerLevels] NOCHECK CONSTRAINT [FK_Ple_Id2_Id_Ply]
ALTER TABLE [dbo].[Role_User_User_Roles] WITH NOCHECK ADD CONSTRAINT [FK_Ro__Id_Id_Rol] FOREIGN KEY (
 [Id]
) REFERENCES [dbo].[Role](

 [Id]
)
ALTER TABLE [dbo].[Role_User_User_Roles] NOCHECK CONSTRAINT [FK_Ro__Id_Id_Rol]
ALTER TABLE [dbo].[Role_User_User_Roles] WITH NOCHECK ADD CONSTRAINT [FK_Ro__Id2_Id_Use] FOREIGN KEY (
 [Id2]
) REFERENCES [dbo].[User](

 [Id]
)
ALTER TABLE [dbo].[Role_User_User_Roles] NOCHECK CONSTRAINT [FK_Ro__Id2_Id_Use]
