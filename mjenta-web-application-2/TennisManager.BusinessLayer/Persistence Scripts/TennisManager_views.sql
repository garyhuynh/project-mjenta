﻿/* CodeFluent Generated Friday, 04 December 2015 10:57. TargetVersion:Sql2014, SqlAzure. Culture:en-AU. UiCulture:en-GB. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vAccount' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vAccount]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vBooking' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vBooking]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vBookingCategory' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vBookingCategory]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vBookingGroup' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vBookingGroup]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vBookingSubType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vBookingSubType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vBookingType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vBookingType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCart' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCart]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCartItem' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCartItem]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCheckout' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCheckout]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vClub' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vClub]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vClubAdmin' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vClubAdmin]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vClubComponents' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vClubComponents]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vClubEventSubTypes' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vClubEventSubTypes]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vClubPlayerLevel' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vClubPlayerLevel]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vComponents' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vComponents]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCourt' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCourt]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCourtHireRate' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCourtHireRate]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vCredit' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vCredit]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vDelivery' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vDelivery]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vDeliveryItem' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vDeliveryItem]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEquipmentHire' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEquipmentHire]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEquipmentHireItem' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEquipmentHireItem]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEvent' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEvent]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventBooking' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventBooking]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventFee' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventFee]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventLevel' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventLevel]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventRegistration' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventRegistration]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventRoll' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventRoll]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventSubType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventSubType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vEventWaitlist' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vEventWaitlist]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vFacility' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vFacility]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vGender' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vGender]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vGenderAge' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vGenderAge]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vInventory' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vInventory]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vInventoryQtyAdjust' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vInventoryQtyAdjust]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vInvoice' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vInvoice]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vInvoiceItem' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vInvoiceItem]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vItemtype' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vItemtype]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vMember' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vMember]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vMembership' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vMembership]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vMembershipType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vMembershipType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vMembershipTypeTimePeriod' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vMembershipTypeTimePeriod]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vPaymentMethod' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vPaymentMethod]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vPlayerLevel' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vPlayerLevel]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vPlayerRating' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vPlayerRating]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProduct' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProduct]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductBrand' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductBrand]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductCategory' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductCategory]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductPrice' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductPrice]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductStatus' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductStatus]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductSupplier' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductSupplier]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vProductType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vProductType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vRole' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vRole]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vRoleClaim' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vRoleClaim]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSize' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSize]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSizeStandard' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSizeStandard]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSizeType' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSizeType]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vStocktake' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vStocktake]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vStocktakeItem' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vStocktakeItem]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSupplier' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSupplier]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSupplierContact' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSupplierContact]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vSurface' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vSurface]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vTimePeriod' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vTimePeriod]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vTimePeriodGroup' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vTimePeriodGroup]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vUser' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vUser]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vUserClaim' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vUserClaim]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vUserLogin' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vUserLogin]
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vZone' AND TABLE_SCHEMA = 'dbo')
DROP VIEW [dbo].[vZone]
GO

CREATE VIEW [dbo].[vAccount]
AS
SELECT [Account].[Id], [Account].[_rowVersion], [Account].[_trackCreationTime], [Account].[_trackLastWriteTime], [Account].[_trackCreationUser], [Account].[_trackLastWriteUser] 
    FROM [Account]
GO

CREATE VIEW [dbo].[vBooking]
AS
SELECT [Booking].[Id], [Booking].[Description], [Booking].[Start], [Booking].[End], [Booking].[StartTimezone], [Booking].[EndTimezone], [Booking].[IsAllDay], [Booking].[Recurrence], [Booking].[RecurrenceException], [Booking].[RecurrenceRule], [Booking].[Title], [Booking].[ReferenceNo], [Booking].[BookingType_Id], [Booking].[Club_Id], [Booking].[isActive], [Booking].[Price], [Booking].[isPaid], [Booking].[NumberOfPlayers], [Booking].[User_Id], [Booking].[Facility_Id], [Booking].[Court_Id], [Booking].[BookingGroup_Id], [Booking].[_rowVersion], [Booking].[_trackCreationTime], [Booking].[_trackLastWriteTime], [Booking].[_trackCreationUser], [Booking].[_trackLastWriteUser] 
    FROM [Booking]
GO

CREATE VIEW [dbo].[vBookingCategory]
AS
SELECT [BookingCategory].[Id], [BookingCategory].[Name], [BookingCategory].[Sequence], [BookingCategory].[_rowVersion], [BookingCategory].[_trackCreationTime], [BookingCategory].[_trackLastWriteTime], [BookingCategory].[_trackCreationUser], [BookingCategory].[_trackLastWriteUser] 
    FROM [BookingCategory]
GO

CREATE VIEW [dbo].[vBookingGroup]
AS
SELECT [BookingGroup].[Id], [BookingGroup].[Club_Id], [BookingGroup].[_rowVersion], [BookingGroup].[_trackCreationTime], [BookingGroup].[_trackLastWriteTime], [BookingGroup].[_trackCreationUser], [BookingGroup].[_trackLastWriteUser] 
    FROM [BookingGroup]
GO

CREATE VIEW [dbo].[vBookingSubType]
AS
SELECT [BookingSubType].[Id], [BookingSubType].[BookingType_Id], [BookingSubType].[Name], [BookingSubType].[Sequence], [BookingSubType].[_rowVersion], [BookingSubType].[_trackCreationTime], [BookingSubType].[_trackLastWriteTime], [BookingSubType].[_trackCreationUser], [BookingSubType].[_trackLastWriteUser] 
    FROM [BookingSubType]
GO

CREATE VIEW [dbo].[vBookingType]
AS
SELECT [BookingType].[Id], [BookingType].[Name], [BookingType].[Sequence], [BookingType].[BookingCategory_Id], [BookingType].[_rowVersion], [BookingType].[_trackCreationTime], [BookingType].[_trackLastWriteTime], [BookingType].[_trackCreationUser], [BookingType].[_trackLastWriteUser] 
    FROM [BookingType]
GO

CREATE VIEW [dbo].[vCart]
AS
SELECT [Cart].[Id], [Cart].[User_Id], [Cart].[Status], [Cart].[Start], [Cart].[ReferenceNo], [Cart].[ItemQuantity], [Cart].[_rowVersion], [Cart].[_trackCreationTime], [Cart].[_trackLastWriteTime], [Cart].[_trackCreationUser], [Cart].[_trackLastWriteUser] 
    FROM [Cart]
GO

CREATE VIEW [dbo].[vCartItem]
AS
SELECT [CartItem].[Id], [CartItem].[Product_Id], [CartItem].[Cart_Id], [CartItem].[ReferenceNo], [CartItem].[Type_Id], [CartItem].[Amount], [CartItem].[_rowVersion], [CartItem].[_trackCreationTime], [CartItem].[_trackLastWriteTime], [CartItem].[_trackCreationUser], [CartItem].[_trackLastWriteUser] 
    FROM [CartItem]
GO

CREATE VIEW [dbo].[vCheckout]
AS
SELECT [Checkout].[Id], [Checkout].[Cart_Id], [Checkout].[Amount], [Checkout].[DateTime], [Checkout].[_rowVersion], [Checkout].[_trackCreationTime], [Checkout].[_trackLastWriteTime], [Checkout].[_trackCreationUser], [Checkout].[_trackLastWriteUser] 
    FROM [Checkout]
GO

CREATE VIEW [dbo].[vClub]
AS
SELECT [Club].[Id], [Club].[FullName], [Club].[ShortName], [Club].[AddressLine1], [Club].[AddressLine2], [Club].[Suburb], [Club].[State], [Club].[Postcode], [Club].[Email], [Club].[Website], [Club].[ReferenceNo], [Club].[Phone], [Club].[isActive], [Club].[_rowVersion], [Club].[_trackCreationTime], [Club].[_trackLastWriteTime], [Club].[_trackCreationUser], [Club].[_trackLastWriteUser] 
    FROM [Club]
GO

CREATE VIEW [dbo].[vClubAdmin]
AS
SELECT [ClubAdmin].[Id], [ClubAdmin].[Club_Id], [ClubAdmin].[User_Id], [ClubAdmin].[isActive], [ClubAdmin].[_rowVersion], [ClubAdmin].[_trackCreationTime], [ClubAdmin].[_trackLastWriteTime], [ClubAdmin].[_trackCreationUser], [ClubAdmin].[_trackLastWriteUser] 
    FROM [ClubAdmin]
GO

CREATE VIEW [dbo].[vClubComponents]
AS
SELECT [ClubComponents].[Id], [ClubComponents].[Club_Id], [ClubComponents].[Component_Id], [ClubComponents].[isActive], [ClubComponents].[_rowVersion], [ClubComponents].[_trackCreationTime], [ClubComponents].[_trackLastWriteTime], [ClubComponents].[_trackCreationUser], [ClubComponents].[_trackLastWriteUser] 
    FROM [ClubComponents]
GO

CREATE VIEW [dbo].[vClubEventSubTypes]
AS
SELECT [ClubEventSubTypes].[Id], [ClubEventSubTypes].[Club_Id], [ClubEventSubTypes].[SubType_Id], [ClubEventSubTypes].[isActive], [ClubEventSubTypes].[_rowVersion], [ClubEventSubTypes].[_trackCreationTime], [ClubEventSubTypes].[_trackLastWriteTime], [ClubEventSubTypes].[_trackCreationUser], [ClubEventSubTypes].[_trackLastWriteUser] 
    FROM [ClubEventSubTypes]
GO

CREATE VIEW [dbo].[vClubPlayerLevel]
AS
SELECT [ClubPlayerLevel].[Id], [ClubPlayerLevel].[Club_Id], [ClubPlayerLevel].[PlayerLevel_Id], [ClubPlayerLevel].[_rowVersion], [ClubPlayerLevel].[_trackCreationTime], [ClubPlayerLevel].[_trackLastWriteTime], [ClubPlayerLevel].[_trackCreationUser], [ClubPlayerLevel].[_trackLastWriteUser] 
    FROM [ClubPlayerLevel]
GO

CREATE VIEW [dbo].[vComponents]
AS
SELECT [Components].[Id], [Components].[Name], [Components].[Sequence], [Components].[isActive], [Components].[_rowVersion], [Components].[_trackCreationTime], [Components].[_trackLastWriteTime], [Components].[_trackCreationUser], [Components].[_trackLastWriteUser] 
    FROM [Components]
GO

CREATE VIEW [dbo].[vCourt]
AS
SELECT [Court].[Id], [Court].[Facility_Id], [Court].[Name], [Court].[Sequence], [Court].[isOnline], [Court].[ReferenceNo], [Court].[Surface_Id], [Court].[isActive], [Court].[Club_Id], [Court].[Colour], [Court].[Title], [Court].[Text], [Court].[Zone_Id], [Court].[_rowVersion], [Court].[_trackCreationTime], [Court].[_trackLastWriteTime], [Court].[_trackCreationUser], [Court].[_trackLastWriteUser] 
    FROM [Court]
GO

CREATE VIEW [dbo].[vCourtHireRate]
AS
SELECT [CourtHireRate].[Id], [CourtHireRate].[Rate], [CourtHireRate].[Zone_Id], [CourtHireRate].[TimePeriod_Id], [CourtHireRate].[IsWeekday], [CourtHireRate].[Club_Id], [CourtHireRate].[DayOfRate], [CourtHireRate].[MembershipType_Id], [CourtHireRate].[_rowVersion], [CourtHireRate].[_trackCreationTime], [CourtHireRate].[_trackLastWriteTime], [CourtHireRate].[_trackCreationUser], [CourtHireRate].[_trackLastWriteUser] 
    FROM [CourtHireRate]
GO

CREATE VIEW [dbo].[vCredit]
AS
SELECT [Credit].[Id], [Credit].[User_Id], [Credit].[DateTime], [Credit].[Amount], [Credit].[Type], [Credit].[Transaction], [Credit].[_rowVersion], [Credit].[_trackCreationTime], [Credit].[_trackLastWriteTime], [Credit].[_trackCreationUser], [Credit].[_trackLastWriteUser] 
    FROM [Credit]
GO

CREATE VIEW [dbo].[vDelivery]
AS
SELECT [Delivery].[Id], [Delivery].[Club_Id], [Delivery].[InvoiceNumber], [Delivery].[DateReceived], [Delivery].[DateRegistered], [Delivery].[Supplier_Id], [Delivery].[Facility_Id], [Delivery].[Subtotal], [Delivery].[GST], [Delivery].[Total], [Delivery].[User_Id], [Delivery].[_rowVersion], [Delivery].[_trackCreationTime], [Delivery].[_trackLastWriteTime], [Delivery].[_trackCreationUser], [Delivery].[_trackLastWriteUser] 
    FROM [Delivery]
GO

CREATE VIEW [dbo].[vDeliveryItem]
AS
SELECT [DeliveryItem].[Id], [DeliveryItem].[Product_Id], [DeliveryItem].[Quantity], [DeliveryItem].[UnitCostPrice], [DeliveryItem].[Discount], [DeliveryItem].[NetAmount], [DeliveryItem].[Total], [DeliveryItem].[Delivery_Id], [DeliveryItem].[_rowVersion], [DeliveryItem].[_trackCreationTime], [DeliveryItem].[_trackLastWriteTime], [DeliveryItem].[_trackCreationUser], [DeliveryItem].[_trackLastWriteUser] 
    FROM [DeliveryItem]
GO

CREATE VIEW [dbo].[vEquipmentHire]
AS
SELECT [EquipmentHire].[Id], [EquipmentHire].[DateRequested], [EquipmentHire].[Start], [EquipmentHire].[End], [EquipmentHire].[Price], [EquipmentHire].[Club_Id], [EquipmentHire].[Facility_Id], [EquipmentHire].[Status], [EquipmentHire].[Invoice_Id], [EquipmentHire].[NumberOfItems], [EquipmentHire].[User_Id], [EquipmentHire].[Comment], [EquipmentHire].[_rowVersion], [EquipmentHire].[_trackCreationTime], [EquipmentHire].[_trackLastWriteTime], [EquipmentHire].[_trackCreationUser], [EquipmentHire].[_trackLastWriteUser] 
    FROM [EquipmentHire]
GO

CREATE VIEW [dbo].[vEquipmentHireItem]
AS
SELECT [EquipmentHireItem].[Id], [EquipmentHireItem].[Quantity], [EquipmentHireItem].[EquipmentHire_Id], [EquipmentHireItem].[Price], [EquipmentHireItem].[Product_Id], [EquipmentHireItem].[Total], [EquipmentHireItem].[_rowVersion], [EquipmentHireItem].[_trackCreationTime], [EquipmentHireItem].[_trackLastWriteTime], [EquipmentHireItem].[_trackCreationUser], [EquipmentHireItem].[_trackLastWriteUser] 
    FROM [EquipmentHireItem]
GO

CREATE VIEW [dbo].[vEvent]
AS
SELECT [Event].[Id], [Event].[Name], [Event].[GenderAge], [Event].[Capacity], [Event].[hasWaitlist], [Event].[Description], [Event].[AvailabilityRule], [Event].[Club_Id], [Event].[isOnline], [Event].[LevelRule], [Event].[isInstructed], [Event].[FeeRule], [Event].[DailyFee], [Event].[FullFee], [Event].[EventType_Id], [Event].[EventSubType_Id], [Event].[isActive], [Event].[_rowVersion], [Event].[_trackCreationTime], [Event].[_trackLastWriteTime], [Event].[_trackCreationUser], [Event].[_trackLastWriteUser] 
    FROM [Event]
GO

CREATE VIEW [dbo].[vEventBooking]
AS
SELECT [EventBooking].[Id], [EventBooking].[Event_Id], [EventBooking].[Booking_Id], [EventBooking].[isActive], [EventBooking].[_rowVersion], [EventBooking].[_trackCreationTime], [EventBooking].[_trackLastWriteTime], [EventBooking].[_trackCreationUser], [EventBooking].[_trackLastWriteUser] 
    FROM [EventBooking]
GO

CREATE VIEW [dbo].[vEventFee]
AS
SELECT [EventFee].[Id], [EventFee].[Event_Id], [EventFee].[MembershipType_Id], [EventFee].[DailyFee], [EventFee].[FullFee], [EventFee].[_rowVersion], [EventFee].[_trackCreationTime], [EventFee].[_trackLastWriteTime], [EventFee].[_trackCreationUser], [EventFee].[_trackLastWriteUser] 
    FROM [EventFee]
GO

CREATE VIEW [dbo].[vEventLevel]
AS
SELECT [EventLevel].[Id], [EventLevel].[Event_Id], [EventLevel].[Level_Id], [EventLevel].[_rowVersion], [EventLevel].[_trackCreationTime], [EventLevel].[_trackLastWriteTime], [EventLevel].[_trackCreationUser], [EventLevel].[_trackLastWriteUser] 
    FROM [EventLevel]
GO

CREATE VIEW [dbo].[vEventRegistration]
AS
SELECT [EventRegistration].[Id], [EventRegistration].[Event_Id], [EventRegistration].[Booking_Id], [EventRegistration].[User_Id], [EventRegistration].[DateRegistered], [EventRegistration].[Fee], [EventRegistration].[isPaid], [EventRegistration].[isAttending], [EventRegistration].[Invoice_Id], [EventRegistration].[_rowVersion], [EventRegistration].[_trackCreationTime], [EventRegistration].[_trackLastWriteTime], [EventRegistration].[_trackCreationUser], [EventRegistration].[_trackLastWriteUser] 
    FROM [EventRegistration]
GO

CREATE VIEW [dbo].[vEventRoll]
AS
SELECT [EventRoll].[Id], [EventRoll].[User_Id], [EventRoll].[Event_Id], [EventRoll].[Booking_Id], [EventRoll].[isAttending], [EventRoll].[Comment], [EventRoll].[_rowVersion], [EventRoll].[_trackCreationTime], [EventRoll].[_trackLastWriteTime], [EventRoll].[_trackCreationUser], [EventRoll].[_trackLastWriteUser] 
    FROM [EventRoll]
GO

CREATE VIEW [dbo].[vEventSubType]
AS
SELECT [EventSubType].[Id], [EventSubType].[EventType_Id], [EventSubType].[Name], [EventSubType].[Sequence], [EventSubType].[isActive], [EventSubType].[_rowVersion], [EventSubType].[_trackCreationTime], [EventSubType].[_trackLastWriteTime], [EventSubType].[_trackCreationUser], [EventSubType].[_trackLastWriteUser] 
    FROM [EventSubType]
GO

CREATE VIEW [dbo].[vEventType]
AS
SELECT [EventType].[Id], [EventType].[Name], [EventType].[Sequence], [EventType].[_rowVersion], [EventType].[_trackCreationTime], [EventType].[_trackLastWriteTime], [EventType].[_trackCreationUser], [EventType].[_trackLastWriteUser] 
    FROM [EventType]
GO

CREATE VIEW [dbo].[vEventWaitlist]
AS
SELECT [EventWaitlist].[Id], [EventWaitlist].[Event_Id], [EventWaitlist].[Booking_Id], [EventWaitlist].[User_Id], [EventWaitlist].[isWaiting], [EventWaitlist].[Comment], [EventWaitlist].[_rowVersion], [EventWaitlist].[_trackCreationTime], [EventWaitlist].[_trackLastWriteTime], [EventWaitlist].[_trackCreationUser], [EventWaitlist].[_trackLastWriteUser] 
    FROM [EventWaitlist]
GO

CREATE VIEW [dbo].[vFacility]
AS
SELECT [Facility].[Id], [Facility].[Club_Id], [Facility].[FullName], [Facility].[ShortName], [Facility].[AddressLine1], [Facility].[AddressLine2], [Facility].[Email], [Facility].[Phone], [Facility].[Suburb], [Facility].[State], [Facility].[Postcode], [Facility].[Sequence], [Facility].[ReferenceNo], [Facility].[isOnline], [Facility].[ReserveName], [Facility].[isActive], [Facility].[_rowVersion], [Facility].[_trackCreationTime], [Facility].[_trackLastWriteTime], [Facility].[_trackCreationUser], [Facility].[_trackLastWriteUser] 
    FROM [Facility]
GO

CREATE VIEW [dbo].[vGender]
AS
SELECT [Gender].[Id], [Gender].[Name], [Gender].[Sequence], [Gender].[_rowVersion], [Gender].[_trackCreationTime], [Gender].[_trackLastWriteTime], [Gender].[_trackCreationUser], [Gender].[_trackLastWriteUser] 
    FROM [Gender]
GO

CREATE VIEW [dbo].[vGenderAge]
AS
SELECT [GenderAge].[Id], [GenderAge].[Name], [GenderAge].[Sequence], [GenderAge].[_rowVersion], [GenderAge].[_trackCreationTime], [GenderAge].[_trackLastWriteTime], [GenderAge].[_trackCreationUser], [GenderAge].[_trackLastWriteUser] 
    FROM [GenderAge]
GO

CREATE VIEW [dbo].[vInventory]
AS
SELECT [Inventory].[Id], [Inventory].[Product_Id], [Inventory].[Supplier_Id], [Inventory].[Facility_Id], [Inventory].[Quantity], [Inventory].[ReOrderLevel], [Inventory].[_rowVersion], [Inventory].[_trackCreationTime], [Inventory].[_trackLastWriteTime], [Inventory].[_trackCreationUser], [Inventory].[_trackLastWriteUser] 
    FROM [Inventory]
GO

CREATE VIEW [dbo].[vInventoryQtyAdjust]
AS
SELECT [InventoryQtyAdjust].[Id], [InventoryQtyAdjust].[Inventory_Id], [InventoryQtyAdjust].[DateTime], [InventoryQtyAdjust].[AdjustAmount], [InventoryQtyAdjust].[_rowVersion], [InventoryQtyAdjust].[_trackCreationTime], [InventoryQtyAdjust].[_trackLastWriteTime], [InventoryQtyAdjust].[_trackCreationUser], [InventoryQtyAdjust].[_trackLastWriteUser] 
    FROM [InventoryQtyAdjust]
GO

CREATE VIEW [dbo].[vInvoice]
AS
SELECT [Invoice].[Id], [Invoice].[ReferenceNo], [Invoice].[Checkout_Id], [Invoice].[_rowVersion], [Invoice].[_trackCreationTime], [Invoice].[_trackLastWriteTime], [Invoice].[_trackCreationUser], [Invoice].[_trackLastWriteUser] 
    FROM [Invoice]
GO

CREATE VIEW [dbo].[vInvoiceItem]
AS
SELECT [InvoiceItem].[Id], [InvoiceItem].[Invoice_Id], [InvoiceItem].[ReferenceNo], [InvoiceItem].[Amount], [InvoiceItem].[CartItem], [InvoiceItem].[_rowVersion], [InvoiceItem].[_trackCreationTime], [InvoiceItem].[_trackLastWriteTime], [InvoiceItem].[_trackCreationUser], [InvoiceItem].[_trackLastWriteUser] 
    FROM [InvoiceItem]
GO

CREATE VIEW [dbo].[vItemtype]
AS
SELECT [Itemtype].[Id], [Itemtype].[Name], [Itemtype].[Sequence], [Itemtype].[_rowVersion], [Itemtype].[_trackCreationTime], [Itemtype].[_trackLastWriteTime], [Itemtype].[_trackCreationUser], [Itemtype].[_trackLastWriteUser] 
    FROM [Itemtype]
GO

CREATE VIEW [dbo].[vMember]
AS
SELECT [Member].[Id], [Member].[Club_Id], [Member].[User_Id], [Member].[JoinDate], [Member].[_rowVersion], [Member].[_trackCreationTime], [Member].[_trackLastWriteTime], [Member].[_trackCreationUser], [Member].[_trackLastWriteUser] 
    FROM [Member]
GO

CREATE VIEW [dbo].[vMembership]
AS
SELECT [Membership].[Id], [Membership].[MembershipType_Id], [Membership].[Start], [Membership].[End], [Membership].[isActive], [Membership].[Member_Id], [Membership].[_rowVersion], [Membership].[_trackCreationTime], [Membership].[_trackLastWriteTime], [Membership].[_trackCreationUser], [Membership].[_trackLastWriteUser] 
    FROM [Membership]
GO

CREATE VIEW [dbo].[vMembershipType]
AS
SELECT [MembershipType].[Id], [MembershipType].[Name], [MembershipType].[Club_Id], [MembershipType].[ExpiryRule], [MembershipType].[Limit], [MembershipType].[ProRata], [MembershipType].[RenewalDate], [MembershipType].[Duration], [MembershipType].[IsActive], [MembershipType].[Fee], [MembershipType].[Description], [MembershipType].[isDefault], [MembershipType].[_rowVersion], [MembershipType].[_trackCreationTime], [MembershipType].[_trackLastWriteTime], [MembershipType].[_trackCreationUser], [MembershipType].[_trackLastWriteUser] 
    FROM [MembershipType]
GO

CREATE VIEW [dbo].[vMembershipTypeTimePeriod]
AS
SELECT [MembershipTypeTimePeriod].[Id], [MembershipTypeTimePeriod].[MembershipType_Id], [MembershipTypeTimePeriod].[TimePeriodGroup_Id], [MembershipTypeTimePeriod].[isWeekday], [MembershipTypeTimePeriod].[Club_Id], [MembershipTypeTimePeriod].[DayOfRate], [MembershipTypeTimePeriod].[_rowVersion], [MembershipTypeTimePeriod].[_trackCreationTime], [MembershipTypeTimePeriod].[_trackLastWriteTime], [MembershipTypeTimePeriod].[_trackCreationUser], [MembershipTypeTimePeriod].[_trackLastWriteUser] 
    FROM [MembershipTypeTimePeriod]
GO

CREATE VIEW [dbo].[vPaymentMethod]
AS
SELECT [PaymentMethod].[Id], [PaymentMethod].[Name], [PaymentMethod].[Sequence], [PaymentMethod].[_rowVersion], [PaymentMethod].[_trackCreationTime], [PaymentMethod].[_trackLastWriteTime], [PaymentMethod].[_trackCreationUser], [PaymentMethod].[_trackLastWriteUser] 
    FROM [PaymentMethod]
GO

CREATE VIEW [dbo].[vPlayerLevel]
AS
SELECT [PlayerLevel].[Id], [PlayerLevel].[Name], [PlayerLevel].[Sequence], [PlayerLevel].[_rowVersion], [PlayerLevel].[_trackCreationTime], [PlayerLevel].[_trackLastWriteTime], [PlayerLevel].[_trackCreationUser], [PlayerLevel].[_trackLastWriteUser] 
    FROM [PlayerLevel]
GO

CREATE VIEW [dbo].[vPlayerRating]
AS
SELECT [PlayerRating].[Id], [PlayerRating].[Name], [PlayerRating].[Sequence], [PlayerRating].[_rowVersion], [PlayerRating].[_trackCreationTime], [PlayerRating].[_trackLastWriteTime], [PlayerRating].[_trackCreationUser], [PlayerRating].[_trackLastWriteUser] 
    FROM [PlayerRating]
GO

CREATE VIEW [dbo].[vProduct]
AS
SELECT [Product].[Id], [Product].[Category_Id], [Product].[Type_Id], [Product].[Brand_Id], [Product].[Description], [Product].[Barcode], [Product].[Model], [Product].[Range], [Product].[Name], [Product].[RetailPrice], [Product].[Tax], [Product].[Club_Id], [Product].[Status_Id], [Product].[Supplier_Id], [Product].[Level_Id], [Product].[GenderAge_Id], [Product].[Year], [Product].[Colour], [Product].[Length], [Product].[HeadSize], [Product].[Weight], [Product].[isStrung], [Product].[Size_Id], [Product].[SKU], [Product].[CostPrice], [Product].[includeGST], [Product].[hasVariant], [Product].[DisplayName], [Product].[BarcodeDisplay], [Product].[hasMemberPrice], [Product].[hasVariablePrice], [Product].[MemberPrice], [Product].[_rowVersion], [Product].[_trackCreationTime], [Product].[_trackLastWriteTime], [Product].[_trackCreationUser], [Product].[_trackLastWriteUser] 
    FROM [Product]
GO

CREATE VIEW [dbo].[vProductBrand]
AS
SELECT [ProductBrand].[Id], [ProductBrand].[Name], [ProductBrand].[Sequence], [ProductBrand].[_rowVersion], [ProductBrand].[_trackCreationTime], [ProductBrand].[_trackLastWriteTime], [ProductBrand].[_trackCreationUser], [ProductBrand].[_trackLastWriteUser] 
    FROM [ProductBrand]
GO

CREATE VIEW [dbo].[vProductCategory]
AS
SELECT [ProductCategory].[Id], [ProductCategory].[Name], [ProductCategory].[Sequence], [ProductCategory].[_rowVersion], [ProductCategory].[_trackCreationTime], [ProductCategory].[_trackLastWriteTime], [ProductCategory].[_trackCreationUser], [ProductCategory].[_trackLastWriteUser] 
    FROM [ProductCategory]
GO

CREATE VIEW [dbo].[vProductPrice]
AS
SELECT [ProductPrice].[Id], [ProductPrice].[Product_Id], [ProductPrice].[MembershipType_Id], [ProductPrice].[Price], [ProductPrice].[_rowVersion], [ProductPrice].[_trackCreationTime], [ProductPrice].[_trackLastWriteTime], [ProductPrice].[_trackCreationUser], [ProductPrice].[_trackLastWriteUser] 
    FROM [ProductPrice]
GO

CREATE VIEW [dbo].[vProductStatus]
AS
SELECT [ProductStatus].[Id], [ProductStatus].[Name], [ProductStatus].[Sequence], [ProductStatus].[_rowVersion], [ProductStatus].[_trackCreationTime], [ProductStatus].[_trackLastWriteTime], [ProductStatus].[_trackCreationUser], [ProductStatus].[_trackLastWriteUser] 
    FROM [ProductStatus]
GO

CREATE VIEW [dbo].[vProductSupplier]
AS
SELECT [ProductSupplier].[Id], [ProductSupplier].[Product_Id], [ProductSupplier].[Supplier_Id], [ProductSupplier].[_rowVersion], [ProductSupplier].[_trackCreationTime], [ProductSupplier].[_trackLastWriteTime], [ProductSupplier].[_trackCreationUser], [ProductSupplier].[_trackLastWriteUser] 
    FROM [ProductSupplier]
GO

CREATE VIEW [dbo].[vProductType]
AS
SELECT [ProductType].[Id], [ProductType].[Name], [ProductType].[Sequence], [ProductType].[Category_Id], [ProductType].[_rowVersion], [ProductType].[_trackCreationTime], [ProductType].[_trackLastWriteTime], [ProductType].[_trackCreationUser], [ProductType].[_trackLastWriteUser] 
    FROM [ProductType]
GO

CREATE VIEW [dbo].[vRole]
AS
SELECT [Role].[Id], [Role].[Name], [Role].[_rowVersion], [Role].[_trackCreationTime], [Role].[_trackLastWriteTime], [Role].[_trackCreationUser], [Role].[_trackLastWriteUser] 
    FROM [Role]
GO

CREATE VIEW [dbo].[vRoleClaim]
AS
SELECT [RoleClaim].[Id], [RoleClaim].[Type], [RoleClaim].[Value], [RoleClaim].[ValueType], [RoleClaim].[Role_Id], [RoleClaim].[_rowVersion], [RoleClaim].[_trackCreationTime], [RoleClaim].[_trackLastWriteTime], [RoleClaim].[_trackCreationUser], [RoleClaim].[_trackLastWriteUser] 
    FROM [RoleClaim]
GO

CREATE VIEW [dbo].[vSize]
AS
SELECT [Size].[Id], [Size].[Value], [Size].[Sequence], [Size].[SizeType_Id], [Size].[SizeStandard_Id], [Size].[GenderAge_Id], [Size].[_rowVersion], [Size].[_trackCreationTime], [Size].[_trackLastWriteTime], [Size].[_trackCreationUser], [Size].[_trackLastWriteUser] 
    FROM [Size]
GO

CREATE VIEW [dbo].[vSizeStandard]
AS
SELECT [SizeStandard].[Id], [SizeStandard].[Name], [SizeStandard].[_rowVersion], [SizeStandard].[_trackCreationTime], [SizeStandard].[_trackLastWriteTime], [SizeStandard].[_trackCreationUser], [SizeStandard].[_trackLastWriteUser] 
    FROM [SizeStandard]
GO

CREATE VIEW [dbo].[vSizeType]
AS
SELECT [SizeType].[Id], [SizeType].[Name], [SizeType].[_rowVersion], [SizeType].[_trackCreationTime], [SizeType].[_trackLastWriteTime], [SizeType].[_trackCreationUser], [SizeType].[_trackLastWriteUser] 
    FROM [SizeType]
GO

CREATE VIEW [dbo].[vStocktake]
AS
SELECT [Stocktake].[Id], [Stocktake].[Start], [Stocktake].[End], [Stocktake].[NumberOfItems], [Stocktake].[User_Id], [Stocktake].[Facility_Id], [Stocktake].[PreviousCount], [Stocktake].[PreviousValuation], [Stocktake].[CurrentValuation], [Stocktake].[State], [Stocktake].[CountResult], [Stocktake].[CurrentCount], [Stocktake].[_rowVersion], [Stocktake].[_trackCreationTime], [Stocktake].[_trackLastWriteTime], [Stocktake].[_trackCreationUser], [Stocktake].[_trackLastWriteUser] 
    FROM [Stocktake]
GO

CREATE VIEW [dbo].[vStocktakeItem]
AS
SELECT [StocktakeItem].[Id], [StocktakeItem].[State], [StocktakeItem].[Stocktake_Id], [StocktakeItem].[Product_Id], [StocktakeItem].[Comment], [StocktakeItem].[PreviousCount], [StocktakeItem].[Result], [StocktakeItem].[CurrentCount], [StocktakeItem].[_rowVersion], [StocktakeItem].[_trackCreationTime], [StocktakeItem].[_trackLastWriteTime], [StocktakeItem].[_trackCreationUser], [StocktakeItem].[_trackLastWriteUser] 
    FROM [StocktakeItem]
GO

CREATE VIEW [dbo].[vSupplier]
AS
SELECT [Supplier].[Id], [Supplier].[Name], [Supplier].[Email], [Supplier].[Phone], [Supplier].[Website], [Supplier].[ReferenceNo], [Supplier].[Club_Id], [Supplier].[_rowVersion], [Supplier].[_trackCreationTime], [Supplier].[_trackLastWriteTime], [Supplier].[_trackCreationUser], [Supplier].[_trackLastWriteUser] 
    FROM [Supplier]
GO

CREATE VIEW [dbo].[vSupplierContact]
AS
SELECT [SupplierContact].[Id], [SupplierContact].[Supplier_Id], [SupplierContact].[Name], [SupplierContact].[Position], [SupplierContact].[Phone], [SupplierContact].[Email], [SupplierContact].[_rowVersion], [SupplierContact].[_trackCreationTime], [SupplierContact].[_trackLastWriteTime], [SupplierContact].[_trackCreationUser], [SupplierContact].[_trackLastWriteUser] 
    FROM [SupplierContact]
GO

CREATE VIEW [dbo].[vSurface]
AS
SELECT [Surface].[Id], [Surface].[Name], [Surface].[Description], [Surface].[Sequence], [Surface].[_rowVersion], [Surface].[_trackCreationTime], [Surface].[_trackLastWriteTime], [Surface].[_trackCreationUser], [Surface].[_trackLastWriteUser] 
    FROM [Surface]
GO

CREATE VIEW [dbo].[vTimePeriod]
AS
SELECT [TimePeriod].[Id], [TimePeriod].[Name], [TimePeriod].[Start], [TimePeriod].[End], [TimePeriod].[Sequence], [TimePeriod].[Club_Id], [TimePeriod].[TimePeriodGroup_Id], [TimePeriod].[StartTime], [TimePeriod].[EndTime], [TimePeriod].[_rowVersion], [TimePeriod].[_trackCreationTime], [TimePeriod].[_trackLastWriteTime], [TimePeriod].[_trackCreationUser], [TimePeriod].[_trackLastWriteUser] 
    FROM [TimePeriod]
GO

CREATE VIEW [dbo].[vTimePeriodGroup]
AS
SELECT [TimePeriodGroup].[Id], [TimePeriodGroup].[Name], [TimePeriodGroup].[isDefault], [TimePeriodGroup].[Description], [TimePeriodGroup].[Club_Id], [TimePeriodGroup].[NumberOfTimePeriods], [TimePeriodGroup].[_rowVersion], [TimePeriodGroup].[_trackCreationTime], [TimePeriodGroup].[_trackLastWriteTime], [TimePeriodGroup].[_trackCreationUser], [TimePeriodGroup].[_trackLastWriteUser] 
    FROM [TimePeriodGroup]
GO

CREATE VIEW [dbo].[vUser]
AS
SELECT [User].[Id], [User].[UserName], [User].[CreationDateUTC], [User].[Email], [User].[EmailConfirmed], [User].[PhoneNumber], [User].[PhoneNumberConfirmed], [User].[Password], [User].[LastPasswordChangeDate], [User].[AccessFailedCount], [User].[AccessFailedWindowStart], [User].[LockoutEnabled], [User].[LockoutEndDateUtc], [User].[LastProfileUpdateDate], [User].[SecurityStamp], [User].[TwoFactorEnabled], [User].[FirstName], [User].[LastName], [User].[Gender_Id], [User].[AddressLine1], [User].[AddressLine2], [User].[Suburb], [User].[State], [User].[Postcode], [User].[TennisId], [User].[DateOfBirth], [User].[Country], [User].[FullName], [User].[PlayerRating_Id], [User].[Pin], [User].[_rowVersion], [User].[_trackCreationTime], [User].[_trackLastWriteTime], [User].[_trackCreationUser], [User].[_trackLastWriteUser] 
    FROM [User]
GO

CREATE VIEW [dbo].[vUserClaim]
AS
SELECT [UserClaim].[Id], [UserClaim].[Type], [UserClaim].[Value], [UserClaim].[ValueType], [UserClaim].[Issuer], [UserClaim].[OriginalIssuer], [UserClaim].[User_Id], [UserClaim].[_rowVersion], [UserClaim].[_trackCreationTime], [UserClaim].[_trackLastWriteTime], [UserClaim].[_trackCreationUser], [UserClaim].[_trackLastWriteUser] 
    FROM [UserClaim]
GO

CREATE VIEW [dbo].[vUserLogin]
AS
SELECT [UserLogin].[Id], [UserLogin].[ProviderName], [UserLogin].[ProviderKey], [UserLogin].[ProviderDisplayName], [UserLogin].[User_Id], [UserLogin].[_rowVersion], [UserLogin].[_trackCreationTime], [UserLogin].[_trackLastWriteTime], [UserLogin].[_trackCreationUser], [UserLogin].[_trackLastWriteUser] 
    FROM [UserLogin]
GO

CREATE VIEW [dbo].[vZone]
AS
SELECT [Zone].[Id], [Zone].[Name], [Zone].[Description], [Zone].[Facility_Id], [Zone].[Sequence], [Zone].[Club_Id], [Zone].[_rowVersion], [Zone].[_trackCreationTime], [Zone].[_trackLastWriteTime], [Zone].[_trackCreationUser], [Zone].[_trackLastWriteUser] 
    FROM [Zone]
GO

