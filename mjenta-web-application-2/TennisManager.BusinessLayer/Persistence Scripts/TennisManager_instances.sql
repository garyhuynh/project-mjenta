﻿/* CodeFluent Generated Monday, 08 August 2016 12:52. TargetVersion:Sql2008, Sql2012, Sql2014, SqlAzure. Culture:en-US. UiCulture:en-US. Encoding:utf-8 (http://www.softfluent.com) */
set quoted_identifier OFF
GO
DECLARE @rv rowversion
DECLARE @su nvarchar(64)
SELECT @su=SYSTEM_USER
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[BookingSubType] WHERE [Id]=1
EXECUTE [dbo].[BookingSubType_Save] @Id=1, @BookingType_Id=1, @Name=N'Regular', @Sequence=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[BookingSubType] WHERE [Id]=2
EXECUTE [dbo].[BookingSubType_Save] @Id=2, @BookingType_Id=1, @Name=N'Permanent', @Sequence=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=1
EXECUTE [dbo].[CancellationType_Save] @Id=1, @Name=N'Rain', @Description=N'This cancellation option is selected when there are unplayable conditions due to excessive rain', @Sequence=default, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=2
EXECUTE [dbo].[CancellationType_Save] @Id=2, @Name=N'Wind', @Description=N'This cancellation option is selected when there are unplayable conditions due to excessive wind', @Sequence=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=3
EXECUTE [dbo].[CancellationType_Save] @Id=3, @Name=N'Heat', @Description=N'This cancellation option is selected when there are unplayable conditions due to excessive heat', @Sequence=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=4
EXECUTE [dbo].[CancellationType_Save] @Id=4, @Name=N'Absent Coach', @Description=N'This cancellation option is selected when a coach is absent', @Sequence=3, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=5
EXECUTE [dbo].[CancellationType_Save] @Id=5, @Name=N'Operational Failure', @Description=N'This cancellation option is selected when there are unplayable conditions due to operational failures, such as; broken lights, broken net, unsafe play surfaces', @Sequence=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[CancellationType] WHERE [Id]=6
EXECUTE [dbo].[CancellationType_Save] @Id=6, @Name=N'Other', @Description=N'This cancellation option is selected when a a booking cancellation is caused by other reasons', @Sequence=5, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Club] WHERE [Id]=1
EXECUTE [dbo].[Club_Save] @Id=1, @FullName=N'City Community Tennis', @ShortName=N'CCT', @AddressLine1=N'Chalmers Street', @AddressLine2=default, @Suburb=N'Surry Hills', @State=N'NSW', @Postcode=N'2010', @Email=N'info@citycommunitytennis.com.au', @Website=N'http://www.citycommunitytennis.com.au/', @ReferenceNo=N'CLB00001', @Phone=N'96989451', @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Club] WHERE [Id]=2
EXECUTE [dbo].[Club_Save] @Id=2, @FullName=N'Marconi Tennis Club', @ShortName=N'MTC', @AddressLine1=N'121 Prairie Vale Rd', @AddressLine2=default, @Suburb=N'Bossley Park', @State=N'NSW', @Postcode=N'2176', @Email=N'tennis@marconi.com.au', @Website=N'http://www.clubmarconi.com.au/', @ReferenceNo=N'CLB00002', @Phone=N'98223333', @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=1
EXECUTE [dbo].[ClubComponents_Save] @Id=1, @Club_Id=1, @Component_Id=1, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=2
EXECUTE [dbo].[ClubComponents_Save] @Id=2, @Club_Id=1, @Component_Id=2, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=3
EXECUTE [dbo].[ClubComponents_Save] @Id=3, @Club_Id=1, @Component_Id=3, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=4
EXECUTE [dbo].[ClubComponents_Save] @Id=4, @Club_Id=1, @Component_Id=4, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=5
EXECUTE [dbo].[ClubComponents_Save] @Id=5, @Club_Id=1, @Component_Id=5, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=6
EXECUTE [dbo].[ClubComponents_Save] @Id=6, @Club_Id=1, @Component_Id=6, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=7
EXECUTE [dbo].[ClubComponents_Save] @Id=7, @Club_Id=1, @Component_Id=7, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=8
EXECUTE [dbo].[ClubComponents_Save] @Id=8, @Club_Id=1, @Component_Id=8, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=9
EXECUTE [dbo].[ClubComponents_Save] @Id=9, @Club_Id=1, @Component_Id=9, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=10
EXECUTE [dbo].[ClubComponents_Save] @Id=10, @Club_Id=2, @Component_Id=1, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=11
EXECUTE [dbo].[ClubComponents_Save] @Id=11, @Club_Id=2, @Component_Id=2, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=12
EXECUTE [dbo].[ClubComponents_Save] @Id=12, @Club_Id=2, @Component_Id=3, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=13
EXECUTE [dbo].[ClubComponents_Save] @Id=13, @Club_Id=2, @Component_Id=4, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=14
EXECUTE [dbo].[ClubComponents_Save] @Id=14, @Club_Id=2, @Component_Id=5, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=15
EXECUTE [dbo].[ClubComponents_Save] @Id=15, @Club_Id=2, @Component_Id=7, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=16
EXECUTE [dbo].[ClubComponents_Save] @Id=16, @Club_Id=2, @Component_Id=8, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=17
EXECUTE [dbo].[ClubComponents_Save] @Id=17, @Club_Id=2, @Component_Id=9, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=18
EXECUTE [dbo].[ClubComponents_Save] @Id=18, @Club_Id=1, @Component_Id=10, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubComponents] WHERE [Id]=19
EXECUTE [dbo].[ClubComponents_Save] @Id=19, @Club_Id=2, @Component_Id=10, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=1
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=1, @Club_Id=1, @PlayerLevel_Id=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=2
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=2, @Club_Id=1, @PlayerLevel_Id=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=3
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=3, @Club_Id=1, @PlayerLevel_Id=5, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=4
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=4, @Club_Id=1, @PlayerLevel_Id=6, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=5
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=5, @Club_Id=1, @PlayerLevel_Id=7, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=6
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=6, @Club_Id=1, @PlayerLevel_Id=8, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[ClubPlayerLevel] WHERE [Id]=7
EXECUTE [dbo].[ClubPlayerLevel_Save] @Id=7, @Club_Id=1, @PlayerLevel_Id=11, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=1
EXECUTE [dbo].[Components_Save] @Id=1, @Name=N'Dashboard', @Sequence=1, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=2
EXECUTE [dbo].[Components_Save] @Id=2, @Name=N'Bookings', @Sequence=2, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=3
EXECUTE [dbo].[Components_Save] @Id=3, @Name=N'Coaching', @Sequence=3, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=4
EXECUTE [dbo].[Components_Save] @Id=4, @Name=N'Competitions', @Sequence=4, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=5
EXECUTE [dbo].[Components_Save] @Id=5, @Name=N'Socials', @Sequence=5, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=6
EXECUTE [dbo].[Components_Save] @Id=6, @Name=N'Fitness and Lifestyle', @Sequence=6, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=7
EXECUTE [dbo].[Components_Save] @Id=7, @Name=N'Members', @Sequence=7, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=8
EXECUTE [dbo].[Components_Save] @Id=8, @Name=N'Pro Shop', @Sequence=8, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=9
EXECUTE [dbo].[Components_Save] @Id=9, @Name=N'Reports', @Sequence=9, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Components] WHERE [Id]=10
EXECUTE [dbo].[Components_Save] @Id=10, @Name=N'Settings', @Sequence=10, @isActive=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Currency] WHERE [Id]=1
EXECUTE [dbo].[Currency_Save] @Id=1, @Code=N'AUD', @Name=N'Australian Dollar
', @DecimalPlaces=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=1
EXECUTE [dbo].[DaysOfWeek_Save] @Id=1, @Name=N'Sunday', @Value=default, @Sequence=default, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=2
EXECUTE [dbo].[DaysOfWeek_Save] @Id=2, @Name=N'Monday', @Value=1, @Sequence=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=3
EXECUTE [dbo].[DaysOfWeek_Save] @Id=3, @Name=N'Tuesday', @Value=2, @Sequence=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=4
EXECUTE [dbo].[DaysOfWeek_Save] @Id=4, @Name=N'Wednesday', @Value=3, @Sequence=3, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=5
EXECUTE [dbo].[DaysOfWeek_Save] @Id=5, @Name=N'Thursday', @Value=4, @Sequence=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=6
EXECUTE [dbo].[DaysOfWeek_Save] @Id=6, @Name=N'Friday', @Value=5, @Sequence=5, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[DaysOfWeek] WHERE [Id]=7
EXECUTE [dbo].[DaysOfWeek_Save] @Id=7, @Name=N'Saturday', @Value=6, @Sequence=6, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Gender] WHERE [Id]=1
EXECUTE [dbo].[Gender_Save] @Id=1, @Name=N'Male', @Sequence=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Gender] WHERE [Id]=2
EXECUTE [dbo].[Gender_Save] @Id=2, @Name=N'Female', @Sequence=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=1
EXECUTE [dbo].[GenderAge_Save] @Id=1, @Name=N'Juniors (Mixed)', @Sequence=1, @AgeGroup=0, @GenderGroup=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=2
EXECUTE [dbo].[GenderAge_Save] @Id=2, @Name=N'Juniors (Boys)', @Sequence=2, @AgeGroup=default, @GenderGroup=0, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=3
EXECUTE [dbo].[GenderAge_Save] @Id=3, @Name=N'Juniors (Girls)', @Sequence=3, @AgeGroup=default, @GenderGroup=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=4
EXECUTE [dbo].[GenderAge_Save] @Id=4, @Name=N'Teens (Mixed)', @Sequence=4, @AgeGroup=1, @GenderGroup=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=5
EXECUTE [dbo].[GenderAge_Save] @Id=5, @Name=N'Teens (Boys)', @Sequence=5, @AgeGroup=1, @GenderGroup=0, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=6
EXECUTE [dbo].[GenderAge_Save] @Id=6, @Name=N'Teens (Girls)', @Sequence=6, @AgeGroup=1, @GenderGroup=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=7
EXECUTE [dbo].[GenderAge_Save] @Id=7, @Name=N'Adults (Mixed)', @Sequence=7, @AgeGroup=2, @GenderGroup=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=8
EXECUTE [dbo].[GenderAge_Save] @Id=8, @Name=N'Adults (Mens)', @Sequence=8, @AgeGroup=2, @GenderGroup=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=9
EXECUTE [dbo].[GenderAge_Save] @Id=9, @Name=N'Adults (Womens)', @Sequence=9, @AgeGroup=2, @GenderGroup=3, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=10
EXECUTE [dbo].[GenderAge_Save] @Id=10, @Name=N'Seniors (Mixed)', @Sequence=10, @AgeGroup=3, @GenderGroup=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=11
EXECUTE [dbo].[GenderAge_Save] @Id=11, @Name=N'Seniors (Mens)', @Sequence=11, @AgeGroup=3, @GenderGroup=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=12
EXECUTE [dbo].[GenderAge_Save] @Id=12, @Name=N'Seniors (Womens)', @Sequence=12, @AgeGroup=3, @GenderGroup=3, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[GenderAge] WHERE [Id]=13
EXECUTE [dbo].[GenderAge_Save] @Id=13, @Name=N'Mixed', @Sequence=13, @AgeGroup=4, @GenderGroup=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Supplier] WHERE [Id]=1
EXECUTE [dbo].[Supplier_Save] @Id=1, @Name=N'Double Line Sports Pty Ltd', @Email=N'orders@babolat.com.au', @Phone=N'9680 0001', @Website=N'', @ReferenceNo=N'SPL00001', @Club_Id=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[SupplierContact] WHERE [Id]=1
EXECUTE [dbo].[SupplierContact_Save] @Id=1, @Supplier_Id=1, @Name=N'James', @Position=N'Sales', @Phone=N'0421325658', @Email=N'james@tennis.com', @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=1
EXECUTE [dbo].[Surface_Save] @Id=1, @Name=N'Hard Court', @Description=default, @Sequence=1, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=2
EXECUTE [dbo].[Surface_Save] @Id=2, @Name=N'Plexicushion', @Description=default, @Sequence=2, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=3
EXECUTE [dbo].[Surface_Save] @Id=3, @Name=N'European Clay', @Description=default, @Sequence=3, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=4
EXECUTE [dbo].[Surface_Save] @Id=4, @Name=N'Synthetic Clay', @Description=default, @Sequence=4, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=5
EXECUTE [dbo].[Surface_Save] @Id=5, @Name=N'Synthetic Grass', @Description=default, @Sequence=5, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=6
EXECUTE [dbo].[Surface_Save] @Id=6, @Name=N'Natural Grass', @Description=default, @Sequence=6, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=7
EXECUTE [dbo].[Surface_Save] @Id=7, @Name=N'Carpet', @Description=default, @Sequence=7, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Surface] WHERE [Id]=8
EXECUTE [dbo].[Surface_Save] @Id=8, @Name=N'Floorboard', @Description=default, @Sequence=8, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[Term] WHERE [Id]=1
EXECUTE [dbo].[Term_Save] @Id=1, @Name=N'Term 2', @StartDateUTC='20160427 00:00:00.000', @EndDateUTC='20160701 00:00:00.000', @isActive=1, @Club_Id=1, @Description=N'This term is used to administer hot shots program in 2016', @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[TimePeriodGroup] WHERE [Id]=1
EXECUTE [dbo].[TimePeriodGroup_Save] @Id=1, @Name=N'Default Group', @isDefault=1, @Description=N'This group consists of one time period', @Club_Id=1, @NumberOfTimePeriods=default, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[TimePeriodGroup] WHERE [Id]=2
EXECUTE [dbo].[TimePeriodGroup_Save] @Id=2, @Name=N'2 Time Period Group', @isDefault=default, @Description=N'This group consists of two time periods', @Club_Id=1, @NumberOfTimePeriods=default, @_trackLastWriteUser=@su, @_rowVersion=@rv
SELECT @rv=null
SELECT @rv=_rowVersion FROM [dbo].[TimePeriodGroup] WHERE [Id]=3
EXECUTE [dbo].[TimePeriodGroup_Save] @Id=3, @Name=N'3 Time Period Group', @isDefault=default, @Description=N'This group consists of three time periods', @Club_Id=1, @NumberOfTimePeriods=default, @_trackLastWriteUser=@su, @_rowVersion=@rv
