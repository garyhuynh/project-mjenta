﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TennisManager.Startup))]
namespace TennisManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
