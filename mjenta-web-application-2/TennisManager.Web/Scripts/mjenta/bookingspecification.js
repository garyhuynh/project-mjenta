﻿
function btnCasualBooking_click(model) {
    $('#ModalDialog').addClass("modal-createbooking-dialog");
    $.ajax({
        type: "POST",
        load: $("#BookingContainer").load("/Booking/RenderPartial_CasualBooking1", model)
    });

}

function btnBooking_click(model) {


    $.ajax({
        type: "POST",
        load: $("#BookingContainer").load("/Booking/RenderPartial_Booking_1", model)
    });
}

function btnClass_click() {

    //pass start time and court
    var courtId = $("#hiddenCourtId").val();
    var start = $("#hiddenStart").val();

    console.log(courtId, start);

    var model = {
        CourtId: courtId,
        longStart: start
    };

    $.ajax({
        type: "POST",
        load: $("#ClassContainer").load("/Booking/RenderPartial_Class_1", model)
    });

    $("#BookingModal").modal("hide");

    $("#ClassModal").modal("show");
}