﻿$(document).ready(function () {


    $('.booking-details').tooltip();

    $("#txtCustomerSearch").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtCustomerSearch").blur();
            customerSearch_click();
        }
    });

    $("#Discount").keyup(function (e) {
        
        if (e.keyCode == 110) {
            return;
        }

        var discountAmount = $(this).val();



        PerformDiscount(discountAmount);
        UpdateSubTotal();
        UpdateTax();
        
    });

    $("#Discount").focusout(function () {
        var input = $(this).val();
        

        if(input == '') {
            $("#Discount").data("kendoNumericTextBox").value(0);
            $("#Discount").attr('data-value', 0)

        }        
    });

    CheckDiscountOptionStatus();
    UpdateSubTotal();
    UpdateTax();
    calculateTotal();
    InitializeProductLookupPrice();
    resizePanels();

    $(".product-description").dotdotdot();


});


$(function () {
    $('#sale-summary-modal').on('hidden.bs.modal', function (e) {
        $("#sale-payment-container").empty();
        $("#spinner").show();
    });
});

$(window).resize(function () {
    resizePanels();
});

function resizePanels() {

    var windowHeight = window.innerHeight;
    var productPanelHeight = windowHeight * 0.52 + 290;
    var salePanelHeight = windowHeight * 0.52;

    $(".product-lookup-panel").css("height", productPanelHeight);
    $(".sale-item-panel").css("height", salePanelHeight);

}

function InitializeProductLookupPrice() {

    var isMember = $('#hiddenIsMember').val();

    var param = 'False';

    if((isMember == true) || (isMember == "True") || (isMember == "true")) {
        param = 'True';

        $('.product-item-add').attr('href', function () {
            return this.href.replace('False', param);
        });
    }

    if ((isMember == false) || (isMember == "false") || (isMember == "False")) {
        param = 'False';

        $('.product-item-add').attr('href', function () {
            return this.href.replace('True', param);
        });
    }

}

$(document).on('focusout', '.item-quantity', function () {

    var itemQuantity = $(this).val();
    var itemGuid = $(this).attr("data-guid");

    if(itemQuantity == '') {
        $('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value(0);
    }
    return false;
});

$(document).on('focusout', '.item-price', function () {

    var itemPrice = $(this).val();
    var itemGuid = $(this).attr("data-guid");

    if (itemPrice == '') {
        $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value(0);
    }
    return false;
});

$(document).on('focusout', '.item-adjust', function () {

    var itemDiscount = $(this).val();
    var itemGuid = $(this).attr("data-guid");

    if (itemDiscount == '') {
        $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value(0);
    }
    return false;
});

$(document).on('keyup', '.item-quantity', function () {

    var itemQuantity = $(this).val();
    var itemGuid = $(this).attr('data-guid');

    if (itemQuantity == '') {
        itemQuantity = 0;
    }
    

    $('[data-bind=item-quantity-label-' + itemGuid + ']').text(itemQuantity);
    $('[data-bind=item-quantity-label-' + itemGuid + ']').attr('data-value', itemQuantity);


    var itemPrice = $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value();
    var itemDiscount = $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value();

    CalculateItemPrice(itemGuid, itemQuantity, itemPrice);

    return false;
});

$(document).on('keyup', '.item-price', function () {

    var itemPrice = $(this).val();

    if(itemPrice == '') {
        itemPrice = 0;
    }

    var itemGuid = $(this).attr('data-guid');
    var itemQuantity = $('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value();
    var itemDiscount = $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value();

    //update item discount textbox
    var itemInitialValue = $('[data-bind=item-price-' + itemGuid + ']').attr("data-initial-value");

    console.log("retail price: " + itemInitialValue + " , edited price: " + itemPrice);

    if(parseFloat(itemInitialValue) >= parseFloat(itemPrice)) {

        var discountPrice = itemInitialValue - itemPrice;

        var discountPercentage = discountPrice / itemInitialValue * 100;
        $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value(discountPercentage);
        $('[data-bind=item-adjust-' + itemGuid + ']').attr("data-item-adjust-type", "0");
        $('[data-bind=item-adjust-label-' + itemGuid + ']').text("Discount (%)");
        var itemDiscount = $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").enable();

    }
    else {
        console.log("perform markup");

        var markupPrice = itemPrice - itemInitialValue;
        var markupPercentage = markupPrice / itemInitialValue * 100;

        var itemDiscount = $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value(markupPercentage);
        $('[data-bind=item-adjust-' + itemGuid + ']').attr("data-item-adjust-type", "1");
        $('[data-bind=item-adjust-label-' + itemGuid + ']').text("Markup (%)");

        var itemDiscount = $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").enable(false);

    }


    CalculateItemPrice(itemGuid, itemQuantity, itemPrice);

    return false;
});

$(document).on('keyup', '.item-adjust', function () {

    var itemAdjust = $(this).val();

    if (itemAdjust == '') {
        itemAdjust = 0;
    }

    var itemGuid = $(this).attr('data-guid');
    var itemQuantity = $('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value();
    var itemPrice = $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value();

    var itemInitialValue = $('[data-bind=item-price-' + itemGuid + ']').attr("data-initial-value");
    var discountPercentage = itemAdjust / 100;
    var discountAmount = itemInitialValue * discountPercentage;
    var newItemPrice = itemInitialValue - discountAmount;

    console.log(newItemPrice);

    $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value(newItemPrice);

    CalculateItemPrice(itemGuid, itemQuantity, newItemPrice);

    return false;
});

function CalculateItemPrice(itemGuid, itemQuantity, itemPrice) {

    var itemTotal = itemQuantity * itemPrice;

    $('[data-bind=item-price-label-' + itemGuid + ']').text(itemTotal.toFixed(2));
    $('[data-bind=item-price-label-' + itemGuid + ']').attr('data-value', itemTotal.toFixed(2));

    calculateTotal();
}

function calculateTotal() {

    CheckDiscountOptionStatus();

    var totalSaleItemPrice = GetSaleItemTotal();

    //perform discount
    var discount = GetDiscount(totalSaleItemPrice);


    var subTotal = totalSaleItemPrice - discount;

    $('[data-bind=sale-total-price]').attr('data-initial-value', totalSaleItemPrice.toFixed(2));

    $('[data-bind=sale-total-price]').text(subTotal.toFixed(2));
    $('[data-bind=sale-total-price]').attr('data-value', subTotal.toFixed(2));

    UpdateSubTotal();
    UpdateTax();
}

function removeSaleItem(productVariantId) {

    $('#tr' + productVariantId).remove();

    var ddlProductType = $("#ProductItems").data("kendoDropDownList");
    var data = ddlProductType.dataSource.data();

    for (var i = 0; i < data.length; i++) {
        if (data[i].Id == productVariantId) {
            data.remove(data[i]);
            calculateTotal();
            return;
        }
    }
}

function customerSearch_click() {

    var search = $("#txtCustomerSearch").val();

    $.ajax({
        type: "POST",
        load: $("#tblCustomer").load("/Sale/RenderPartial_CustomerSearch", { search: search })
    });
}

function btnAddCustomer_click(customerId) {

    $.ajax({
        url: '/Sale/GetCustomerDetails',
        type: 'GET',
        dataType: 'json',
        data: { customerId: customerId },
        cache: false,
        async: false,
        success: function (model) {

            refreshSaleItemPrices(model.IsMember);

            $("#hiddenCustomerId").val(model.Id);

            $("#hiddenIsMember").val(model.IsMember);

            $("#CustomerContainer2").hide();

            $("#CustomerContainer3").show();

            $("#btn-reset-customer").show();

            $("#CustomerName").text(model.FirstName + ' ' + model.LastName);

            $("#modalCustomerSearch").modal('hide');

            $("#txtCustomerSearch").val('');

            $("#tblCustomer").empty();


            InitializeProductLookupPrice();

        },
    });
}

function resetCustomer() {

    refreshSaleItemPrices(false);

    $("#CustomerName").text('');

    $("#CustomerContainer2").show();

    $("#CustomerContainer3").hide();

    $("#btn-reset-customer").hide();

    $("#hiddenCustomerId").val("");

    $("#hiddenIsMember").val(false);


    InitializeProductLookupPrice();

}

function refreshSaleItemPrices(state) {

    var currentIsMemberState = ($("#hiddenIsMember").val() == 'true');
    
    var newIsMemberState = state;
    


    if ((currentIsMemberState == false) && (newIsMemberState == true)){

        //update items to member prices
        $('.product-item').each(function () {
            var productVariantId = $(this).attr('data-product-id');
            var itemGuid = $(this).attr('data-guid');

            $.ajax({
                url: '/Sale/GetProductPrices',
                type: 'GET',
                dataType: 'json',
                data: { productVariantId: productVariantId },
                cache: false,
                async: false,
                success: function (model) {

                    var salePrice = 0;

                    if (model.HasMemberPrice == true) {
                        itemPrice = model.MemberPrice;

                        var itemQuantity = $('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value();

                        $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value(itemPrice);

                        $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value(0);

                        CalculateItemPrice(itemGuid, itemQuantity, itemPrice);
                    }
                },
            });
        });

        calculateTotal();
    }
    if ((currentIsMemberState == true) && (newIsMemberState == false)) {

        //update items to retail prices
        $('.product-item').each(function () {
            var productVariantId = $(this).attr('data-product-id');
            var itemGuid = $(this).attr('data-guid');

            $.ajax({
                url: '/Sale/GetProductPrices',
                type: 'GET',
                dataType: 'json',
                data: { productVariantId: productVariantId },
                cache: false,
                async: false,
                success: function (model) {
                        
                    var itemPrice = model.RetailPrice;

                    var itemQuantity = $('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value();

                    $('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value(itemPrice);

                    $('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value(0);

                    CalculateItemPrice(itemGuid, itemQuantity, itemPrice);
                },
            });
        });

        calculateTotal();
    }
    
}

function GetDiscount(totalPrice) {

    var discountType = $("#DiscountTypeId").data("kendoDropDownList").value();

    var discountAmount = $("#Discount").data("kendoNumericTextBox");

    if (discountType == 1) {
        if (parseFloat(discountAmount.value()) > parseFloat(totalPrice)) {
            discountAmount.value(0);
            $("#Discount").attr('data-value', 0);

            return 0;
        }
        else {
            $("#Discount").attr('data-value', discountAmount.value());

            return discountAmount.value();
        }
    }

    else {
        if (discountAmount.value() > 100) {
            discountAmount.value(0);
            $("#Discount").attr('data-value', 0);

            return 0;
        }
        else {

            var discount = discountAmount.value() / 100 * totalPrice;
            $("#Discount").attr('data-value', discount);


            return discount;
        }
    }
}

function GetDiscountType(value) {
    if (value == 1) {
        return "$";
    }
    else {
        return "%";
    }
}

function discounttype_changed() {
    calculateTotal();
}

function PerformDiscount(discountAmount) {

    var discountType = $("#DiscountTypeId").data("kendoDropDownList").value();

    var txtDiscountAmount = $("#Discount").data("kendoNumericTextBox");

    var totalPrice = GetSaleItemTotal();

    if (discountType == 1) {

        if (parseFloat(discountAmount) > parseFloat(totalPrice)) {

            console.log(parseFloat(discountAmount), parseFloat(totalPrice));

            txtDiscountAmount.value(0);
            calculateTotal();
            $("#Discount").attr('data-value', 0)

            $("#Discount").trigger("focus");


        }
        else {
            var subTotal = totalPrice - discountAmount;
            txtDiscountAmount.value(discountAmount);

            //store discount amount in attribute
            
            $("#Discount").attr('data-value', discountAmount)

            $('[data-bind=sale-total-price]').attr('data-initial-value', totalPrice.toFixed(2));

            $('[data-bind=sale-total-price]').text(subTotal.toFixed(2));
            $('[data-bind=sale-total-price]').attr('data-value', subTotal.toFixed(2));
        }
    }
    else {
        if (discountAmount > 100) {
            txtDiscountAmount.value(0);
            calculateTotal();

            $("#Discount").attr('data-value', 0)

            $("#Discount").trigger("focus");


        }
        else {

            var discount = discountAmount / 100 * totalPrice;
            var subTotal = totalPrice - discount;
            txtDiscountAmount.value(discountAmount);

            //store discount amount in attribute

            $("#Discount").attr('data-value', discount)


            $('[data-bind=sale-total-price]').attr('data-initial-value', totalPrice.toFixed(2));

            $('[data-bind=sale-total-price]').text(subTotal.toFixed(2));
            $('[data-bind=sale-total-price]').attr('data-value', subTotal.toFixed(2));

        }
    }

    
}

function CheckDiscountOptionStatus() {

    var count = 0;

    $('.sale-item').each(function () {   
        count = count + 1;
    });

    if (count > 0) {
        var ddlDiscountType = $("#DiscountTypeId").data("kendoDropDownList");
        ddlDiscountType.enable();
        var ddlDiscountAmount = $("#Discount").data("kendoNumericTextBox");
        ddlDiscountAmount.enable();
        $("#btnPay").removeClass("disabled");
    }
    else {

        var ddlDiscountType = $("#DiscountTypeId").data("kendoDropDownList");
        ddlDiscountType.enable(false);
        var ddlDiscountAmount = $("#Discount").data("kendoNumericTextBox");
        ddlDiscountAmount.value(0);
        ddlDiscountAmount.enable(false);
        $("#btnPay").addClass("disabled");
    }

}

function GetSaleItemTotal() {

    var total = parseFloat(0);

    $('.item-total-price-label').each(function () {
        var itemPrice = $(this).attr('data-value');
        total = total + parseFloat(itemPrice);
    });

    return total;
}

function CalculateTax() {

    var taxableSubtotal = parseFloat(0);

    $('[data-taxable=True]').each(function () {
        var itemPrice = $(this).attr('data-value');
        taxableSubtotal = taxableSubtotal + parseFloat(itemPrice);
    });

    var totalPrice = $('[data-bind=sale-total-price]').attr("data-initial-value");

    var discountAmount = GetDiscount(totalPrice);

    var taxableTotal = taxableSubtotal - discountAmount;

    var tax = taxableTotal / 11;

    return tax;
}

function UpdateTax() {
    var tax = CalculateTax();

    //if (parseFloat(tax) < 0) {
    //    tax = 0;
    //}

    $('[data-bind=sale-tax]').text(tax.toFixed(2));
    $('[data-bind=sale-tax]').attr('data-value', tax.toFixed(2));

}

function CalculateSubTotal() {
    var saleItemTotal = GetSaleItemTotal();
    var tax = CalculateTax();
    var discount = GetDiscount(saleItemTotal);
    var subTotal = saleItemTotal - tax - discount;

    return subTotal;
}

function UpdateSubTotal() {

    var subTotal = CalculateSubTotal();
    $('[data-bind=sale-sub-total]').text(subTotal.toFixed(2));
    $('[data-bind=sale-sub-total]').attr('data-value', subTotal.toFixed(2));
}

$(document).on('click', '.collapse-link-dynamic', function () {

    var ibox = $(this).closest('div.ibox');
    var button = $(this).find('i');
    var content = ibox.find('div.ibox-content');
    content.slideToggle(200);
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    ibox.toggleClass('').toggleClass('border-bottom');
    setTimeout(function () {
        ibox.resize();
        ibox.find('[id^=map-]').resize();
    }, 50);
});

$(document).on('click', '.close-link-dynamic', function () {
    var content = $(this).closest('div.ibox');
    content.remove();
    CheckDiscountOptionStatus();
    calculateTotal();
});

function AddItem_success(data) {

    var guid = data.substring(data.indexOf('#####_') + 6, data.indexOf('_#####'));


    $('#ItemContainer').animate({ scrollTop: $('#ItemContainer').prop("scrollHeight") }, 500);

    var productVariantId = $("#hiddenProductVariantId").val();

    $('[data-bind=item-quantity-' + guid + ']').kendoNumericTextBox({
        format: "#",
        value: 1,
        decimals: 0,
        min: 0,
        spinners: false
    });

    $('[data-bind=item-adjust-' + guid + ']').kendoNumericTextBox({
        decimals: 2,
        min: 0,
        spinners: false
    });

    $('[data-bind=item-price-' + guid + ']').kendoNumericTextBox({
        decimals: 2,
        min: 0,
        spinners: false
    });

    CheckDiscountOptionStatus();

    calculateTotal();

}

function AddItem_begin(productVariantId) {

    $("#hiddenProductVariantId").val(productVariantId);

    var productVariantId = $("#hiddenProductVariantId").val();

    return false;
}

function btnPay_clicked() {
    var isDisabled = $("#btnPay").hasClass("disabled");

    if (isDisabled == false) {

        var model = GetSaleSummary();

        console.log(model);

        $("#sale-summary-modal").modal("show");

        var data = JSON.stringify(model);

        $.ajax({
            type: "POST",
            load: $("#sale-payment-container").load("/Sale/RenderPartial_SaleSummary", { data: data }, function () {
                $("#spinner").hide();
            })
        });


    }

}

function GetSaleSummary() {

    var saleItems = [];

    var count = 0;

    $('.sale-item').each(function () {

        count = count + 1;

        var itemGuid = $(this).attr('data-guid');
        var orderItemTypeId = parseInt($(this).attr('data-order-type'));
        var metaId = parseInt($(this).attr('data-meta-id'));
        var itemQuantity = parseInt($('[data-bind=item-quantity-' + itemGuid + ']').data("kendoNumericTextBox").value());
        var itemPrice = parseFloat($('[data-bind=item-price-' + itemGuid + ']').data("kendoNumericTextBox").value());
        var itemAdjust = parseFloat($('[data-bind=item-adjust-' + itemGuid + ']').data("kendoNumericTextBox").value());
        var itemAdjustType = parseInt($('[data-bind=item-adjust-' + itemGuid + ']').attr("data-item-adjust-type"));
        var salePrice = parseFloat($('[data-bind=item-price-label-' + itemGuid + ']').attr("data-value"));
        var itemNote = $('#Notes-' + itemGuid).val();
        var itemName = $('[data-bind=item-name-' + itemGuid + ']').attr("data-value");
        var orderItemId = parseInt($(this).attr('data-orderitem-id'));

        if (isNaN(orderItemId)) {
            orderItemId = 0;
        }

        var saleItem = {
            ItemGuid: itemGuid,
            OrderItemTypeId: orderItemTypeId,
            Quantity: itemQuantity,
            MetaDataId: metaId,
            ItemAdjust: itemAdjust,
            ItemAdjustTypeId: itemAdjustType,
            Note: itemNote,
            ItemPrice: itemPrice,
            SalePrice: salePrice,
            ItemName: itemName,
            OrderItemId: orderItemId
        };

        saleItems.push(saleItem);
    });

    var customerId = ($("#hiddenCustomerId").val() == "") ? 0 : $("#hiddenCustomerId").val();
    var discountType = $("#DiscountTypeId").data("kendoDropDownList").value();
    var discount = $("#Discount").data("kendoNumericTextBox").value();
    var subTotal = $('[data-bind=sale-sub-total]').attr('data-value');
    var tax = $('[data-bind=sale-tax]').attr('data-value');
    var total = $('[data-bind=sale-total-price]').attr('data-value');
    var discountAmount = $("#Discount").attr('data-value');


    var saleSummary = {
        CustomerId: parseInt(customerId),
        DiscountTypeId: parseInt(discountType),
        Discount: parseFloat(discount),
        DiscountAmount: parseFloat(discountAmount),
        Subtotal: parseFloat(subTotal),
        Tax: parseFloat(tax),
        TotalAmount: parseFloat(total),
        SaleItems: saleItems,
        ItemCount: parseInt(count)
    }

    return saleSummary;
}

