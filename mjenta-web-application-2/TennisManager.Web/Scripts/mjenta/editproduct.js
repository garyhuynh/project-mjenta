﻿$(function () {

    $(".tab1 a").click(function (e) {

        var productId = $("#hiddenProductId").val();

    });

    $(".tab2 a").click(function (e) {
        console.log("tab 2 clicked");
        var productId = $("#hiddenProductId").val();
        $("#ProductDiscountContainer").empty();

        $.ajax({
            type: "GET",                  
            load: $("#ProductDiscountContainer").load("/Product/RenderPartial_ViewProductDiscount", { productId: productId })
        }); 
    });     
     
    $(".tab3 a").click(function (e) {
        console.log("tab 3 clicked");
        var productId = $("#hiddenProductId").val();
        $("#ProductImageContainer").empty();

        $.ajax({
            type: "GET",                  
            load: $("#ProductImageContainer").load("/Product/RenderPartial_ViewProductImage", { productId: productId })
        });
    });




});

$(document).ready(function () {
    if (location.hash) {
        $('a[href=' + location.hash + ']').tab('show');

        if (location.hash == "#product-info") {
            console.log("product-info show");
        }

        if (location.hash == "#product-discount") {
            var productId = $("#hiddenProductId").val();
            $("#ProductDiscountContainer").empty();


            $.ajax({
                type: "GET",
                load: $("#ProductDiscountContainer").load("/Product/RenderPartial_ViewProductDiscount", { productId: productId })
            });

        }

        if (location.hash == "#product-images") {
            var productId = $("#hiddenProductId").val();
            $("#ProductImageContainer").empty();


            $.ajax({
                type: "GET",
                load: $("#ProductImageContainer").load("/Product/RenderPartial_ViewProductImage", { productId: productId })
            });
        }
}
$(document.body).on("click", "a[data-toggle]", function (event) {
    location.hash = this.getAttribute("href");
});

});

function btnUploadImages_click() {

    console.log("upload images btn clicked");
}

function btnDeleteImages_click(productId, imageId) {
    $("#ProductId").val(productId);
    $("#ProductImageId").val(imageId);

    $("#ImageDeleteModal").modal('show');
}

function ddlInventoryOptionEdit_change(e) {
    console.log("inventory option changed");

    var inventoryOption = $("#InventoryOptionId").data("kendoDropDownList").value();
    if (inventoryOption == 0) {
        //show
        $("#InventoryContainer").removeClass("hidden");
    }
    if (inventoryOption == 1) {
        //hide
        $("#InventoryContainer").addClass("hidden");
    }

}

$(document).ready(function () {

    $("[rel=tooltip]").tooltip({ placement: 'bottom' });

    //check hidden hasvariant field
    var hiddenHasVariant = $("#hiddenHasVariant").val();

    if (hiddenHasVariant == "True") {

        var inventoryOptionId = $("#VariantInventoryOptionId").data("kendoDropDownList").value();

        if (inventoryOptionId == 1) {
            $("#InventoryContainer").addClass("hidden");

            $('.td5').hide();

        }

        var hasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList").value();

        if (hasMemberPrice == 0) {

            $('.td7').hide();
        }
    }

    if (hiddenHasVariant == "False") {

    var hiddenHasMemberPrice = $("#hiddenHasMemberPrice").val();

    console.log(hiddenHasMemberPrice);

    if ((hiddenHasMemberPrice == "False") || (hiddenHasMemberPrice == false)) {

        var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

        txtMemberPrice.enable(false);

        $("#HasMemberDisable").removeClass("btn btn-default");
        $("#HasMemberDisable").addClass("btn btn-primary");

        $("#HasMemberEnable").removeClass("btn btn-primary");
        $("#HasMemberEnable").addClass("btn btn-default");

    }
    }

    if ((hiddenHasMemberPrice == "True") || (hiddenHasMemberPrice == true)) {

        var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

        txtMemberPrice.enable();

        $("#HasMemberEnable").removeClass("btn btn-default");
        $("#HasMemberEnable").addClass("btn btn-primary");

        $("#HasMemberDisable").removeClass("btn btn-primary");
        $("#HasMemberDisable").addClass("btn btn-default");

    }


});

function getEditFormPostResult(data) {

    $('html, body').animate({ scrollTop: 0 }, 'fast');

    var hiddenHasVariant = $("#hiddenHasVariant").val();

    if (hiddenHasVariant == "True") {

        var inventoryOptionId = $("#VariantInventoryOptionId").data("kendoDropDownList").value();

        if (inventoryOptionId == 1) {
            $("#InventoryContainer").addClass("hidden");

            $('.td5').hide();

        }

    }

    if (hiddenHasVariant == "False") {

        resetTagsInput();

        var inventoryOption = $("#InventoryOptionId").data("kendoDropDownList").value();

        if (inventoryOption == 0) {
            //show
            $("#InventoryContainer").removeClass("hidden");
        }
        if (inventoryOption == 1) {
            //hide
            $("#InventoryContainer").addClass("hidden");
        }
    }

    var hasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList").value();

    if (hasMemberPrice == 0) {

        $('.td7').hide();
    }

    var hiddenHasMemberPrice = $("#hiddenHasMemberPrice").val();


    console.log("Ajax submit has member state" + hiddenHasMemberPrice);
    console.log($("#hiddenHasVariant").val());


    if ((hiddenHasMemberPrice == "False") || (hiddenHasMemberPrice == false)) {

        var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

        txtMemberPrice.enable(false);

        $("#HasMemberDisable").removeClass("btn btn-default");
        $("#HasMemberDisable").addClass("btn btn-primary");

        $("#HasMemberEnable").removeClass("btn btn-primary");
        $("#HasMemberEnable").addClass("btn btn-default");


    }
    if ($("#hiddenHasVariant").val() == "False") {
        if ((hiddenHasMemberPrice == "True") || (hiddenHasMemberPrice == true)) {

            var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

            txtMemberPrice.enable();

            $("#HasMemberEnable").removeClass("btn btn-default");
            $("#HasMemberEnable").addClass("btn btn-primary");

            $("#HasMemberDisable").removeClass("btn btn-primary");
            $("#HasMemberDisable").addClass("btn btn-default");


        }
    }

}

function ddlProductBrand_changed(e) {
    console.log("brand changed");
}

function btnDeleteVariant_click(productVariantId) {
    
    $.ajax({
        type: "GET",
        load: $("#ProductVariantModalContainer").load("/Product/RenderPartial_DeleteProductVariant", { productVariantId: productVariantId })
    });
    $("#productVariantModal").modal('show');
}

function btn_DeleteVariantConfirmed(productVariantId) {

    $.ajax({
        url: '/Product/DeleteProductVariant',
        type: 'GET',
        dataType: 'json',
        data: { productVariantId: productVariantId },
        cache: false,
        async: false,
        success: function (result) {

            $('#productVariantModal').modal('hide');
            if (result.Success == true) {
                console.log("this page update the variant table!");
                $('#tr' + productVariantId + ' > tr').html("");
                $('table#tblProductVariant tr#' + productVariantId).remove();
            }

            if (result.HasMultiple == false) {
                console.log("this page should reload!");
                location.reload(true);
            }

        },
    });

}

$(function () {
    $('#productVariantModal').on('hidden.bs.modal', function (e) {
        $("#ProductVariantModalContainer").empty();


    });
});

function VariantInventoryOption_change(e) {
    var inventoryOptionId = $("#VariantInventoryOptionId").data("kendoDropDownList").value();

    if(inventoryOptionId == 0) {
        $('.td5').show();
        $("#hiddenInventoryOptionId").val(0);
    }
    if (inventoryOptionId == 1) {
        $('.td5').hide();
        $("#hiddenInventoryOptionId").val(1);

    }




}


function btnAddProductVariant_click() {

    var productId = $("#hiddenProductId").val();

    $.ajax({
        type: "GET",
        load: $("#ProductVariantModalContainer").load("/Product/RenderPartial_AddProductVariant", { productId: productId })
    });
    $("#productVariantModal").modal('show');
}

function btnAddProductVariant_confirm() {

    var model = {
            ProductId: $("#hiddenProductId").val(),
            ProductOptionValue: $("#txtProductVariantOptionValue").val(),
            Barcode: $("#txtProductVariantBarcode").val(),
            Quantity: $("#txtProductVariantQuantity").val(),
            Price: $("#txtProductVariantPrice").val()
    }

    //model = JSON.stringify({ 'model': model });

    console.log(model);

    $.ajax({
        contentType: 'application/json; charset=utf-8',
        url: '/Product/AddProductVariant',
        type: 'GET',
        dataType: 'json',
        data: model,
        success: function (result) {

            if (result == false)
            {
                alert("This variant already exists!");
                return;
            }
            $('#specificationModal').modal('hide');
            location.reload(true);
        },
    });


}

function ddlEditVariantHasMemberPrice_change(e) {
    console.log("variant has member price changed");

    var hasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList").value();

    if (hasMemberPrice == 0) {
        $("#hiddenHasMemberPrice").val(false);
        $('.td7').hide();
    }

    if (hasMemberPrice == 1) {
        $("#hiddenHasMemberPrice").val(true);
        $('.td7').show();

    }
}