﻿$(document).on('keyup', '#txtClassSearch', function (e) {
    if (e.keyCode == 13) {
        $("#txtClassSearch").blur();
        classSearch_click();
    }

});

$(document).on('keyup', '#registration-customer-search-txt', function (e) {
    if (e.keyCode == 13) {
        $("#registration-customer-search-txt").blur();
        registerCustomerSearch_click();
    }
});

$(document).on('keyup', '#waitlist-customer-search-txt', function (e) {
    if (e.keyCode == 13) {
        $("#waitlist-customer-search-txt").blur();
        waitlistCustomerSearch_click();
    }
});


function classSearch_click() {

    var search = $("#txtClassSearch").val();

    $("#UserIconContainer").hide();

    $.ajax({
        type: "POST",
        load: $("#ClassListContainer").load("/Booking/RenderPartial_ClassList", { search: search })
    });
}

function btnSelectClass_click(classId, bookingTypeId) {
    console.log("creating class bookings for ClassID: " + classId);


    $("#hiddenEventId").val(classId);
    $("#hiddenBookingTypeId").val(bookingTypeId);



    var courtId = $("#hiddenCourtId").val();
    var start = $("#hiddenStart").val();
    var offset = $("#hiddenOffset").val();

    var model = {
        CourtId: courtId,
        longStart: start,
        Offset: offset,
        EventId: classId
    };

    $.ajax({
        type: "POST",
        load: $("#ClassContainer").load("/Booking/RenderPartial_Class_2", model)
    });
}

function classrecurrence_changed(e) {

    var dataItem = e.sender.dataItem();
    var classRecurrence = dataItem.Text;

    console.log("class recurrence text: " + dataItem.Text);
    console.log("class recurrence value: " + dataItem.Value);

    if (dataItem.Value == 2) {
        $("#RecurrenceGroup").show();
    }
    else {
        $("#RecurrenceGroup").hide();
    }

}

function classcourts_changed() {

    var courtCount = this.value().length;

    if (courtCount == 0) {
        this.value($("#hiddenCourtId").val());
    }
}

function classcourts_open(e) {
    e.preventDefault();
}

function btnClassBookNow_click() {

    $("#btnBookClass").addClass("disabled");

    CreateClass();
}

function CreateClass() {
    
    var classId = parseInt($("#hiddenEventId").val());
    var bookingTypeId = parseInt($("#hiddenBookingTypeId").val());
    var start = parseInt($("#hiddenStart").val());
    var end = parseInt($("#hiddenEnd").val());
    var strCourts = $('#SelectedCourts').data('kendoMultiSelect').value().toString();
    var classReccurrence = $("#Recurrence").data("kendoDropDownList").value();
    var occurences = $('#Occurrences').data('kendoNumericTextBox').value()
    var courts = strCourts.split(',');
    var startTime = new Date(start);
    var endTime = new Date(end);

    var startUTC = convertDateToUTC(startTime);
    var endUTC = convertDateToUTC(endTime);

    var addDays = 0;
    
    var availabilityStatus = true;

    if(classReccurrence == 1) {
        if (checkCourtAvailability(startUTC, endUTC, courts) == true) {

            availabilityStatus = false;

            setTimeout(function () {
                alert("This court is not available in this time...");
            }, 0);

            $("#btnBookClass").removeClass("disabled");
        }
    }
    else {
        if (checkRecurrenceAvailability(startUTC, endUTC, occurences, courts) == true) {

            availabilityStatus = false;

            setTimeout(function () {
                alert("This court is not available in the following occurrence...");
            }, 0);

            $("#btnBookNow").removeClass("disabled");
        }
    }

    if(availabilityStatus == true) {
        var booking = {
            BookingTypeId: bookingTypeId,
            EventId: classId,
            longStart: start,
            longEnd: end,
            strCourts: strCourts,
            Occurrences: occurences
        };

        var result = false;
        var hubStart = {};
        var bookingIds = new Array();

        $.connection.bookingHub.server.create(booking).done(function (bookings) {
            $.each(bookings, function () {
                bookingIds.push(this.Id);
            });

            console.log(bookingIds);

        });

        hubStart = $.connection.hub.start();

        var scheduler = $("#BookingScheduler").data("kendoScheduler");
        scheduler.dataSource.read();

        $("#ClassModal").modal("hide");



    }

}

function classendtime_changed(e) {
    var start = parseInt($("#hiddenStart").val());
    var startTime = new Date(start);

    var endTime = this.value();
    var endTimeUTC = Date.UTC(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), endTime.getHours(), endTime.getMinutes(), 0, 0);
    $("#hiddenEnd").val(endTimeUTC);
}

function btnRegisterCustomer_click(eventBookingId) {
    console.log("register customer for event booking: " + eventBookingId);

    $("#hiddenEventBookingId").val(eventBookingId);

    $("#modalCustomerSearch").modal("show");

}

$(document).ready(function () {

    $('#modalCustomerSearch').on('shown.bs.modal', function () {

        $("#ViewClassModal").fadeOut();

        $("#hiddenSearchOption").val("class");
    });

    $('#modalCustomerSearch').on('hidden.bs.modal', function () {

        $("#ViewClassModal").css("display", "block");

        $("#CustomerListContainer").empty();
        $("#txtRegisterCustomerSearch").val("");
        $("#hiddenSearchOption").val("");
    });

    $('#sms-contact-modal').on('shown.bs.modal', function () {

        $("#ViewClassModal").fadeOut();
    });

    $('#sms-contact-modal').on('hidden.bs.modal', function () {

        $("#ViewClassModal").css("display", "block");
    });

    $('#ViewClassModal').on('hidden.bs.modal', function () {

        location.hash = '';
    });

});
function registerCustomerSearch_click() {

    var search = $("#registration-customer-search-txt").val();
    $("#hiddenSearchOption").val("class_register");
    $("#user-icon-container").hide();
    $.ajax({
        type: "POST",
        load: $("#customer-search-results").load("/Booking/RenderPartial_CustomerList", { search: search })
    });
}

function waitlistCustomerSearch_click() {

    var search = $("#waitlist-customer-search-txt").val();
    $("#hiddenSearchOption").val("class_waitlist");
    $("#user-icon-container").hide();
    $.ajax({
        type: "POST",
        load: $("#customer-search-results").load("/Booking/RenderPartial_CustomerList", { search: search })
    });

}

function classbooking_click(eventBookingId) {
    console.log("display attendees for event booking: " + eventBookingId);

    $(".register-container").fadeOut();

    $("#hiddenEventBookingId").val(eventBookingId);

    $("#class-registration-container").empty();

    $.ajax({
        type: "POST",
        load: $("#class-registration-container").load("/Booking/RenderPartial_ClassAttendees", { eventBookingId: eventBookingId })
    });

}

function registercustomer_clicked(eventBookingId) {

    $("#event-booking-container").empty();

    var bookingId = $("#hiddenBookingId").val();
    var eventId = $("#hiddenEventId").val();

    console.log("booking id: " + bookingId);
    console.log("event id: " + eventId);

    //$.ajax({
    //    type: "POST",
    //    load: $("#event-booking-container").load("/Booking/RenderPartial_ClassRegistrationBookingsList", { eventBookingId: eventBookingId })
    //});

    $(".register-container").fadeIn();
}

function registercustomer_begin(eventBookingId)
{
    $("#hiddenEventBookingId").val(eventBookingId);
}

function smscustomer_clicked(eventBookingId) {
    console.log("send sms to registrations in event booking id: " + eventBookingId);

    $.ajax({
        type: "POST",
        load: $("#customer-sms-container").load("/Booking/RenderPartial_Contact_Sms", { eventBookingId: eventBookingId })
    });
}

function smscustomer_complete(data) {

}

function smscustomer_success(data) {

}

function smscustomer_begin(data) {

    $("#btn-send-sms").addClass("disabled");

}



$(window).on('popstate', function () {
    var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
    $('a[href=' + anchor + ']').tab('show');
});

$(document).ready(function () {

    $(document.body).on("click", "a[data-toggle]", function (event) {

        var clickedTab = this.getAttribute("href");

        location.hash = clickedTab;

        var tabs = {
            ClassRegistrations: "#ClassRegistrations",
            ClassWaitList: "#ClassWaitlist",
            ClassRoll: "#ClassRoll",
        };

        var eventBookingId = $("#hiddenEventBookingId").val();

        if (clickedTab == tabs.ClassRegistrations) {

            console.log("class regs clicked");
            console.log("event booking id: " + eventBookingId);

        }

        if (clickedTab == tabs.ClassWaitList) {

            $.ajax({
                type: "POST",
                load: $("#ClassWaitlistContainer").load("/Booking/RenderPartial_ClassWaitList", { eventBookingId: eventBookingId })
            });
        }

        if (clickedTab == tabs.ClassRoll) {

        }
    });
});

function BackToWaitlist(data) {

    console.log(data);

    var eventBookingId = $("#hiddenEventBookingId").val();

    console.log(eventBookingId);

    $("a[href='#ClassWaitlist']").tab('show');

    $.ajax({
        type: "POST",
        load: $("#ClassWaitlistContainer").load("/Booking/RenderPartial_ClassWaitList", { eventBookingId: eventBookingId })
    });
}

function classDetailsEdit_success(data) {

    console.log("save class details success");
    var eventBookingId = $("#hiddenEventBookingId").val();

    $("#class-booking-container").empty();


    $.ajax({
        type: "POST",
        load: $("#class-booking-container").load("/Booking/RenderPartial_ClassBookings", { eventBookingId: eventBookingId }, function () {
            $("li[class='list-group-item active']").ScrollTo();
            var scheduler = $("#BookingScheduler").data("kendoScheduler");
            scheduler.dataSource.read();
        })
    });
}
