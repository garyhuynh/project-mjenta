﻿
function GetFacilityShortName(courtId) {
    var result = null;

    $.ajax({
        url: 'Booking/GetFacility',
        type: 'GET',
        dataType: 'json',
        data: { courtId: courtId },
        cache: false,
        async: false,
        success: function (facility) {
            result = facility.ShortName;
        },
    });
    return result;
}


