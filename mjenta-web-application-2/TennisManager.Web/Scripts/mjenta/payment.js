﻿
    //$(document).ready(function () {

    //    var posProductData = {
    //        posProductVendor: "City Community Tennis",
    //        posProductName: "Tennis Manager",
    //        posProductVersion: "v1.0"
    //    };

    //    var iclient = new TYRO.IClient("abc123", posProductData);

    //    iclient.pairTerminal("25", "389", function (response) {
    //        console.log(response);
    //        console.log(response.message);
    //    });
    //});

function paymentType_Clicked(paymentTypeId) {

    $("#hiddenPaymentTypeId").val(paymentTypeId);

    var balance = $('[data-bind=balance-due]').attr('data-value');

    switch (paymentTypeId) {

        case 0:
            $(".btn-cash").addClass("disabled");

            var balance = getBalance();
            var tendered = getTenderedAmount();

            addPaymentLine(tendered, 0);

            var currentChange = getCurrentChange();
            setChange(currentChange, tendered, balance);

            var newBalance = getNewBalance(balance, tendered);
            setNewBalance(newBalance);

            setNewTenderedAmount(newBalance);

            var updatedBalance = getBalance();

            var isCompleted = checkBalance(updatedBalance);

            if (isCompleted) {
                finalizeSale();
            }

            console.log("Sale completed: " + isCompleted);

            $(".btn-cash").removeClass("disabled");

            break;

        case 1:
            $(".btn-card").addClass("disabled");

            break;

        case 3:
            $(".btn-credit").addClass("disabled");
            break;

        case 4:
            $(".btn-cheque").addClass("disabled");
            break;

    }

}

function getBalance() {
    var balance = parseFloat($('[data-bind=balance-due]').attr('data-value'));
    return balance;
}

function getNewBalance(balance, tendered) {
    return balance - tendered;
}

function setNewBalance(newBalance) {

    var balance = (newBalance > 0) ? newBalance : 0;

    $('[data-bind=balance-due]').attr('data-value', balance);
    $('[data-bind=balance-due]').html("$" + balance.toFixed(2));
}

function getTenderedAmount() {
    var tenderedAmount = parseFloat($("#Amount").data("kendoNumericTextBox").value());
    return tenderedAmount;
}

function addPaymentLine(tendered, paymentTypeId) {

    $.ajax({
        url: '/Sale/RenderPartial_PaymentLine',
        type: 'GET',
        data: { amount: tendered, paymentTypeId: paymentTypeId },
        cache: false,
        async: false,
        success: function (html) {
            $(".payments-box").append(html);
            $(".payments-box").removeClass("hidden");
        },
    });
}

function getCurrentChange() {
    var change = parseFloat($('[data-bind=change]').attr('data-value'));
    return change;
}

function setChange(currentChange, tendered, balance) {
    var change = tendered - balance;
    var updatedChange = (change > 0) ? change : 0;

    if(updatedChange > 0) {
        $(".change-box").removeClass("hidden");
    }
    else {
        $(".change-box").addClass("hidden");
    }

    $('[data-bind=change]').attr('data-value', updatedChange);
    $('[data-bind=change]').html("$" + updatedChange.toFixed(2));
}


function setNewTenderedAmount(newBalance) {
    $("#Amount").data("kendoNumericTextBox").value(newBalance)
}

function checkBalance(updatedBalance) {
    if (updatedBalance > 0) {
        return false;
    }
    return true;
}

function finalizeSale() {
    $("#payment-container").empty();

    $("#payment-spinner").show();

    $(".btn-back-to-sale").hide();

    var model = getSaleModel();

    var data = JSON.stringify(model);


    console.log(model);

    $.ajax({
        type: "POST",
        load: $("#payment-container").load("/Sale/RenderPartial_SaleComplete", { data: data } , function () {
            $("#payment-spinner").hide();
        })
    });
}

function getSaleModel() {

    var saleItems = [];

    $('.line-item').each(function () {
        var itemGuid = $(this).attr('data-guid');
        var orderTypeId = parseInt($(this).attr('data-order-type'));

        if (isNaN(orderItemId)) {
            orderItemId = 0;
        }

        var metaId = parseInt($(this).attr('data-meta-id'));
        var itemAdjust = parseFloat($(this).attr('data-item-adjust'));
        var itemAdjustTypeId = parseInt($(this).attr('data-item-adjust-type'));
        var quantity = parseInt($(this).attr('data-quantity'));
        var note = $(this).attr('data-note');
        var itemPrice = parseFloat($(this).attr('data-item-price'));
        var salePrice = parseFloat($(this).attr('data-sale-price'));
        var itemName = $(this).attr('data-item-name');
        var orderItemId = parseInt($(this).attr('data-orderitem-id'));


        var details = [];

        $('[data-bind=detail-' + itemGuid + ']').each(function () {

            var detail = $(this).attr('data-value');

            details.push(detail);
        });

        var saleItem = {
            ItemGuid: itemGuid,
            OrderItemId: orderItemId,
            OrderItemTypeId: orderTypeId,
            Quantity: quantity,
            MetaDataId: metaId,
            ItemAdjust: itemAdjust,
            ItemAdjustTypeId: itemAdjustTypeId,
            Note: note,
            ItemPrice: itemPrice,
            SalePrice: salePrice,
            ItemName: itemName,
            DetailList: details
        }
        saleItems.push(saleItem);
    });

    var payments = [];

    var paidAmount = 0;

    $('.payment-line').each(function () {
        var amount = parseFloat($(this).attr('data-value'));
        var sourceTypeId = parseInt($(this).attr('data-source-type'));
        var reference = $(this).attr('data-reference');

        var payment = {
            SourceTypeId: 0,
            Amount: amount,
            Reference: reference
        }
        payments.push(payment);

        paidAmount += paidAmount + amount;

    });

    var customerId = parseInt($(".customer-id").attr("data-value"));
    var subTotal = parseFloat($("[data-bind=subtotal]").attr("data-value"));
    var tax = parseFloat($("[data-bind=tax]").attr("data-value"));
    var discount = parseFloat($("[data-bind=discount-amount]").attr("data-value"));
    var total = parseFloat($("[data-bind=total-amount]").attr("data-value"));
    var balance = parseFloat($("[data-bind=balance-due]").attr("data-value"));
    var change = parseFloat($("[data-bind=change]").attr("data-value"));

    var sale = {
        CustomerId: customerId,
        Subtotal: subTotal,
        Tax: tax,
        Discount: discount,
        TotalAmount: total,
        AmountPaid: paidAmount,
        Change: change,
        Balance: balance,
        SaleItems: saleItems,
        Payments: payments
    }

    return sale;

}

function generateReceiptDom() {

}

function printReceipt() {

    var invoiceId = 1;

    $.ajax({
        type: "POST",
        load: $("#print-container").load("/Sale/RenderPartial_Invoice", { invoiceId: invoiceId }, function () {
            window.print();
        })
    });

}

function saleComplete() {
    $("#print-container").empty();

    location.reload();
}