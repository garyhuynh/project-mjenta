﻿$(document).ready(function () {

    console.log("resizing browser from document ready");



    var h = window.innerHeight;
    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    scheduler.element.height(h - 125);
    scheduler.refresh();

    console.log("document is ready!");
    console.log(scheduler);
});

$(function () {

    var scheduler = $("#BookingScheduler").data("kendoScheduler");

    scheduler.element.on("dblclick", ".k-event", function (e) {
        e.preventDefault();

        var eventUid = $(this).closest(".k-event").attr(kendo.attr("uid"));
        var event = scheduler.occurrenceByUid(eventUid);

        if (!lockedRecords[event.id]) {
            $.connection.bookingHub.server.lockRecord(event.id);

            //scheduler.view().trigger("edit", { uid: eventUid });
        }
        else {
            alert("Currently the event cannot be edited");
            return false;
        }

    });
    $("td").hover(
          function () {
              $(this).append($("<span> ***</span>"));
              console.log("hover");
          }
    );
})

function scheduler_databound(e) {
    {
        //remove date row
        $(".k-scheduler-layout tr:first .k-scheduler-table")
          .not(".k-scheduler-header-date")
          .find("tr:eq(1)")
          .hide();

        this.view().content.on("click", ".k-event-delete", preventEvent);

        console.log("scheduler databound");

    }
}

function preventEvent(e) {
    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    var eventUid = $(this).closest(".k-event").attr(kendo.attr("uid"));
    var event = scheduler.occurrenceByUid(eventUid);

    if (lockedRecords[event.id]) {
        e.stopImmediatePropagation()
        alert("Currently the event cannot be deleted");
    }
}




function scheduler_sync(e) {

    //if booking has been created, do this
   
        var scheduler = $("#BookingScheduler").data("kendoScheduler");
        scheduler.dataSource.read();

        console.log("sync");
   
}

function scheduler_navigate(e) {

    console.log("scheduler date changed.");

    var addCourtStatus = $("#hiddenAddCourt").val();

    if(addCourtStatus == "1")
    {
        $("#hiddenAddCourt").val(0);
        $("#add-court-container").empty();

        setTimeout(function () {
            var h = window.innerHeight;
            var scheduler = $("#BookingScheduler").data("kendoScheduler");
            scheduler.element.height(h - 125);
            scheduler.dataSource.read();
        }, 500);
        clearTimeout(setTimeout);
    }


}

function scheduler_change(e) {

    //$("#BookingScheduler").removeClass('k-state-selected');

    console.log("change");

}

function scheduler_edit(e) {
    e.preventDefault();

    if (e.event.isNew()) {
        

            var courtId = e.event.CourtId;
            var startTime = new Date(e.event.start);
            var endTime = new Date(e.event.start);
            endTime.setHours(startTime.getHours() + 1);

            var offSet = -(new Date().getTimezoneOffset() / 60);
            
            var startTimeUTC = Date.UTC(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), startTime.getHours(), startTime.getMinutes(), 0, 0);
            var endTimeUTC = Date.UTC(endTime.getFullYear(), endTime.getMonth(), endTime.getDate(), endTime.getHours(), endTime.getMinutes(), 0, 0);

            $("#hiddenCourtId").val(courtId);
            $("#hiddenStart").val(startTimeUTC);
            $("#hiddenEnd").val(endTimeUTC);
            $("#hiddenOffset").val(offSet);
            
            $("#BookingContainer").empty();
            
            var addCourtStatus = $("#hiddenAddCourt").val();
            if (addCourtStatus == "1")
            {
                var eventBookingId = $("#hiddenEventBookingId").val();

                var model = {
                    CourtId: courtId,
                    EventBookingId: eventBookingId,
                    longStart: startTimeUTC,
                    Offset: offSet
                };

                console.log(model);

                $.ajax({
                    type: "POST",
                    load: $("#AddCourtContainer").load("/Booking/RenderPartial_AddCourt", model, function () {
                        e.preventDefault();
                        $('#AddCourtModal').modal('show');
                    })
                });
            }
            else {

                $.ajax({
                    type: "POST",
                    load: $("#BookingContainer").load("/Booking/RenderPartial_BookingType")
                });

                e.preventDefault();
                $('#BookingModal').modal('show');
            }


    }

    if (!e.event.isNew()) {

        console.log(e);

        var bookingId = e.event.id;

        $("#hiddenBookingId").val(bookingId);

        if(e.event.BookingCategory == 0) {

            $("#ViewBookingContainer").empty();

            $('#ViewBookingModal').modal('show');

            $.ajax({
                type: "POST",
                load: $("#ViewBookingContainer").load("/Booking/RenderPartial_ViewBooking", { bookingId: bookingId })
            });
        }

        if (e.event.BookingCategory == 1) {

            $(".class-header h4").html(e.event.title);

            var eventId = e.event.EventId;

            $("#hiddenEventId").val(eventId);
            $("#hiddenCourtId").val(e.event.CourtId);

            $("#ViewClassContainer").empty();

            $.ajax({
                type: "POST",
                load: $("#ViewClassContainer").load("/Booking/RenderPartial_ViewClass", { bookingId: bookingId, eventId: eventId },
                    function () {
                        $('#ViewClassModal').modal('show');
                        $("li[class='list-group-item active']").ScrollTo();
                })
            });
        }
    }
}

function scheduler_move(e) {

    if (roomIsOccupied(e.start, e.end, e.event, e.resources)) {
        this.wrapper.find(".k-event-drag-hint").addClass("invalid-slot");
    }
    console.log("move");
}

function scheduler_moveStart(e) {
    var scheduler = this;
    var event = e.event;


    if (!lockedRecords[event.id]) {
        $.connection.bookingHub.server.lockRecord(event.id);
    }
    else {
        alert("Currently the event cannot be moved");
        e.preventDefault();
    }

    console.log("movestart");
}

function scheduler_moveEnd(e) {


    if (!checkAvailability(e.start, e.end, e.event, e.resources)) {
        console.log(e.event);
        console.log("moveend checkavail");
        e.preventDefault();
    }

    $.connection.bookingHub.server.unlockRecord(e.event.id);

    console.log("moveend");

}

function scheduler_resize(e) {
    if (roomIsOccupied(e.start, e.end, e.event, e.resources)) {
        this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
        e.preventDefault();
    }
}

function scheduler_resizeStart(e) {
    var scheduler = this;
    var event = e.event;


    if (!lockedRecords[event.id]) {
        $.connection.bookingHub.server.lockRecord(event.id);
    }
    else {
        alert("Currently the event cannot be resized");
        e.preventDefault();
    }
}

function scheduler_resizeEnd(e) {

    console.log(e.start, e.end);

    if (!checkAvailability(e.start, e.end, e.events)) {
        e.preventDefault();
    }

    $.connection.bookingHub.server.unlockRecord(e.event.id);
}

function scheduler_save(e) {

            if (!checkAvailability(e.event.start, e.event.end, e.event)) {
                e.preventDefault();
            }
       
            $.connection.bookingHub.server.unlockRecord(e.event.id);

            console.log("save");
}

function scheduler_add(e) {
    if (!checkAvailability(e.event.start, e.event.end, e.event, e.event.resources)) {
        e.preventDefault();
    }
}

function scheduler_remove(e) {
    e.preventDefault()
    console.log("remove");
}
