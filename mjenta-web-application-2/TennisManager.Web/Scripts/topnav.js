﻿$(document).ready(function () {

    $("#menu-button").click(function (e) {
        if (location.hash) {
            $(document.body).on("click", "a[data-toggle]", function (event) {
                location.hash = this.getAttribute("href");
            });
        }
       
        
        //var schedulerExists = document.getElementById("Scheduler");
        var schedulerExists = document.getElementById("BookingScheduler");
   
        if (schedulerExists != null)
        {

            setTimeout(function () {
                var scheduler = $("#BookingScheduler").data("kendoScheduler");
                scheduler.dataSource.read();
            }, 500);
            clearTimeout(setTimeout);
            
        }

     
    });
});
