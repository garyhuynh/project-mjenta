﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Globalization;
using Bookings;
using Users;
using Clubs;
using Rates;
using Members;
using TennisManager.Models;
using Classification;
using System.Security.Principal;


namespace TennisManager.Models
{
    public class SchedulerBookingService : ISchedulerEventService<BookingViewModel>
    {
          public enum DayOfRate
            {
                Weekday = 0, 
                Weekend = 1
            }

        [Authorize]
        public virtual IQueryable<BookingViewModel> GetAll()
        {
            List<Booking> bookingList = BookingCollection.LoadAll().ToList();
           

            return bookingList.Select(booking => new BookingViewModel
            {
                Id = booking.Id,
                Title = booking.Title,
                Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc),
                End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc),
                StartTimezone = booking.StartTimezone,
                EndTimezone = booking.EndTimezone,
                Description = booking.Description,
                IsAllDay = booking.IsAllDay,
                isActive = booking.isActive,
                isPaid = booking.isPaid,
                
                RecurrenceRule = booking.RecurrenceRule,
                RecurrenceException = booking.RecurrenceException,
                Recurrence = booking.Recurrence,
                Price = booking.Price,
                NumberOfPlayers = booking.NumberOfPlayers,
                UserId = booking.UserId,
                Club = booking.ClubId,
                FacilityId = booking.FacilityId,
                BookingTypeId = booking.BookingTypeId,
                CourtId = booking.CourtId,

            }).AsQueryable();
        }


        private bool ValidateModel(BookingViewModel booking, ModelStateDictionary modelState)
        {
            if (booking.Start > booking.End)
            {
                modelState.AddModelError("errors", "End date must be greater or equal to Start date.");
                return false;
            }

            return true;
        }

        public virtual void Insert(BookingViewModel booking, ModelStateDictionary modelState)
        {

            if (ValidateModel(booking, modelState))
            {
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                User user = User.LoadOneById(booking.UserId);

                if (booking.CourtIds == null)
                {
                    booking.CourtIds = new int[0];
                }
                List<BookingType> bookingTypes = BookingTypeCollection.LoadAll().ToList();
                
                BookingType bookingType = bookingTypes.First();
                //Booking entity = booking.ToEntity();


                foreach (int i in booking.CourtIds)
                {
                    Court court = Court.LoadById(i);

                   // IEnumerable<BookingType> bookingTypes = BookingTypeCollection.LoadAll().Where(m => m.Name == "Casual Booking");
                    if (booking.NumberOfPlayers == 0)
                    {
                        booking.NumberOfPlayers = 2;
                    }



                    //Create [Booking] instance
                    Booking bookingInstance = new Booking()
                    {
                        Start = booking.Start,
                        End = booking.End,
                        StartTimezone = booking.StartTimezone,
                        EndTimezone = booking.EndTimezone,
                        Description = booking.Description,
                        Title = user.FirstName,
                        NumberOfPlayers = booking.NumberOfPlayers,
                        BookingTypeId = bookingType.Id,
                        ClubId = court.ClubId,
                        FacilityId = court.FacilityId,
                        UserId = booking.UserId,
                        CourtId = i,
                        Price = CalculateBookingFee(booking.UserId, i, booking.Start, booking.End)
                    };

                    bookingInstance.Save();

                    int bookingInstanceId = bookingInstance.Id;


                }


 



                
            }
        }

        public virtual void Update(BookingViewModel booking, ModelStateDictionary modelState)
        {
            if (ValidateModel(booking, modelState))
            {
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                if (string.IsNullOrEmpty(booking.Title))
                {
                    booking.Title = "";
                }
                User user = User.LoadOneById(booking.UserId);

                Booking bookingUpdate = Booking.LoadOneById(booking.Id);
                bookingUpdate.Title = user.FirstName;
                bookingUpdate.Start = booking.Start;
                bookingUpdate.End = booking.End;
                bookingUpdate.Description = booking.Description;
                bookingUpdate.IsAllDay = booking.IsAllDay;
                bookingUpdate.UserId = booking.UserId;
                bookingUpdate.Recurrence = booking.Recurrence;
                bookingUpdate.RecurrenceRule = booking.RecurrenceRule;
                bookingUpdate.RecurrenceException = booking.RecurrenceException;
                bookingUpdate.StartTimezone = booking.StartTimezone;
                bookingUpdate.EndTimezone = booking.EndTimezone;
                bookingUpdate.FacilityId = booking.FacilityId;
                bookingUpdate.BookingTypeId = booking.BookingTypeId;
                bookingUpdate.ClubId = booking.Club;
                bookingUpdate.Price = booking.Price;
                bookingUpdate.isActive = booking.isActive;
                bookingUpdate.isPaid = booking.isPaid;
                bookingUpdate.NumberOfPlayers = booking.NumberOfPlayers;
                bookingUpdate.CourtId = booking.CourtId;
                bookingUpdate.Price = CalculateBookingFee(booking.UserId, booking.CourtId, booking.Start, booking.End);
                bookingUpdate.Save();
                

            }
        }
        public virtual void Delete(BookingViewModel booking, ModelStateDictionary modelState)
        {
 
        }
        //public IEnumerable<int> GetCourts (int bookingId)
        //{
        //    IEnumerable<BookingCourts> bookingCourts = BookingCourtsCollection.LoadAllByBooking(bookingId);

        //    IEnumerable<int> courts = bookingCourts.Select(m => m.CourtId).ToArray();


        //    return courts;
        //}

        public void Dispose()
        {
            Dispose();
        }

        public decimal CalculateBookingFee(int customerId, int courtId, DateTime start, DateTime end)
        {
   
            decimal price = 0;

            Member member = Member.LoadByActiveCustomer(customerId, true);

            // get zone
            Court court = Court.LoadById(courtId);
            int zoneId = court.ZoneId;

            //check if booking is on weekend
            int dayValue = (int)start.DayOfWeek;
            bool isWeekday = false;

            if ((dayValue == 0) || (dayValue == 6))
            {
                isWeekday = false;
            }
            else
            {
                isWeekday = true;
            }

            IEnumerable<MembershipTypeTimePeriod> test = MembershipTypeTimePeriodCollection.LoadByMembershipType(member.MembershipType).Where(m => m.isWeekday == isWeekday);

            MembershipTypeTimePeriod test1 = test.First();

            //get time periods
            IEnumerable<TimePeriod> timePeriods = TimePeriodCollection.LoadByTimePeriodGroup(test1.TimePeriodGroup);

            //determine what time period(s) the booking times fall into
            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.Now);

            TimeSpan info = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            double offset1 = info.TotalHours;

            double totalHours = 0;

            foreach (TimePeriod period in timePeriods)
            {
                
                TimeSpan bookingStartTime = start.TimeOfDay;
                TimeSpan bookingEndTime = end.TimeOfDay;
                TimeSpan periodStartTime = period.StartTime.TimeOfDay;
                TimeSpan periodEndTime = period.EndTime.TimeOfDay;

                //booking time falls into one time period
                if ((bookingStartTime >= periodStartTime) && (bookingEndTime <= periodEndTime))
                {
                    totalHours = (bookingEndTime - bookingStartTime).TotalHours;
                    decimal calculableHours = System.Convert.ToDecimal(totalHours);
                    CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id,zoneId,isWeekday);
                    decimal hourlyRate = rate.Rate;
                    price = calculableHours * hourlyRate;
                    break;
                }

                //booking falls within multiple periods
                if ((bookingStartTime < periodEndTime) && (periodStartTime < bookingEndTime))
                {
                    //if start time persists in period
                    if ((bookingStartTime >= periodStartTime) && (bookingStartTime < periodEndTime))
                    {
                        totalHours = (periodEndTime - bookingStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);      
                    }

                    //if booking time is inside period
                    if ((periodStartTime > bookingStartTime) && (periodEndTime < bookingEndTime))
                    {
                        totalHours = (periodEndTime - periodStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);   
                    }

                    //if end time is in period
                    if ((bookingEndTime > periodStartTime) && (bookingEndTime <= periodEndTime))
                    {
                        totalHours = (bookingEndTime - periodStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);   
                    }
                }
            }
            return price;
        }

        public List<CustomerDisplayiewModel> GetCustomerList(string search)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<Customer> customerList = CustomerCollection.LoadBySearch("%" + search + "%", club.Id);

            List<CustomerDisplayiewModel> customerCollection = new List<CustomerDisplayiewModel>();

            foreach (Customer c in customerList)
            {
                customerCollection.Add(new CustomerDisplayiewModel()
                {
                    Id = c.Id,
                    Name = c.User.FirstName + " " + c.User.LastName,
                    Phone = c.User.PhoneNumber,
                    Email = c.User.Email,
                });
            }
            return customerCollection;
        }
    }
}
