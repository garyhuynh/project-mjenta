﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Users;
using Members;
using Bookings;
using Events;
using Clubs;
using Orders;
using Kendo.Mvc.UI;
using TennisManager.Models;
using Newtonsoft.Json;


namespace TennisManager.Models
{

    public class BookingViewModel : ISchedulerEvent
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        public int BookingCategoryId { get; set; }

        public BookingCategory BookingCategory { get; set; }

        [Display(Name = "Booking Type")]
        public int BookingTypeId { get; set; }

        public int BookingStatusId { get; set; }

        public int BookingGroupId { get; set; }

        public Member Member { get; set; }

        public Users.User User { get; set; }

        public bool? isMultiCourt { get; set; }

        public int MultiCourt { get; set; }

        public List<User> UserList { get; set; }

        public List<Customer> CustomerList { get; set; }

        public List<CustomerDisplayiewModel> CustomerCollection { get; set; }

        public List<EventDisplayViewModel> EventCollection { get; set; }

        private DateTime start;

        [Required]
        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value.ToUniversalTime();
            }
        }


        public string StartTimezone { get; set; }

        private DateTime end;

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DateGreaterThan(OtherField = "Start")]
        public DateTime End
        {
            get
            {
                return end;
            }
            set
            {
                end = value.ToUniversalTime();
            }
        }

        public string EndTimezone { get; set; }

        public string RecurrenceRule { get; set; }

        public int Recurrence { get; set; }

        [Display(Name = "Recurrence Option")]
        public int RecurrenceOption { get; set; }

        public string RecurrenceException { get; set; }

        public int Occurrences { get; set; }

        public bool IsAllDay { get; set; }

        public string Timezone { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int NumberOfPlayers { get; set; }

        [Display(Name = "Coach")]
        public int CoachId { get; set; }

        public Coach Coach { get; set; }

        public bool isActive { get; set; }

        public bool isPaid { get; set; }

        public decimal Price { get; set; }

        public BookingType BookingType { get; set; }

        public int Club { get; set; }
        public int FacilityId { get; set; }

        public Facility Facility { get; set; }

        public int Duration { get; set; }

        public IEnumerable<Booking> Bookings { get; set; }

        public string strBookingIds { get; set; }

        public int[] CourtIdList { get; set; }

        public List<int> recentBookingIds { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public List<OrderViewModel> OrderList { get; set; }

        public int CourtId { get; set; }

        public int[] SelectedCourts { get; set; }

        public Court Court { get; set; }

        public string strDate { get; set; }

        public string strStart { get; set; }

        public string strEnd { get; set; }

        public long longStart { get; set; }

        public long longEnd { get; set; }

        public string Search { get; set; }

        public Customer Customer { get; set; }

        public int CustomerId { get; set; }

        public double Offset { get; set; }

        [Required]
        public IEnumerable<int> CourtIds { get; set; }

        public List<Court> Courts { get; set; }

        public EventViewModel Event { get; set; }

        public EventBooking EventBooking { get; set; }

        public int EventBookingId { get; set; }

        public int EventId { get; set; }

        public string strCourts { get; set; }

        public BreadCrumbViewModel BreadCrumb { get; set; }

        public Booking ToEntity()
        {
            var booking = new Booking
            {
                
                Title = Title,
                Start = Start,
                StartTimezone = StartTimezone,
                End = End,
                EndTimezone = EndTimezone,
                Description = Description,
                IsAllDay = IsAllDay,
                RecurrenceRule = RecurrenceRule,
                RecurrenceException = RecurrenceException,
                Recurrence = Recurrence,
                UserId = UserId,
                FacilityId = FacilityId,
                BookingTypeId = BookingTypeId, 
                ClubId = Club, 
                Price = Price, 
                isActive = isActive,
                isPaid = isPaid, 
                NumberOfPlayers = NumberOfPlayers,
                CourtId = CourtId
                
            };

            return booking;
        }
    }






}