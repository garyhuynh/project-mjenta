﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Users;
using Members;
using Events;
using Bookings;
using Classification;
using Clubs;
using Orders;
using Kendo.Mvc.UI;
using TennisManager.Models;
using Newtonsoft.Json;

namespace TennisManager.Models
{
    public class EventViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Title")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }

        public string SearchValue { get; set; }

        public string strCoaches { get; set; }

        public string strLevels { get; set; }

        public int Capacity { get; set; }

        public int Occurrences { get; set; }

        public int TermId { get; set; }

        public int PricingStructureId { get; set; }

        public int PricingOptionId { get; set; }

        [Display(Name = "Booking Type")]
        public int BookingTypeId { get; set; }

        public int CoachId { get; set; }

        public int CourtId { get; set; }

        public int DayId { get; set; }

        public int LevelId { get; set; }

        public int RecurrenceOptionId { get; set; }

        public int RecordCount { get; set; }

        public int PageCount { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public bool hasWaitlist { get; set; }

        public bool isActive { get; set; }

        public bool isInstructed { get; set; }

        public bool isOnline { get; set; }

        public bool isUnlimitedCapacity { get; set; }

        public Club Club { get; set; }

        public GenderAge GenderAge { get; set; }

        public BookingType BookingType { get; set; }

        public DateTime RegistrationDate { get; set; }

        public PlayerLevel PlayerLevel { get; set; }

        public CoachCollection Coaches { get; set; }

        public PlayerLevelCollection PlayerLevels { get; set; }

        public GenderAgeCollection GenderAges { get; set; }

        public DaysOfWeekCollection Days { get; set; }

        public CourtCollection Courts { get; set; }

        public List<MembershipType> MembershipTypes { get; set; }

        public IEnumerable<EventRegistration> Registrations { get; set; }

        public IEnumerable<EventWaitlist> Waitlists { get; set; }

        public int[] SelectedCoaches { get; set; }

        public int[] SelectedPlayerLevels { get; set; }

        public int[] SelectedGenderAgeGroups { get; set; }

        public int[] SelectedDays { get; set; }

        public int[] SelectedCourts { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime Date { get; set; }

        public decimal DailyCost { get; set; }

        public decimal FullCost { get; set; }

        public decimal MemberDailyCost { get; set; }

        public decimal NonMemberDailyCost { get; set; }

        public decimal MemberFullCost { get; set; }

        public decimal NonMemberFullCost { get; set; }

        public List<EventPrice> Fees { get; set; }

        public List<EventDisplayViewModel> Events { get; set; }

        public List<Booking> Bookings { get; set; }

        public List<EventBookingViewModel> EventBookings { get; set; }

        public List<OrderViewModel> OutstandingBalances { get; set; }

        public EventBooking EventBooking { get; set; }

        public EventFilters Filters { get; set; }

        public CustomerViewModel Customer { get; set; }

        public bool IsRegistration { get; set; }

        public int CancellationTypeId { get; set; }
    }

    public class EventPrice
    {
        public int Id { get; set; }

        public decimal DailyFee { get; set; }

        public decimal FullFee { get; set; }

        public MembershipType MembershipType { get; set; }

        public int MembershipTypeId { get; set; }
    }

    public class EventDisplayViewModel
    {
        public int Id { get; set; }

        public BookingType BookingType { get; set; }

        public string Title { get; set; }

        public int Capacity { get; set; }

        public int BookingCount { get; set; }

        public bool HasCapacity { get; set; }

        public bool IsActive { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public CoachCollection Coaches { get; set; }

        public GenderAgeCollection GenderAges { get; set; }

        public IEnumerable<Booking> Bookings { get; set; }

        public PlayerLevelCollection PlayerLevels { get; set; }

        public CourtCollection Courts { get; set; }

        public DaysOfWeekCollection Days { get; set; }

    }

    public class EventFilters
    {
        public int BookingTypeId { get; set; }

        public int CoachID { get; set; }

        public int DayId { get; set; }

        public int LevelId { get; set; }
    }

    public class EventRegistrationViewModel
    {
        public int Id { get; set; }

        public bool isRegisteredNextSession { get; set; }

        public DateTime NextSessionDate { get; set; }
    }

    public class EventBookingViewModel
    {
        public int Id { get; set; }

        public EventBooking EventBooking { get; set; }

        public bool IsSelected { get; set; }

        public bool IsRegistered { get; set; }

        public bool IsWaitlisted { get; set; }

        public BookingCollection Bookings { get; set; }

        public DateTime Date { get; set; }

        public int CapacityCount { get; set; }

        public int Capacity { get; set; }

        public bool IsFull { get; set; }

        public bool HasCapacity { get; set; }

        public CoachCollection Coaches { get; set; }

        public PlayerLevelCollection PlayerLevels { get; set; }

    }

    public class RegistrationResult
    {
        public int Id { get; set; }

        public int EventBookingId { get; set; }

        public int EventRegistrationId { get; set; }

        public bool Result { get; set;  }
    }

    public class WaitlistResult
    {
        public int Id { get; set; }

        public int EventBookingId { get; set; }

        public int EventWaitlistId { get; set; }

        public bool Result { get; set; }
    }

    public class EventBookingCancellationViewModel
    {
        public int Id { get; set; }

        public EventBooking EventBooking { get; set; }

        public int CancellationTypeId { get; set; }

        public int RefundType { get; set; }

        public int RefundPercentage { get; set; }

        public int IsVisible { get; set; }

        public string Description { get; set; }

        public int RegistrationCount { get; set; }
    }


}