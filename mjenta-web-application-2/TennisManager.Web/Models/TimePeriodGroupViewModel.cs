﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class TimePeriodGroupViewModel
    {
        public int Id { get; set; }

        public int Club { get; set; }

        public string Name { get; set; }

        public bool? IsDefault { get; set; }

        public string Description { get; set; }

        public int NumberOfTimePeriod { get; set; }

    }
}