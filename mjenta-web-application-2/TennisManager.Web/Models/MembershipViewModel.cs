﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Members;
using Clubs;

namespace TennisManager.Models
{
    public class MembershipViewModel
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public bool? IsActive { get; set; }

        public int ClubId { get; set; }

        public Club Club { get; set; }

        public int MembershipTypeId { get; set; }

        public MembershipType MembershipType { get; set; }

        public int UserId { get; set; }

    }
}