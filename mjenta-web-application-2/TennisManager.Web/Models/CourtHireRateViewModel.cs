﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TennisManager.Models
{
    public class CourtHireRateViewModel
    {
        public int Id { get; set; }

        public int Club { get; set; }

        public bool IsWeekend { get; set; }

        public Members.MembershipType MembershipType { get; set; }

        public int MembershipTypeId { get; set; }

        public Clubs.Zone Zone { get; set; }

        public int ZoneId { get; set; }

        public decimal Rate { get; set; }

        public int TimePeriod { get; set; }

        public TimeSpan TimePeriodStart { get; set; }
        
        public TimeSpan TimePeriodEnd { get; set; }

        public string strTimePeriodStart { get; set; }

        public string strTimePeriodEnd { get; set; }

    }
        

}