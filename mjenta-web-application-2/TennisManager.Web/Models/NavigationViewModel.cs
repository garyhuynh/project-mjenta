﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class NavigationViewModel
    {
        
    }

    public class BreadCrumbViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public List<BreadCrumbItemViewModel> Items { get; set; }
    }

    public class BreadCrumbItemViewModel
    {
        public string Name { get; set; }

        public string Reference { get; set; }

        public string ActionName { get; set; }

        public int Sequence { get; set; }

        public bool IsActive { get; set; }
    }
}