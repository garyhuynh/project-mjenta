﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Accounts;
using Invoices;
using Orders;
using Clubs;

namespace TennisManager
{
    public class TransactionViewModel
    {
        public int Id { get; set; }
        
        public DateTime CreationDateUTC { get; set; }

        public string Description { get; set; }

        public string MetaData { get; set; }

        public string Reference { get; set; }

        public decimal Balance { get; set; }

        public decimal Amount { get; set; }

        public DestinationType DestinationType { get; set; }

        public SourceType SourceType { get; set; }

        public TransactionStatus TransactionStatus { get; set; }

        public TransactionType TransactionType { get; set; }

        public Invoice Invoice { get; set; }

        public Journal Journal { get; set; }

        public Account Account { get; set; }

        public Club Club { get; set; }

        public AccountingPeriod AccountingPeriod { get; set; }

    }
}