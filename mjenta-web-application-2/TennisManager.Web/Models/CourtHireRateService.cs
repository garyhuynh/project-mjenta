﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Bookings;
using Clubs;
using Members;
using TennisManager.Models;
using Users;
using Rates;


namespace TennisManager.Models
{
    public class CourtHireRateService
    {
        public DateTime GetTimePeriodStartTime (int timePeriodId)
        {
            TimePeriod period = TimePeriod.LoadById(timePeriodId);
            DateTime startTime = period.Start;

            return startTime;
        }

        public DateTime GetTimePeriodEndTime(int timePeriodId)
        {
            TimePeriod period = TimePeriod.LoadById(timePeriodId);
            DateTime endTime = period.End;

            return endTime;
        }

    }
}