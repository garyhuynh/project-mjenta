﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class MembershipTypeTimePeriodViewModel
    {
        public int Id { get; set; }

        public bool? IsWeekday { get; set; }

        //public Members.MembershipType MembershipType { get; set; }

        public int MembershipTypeId { get; set; }

        
        //public Rates.TimePeriodGroup TimePeriodGroup { get; set; }

        public int TimePeriodGroupId { get; set; }

        public string DayOfWeek { get; set; }

        public string DayOfRate { get; set; }
    }
}