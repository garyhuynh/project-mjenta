﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TennisManager.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class MyUser : IdentityUser<int, MyLogin, MyUserRole, MyClaim>
    {
    }

    public class MyLogin : IdentityUserLogin<int> { }

    public class MyUserRole : IdentityUserRole<int> { }

    public class MyClaim : IdentityUserClaim<int> { }

    public class MyRole : IdentityRole<int, MyUserRole> { }

    public class ApplicationDbContext : IdentityDbContext<MyUser, MyRole, int, MyLogin, MyUserRole, MyClaim>
    {
        public ApplicationDbContext()
            : base("MjentaData")
        {
        }
    }
}