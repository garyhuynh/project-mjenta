﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Users;

namespace TennisManager.Models
{
    public class InvoiceViewModel
    {
        public int Id { get; set; }

        public DateTime CreationDatetImeUTC { get; set; }

        public Customer Customer { get; set; }

        public string Note { get; set; }
    }
}