﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string TennisId { get; set; }

        public int Pin { get; set; }

        public int PlayerRating { get; set; }

        public string Gender { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public int GenderId { get; set; }

        public string Suburb { get; set; }

        public string Postcode { get; set; }

        public string strPin { get; set; }

        public string Search { get; set; }
    }
}