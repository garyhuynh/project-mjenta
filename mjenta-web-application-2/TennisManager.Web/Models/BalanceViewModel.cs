﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Users;

namespace TennisManager.Models
{
    public class BalanceViewModel
    {
    }

    public class OutstandingBalanceViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public int RedirectId { get; set; }

        public List<OrderViewModel> OutstandingBalances { get; set; }

        public List<OrderItemViewModel> OrderItems { get; set; }

        public Customer Customer { get; set; }

        public int CustomerId { get; set; }

        public int RegistrationId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();

            int counter = 0;

            foreach(var item in OrderItems)
            {
                if (item.IsSelected == true)
                {
                    counter += counter + 1;
                }
            }
            if(counter < 1)
            {
                result.Add(new ValidationResult("Outstanding balance must be selected"));
            }

            return result;
        }


    }
}