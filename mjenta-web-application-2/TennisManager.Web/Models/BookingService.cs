﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Globalization;
using Bookings;
using Users;
using Clubs;
using Orders;
using Events;
using Rates;
using Members;
using TennisManager.Models;
using Classification;
using System.Security.Principal;
using TennisManager.Services;
using Credits;

namespace TennisManager.Models
{
    public class BookingService
    {

        private OrderService orderService;

        public BookingService()
        {
            this.orderService = new OrderService();
        }

        [Authorize]
        public virtual IEnumerable<BookingViewModel> Read()
        {  
            IEnumerable<Booking> bookingList = BookingCollection.LoadAll();

            return bookingList.Select(booking => new BookingViewModel
            {
                Id = booking.Id,
                Title = booking.Title,
                BookingCategory = booking.BookingType.BookingCategory,
                Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc),
                End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc),
                StartTimezone = booking.StartTimezone,
                EndTimezone = booking.EndTimezone,
                Description = booking.Description,
                IsAllDay = booking.IsAllDay,
                isActive = booking.isActive,
                isPaid = booking.isPaid,
                RecurrenceRule = booking.RecurrenceRule,
                RecurrenceException = booking.RecurrenceException,
                Recurrence = booking.Recurrence,
                Price = booking.Price,
                isMultiCourt = booking.isMultiCourt,
                MultiCourt = booking.MultiCourt,
                NumberOfPlayers = booking.NumberOfPlayers,
                UserId = booking.UserId,
                Club = booking.ClubId,
                FacilityId = booking.FacilityId,
                BookingTypeId = booking.BookingTypeId,
                BookingStatusId = booking.BookingStatusId,
                CourtId = booking.CourtId,
                CustomerId = booking.CustomerId,
                EventId = booking.EventId
            });
        }

        public List<BookingViewModel> Insert(BookingViewModel booking)
        {
            var result = new List<BookingViewModel>();

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

            BookingType bookingType = BookingType.LoadById(booking.BookingTypeId);
            booking.BookingType = bookingType;

            if ((booking.BookingType.BookingCategory == BookingCategory.Booking) && (booking.BookingType.BookingOption == BookingOption.Booking) && (booking.BookingType.BookingRule == BookingRule.Single))
            {
                result = InsertCasualBooking(booking);
            }

            if ((booking.BookingType.BookingCategory == BookingCategory.Booking) && (booking.BookingType.BookingOption == BookingOption.Booking) && (booking.BookingType.BookingRule == BookingRule.Recurring))
            {
                result = InsertRecurringBooking(booking);
            }

            if ((booking.BookingType.BookingCategory == BookingCategory.Booking) && (booking.BookingType.BookingOption == BookingOption.Lesson) && (booking.BookingType.BookingRule == BookingRule.Single))
            {
                result = InsertPrivateLesson(booking);
            }

            if (booking.BookingType.BookingCategory == BookingCategory.Class)
            {
                result = InsertClass(booking);
            }

            return result;
        }

        public List<BookingViewModel> InsertCasualBooking(BookingViewModel booking)
        {
            var result = new List<BookingViewModel>();

            Customer customer = Customer.Load(booking.CustomerId);

            if (booking.CourtIds.Count() > 1)
            {
                booking.isMultiCourt = true;
            }

            if (booking.CourtIds.Count() == 1)
            {
                booking.isMultiCourt = false;
            }


            foreach (int i in booking.CourtIds)
            {
                Court court = Court.LoadById(i);

                decimal price = CalculateBookingFee(booking.CustomerId, i, booking.Start, booking.End);

                BookingStatus status = GetBookingStatus(booking.BookingType.Id, price);

                Booking bookingInstance = new Booking()
                {
                    Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc),
                    BookingType = booking.BookingType,
                    BookingStatus = status,
                    StartTimezone = booking.StartTimezone,
                    EndTimezone = booking.EndTimezone,
                    Description = booking.Description,
                    Title = customer.User.FirstName,
                    isPaid = status.isPaid,
                    NumberOfPlayers = booking.NumberOfPlayers,
                    BookingTypeId = booking.BookingTypeId,
                    isMultiCourt = booking.isMultiCourt,
                    ClubId = court.ClubId,
                    FacilityId = court.FacilityId,
                    CustomerId = booking.CustomerId,
                    CoachId = booking.CoachId,
                    CourtId = i,
                    Price = price
                };

                bookingInstance.Save();

                int bookingInstanceId = bookingInstance.Id;

                result.Add(new BookingViewModel()
                {
                    Id = bookingInstance.Id,
                    Title = bookingInstance.Title,
                    Start = bookingInstance.Start,
                    End = bookingInstance.End,
                    StartTimezone = bookingInstance.StartTimezone,
                    EndTimezone = bookingInstance.EndTimezone,
                    Description = bookingInstance.Description,
                    IsAllDay = bookingInstance.IsAllDay,
                    isActive = bookingInstance.isActive,
                    isPaid = bookingInstance.isPaid,
                    Price = bookingInstance.Price,
                    NumberOfPlayers = bookingInstance.NumberOfPlayers,
                    CustomerId = bookingInstance.Customer.Id,
                    Club = bookingInstance.ClubId,
                    FacilityId = bookingInstance.FacilityId,
                    BookingTypeId = bookingInstance.BookingTypeId,
                    BookingStatusId = bookingInstance.BookingStatusId,
                    CourtId = bookingInstance.CourtId
                });

                if (booking.isMultiCourt == true)
                {

                    if (booking.CourtIds.FirstOrDefault() == i)
                    {
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = bookingInstanceId;
                        recentBooking.Save();
                    }

                    if (booking.CourtIds.First() != i)
                    {
                        Booking parentMultiCourt = Booking.LoadParentMultiCourt(booking.CourtIds.FirstOrDefault(), booking.Start, booking.End);
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = parentMultiCourt.Id;
                        recentBooking.Save();
                    }
                }

                //var orderStatus = new OrderStatus();

                //if (price > 0)
                //{
                //    orderStatus = OrderStatus.created;
                //}
                //else
                //{
                //    orderStatus = OrderStatus.paid;
                //}

                
                //Order order = new Order()
                //{
                //    OrderType = OrderItemType.booking,
                //    Quantity = 1,
                //    isPaid = status.isPaid,
                //    OrderStatus = orderStatus,
                //    CustomerId = booking.CustomerId,
                //    BookingId = bookingInstance.Id,
                //    CreationDateUTC = DateTime.UtcNow,
                //    Amount = price
                    
                //};
                //order.Save();
            }

            InsertOrder(result);


            return result;
        }

        public List<BookingViewModel> InsertRecurringBooking(BookingViewModel booking)
        {
            var result = new List<BookingViewModel>();

            Customer customer = Customer.Load(booking.CustomerId);

            if (booking.CourtIds.Count() > 1)
            {
                booking.isMultiCourt = true;
            }

            if (booking.CourtIds.Count() == 1)
            {
                booking.isMultiCourt = false;
            }

            int occurences = booking.Occurrences;

            int addDays = 0;

            for (int x = 0; x < occurences; x++)
            {
                if (x > 0)
                {
                    addDays = x * 7;
                }

                foreach (int i in booking.CourtIds)
                {
                    Court court = Court.LoadById(i);

                    decimal price = CalculateBookingFee(booking.CustomerId, i, booking.Start, booking.End);

                    BookingStatus status = GetBookingStatus(booking.BookingType.Id, price);

                    Booking bookingInstance = new Booking()
                    {
                        Start = DateTime.SpecifyKind(booking.Start.AddDays(addDays), DateTimeKind.Utc),
                        End = DateTime.SpecifyKind(booking.End.AddDays(addDays), DateTimeKind.Utc),
                        BookingType = booking.BookingType,
                        BookingStatus = status,
                        isPaid = status.isPaid,
                        StartTimezone = booking.StartTimezone,
                        EndTimezone = booking.EndTimezone,
                        Description = booking.Description,
                        Title = customer.User.FirstName,
                        NumberOfPlayers = booking.NumberOfPlayers,
                        BookingTypeId = booking.BookingTypeId,
                        isMultiCourt = booking.isMultiCourt,
                        ClubId = court.ClubId,
                        FacilityId = court.FacilityId,
                        CustomerId = booking.CustomerId,
                        CoachId = booking.CoachId,
                        CourtId = i,
                        Price = price,
                    };

                    bookingInstance.Save();

                    int bookingInstanceId = bookingInstance.Id;

                    result.Add(new BookingViewModel()
                    {
                        Id = bookingInstance.Id,
                        Title = bookingInstance.Title,
                        Start = bookingInstance.Start,
                        End = bookingInstance.End,
                        StartTimezone = bookingInstance.StartTimezone,
                        EndTimezone = bookingInstance.EndTimezone,
                        Description = bookingInstance.Description,
                        IsAllDay = bookingInstance.IsAllDay,
                        isActive = bookingInstance.isActive,
                        isPaid = bookingInstance.isPaid,
                        Price = bookingInstance.Price,
                        NumberOfPlayers = bookingInstance.NumberOfPlayers,
                        Customer = bookingInstance.Customer,
                        Club = bookingInstance.ClubId,
                        FacilityId = bookingInstance.FacilityId,
                        BookingTypeId = bookingInstance.BookingTypeId,
                        BookingStatusId = bookingInstance.BookingStatusId,
                        CourtId = bookingInstance.CourtId
                    });

                    if (booking.isMultiCourt == true)
                    {

                        if (booking.CourtIds.FirstOrDefault() == i)
                        {
                            Booking recentBooking = Booking.Load(bookingInstanceId);
                            recentBooking.MultiCourt = bookingInstanceId;
                            recentBooking.Save();
                        }

                        if (booking.CourtIds.First() != i)
                        {
                            Booking parentMultiCourt = Booking.LoadParentMultiCourt(booking.CourtIds.FirstOrDefault(), booking.Start, booking.End);
                            Booking recentBooking = Booking.Load(bookingInstanceId);
                            recentBooking.MultiCourt = parentMultiCourt.Id;
                            recentBooking.Save();
                        }
                    }

                }
            }

            InsertOrder(result);

            return result;
        }

        public List<BookingViewModel> InsertPrivateLesson(BookingViewModel booking)
        {
            var result = new List<BookingViewModel>();

            Customer customer = Customer.Load(booking.CustomerId);

            decimal price = 0;

            if (booking.CourtIds.Count() > 1)
            {
                booking.isMultiCourt = true;
                price = booking.Price / booking.CourtIds.Count();
            }

            if (booking.CourtIds.Count() == 1)
            {
                booking.isMultiCourt = false;
                price = booking.Price;
            }

            foreach (int i in booking.CourtIds)
            {
                Court court = Court.LoadById(i);

                BookingStatus status = GetBookingStatus(booking.BookingType.Id, booking.Price);

                Booking bookingInstance = new Booking()
                {
                    Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc),
                    BookingType = booking.BookingType,
                    BookingStatus = status,
                    StartTimezone = booking.StartTimezone,
                    isPaid = status.isPaid,
                    EndTimezone = booking.EndTimezone,
                    Description = booking.Description,
                    Title = customer.User.FirstName,
                    NumberOfPlayers = booking.NumberOfPlayers,
                    isMultiCourt = booking.isMultiCourt,
                    ClubId = court.ClubId,
                    FacilityId = court.FacilityId,
                    CustomerId = booking.CustomerId,
                    CoachId = booking.CoachId,
                    CourtId = i,
                    Price = price,
                };

                bookingInstance.Save();

                int bookingInstanceId = bookingInstance.Id;

                result.Add(new BookingViewModel()
                {
                    Id = bookingInstance.Id,
                    Title = bookingInstance.Title,
                    Start = bookingInstance.Start,
                    End = bookingInstance.End,
                    StartTimezone = bookingInstance.StartTimezone,
                    EndTimezone = bookingInstance.EndTimezone,
                    Description = bookingInstance.Description,
                    IsAllDay = bookingInstance.IsAllDay,
                    isActive = bookingInstance.isActive,
                    isPaid = bookingInstance.isPaid,
                    Price = bookingInstance.Price,
                    NumberOfPlayers = bookingInstance.NumberOfPlayers,
                    Customer = bookingInstance.Customer,
                    Club = bookingInstance.ClubId,
                    FacilityId = bookingInstance.FacilityId,
                    BookingTypeId = bookingInstance.BookingTypeId,
                    BookingStatusId = bookingInstance.BookingStatusId,
                    CourtId = bookingInstance.CourtId
                });

                if (booking.isMultiCourt == true)
                {

                    if (booking.CourtIds.FirstOrDefault() == i)
                    {
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = bookingInstanceId;
                        recentBooking.Save();
                    }

                    if (booking.CourtIds.First() != i)
                    {
                        Booking parentMultiCourt = Booking.LoadParentMultiCourt(booking.CourtIds.FirstOrDefault(), booking.Start, booking.End);
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = parentMultiCourt.Id;
                        recentBooking.Save();
                    }
                }
            }

            InsertOrder(result);

            return result;
        }

        public List<BookingViewModel> InsertClass(BookingViewModel booking)
        {
            var result = new List<BookingViewModel>();

            Event e = Event.LoadById(booking.EventId);

            if (booking.CourtIds.Count() > 1)
            {
                booking.isMultiCourt = true;
            }

            if (booking.CourtIds.Count() == 1)
            {
                booking.isMultiCourt = false;
            }

            int occurences = booking.Occurrences;

            int addDays = 0;

            for (int x = 0; x < occurences; x++)
            {
                if (x > 0)
                {
                    addDays = x * 7;
                }

                EventBooking eventBooking = new EventBooking();
                eventBooking.EventStatus = EventStatus.active;
                eventBooking.Date = DateTime.SpecifyKind(booking.Start.AddDays(addDays), DateTimeKind.Utc);
                eventBooking.EventId = e.Id;

                foreach (int i in booking.CourtIds)
                {
                    Court court = Court.LoadById(i);

                    BookingStatus status = GetBookingStatus(booking.BookingType.Id, 50);

                    Booking bookingInstance = new Booking()
                    {
                        Start = DateTime.SpecifyKind(booking.Start.AddDays(addDays), DateTimeKind.Utc),
                        End = DateTime.SpecifyKind(booking.End.AddDays(addDays), DateTimeKind.Utc),
                        BookingType = booking.BookingType,
                        BookingStatus = status,
                        StartTimezone = booking.StartTimezone,
                        EndTimezone = booking.EndTimezone,
                        Title = e.Title,
                        BookingTypeId = booking.BookingTypeId,
                        isMultiCourt = booking.isMultiCourt,
                        ClubId = court.ClubId,
                        FacilityId = court.FacilityId,
                        CourtId = i,
                        EventId = e.Id
                    };

                    bookingInstance.Save();

                    eventBooking.Booking.Add(bookingInstance);

                    int bookingInstanceId = bookingInstance.Id;

                    result.Add(new BookingViewModel()
                    {
                        Id = bookingInstance.Id,
                        Title = bookingInstance.Title,
                        Start = bookingInstance.Start,
                        End = bookingInstance.End,
                        StartTimezone = bookingInstance.StartTimezone,
                        EndTimezone = bookingInstance.EndTimezone,
                        Description = bookingInstance.Description,
                        IsAllDay = bookingInstance.IsAllDay,
                        Club = bookingInstance.ClubId,
                        FacilityId = bookingInstance.FacilityId,
                        BookingTypeId = bookingInstance.BookingTypeId,
                        CourtId = bookingInstance.CourtId,
                        EventId = bookingInstance.EventId
                    });

                    if (booking.isMultiCourt == true)
                    {

                        if (booking.CourtIds.FirstOrDefault() == i)
                        {
                            Booking recentBooking = Booking.Load(bookingInstanceId);
                            recentBooking.MultiCourt = bookingInstanceId;
                            recentBooking.Save();
                        }

                        if (booking.CourtIds.First() != i)
                        {
                            Booking parentMultiCourt = Booking.LoadParentMultiCourt(booking.CourtIds.FirstOrDefault(), booking.Start, booking.End);
                            Booking recentBooking = Booking.Load(bookingInstanceId);
                            recentBooking.MultiCourt = parentMultiCourt.Id;
                            recentBooking.Save();
                        }
                    }
                }

                foreach (Coach coach in e.Coaches)
                {
                    eventBooking.Coaches.Add(coach);
                }

                foreach (PlayerLevel level in e.Levels)
                {
                    eventBooking.Levels.Add(level);
                }

                eventBooking.Save();

            }

            return result;
        }

        public void Update(BookingViewModel booking)
        {
            decimal price = 0;

            if (booking.BookingTypeId == 3)
            {
                price = booking.Price;
            }
            
            if ((booking.BookingTypeId == 1) ||(booking.BookingTypeId == 2))
            {
                price = CalculateBookingFee(booking.CustomerId, booking.CourtId, booking.Start, booking.End);
            }

            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                User user = User.LoadOneById(booking.UserId);
                Booking bookingUpdate = Booking.LoadOneById(booking.Id);
                bookingUpdate.Start = booking.Start;
                bookingUpdate.End = booking.End;
                bookingUpdate.Description = booking.Description;
                bookingUpdate.IsAllDay = booking.IsAllDay;
                bookingUpdate.Recurrence = booking.Recurrence;
                bookingUpdate.RecurrenceRule = booking.RecurrenceRule;
                bookingUpdate.RecurrenceException = booking.RecurrenceException;
                bookingUpdate.StartTimezone = booking.StartTimezone;
                bookingUpdate.EndTimezone = booking.EndTimezone;
                bookingUpdate.FacilityId = booking.FacilityId;
                bookingUpdate.BookingTypeId = booking.BookingTypeId;
                bookingUpdate.ClubId = booking.Club;
                bookingUpdate.isActive = booking.isActive;
                bookingUpdate.isPaid = booking.isPaid;
                bookingUpdate.NumberOfPlayers = booking.NumberOfPlayers;
                bookingUpdate.CourtId = booking.CourtId;
                bookingUpdate.FacilityId = GetFacilityId(booking.CourtId);
                bookingUpdate.Price = price;
                bookingUpdate.Save();
        }

        public void Delete(Booking booking)
        {
            Booking.Delete(booking);
        }

        public List<BookingViewModel> AddCourt(BookingViewModel model)
        {
            var result = new List<BookingViewModel>();


            if (model.CourtIds.Count() > 1)
            {
                model.isMultiCourt = true;
            }
            else
            {
                model.isMultiCourt = false;
            }

            foreach (int i in model.CourtIds)
            {
                EventBooking eventBooking = EventBooking.LoadById(model.EventBookingId);

                Court court = Court.LoadById(i);

                BookingStatus status = GetBookingStatus(eventBooking.Event.BookingType.Id, 50);

                Booking bookingInstance = new Booking()
                {
                    Start = DateTime.SpecifyKind(model.Start, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(model.End, DateTimeKind.Utc),
                    BookingStatus = status,
                    StartTimezone = model.StartTimezone,
                    EndTimezone = model.EndTimezone,
                    Title = eventBooking.Event.Title,
                    BookingTypeId = eventBooking.Event.BookingType.Id,
                    isMultiCourt = model.isMultiCourt,
                    ClubId = court.ClubId,
                    FacilityId = court.FacilityId,
                    CourtId = i,
                    EventId = eventBooking.Event.Id
                };

                bookingInstance.Save();

                eventBooking.Booking.Add(bookingInstance);

                int bookingInstanceId = bookingInstance.Id;

                result.Add(new BookingViewModel()
                {
                    Id = bookingInstance.Id,
                    Title = bookingInstance.Title,
                    Start = bookingInstance.Start,
                    End = bookingInstance.End,
                    StartTimezone = bookingInstance.StartTimezone,
                    EndTimezone = bookingInstance.EndTimezone,
                    Description = bookingInstance.Description,
                    IsAllDay = bookingInstance.IsAllDay,
                    Club = bookingInstance.ClubId,
                    FacilityId = bookingInstance.FacilityId,
                    BookingTypeId = bookingInstance.BookingTypeId,
                    CourtId = bookingInstance.CourtId,
                    EventId = bookingInstance.EventId
                });

                if (model.isMultiCourt == true)
                {

                    if (model.CourtIds.FirstOrDefault() == i)
                    {
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = bookingInstanceId;
                        recentBooking.Save();
                    }

                    if (model.CourtIds.First() != i)
                    {
                        Booking parentMultiCourt = Booking.LoadParentMultiCourt(model.CourtIds.FirstOrDefault(), model.Start, model.End);
                        Booking recentBooking = Booking.Load(bookingInstanceId);
                        recentBooking.MultiCourt = parentMultiCourt.Id;
                        recentBooking.Save();
                    }
                }
                eventBooking.Save();
            }

            return result;
        }

        public decimal CalculateBookingFee(int customerId, int courtId, DateTime start, DateTime end)
        {
            decimal price = 0;

 

            Member member = Member.LoadByActiveCustomer(customerId, true);


            // get zone
            Court court = Court.LoadById(courtId);
            int zoneId = court.ZoneId;

            //check if booking is on weekend
            int dayValue = (int)start.DayOfWeek;
            bool isWeekday = false;

            if ((dayValue == 0) || (dayValue == 6))
            {
                isWeekday = false;
            }
            else
            {
                isWeekday = true;
            }

            IEnumerable<MembershipTypeTimePeriod> test = MembershipTypeTimePeriodCollection.LoadByMembershipType(member.MembershipType).Where(m => m.isWeekday == isWeekday);

            MembershipTypeTimePeriod test1 = test.First();

            //get time periods
            IEnumerable<TimePeriod> timePeriods = TimePeriodCollection.LoadByTimePeriodGroup(test1.TimePeriodGroup);

            //determine what time period(s) the booking times fall into
            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.Now);

            TimeSpan info = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            double offset1 = info.TotalHours;

            double totalHours = 0;

            foreach (TimePeriod period in timePeriods)
            {

                TimeSpan bookingStartTime = start.TimeOfDay;
                TimeSpan bookingEndTime = end.TimeOfDay;
                TimeSpan periodStartTime = period.StartTime.TimeOfDay;
                TimeSpan periodEndTime = period.EndTime.TimeOfDay;

                //booking time falls into one time period
                if ((bookingStartTime >= periodStartTime) && (bookingEndTime <= periodEndTime))
                {
                    totalHours = (bookingEndTime - bookingStartTime).TotalHours;
                    decimal calculableHours = System.Convert.ToDecimal(totalHours);
                    CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                    decimal hourlyRate = rate.Rate;
                    price = calculableHours * hourlyRate;
                    break;
                }

                //booking falls within multiple periods
                if ((bookingStartTime < periodEndTime) && (periodStartTime < bookingEndTime))
                {
                    //if start time persists in period
                    if ((bookingStartTime >= periodStartTime) && (bookingStartTime < periodEndTime))
                    {
                        totalHours = (periodEndTime - bookingStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);
                    }

                    //if booking time is inside period
                    if ((periodStartTime > bookingStartTime) && (periodEndTime < bookingEndTime))
                    {
                        totalHours = (periodEndTime - periodStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);
                    }

                    //if end time is in period
                    if ((bookingEndTime > periodStartTime) && (bookingEndTime <= periodEndTime))
                    {
                        totalHours = (bookingEndTime - periodStartTime).TotalHours;
                        decimal calculableHours = System.Convert.ToDecimal(totalHours);
                        CourtHireRate rate = CourtHireRate.LoadByMembershipTypeTimePeriodIsWeekdayZone(member.MembershipTypeId, period.Id, zoneId, isWeekday);
                        decimal hourlyRate = rate.Rate;
                        price += (calculableHours * hourlyRate);
                    }
                }
            }
            return price;
        }

        public int GetFacilityId(int courtId)
        {
            Court court = Court.Load(courtId);

            return court.FacilityId;
        }

        [Authorize]
        public decimal CalculateRecurringBookingPrice(BookingViewModel model)
        {
            List<int> courtIds = new List<int>(model.strCourts.Split(',').Select(int.Parse));

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longEnd);

            decimal singleBookingPrice = 0;

            foreach (int courtId in courtIds)
            {
                singleBookingPrice += CalculateBookingFee(model.CustomerId, courtId, startTime, endTime);
            }

            decimal totalPrice = singleBookingPrice * model.Occurrences;

            return totalPrice;
        }

        public List<CustomerDisplayiewModel> GetCustomerList(string search)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<Customer> customerList = CustomerCollection.LoadBySearch("%" + search + "%", club.Id);

            List<CustomerDisplayiewModel> customerCollection = new List<CustomerDisplayiewModel>();

            foreach (Customer c in customerList)
            {
                customerCollection.Add(new CustomerDisplayiewModel()
                {
                    Id = c.Id,
                    Name = c.User.FirstName + " " + c.User.LastName,
                    Phone = c.User.PhoneNumber,
                    Email = c.User.Email,
                });
            }
            return customerCollection;
        }

        public List<EventDisplayViewModel> GetClassList(string search)
        {
            var result = new List<EventDisplayViewModel>();

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            int[] classTypeIds = GetClassTypeIds(club);

            foreach (Event e in EventCollection.LoadByTitle("%" + search + "%", classTypeIds))
            {
                result.Add(new EventDisplayViewModel()
                {
                    Id = e.Id,
                    BookingType = e.BookingType,
                    Title = e.Title,
                    Capacity = e.Capacity,
                    Coaches = e.Coaches,
                    GenderAges = e.GenderAges,
                    PlayerLevels = e.Levels,
                    Courts = e.Courts,
                    HasCapacity = e.hasCapacity,
                    IsActive = e.isActive,
                    StartTime = e.StartTime,
                    EndTime = e.EndTime,
                    Days = e.Days
                });
            }

            return result;
        }

        public BookingViewModel GetBookingDetails(int bookingId)
        {
            Booking booking = Booking.LoadById(bookingId);

            BookingViewModel model = new BookingViewModel()
            {
                Id = booking.Id,
                Court = booking.Court,
                Customer = booking.Customer,
                BookingType = booking.BookingType,
                Coach = booking.Coach,
                strDate = booking.Start.ToShortDateString(),
                strStart = booking.Start.ToShortTimeString(),
                strEnd = booking.End.ToShortTimeString(),
                NumberOfPlayers = booking.NumberOfPlayers,
                Price = booking.Price,
                isPaid = booking.isPaid  
            };

            if (booking.Customer.isMember == true)
            {
                Member member = Member.LoadByActiveCustomer(booking.Customer.Id, true);
                model.Member = member;
            }

            if (booking.isMultiCourt == true)
            {
                model.Bookings = BookingCollection.LoadByMultiCourt(booking.MultiCourt).OrderBy(s => s.Court.Sequence);
            }
            else
            {
                model.Court = booking.Court;
            }

            return model;
        }

        public BookingViewModel GetClassBookingDetails(int bookingId, int eventId)
        {

            Event e = Event.LoadById(eventId);

            EventViewModel eventModel = new EventViewModel()
            {
                Id = e.Id, 
                BookingType = e.BookingType,
                Title = e.Title,
                hasWaitlist = e.hasWaitlist,
                isOnline = e.isOnline,
                isUnlimitedCapacity = !e.hasCapacity,
                Capacity = e.Capacity,
                strCoaches = GetStrCoaches(e.Coaches),
                strLevels = GetStrLevels(e.Levels)
            };

            Booking booking = Booking.LoadById(bookingId);

            EventBooking eventBooking = EventBooking.LoadByBookingId(bookingId);

            BookingViewModel model = new BookingViewModel()
            {
                Id = booking.Id,
                Court = booking.Court,
                Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc),
                End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc),
                Event = eventModel,
                EventBooking = eventBooking
            };

            return model;
        }

        public List<EventBookingViewModel> GetClassBookings(int bookingId, int eventId)
        {
            var result = new List<EventBookingViewModel>();

            Event e = Event.LoadById(eventId);

            foreach (EventBooking eventBooking in EventBookingCollection.LoadByEvent(e))
            {
                result.Add(new EventBookingViewModel()
                {
                    Id = eventBooking.Id,
                    IsSelected = GetIsSelected(bookingId, eventBooking.Booking),
                    Bookings = eventBooking.Booking,
                    Date = eventBooking.Date
                });
            }

            return result;
        }

        public List<OrderViewModel> GetOutstandingBalances(int customerId, int orderId)
        {
            List<OrderViewModel> unpaidOrders = new List<OrderViewModel>();

            foreach(Order order in OrderCollection.LoadByStatus(customerId, OrderStatus.created).OrderByDescending(date => date.OrderDateUTC))
            {
                bool isSelected = (order.Id == orderId) ? true : false;

                unpaidOrders.Add(new OrderViewModel()
                {
                    Id = order.Id,
                    IsSelected = isSelected,
                    Order = order
                });
            }

            return unpaidOrders;
        }

        public List<OrderItemViewModel> GetOutstandingOrderItems(int customerId, int orderItemId)
        {
            var items = new List<OrderItemViewModel>();

            foreach (OrderItem item in OrderItemCollection.LoadByStatus(customerId, OrderItemStatus.created))
            {
                var selected = item.Id == orderItemId ? true : false;

                items.Add(new OrderItemViewModel()
                {
                    Id = item.Id,
                    IsSelected = selected,
                    OrderItem = item
                });
            }


                return items;
        }

        public BookingStatus GetBookingStatus(int bookingTypeId, decimal price)
        {
            bool isPaid = true;

            if (price > 0)
            {
                isPaid = false;
            }

            BookingType bookingType = BookingType.LoadById(bookingTypeId);

            BookingStatus bookingStatus = BookingStatus.GetStatus(bookingTypeId, isPaid);

            return bookingStatus;
        }

        public int[] GetClassTypeIds(Club club)
        {
            var result = new List<int>();

            IEnumerable<BookingType> classTypes = BookingTypeCollection.LoadByClub(club).Where(x => x.BookingCategory == BookingCategory.Class);

            foreach (BookingType type in classTypes)
            {
                result.Add(type.Id);
            }

            return result.ToArray();
        }
        
        public string GetStrCoaches(CoachCollection coaches)
        {
            List<string> coachList = new List<string>();

            if (coaches.Count() != 0)
            {
                foreach(Coach coach in coaches)
                {
                    coachList.Add(coach.User.FirstName + " " + coach.User.LastName);
                }

                string strCoach = string.Join(", ", coachList);

                return strCoach;
            }

            return "No set coach";
        }


        public string GetStrLevels(PlayerLevelCollection levels)
        {
            List<string> levelList = new List<string>();

            if (levels.Count() != 0)
            {
                foreach (PlayerLevel level in levels)
                {
                    levelList.Add(level.Name);
                }

                string strCoach = string.Join(", ", levelList);

                return strCoach;
            }

            return "No set level";
        }

        public bool GetIsSelected(int bookingId, BookingCollection bookings)
        {
            var result = false;

            foreach (Booking booking in bookings)
            {
                if (booking.Id == bookingId)
                {
                    return true;
                }
            }

            return result;
        }

        public void InsertOrder(List<BookingViewModel> model)
        {
            Customer customer = Customer.LoadById(model[0].CustomerId);


            foreach(BookingViewModel booking in model)
            {
                OrderItemStatus status = (booking.Price > 0) ? OrderItemStatus.created : OrderItemStatus.paid;

                BookingType bookingType = BookingType.LoadById(booking.BookingTypeId);

                OrderItem item = new OrderItem()
                {
                    CreationDateUTC = DateTime.UtcNow,
                    OrderItemStatus = status,
                    OrderItemType = OrderItemType.booking,
                    Amount = booking.Price,
                    BookingId = booking.Id,
                    Quantity = 1,
                    CustomerId = customer.Id,
                    Name = bookingType.Name,
                };
                item.Save();
            }


        }

        public List<OrderViewModel> GetUnpaidOrders(int orderId, int customerId)
        {
            var result = new List<OrderViewModel>();

            return result;
        }

    }
}