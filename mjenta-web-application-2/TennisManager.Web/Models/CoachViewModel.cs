﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Users;
using Members;

namespace TennisManager.Models
{
    public class CoachViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }

        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Display(Name = "Tennis ID")]
        public string TennisID { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public int ClubId { get; set; }

        public User User { get; set; }

        public IEnumerable<Coach> Coaches { get; set; }

        public IEnumerable<CoachDisplayViewModel> CoachList { get; set; }
    }

    public class CoachDisplayViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Pin { get; set; }

        public int ClassCount { get; set; }

        public int LessonCount { get; set; }
    }
}