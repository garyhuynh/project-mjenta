﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class CourtViewModel
    {
        public int Id { get; set; }

        public string Colour { get; set; }

        public string Name { get; set; }

        public Clubs.Club Club { get; set; }

        public Clubs.Facility Facility { get; set; }

        public Clubs.Zone Zone { get; set; }

        public Clubs.Surface Surface { get; set; }

        public string Text { get; set; }
    }
}