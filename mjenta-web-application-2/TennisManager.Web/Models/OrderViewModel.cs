﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders;
using Users;
using Bookings;
using Members;
using Events;
using Products;
using Accounts;

namespace TennisManager.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public Order Order { get; set; }

        public bool IsSelected { get; set; }

        public string OrderNumber { get; set; }

        public Admin Admin { get; set; }

        public Booking Booking { get; set; }

        public Customer Customer { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public DateTime CreationDate { get; set; }

        public decimal Subtotal { get; set; }

        public decimal Tax { get; set; }

        public decimal Amount { get; set; }

        public decimal Discount { get; set; }
    }

    public class OrderItemViewModel
    {
        public int Id { get; set; }

        public OrderItem OrderItem { get; set; }

        public int Quantity { get; set; }

        public decimal AdjustAmount { get; set; }

        public AdjustType AdjustType { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreationDate { get; set; }

        public string Note { get; set; }

        public OrderItemStatus OrderItemStatus { get; set; }

        public OrderItemType OrderItemType { get; set; }

        public Booking Booking { get; set; }

        public EventRegistration Registration { get; set; }

        public Member Membership { get; set; }

        public ProductVariant ProductVariant { get; set; }

        public List<Transaction> Transactions { get; set; }

        public bool IsSelected { get; set; }
    }
}