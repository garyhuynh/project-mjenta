﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Users;
using Members;
using Bookings;
using Clubs;
using Kendo.Mvc.UI;
using TennisManager.Models;

namespace TennisManager.Models
{
    public class BookingDetailViewModel
    {
        public BookingViewModel BookingViewModel { get; set; }

        public UserViewModel UserViewModel { get; set; }

        public MembershipViewModel MembershipViewModel { get; set; }

        public MembershipTypeViewModel MembershipTypeViewModel { get; set; }
        
    }
}