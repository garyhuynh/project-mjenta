﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Orders;
using Users;
using Sales;
using Invoices;
using Accounts;
using Classification;


namespace TennisManager.Models
{
    public class SaleViewModel
    {
        public int Id { get; set; }

        public List<Order> Orders { get; set; }

        public string Description { get; set; }

        public decimal Tax { get; set; }

        public Customer Customer { get; set; }

        public int CustomerId { get; set; }

        public Admin Admin { get; set; }

        public bool IsMember { get; set; }

        public string Note { get; set; }

        public decimal Discount { get; set; }

        public int DiscountTypeId { get; set; }

        //[JsonConverter(typeof(StringEnumConverter))]
        public DiscountType DiscountType { get; set; }

        public int ItemCount { get; set; }

        public decimal SubTotal { get; set; }

        public DateTime SaleDateUTC { get; set; }

        public SaleStatus SaleStatus { get; set; }

        public decimal TotalAmount { get; set; }

        public Invoice Invoice { get; set; }

        public List<int> Items { get; set; }

        public List<CustomerViewModel> CustomerList { get; set; }

        public List<SaleItemViewModel> SaleItems { get; set; }

        public List<PaymentViewModel> Payments { get; set; }

        public decimal DiscountAmount { get; set; }

        public decimal Change { get; set; }

        public decimal AmountPaid { get; set; }

        public int SourceType { get; set; }

        public decimal Balance { get; set; }

        public Order Order { get; set; }

    }

    public class SaleItemViewModel
    {
        public int Id { get; set; }

        public string ItemGuid { get; set; }

        public OrderItem OrderItem { get; set; }

        public OrderItemType OrderItemType { get; set; }

        public int OrderItemTypeId { get; set; }

        public int OrderItemId { get; set; }

        public int ProductVariantId { get; set; }

        public string DisplayName { get; set; }

        public string Variant { get; set; }

        public bool HasVariant { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal RetailPrice { get; set; }

        public int Quantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal MemberPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal SalePrice { get; set; }

        public DiscountType DiscountType { get; set; }

        public decimal DiscountAmount { get; set; }

        public string ThumbnailUrl { get; set; }

        public bool HasMemberPrice { get; set; }

        public bool Taxable { get; set; }

        public decimal ItemDiscount { get; set; }

        public decimal ItemAdjust { get; set; }

        public int ItemAdjustTypeId { get; set; }

        public AdjustType AdjustType { get; set; }

        public decimal ItemPrice { get; set; }

        public string Note { get; set; }

        public bool isMember { get; set; }

        public List<string> DetailList { get; set; }

        public int MetaDataId { get; set; }

        public string Details { get; set; }

        public string ItemName { get; set; }
    }

    public class PaymentViewModel
    {
        public int Id { get; set; }

        public SourceType SourceType { get; set; }

        public int SourceTypeId { get; set; }

        public decimal Amount { get; set; }

        public string Reference { get; set; }
    }
}