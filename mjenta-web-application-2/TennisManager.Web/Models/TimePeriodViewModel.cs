﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class TimePeriodViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int TimePeriodGroupId { get; set; }

        public string strTimePeriodStart { get; set; }

        public string strTimePeriodEnd { get; set; }
    }
}