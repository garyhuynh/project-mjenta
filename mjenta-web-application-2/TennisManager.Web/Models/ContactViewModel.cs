﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Users;
using Communications;
using libphonenumber;
using System.ComponentModel.DataAnnotations;


namespace TennisManager.Models
{
    public class ContactViewModel
    {

    }

    public class SmsViewModel : IValidatableObject
    {
        public int Id { get; set; }

        public int Reference { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public List<RecipientViewModel> Recipients { get; set; }

        public List<Admin> Admins { get; set; }

        public Direction Direction { get; set; }

        public Status Status { get; set; }

        public decimal Price { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsRegistrations { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            int selectedCount = 0;

            foreach (var recipient in Recipients)
            {
                if (recipient.IsSelected == true)
                {
                    selectedCount += selectedCount + 1;
                }
            }

            if (selectedCount == 0)
            {
                results.Add(new ValidationResult("No customers selected."));
            }

            if(string.IsNullOrEmpty(Body))
            {
                results.Add(new ValidationResult("Message body is required."));
            }

            return results;
        }
    }

    public class RecipientViewModel
    {
        public int CustomerId { get; set; }

        public Customer Customer { get; set; }

        public string Destination { get; set; }

        public bool IsValid { get; set; }

        public bool IsSelected { get; set; }

    }

    public class SmsResponseViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int Reference { get; set; }

        public string Body { get; set; }

        public List<Customer> Customers { get; set; }

        public Status Status { get; set; }

        public bool IsRegistration { get; set; }
    }
}