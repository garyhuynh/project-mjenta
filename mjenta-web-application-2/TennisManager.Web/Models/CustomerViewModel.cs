﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Users;
using Members;

namespace TennisManager.Models
{
    public class CustomerViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Suburb")]
        public string Suburb { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Display(Name = "Tennis ID")]
        public string TennisID { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Membership Type")]
        public string MembershipTypeId { get; set; }

        public DateOfBirth DOB { get; set; }

        public int ClubId { get; set; }

        public List<MembershipType> MembershipTypeList { get; set; }

        public int SelectedMembershipTypeId { get; set; }

        public Clubs.Club Club { get; set; }

        public Users.User User { get; set; }

        [Display(Name = "Gender")]
        public int GenderId { get; set; }

        [Display(Name = "Date of birth")]
        public DateTime DateOfBirth { get; set; }

        public DateTime CreationDateUTC { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsMember { get; set; }

        public Members.Member Member { get; set; }

        public string Search { get; set; }

        public IEnumerable<Customer> CustomerList { get; set; }

        public List<CustomerDisplayiewModel> CustomerCollection { get; set; }

        public List<BookingViewModel> CustomerBookings { get; set; }

        public CustomerFilterViewModel CustomerFilters { get; set; }

        public Pagination Pagination { get; set; }

        public int BookingCount { get; set; }

        public int RegistrationCount { get; set; }

        public string Pin { get; set; }

        public Member Membership { get; set; }

    }

    public class CustomerDisplayiewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool isMember { get; set; }

        public string MembershipType { get; set; }

        public int BookingCount { get; set; }

        public int RegistrationCount { get; set; }

        public string Pin { get; set; }

        


    }

    public class CustomerFilterViewModel
    {
        public int MembershipTypeId { get; set; }

    }

    public class DateOfBirth
    {
        public int Day { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }
    }

    public class Day
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }

    public class Month
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }

    public class Year
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }

    public class Pagination
    {
        public int PageSize { get; set; }

        public int PageIndex { get; set; }

        public int PageCount { get; set; }

        public int Records { get; set; }
    }
}