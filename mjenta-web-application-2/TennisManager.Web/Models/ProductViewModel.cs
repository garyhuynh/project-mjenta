﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Products;
using Clubs;

namespace TennisManager.Models
{
    public class ProductViewModel : IValidatableObject
    {
        public int Id { get; set; }

        [Display(Name = "Barcode")]
        public string Barcode { get; set; }

        public decimal CostPrice { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Enable")]
        public bool HasMemberPrice { get; set; }

        public bool HasVariablePrice { get; set; }

        [Display(Name = "Has Member Price?")]
        public int VariantHasMemberPrice { get; set; }

        public bool HasVariant { get; set; }

        public bool EditHasVariant { get; set; }

        public bool HasImages { get; set; }

        public bool IncludeGST { get; set; }

        [Required]
        [Display(Name = "Inventory Option")]
        public InventoryOption InventoryOption { get; set; }

        [Required]
        [Display(Name = "Inventory Option")]
        public int InventoryOptionId { get; set; }

        [Required]
        [Display(Name = "Inventory Option")]
        public InventoryOption VariantInventoryOption { get; set; }

        [Required]
        [Display(Name = "Inventory Option")]
        public int VariantInventoryOptionId { get; set; }

        [Display(Name = "Images")]
        public string PhotoUrl { get; set; }

        public ProductStatus ProductStatus { get; set; }

        [Required]
        [Display(Name = "Retail Price")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public double RetailPrice { get; set; }

        [Required]
        [Display(Name = "Members Price")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public double MembersPrice { get; set; }


        [Display(Name = "SKU")]
        public string SKU { get; set; }


        public string Title { get; set; }

        public Club Club { get; set; }


        [Display(Name = "Product Type")]
        public ProductType ProductType { get; set; }

        [Required]
        [Display(Name = "Product Type")]
        public int ProductTypeId { get; set; }

        [Display(Name = "Collection")]
        public ProductClass ProductClass { get; set; }

        [Required]
        [Display(Name = "Collection")]
        public int ProductClassId { get; set; }

        [Display(Name = "Vendor")]
        public ProductVendor ProductVendor { get; set; }

        [Required]
        [Display(Name = "Vendor")]
        public int ProductVendorId { get; set; }


        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        public ProductOption ProductOption { get; set; }

        [Display(Name = "Variant Option")]
        public int ProductOptionId { get; set; }

        [Display(Name = "Product Brand")]
        public int ProductBrandId { get; set; }

        public IEnumerable<ProductType> ProductTypes { get; set; }

        public IList<ProductOptionValueViewModel> ProductOptionValues { get; set; }

        public List<ProductVariantViewModel> ProductVariants { get; set; }

        public IEnumerable<Product> Products { get; set; }

        public ProductFilterViewModel ProductFilters { get; set; }

        public string Search { get; set; }

        public int PageSize { get; set; }

        public int PageIndex { get; set; }

        public int PageCount { get; set; }

        public int Records { get; set; }

        public IEnumerable<HttpPostedFileBase> ProductImages { get; set; }

        public FileStorageModel FileStorage { get; set; }

        public IEnumerable<ProductImage> Images { get; set; }

        public int ImageDeleteId { get; set; }

        public string ThumbnailUrl { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            if ((HasVariant == true) && (ProductOptionValues == null) && (ProductVariants == null))
            {
                results.Add(new ValidationResult("Invalid inputs for product variants"));
            }

            if (string.IsNullOrEmpty(Title))
            {
                results.Add(new ValidationResult("Product Title is required"));
            }

            return results;
        }

        public List<ProductDisplayViewModel> ProductList { get; set; }

    }

    public class ProductTypeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class ProductGroupViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class ProductVendorViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class ProductOptionViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }

    public class ProductBrandViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }

    public class ProductOptionValueViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ProductOption ProductOption { get; set; }

        public int ProductOptionId { get; set; }

        public string Barcode { get; set; }

        public string SKU { get; set; }

        public int Quantity { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public double RetailPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public double MemberPrice { get; set; }
    }

    public class ProductVariantViewModel
    {
        public int Id { get; set; }

        public string Barcode { get; set; }

        public string SKU { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal RetailPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal MemberPrice { get; set; }

        public ProductOption ProductOption { get; set; }

        public string ProductOptionValue { get; set; }

        public int Quantity { get; set; }

        [Display(Name = "Inventory Option")]
        public InventoryOption InventoryOption { get; set; }

        public Product Product { get; set; }

        public int ProductId { get; set; }
    }

    public class ProductFilterViewModel
    {
        public int ProductTypeId { get; set; }

        public int ProductClassId { get; set; }

        public int ProductVendorId { get; set; }

        public int ProductBrandId { get; set; }
    }

    public class ProductDisplayViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public ProductType ProductType { get; set; }

        public ProductClass ProductClass { get; set; }

        public ProductBrand ProductBrand { get; set; }

        public int InventoryCount { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal RetailPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal MemberPrice { get; set; }

        public string PhotoUrl { get; set; }

        public bool hasImage { get; set; }

        public bool hasMemberPrice { get; set; }

        public bool hasVariant { get; set; }

        public InventoryOption InventoryOption { get; set; }

        public int VariantCount { get; set; }

        public decimal VariantBasePrice { get; set; }

    }

    public class DeleteVariantResult
    {
        public bool Success { get; set; }

        public string AccessToken { get; set; }

        public string ErrorMessage { get; set; }

        public bool HasMultiple { get; set; }
    }

    public class ProductLookupViewModel
    {
        public int ProductVariantId { get; set; }

        public string DisplayName { get; set; }

        public string Variant { get; set; }

        public bool HasVariant { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal RetailPrice { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal MemberPrice { get; set; }

        public string ThumbnailUrl { get; set; }

        public bool HasMemberPrice { get; set; }

    }

}