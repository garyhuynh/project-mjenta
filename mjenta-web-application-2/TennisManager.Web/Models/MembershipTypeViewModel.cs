﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class MembershipTypeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Club { get; set; }

        public int Duration { get; set; }

        public decimal Fee { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDefault { get; set; }

        public decimal ProRata { get; set; }

        public DateTime RenewalDate { get; set; }

        public int Limit { get; set; }

    }
}