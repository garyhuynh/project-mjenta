﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class MemberViewModel
    {
        public int ID { get; set; }

        public DateTime JoinDate { get; set; }

        public Clubs.Club Club { get; set; }

        public Users.User User { get; set; }
    }
}