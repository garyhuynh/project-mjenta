﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TennisManager.Models
{
    public class FileStorageModel
    {
        public string Container { get; set; }

        public int ObjectId { get; set; }

        public string FileReference { get; set; }

        public string FileName { get; set; }

        public IEnumerable<HttpPostedFileBase> Files { get; set; }

        public string FileUrl { get; set; }

        public int ImageId { get; set; }
    }
}