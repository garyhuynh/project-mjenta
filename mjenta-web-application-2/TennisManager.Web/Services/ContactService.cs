﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Users;
using Events;
using Bookings;
using Communications;
using TennisManager.Models;
using Clubs;
using Microsoft.AspNet.Identity;
using Twilio;

namespace TennisManager.Services
{
    public class ContactService
    {
        public SmsResponseViewModel Send(SmsViewModel model)
        {
            List<Customer> customers = new List<Customer>();

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);

            MessageNumber messageNumber = MessageNumber.GetNumber(admin.Club.Id, true);

            var accountSid = messageNumber.MessageService.MessageAccount.Reference;
            var authToken = messageNumber.MessageService.MessageAccount.Token;

            foreach (var recipient in model.Recipients)
            {
                if(recipient.IsSelected)
                {
                    var twilio = new TwilioRestClient(accountSid, authToken);
                    var message = twilio.SendMessage(messageNumber.MessageService.AlphaId, recipient.Destination, model.Body);

                    if (message.RestException != null)
                    {
                        var error = message.RestException.Message;
                    }

                    Insert(recipient, admin.Id, message, model.Body, messageNumber);

                    customers.Add(Customer.LoadById(recipient.CustomerId));
                }
            
            }

            var result = new SmsResponseViewModel()
            {
                Title = model.Title,
                Body = model.Body,
                Customers = customers,
                Status = Status.sent
            };

            return result;
        }

        public void Insert(RecipientViewModel recipient, int adminId, Message message, string body, MessageNumber messageNumber)
        {
            MessageLog log = new MessageLog()
            {
                 AdminId = adminId,
                 CustomerId = recipient.CustomerId,
                 Body = body,
                 Price = System.Convert.ToDecimal(0.10),
                 Status = Status.queued,
                 Direction = Direction.inbound,
                 Reference = message.Sid,
                 DateSent = message.DateUpdated,
                 MessageNumber = messageNumber 
            };
            log.Save();
        }
    }
}