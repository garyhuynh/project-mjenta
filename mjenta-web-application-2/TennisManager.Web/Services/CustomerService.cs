﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TennisManager.Models;
using TennisManager.Web.Security;
using Clubs;
using Microsoft.WindowsAzure;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Owin.Security;
using System.Security.Principal;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using Users;
using Members;
using Bookings;

namespace TennisManager.Services
{
    public class CustomerService
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CustomerService(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public List<CustomerDisplayiewModel> Read(CustomerViewModel model)
        {  
            if (!string.IsNullOrEmpty(model.Search))
            {
                return GetSearchedCustomers(model);
            }

            return GetAllCustomers(model);
        }

        public void Update(CustomerViewModel model)
        {
            Customer customer = Customer.LoadById(model.Id);

            User user = User.Load(customer.User.Id);
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.GenderId = model.GenderId;
            user.DateOfBirth = new DateTime(model.DOB.Year, model.DOB.Month, model.DOB.Day);
            user.AddressLine1 = model.AddressLine1;
            user.AddressLine2 = model.AddressLine2;
            user.Suburb = model.Suburb;
            user.PhoneNumber = model.Mobile;
            user.Postcode = model.Postcode;
            user.State = model.State;
            user.TennisId = model.TennisID;

            user.Save();
        }

        public async Task<IdentityResult> CreateUser(CustomerViewModel model)
        {

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.ClubId = club.Id;

            DateTime dateOfBirth = new DateTime(model.DOB.Year, model.DOB.Month, model.DOB.Day);

            User newUser = new User()
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.Mobile,
                FirstName = model.FirstName,
                LastName = model.LastName,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                Suburb = model.Suburb,
                Postcode = model.Postcode,
                State = model.State,
                Pin = "5555",
                CreationDateUTC = DateTime.UtcNow,
                LastPasswordChangeDate = DateTime.UtcNow,
                AccessFailedWindowStart = DateTime.UtcNow,
                LockoutEnabled = false,
                LastProfileUpdateDate = DateTime.UtcNow,
                TwoFactorEnabled = false,
                GenderId = model.GenderId,
                DateOfBirth = dateOfBirth,
                Country = "AU",
                TennisId = model.TennisID
            };

            var result = await UserManager.CreateAsync(newUser);

            UserManager.AddToRole(newUser.Id.ToString(), "Customer");

            model.User = newUser;

            if (result.Succeeded)
            {
                CreateCustomer(model);
            }
            return result;
        }

        public List<CustomerDisplayiewModel> GetAllCustomers(CustomerViewModel model)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.Pagination.Records = CustomerCollection.CountByClub(club.Id);
            model.Pagination.PageCount = GetPageCount(model);
 

            IEnumerable<Customer> customerList = CustomerCollection.PageLoadByClub(model.Pagination.PageIndex, model.Pagination.PageSize, club.Id);

            List<CustomerDisplayiewModel> customerCollection = new List<CustomerDisplayiewModel>();

            foreach(Customer c in customerList)
            {
                customerCollection.Add(new CustomerDisplayiewModel()
                {
                    Id = c.Id,
                    Name = c.User.FirstName + " " + c.User.LastName,
                    Phone = c.User.PhoneNumber,
                    Email = c.User.Email,
                    Pin = c.User.Pin,
                    isMember = c.isMember,
                    MembershipType = GetCustomertMembership(c),
                    BookingCount = GetBookingCount(c.Id),
                    RegistrationCount = 0
                });
            }

            return customerCollection;
        }

        public List<CustomerDisplayiewModel> GetSearchedCustomers(CustomerViewModel model)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.Pagination.Records = CustomerCollection.CountBySearch("%" + model.Search + "%", club.Id);
            model.Pagination.PageCount = GetPageCount(model);

            IEnumerable<Customer> customerList = CustomerCollection.PageLoadBySearch(model.Pagination.PageIndex, model.Pagination.PageSize, "%" + model.Search + "%", club.Id);

            List<CustomerDisplayiewModel> customerCollection = new List<CustomerDisplayiewModel>();

            foreach (Customer c in customerList)
            {
                customerCollection.Add(new CustomerDisplayiewModel()
                {
                    Id = c.Id,
                    Name = c.User.FirstName + " " + c.User.LastName,
                    Phone = c.User.PhoneNumber,
                    Email = c.User.Email,
                    Pin = c.User.Pin,
                    isMember = c.isMember,
                    MembershipType = GetCustomertMembership(c),
                    BookingCount = GetBookingCount(c.Id),
                    RegistrationCount = 0
                });
            }
            return customerCollection;
        
        }

        public List<MembershipType> GetMembershipTypes()
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            List<MembershipType> membershipTypeList = MembershipTypeCollection.LoadByClub(club).ToList();

            return membershipTypeList;
        }

        public void CreateCustomer(CustomerViewModel model)
        {
            bool isMember = false;

            if (model.SelectedMembershipTypeId != 0)
            {
                isMember = true;
            }

            Customer customer = new Customer()
            {
                ClubId = model.ClubId,
                UserId = model.User.Id,
                isMember = isMember,
                isActive = true,
                CreationDateUTC = DateTime.UtcNow
            };

            customer.Save();

            if (model.SelectedMembershipTypeId != 0)
            {
                MembershipType membershipType = MembershipType.LoadById(model.SelectedMembershipTypeId);

                Member member = new Member()
                {
                    CustomerId = customer.Id,
                    MembershipTypeId = model.SelectedMembershipTypeId,
                    StartDateUTC = DateTime.UtcNow,
                    EndDateUTC = DateTime.UtcNow.AddDays(membershipType.Duration),
                    isActive = true
                };
                member.Save();
            }
        }

        public int GetPageCount(CustomerViewModel model)
        {
            int records = model.Pagination.Records;
            int recordsPerPage = model.Pagination.PageSize;
            int pageCount = (records + recordsPerPage - 1) / recordsPerPage;

            return pageCount;
        }

        public CustomerViewModel GetCustomerDetail(int customerId)
        {
            
            Customer customer = Customer.LoadById(customerId);

            DateOfBirth DOB = new DateOfBirth()
            {
                Day = customer.User.DateOfBirth.Day,
                Month = customer.User.DateOfBirth.Month,
                Year = customer.User.DateOfBirth.Year
            };

            CustomerViewModel model = new CustomerViewModel()
            {
                Id = customer.Id,
                FirstName = customer.User.FirstName,
                LastName = customer.User.LastName,
                Email = customer.User.Email,
                Mobile = customer.User.PhoneNumber,
                GenderId = customer.User.GenderId,
                DateOfBirth = customer.User.DateOfBirth,
                AddressLine1 = customer.User.AddressLine1,
                AddressLine2 = customer.User.AddressLine2,
                Suburb = customer.User.Suburb,
                Postcode = customer.User.Postcode,
                State = customer.User.State,
                TennisID = customer.User.TennisId,
                DOB = DOB,
                Pin = customer.User.Pin,
                Membership = Member.LoadByActiveCustomer(customerId, true)
            };

            return model;
        }

        public int GetBookingCount(int customerId)
        {
            int bookingCount = BookingCollection.CountByCustomer(customerId);

            return bookingCount;
        }

        public string GetCustomertMembership(Customer customer)
        {
            if (customer.isMember == false)
            {
                return null;
            }

            Member member = Member.LoadByActiveCustomer(customer.Id, true);

            return member.MembershipType.Name;
        }

        public List<BookingViewModel> GetCustomerBookings(int customerId, int bookingTypeId)
        {
            List<BookingViewModel> bookings = new List<BookingViewModel>();

            Customer customer = Customer.LoadById(customerId);

            if (bookingTypeId != 0)
            {
                foreach (Booking booking in BookingCollection.LoadByCustomer(customer).OrderByDescending(date => date.Start).Where(x => x.BookingTypeId == bookingTypeId))
                {
                    bookings.Add(new BookingViewModel()
                    {
                        Id = booking.Id,
                        Price = booking.Price,
                        NumberOfPlayers = booking.NumberOfPlayers,
                        Court = booking.Court,
                        isPaid = booking.isPaid,
                        strDate = booking.Start.ToShortDateString(),
                        strStart = booking.Start.ToShortTimeString(),
                        strEnd = booking.End.ToShortTimeString(),
                        BookingType = booking.BookingType
                    });
                }
                return bookings;
            }

            foreach (Booking booking in BookingCollection.LoadByCustomer(customer).OrderByDescending(date => date.Start))
            {
                bookings.Add(new BookingViewModel()
                {
                    Id = booking.Id,
                    Price = booking.Price,
                    NumberOfPlayers = booking.NumberOfPlayers,
                    Court = booking.Court,
                    isPaid = booking.isPaid,
                    strDate = booking.Start.ToShortDateString(),
                    strStart = booking.Start.ToShortTimeString(),
                    strEnd = booking.End.ToShortTimeString(),
                    BookingType = booking.BookingType
                });
            }

            return bookings;
        }


    }
}