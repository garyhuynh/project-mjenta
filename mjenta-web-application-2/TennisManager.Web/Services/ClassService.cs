﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TennisManager.Models;
using TennisManager.Web.Security;
using Clubs;
using Microsoft.WindowsAzure;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Owin.Security;
using System.Security.Principal;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using Users;
using Members;
using Bookings;
using Orders;
using Events;
using Classification;
using Communications;
using libphonenumber;
using TennisManager.Hubs;

namespace TennisManager.Services
{
    public class ClassService
    {


        [Authorize]
        public EventViewModel Read(EventViewModel model)
        {

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.PageSize = 15;
            model.Club = club;


            if ((model.BookingTypeId != 0) || (model.CoachId != 0) || (model.LevelId != 0) || (model.DayId != 0) || (!string.IsNullOrEmpty(model.SearchValue)))
            {
                return GetFilteredResult(model);
            }
            else
            {
                var result = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(club);

                model.RecordCount = EventCollection.CountByBookingTypeIds(classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeIds(model.PageIndex, model.PageSize, classTypeIds))
                {
                    result.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = result;
            }

            return model;
        }


        public EventViewModel GetFilteredResult(EventViewModel model)
        {
            var result = new EventViewModel();

            //filter title
            if (!string.IsNullOrEmpty(model.SearchValue))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByTitle("%" + model.SearchValue + "%", classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByTitle(model.PageIndex, model.PageSize, "%" + model.SearchValue + "%", classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - Coach - Day - Level
            if ((model.BookingTypeId != 0) && (model.CoachId != 0) && (model.DayId != 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByAllFilters(model.BookingTypeId, model.CoachId, model.DayId, model.LevelId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByAllFilters(model.PageIndex, model.PageSize, model.BookingTypeId, model.CoachId, model.DayId, model.LevelId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;

            }

            // BookingType - null - null - null
            if ((model.BookingTypeId != 0) && (model.CoachId == 0) && (model.DayId == 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeId(model.BookingTypeId);

                model.PageCount = GetPageCount(model);

                BookingType bookingType = BookingType.LoadById(model.BookingTypeId);

                foreach (Event e in EventCollection.PageLoadByBookingType(model.PageIndex, model.PageSize, null, bookingType))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - Coach - null - null
            if ((model.BookingTypeId == 0) && (model.CoachId != 0) && (model.DayId == 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByBookingTypeIdsCoach(model.CoachId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeIdsCoach(model.PageIndex, model.PageSize, model.CoachId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - null - Day - null
            if ((model.BookingTypeId == 0) && (model.CoachId == 0) && (model.DayId != 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByBookingTypeIdsDay(model.DayId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeIdsDays(model.PageIndex, model.PageSize, model.DayId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - null - null - Level
            if ((model.BookingTypeId == 0) && (model.CoachId == 0) && (model.DayId == 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByBookingTypeIdsLevel(model.LevelId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeIdsLevel(model.PageIndex, model.PageSize, model.LevelId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - Coach - null - null
            if ((model.BookingTypeId != 0) && (model.CoachId != 0) && (model.DayId == 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeCoach(model.BookingTypeId, model.CoachId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeCoach(model.PageIndex, model.PageSize, model.BookingTypeId, model.CoachId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - null - Day - null
            if ((model.BookingTypeId != 0) && (model.CoachId == 0) && (model.DayId != 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeDay(model.BookingTypeId, model.DayId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeDay(model.PageIndex, model.PageSize, model.BookingTypeId, model.DayId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - null - null - Level
            if ((model.BookingTypeId != 0) && (model.CoachId == 0) && (model.DayId == 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeLevel(model.BookingTypeId, model.LevelId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeLevel(model.PageIndex, model.PageSize, model.BookingTypeId, model.LevelId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - Coach - null - Level
            if ((model.BookingTypeId != 0) && (model.CoachId != 0) && (model.DayId == 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeCoach(model.BookingTypeId, model.CoachId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeCoach(model.PageIndex, model.PageSize, model.BookingTypeId, model.CoachId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - Coach - Day - null
            if ((model.BookingTypeId != 0) && (model.CoachId != 0) && (model.DayId != 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeCoachDay(model.BookingTypeId, model.CoachId, model.DayId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeCoachDay(model.PageIndex, model.PageSize, model.BookingTypeId, model.CoachId, model.DayId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // BookingType - null - Day - Level
            if ((model.BookingTypeId != 0) && (model.CoachId == 0) && (model.DayId != 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                model.RecordCount = EventCollection.CountByBookingTypeDayLevel(model.BookingTypeId, model.DayId, model.LevelId);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByBookingTypeDayLevel(model.PageIndex, model.PageSize, model.BookingTypeId, model.DayId, model.LevelId))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - Coach - Day - null
            if ((model.BookingTypeId == 0) && (model.CoachId != 0) && (model.DayId != 0) && (model.LevelId == 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByCoachDay(model.CoachId, model.DayId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByCoachDay(model.PageIndex, model.PageSize, model.CoachId, model.DayId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - Coach - null - Level
            if ((model.BookingTypeId == 0) && (model.CoachId != 0) && (model.DayId == 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByCoachLevel(model.CoachId, model.LevelId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByCoachLevel(model.PageIndex, model.PageSize, model.CoachId, model.LevelId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - Coach - Day - Level
            if ((model.BookingTypeId == 0) && (model.CoachId != 0) && (model.DayId != 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByCoachDayLevel(model.CoachId, model.DayId, model.LevelId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByCoachDayLevel(model.PageIndex, model.PageSize, model.CoachId, model.DayId, model.LevelId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }

            // null - null - Day - Level
            if ((model.BookingTypeId == 0) && (model.CoachId == 0) && (model.DayId != 0) && (model.LevelId != 0))
            {
                var list = new List<EventDisplayViewModel>();

                int[] classTypeIds = GetClassTypeIds(model.Club);

                model.RecordCount = EventCollection.CountByDayLevel(model.DayId, model.LevelId, classTypeIds);

                model.PageCount = GetPageCount(model);

                foreach (Event e in EventCollection.PageLoadByDayLevel(model.PageIndex, model.PageSize, model.DayId, model.LevelId, classTypeIds))
                {
                    list.Add(new EventDisplayViewModel()
                    {
                        Id = e.Id,
                        BookingType = e.BookingType,
                        Title = e.Title,
                        Capacity = e.Capacity,
                        Coaches = e.Coaches,
                        GenderAges = e.GenderAges,
                        PlayerLevels = e.Levels,
                        Courts = e.Courts,
                        HasCapacity = e.hasCapacity,
                        IsActive = e.isActive,
                        StartTime = e.StartTime,
                        EndTime = e.EndTime,
                        Days = e.Days
                    });
                }

                model.Events = list;

                return model;
            }


            return result;
        }

        public void Insert(EventViewModel model)
        {

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            Event newEvent = new Event()
            {
                ClubId = club.Id,
                BookingTypeId = model.BookingTypeId,
                Title = model.Title,
                Description = model.Description,
                Capacity = model.Capacity,
                hasCapacity = !model.isUnlimitedCapacity,
                StartTime = DateTime.SpecifyKind(model.StartTime, DateTimeKind.Utc),
                EndTime = DateTime.SpecifyKind(model.EndTime, DateTimeKind.Utc),
                isOnline = model.isOnline,
                hasWaitlist = model.hasWaitlist,
                isActive = false,
                RegistrationDateTime = DateTime.UtcNow,
                FeeOption = GetFeeOption(model.PricingOptionId),
            };

            if (model.SelectedCoaches != null)
            {
                foreach (int coachId in model.SelectedCoaches)
                {
                    Coach coach = Coach.LoadById(coachId);
                    newEvent.Coaches.Add(coach);
                }
            }
            
            if (model.SelectedPlayerLevels != null)
            {
                foreach (int levelId in model.SelectedPlayerLevels)
                {
                    PlayerLevel level = PlayerLevel.LoadById(levelId);
                    newEvent.Levels.Add(level);
                }

            }

            if (model.SelectedGenderAgeGroups != null)
            {
                foreach (int groupId in model.SelectedGenderAgeGroups)
                {
                    GenderAge genderAge = GenderAge.LoadById(groupId);
                    newEvent.GenderAges.Add(genderAge);
                }
            }

            if (model.SelectedCoaches != null)
            {
                foreach (int courtId in model.SelectedCourts)
                {
                    Court court = Court.LoadById(courtId);
                    newEvent.Courts.Add(court);
                }
            }

            if (model.SelectedDays != null)
            {
                foreach (int dayId in model.SelectedDays)
                {
                    Classification.DaysOfWeek day = Classification.DaysOfWeek.LoadById(dayId);
                    newEvent.Days.Add(day);
                }
            }

            bool result = newEvent.Save();

            model.Id = newEvent.Id;

            InsertPrices(model);

        }

        public void InsertPrices(EventViewModel model)
        {
           foreach(EventPrice price in model.Fees)
            {
                EventFee fee = new EventFee()
                {
                    DailyFee = price.DailyFee,
                    FullFee = price.FullFee,
                    MembershipTypeId = price.MembershipTypeId,
                    EventId = model.Id
                };
                fee.Save();
            }
        }

        public void Update(EventViewModel model)
        {

            Event e = Event.LoadById(model.Id);
            e.Title = model.Title;
            e.BookingTypeId = model.BookingTypeId;
            e.Description = model.Description;
            e.Capacity = model.Capacity;
            e.hasCapacity = !model.isUnlimitedCapacity;
            e.hasWaitlist = model.hasWaitlist;
            e.isOnline = model.isOnline;
            e.StartTime = model.StartTime;
            e.EndTime = model.EndTime;

            int[] currentCoaches = GetSelectedCoaches(e.Coaches);

            if (model.SelectedCoaches == null)
            {
                if(currentCoaches.Count() != 0)
                {
                    foreach (int coachId in currentCoaches)
                    {
                        Coach coach = Coach.LoadById(coachId);
                        e.Coaches.Remove(coach);
                    }
                }
            }
            else
            {
                if (!currentCoaches.SequenceEqual(model.SelectedCoaches))
                {
                    foreach (int coachId in currentCoaches)
                    {
                        Coach coach = Coach.LoadById(coachId);
                        e.Coaches.Remove(coach);
                    }

                    if (model.SelectedCoaches != null)
                    {
                        foreach (int coachId in model.SelectedCoaches)
                        {
                            Coach coach = Coach.LoadById(coachId);
                            e.Coaches.Add(coach);
                        }
                    }
                }
            }

            int[] currentGenderAgeGroups = GetSelectedGenderAges(e.GenderAges);

            if (model.SelectedGenderAgeGroups == null)
            {
                if (currentGenderAgeGroups.Count() != 0)
                {
                    foreach (int genderId in currentGenderAgeGroups)
                    {
                        GenderAge genderAge = GenderAge.LoadById(genderId);
                        e.GenderAges.Remove(genderAge);
                    }
                }
            }
            else
            {

                if (!currentGenderAgeGroups.SequenceEqual(model.SelectedGenderAgeGroups))
                {
                    foreach (int genderId in currentGenderAgeGroups)
                    {
                        GenderAge genderGroup = GenderAge.LoadById(genderId);
                        e.GenderAges.Remove(genderGroup);
                    }

                    if (model.SelectedGenderAgeGroups != null)
                    {
                        foreach (int genderId in model.SelectedGenderAgeGroups)
                        {
                            GenderAge genderGroup = GenderAge.LoadById(genderId);
                            e.GenderAges.Add(genderGroup);
                        }
                    }
                }
            }

            int[] currentPlayerLevels = GetSelectedPlayerLevels(e.Levels);

            if (model.SelectedPlayerLevels == null)
            {
                if (currentPlayerLevels.Count() != 0)
                {
                    foreach (int levelId in currentPlayerLevels)
                    {
                        PlayerLevel level = PlayerLevel.LoadById(levelId);
                        e.Levels.Remove(level);
                    }
                }
            }
            else
            {
                if (!currentPlayerLevels.SequenceEqual(model.SelectedPlayerLevels.ToArray()))
                {
                    foreach (int levelId in currentPlayerLevels)
                    {
                        PlayerLevel level = PlayerLevel.LoadById(levelId);
                        e.Levels.Remove(level);
                    }

                    if (model.SelectedPlayerLevels != null)
                    {
                        foreach (int levelId in model.SelectedPlayerLevels)
                        {
                            PlayerLevel level = PlayerLevel.LoadById(levelId);
                            e.Levels.Add(level);
                        }
                    }
                }
            }

            int[] currentCourts = GetSelectedCourts(e.Courts);

            if (model.SelectedCourts == null)
            {
                if (currentCourts.Count() != 0)
                {
                    foreach (int courtId in currentCourts)
                    {
                        Court court = Court.LoadById(courtId);
                        e.Courts.Remove(court);
                    }
                }
            }
            else
            {
                if (!currentCourts.SequenceEqual(model.SelectedCourts))
                {
                    foreach (int courtId in currentCourts)
                    {
                        Court court = Court.LoadById(courtId);
                        e.Courts.Remove(court);
                    }

                    if (model.SelectedCourts != null)
                    {
                        foreach (int courtId in model.SelectedCourts)
                        {
                            Court court = Court.LoadById(courtId);
                            e.Courts.Add(court);
                        }
                    }
                }
            }


            int[] currentDays = GetSelectedDays(e.Days);

            if (model.SelectedDays == null)
            {
                if (currentDays.Count() != 0)
                {
                    foreach (int dayId in currentDays)
                    {
                        DaysOfWeek day = DaysOfWeek.LoadById(dayId);
                        e.Days.Remove(day);
                    }
                }
            }
            else
            {
                if (!currentDays.SequenceEqual(model.SelectedDays))
                {
                    foreach (int dayId in currentDays)
                    {
                        DaysOfWeek day = DaysOfWeek.LoadById(dayId);
                        e.Days.Remove(day);
                    }

                    if (model.SelectedDays != null)
                    {
                        foreach (int dayId in model.SelectedDays)
                        {
                            DaysOfWeek day = DaysOfWeek.LoadById(dayId);
                            e.Days.Add(day);
                        }
                    }
                }
            }

            e.Save();

        }

        public void UpdateClassBooking(EventViewModel model)
        {
            EventBooking eventBooking = EventBooking.LoadById(model.EventBooking.Id);

            //update coaches
            int[] currentCoaches = GetSelectedCoaches(eventBooking.Coaches);

            if (model.SelectedCoaches == null)
            {
                if (currentCoaches.Count() != 0)
                {
                    foreach (int coachId in currentCoaches)
                    {
                        Coach coach = Coach.LoadById(coachId);
                        eventBooking.Coaches.Remove(coach);
                    }
                }
            }
            else
            {
                if (!currentCoaches.SequenceEqual(model.SelectedCoaches))
                {
                    foreach (int coachId in currentCoaches)
                    {
                        Coach coach = Coach.LoadById(coachId);
                        eventBooking.Coaches.Remove(coach);
                    }

                    if (model.SelectedCoaches != null)
                    {
                        foreach (int coachId in model.SelectedCoaches)
                        {
                            Coach coach = Coach.LoadById(coachId);
                            eventBooking.Coaches.Add(coach);
                        }
                    }
                }
            }


            //update levels
            int[] currentPlayerLevels = GetSelectedPlayerLevels(eventBooking.Levels);

            if (model.SelectedPlayerLevels == null)
            {
                if (currentPlayerLevels.Count() != 0)
                {
                    foreach (int levelId in currentPlayerLevels)
                    {
                        PlayerLevel level = PlayerLevel.LoadById(levelId);
                        eventBooking.Levels.Remove(level);
                    }
                }
            }
            else
            {
                if (!currentPlayerLevels.SequenceEqual(model.SelectedPlayerLevels.ToArray()))
                {
                    foreach (int levelId in currentPlayerLevels)
                    {
                        PlayerLevel level = PlayerLevel.LoadById(levelId);
                        eventBooking.Levels.Remove(level);
                    }

                    if (model.SelectedPlayerLevels != null)
                    {
                        foreach (int levelId in model.SelectedPlayerLevels)
                        {
                            PlayerLevel level = PlayerLevel.LoadById(levelId);
                            eventBooking.Levels.Add(level);
                        }
                    }
                }
            }


            //update courts

            int[] currentCourts = GetSelectedCourtBookings(eventBooking.Booking);
            Array.Sort(currentCourts);

            int[] selectedCourts = model.SelectedCourts;
            Array.Sort(selectedCourts);

            if (!currentCourts.SequenceEqual(selectedCourts))
            {
                List<Booking> bookingsToRemove = new List<Booking>();

                foreach (Booking booking in eventBooking.Booking)
                {
                    if (!selectedCourts.Contains(booking.CourtId))
                    {
                        bookingsToRemove.Add(booking);
                    }
                }

                foreach(Booking booking in bookingsToRemove)
                {
                    Booking.Delete(booking);
                    eventBooking.Booking.Remove(booking);
                }

            }
 
            eventBooking.Save();
        }

        public List<MembershipType> GetMembershipTypes()
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            List<MembershipType> membershipTypeList = MembershipTypeCollection.LoadByClub(club).ToList();

            return membershipTypeList;
        }

        public List<EventPrice> GetFeeStructure()
        {
            List<EventPrice> prices = new List<EventPrice>();
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            foreach(MembershipType membershipType in MembershipTypeCollection.LoadByClub(club))
            {
                prices.Add(new EventPrice()
                {
                    MembershipType = membershipType
                });
            }

            return prices;
        }

        public FeeOption GetFeeOption(int feeOptionId)
        {
            FeeOption option = new FeeOption();

            if (feeOptionId == 0)
            {
                option = FeeOption.per_session_payment;
            }
            if (feeOptionId == 1)
            {
                option = FeeOption.per_session_payment;
            }
            if (feeOptionId == 2)
            {
                option = FeeOption.both;
            }

            return option;
        }

        public int GetPageCount(EventViewModel model)
        {
            int records = model.RecordCount;
            int recordsPerPage = model.PageSize;
            int pageCount = (records + recordsPerPage - 1) / recordsPerPage;

            return pageCount;
        }

        public EventViewModel GetClassDetails(int eventId)
        {
            Event e = Event.LoadById(eventId);

            EventViewModel result = new EventViewModel()
            {
                Id = e.Id,
                BookingTypeId = e.BookingTypeId,
                BookingType = e.BookingType,
                Title = e.Title,
                Description = e.Description,
                Capacity = e.Capacity,
                Coaches = e.Coaches,
                PlayerLevels = e.Levels,
                SelectedCoaches = GetSelectedCoaches(e.Coaches),
                SelectedPlayerLevels = GetSelectedPlayerLevels(e.Levels),
                SelectedGenderAgeGroups = GetSelectedGenderAges(e.GenderAges),
                SelectedCourts = GetSelectedCourts(e.Courts),
                SelectedDays = GetSelectedDays(e.Days),
                hasWaitlist = e.hasWaitlist,
                isOnline = e.isOnline,
                isUnlimitedCapacity = !e.hasCapacity,
                Courts = e.Courts,
                Days = e.Days,
                StartTime = e.StartTime,
                EndTime = e.EndTime
            };

            return result;
        }

        public List<EventBookingViewModel> GetClassBookings(int eventBookingId)
        {
            var result = new List<EventBookingViewModel>();

            EventBooking currentEventBooking = EventBooking.LoadById(eventBookingId);
            
            foreach (EventBooking eventBooking in EventBookingCollection.LoadByEvent(currentEventBooking.Event))
            {
                int capacity = GetCapacityCount(eventBooking.Id);
                bool isFull = CheckCapacityStatus(capacity, eventBooking.Event.Capacity);
                bool isSelected = false;

                if(currentEventBooking.Id == eventBooking.Id)
                {
                    isSelected = true;
                }


                result.Add(new EventBookingViewModel()
                {
                    Id = eventBooking.Id,
                    IsSelected = isSelected,
                    Bookings = eventBooking.Booking,
                    Date = eventBooking.Date,
                    Capacity = eventBooking.Event.Capacity,
                    CapacityCount = capacity,
                    IsFull = isFull,
                    HasCapacity = eventBooking.Event.hasCapacity,
                    Coaches = eventBooking.Coaches,
                    PlayerLevels = eventBooking.Levels
                });
            }

            return result;
        }

        public List<EventBookingViewModel> GetClassRegistrationBookings(int eventBookingId)
        {
            var result = new List<EventBookingViewModel>();

            EventBooking selectedBooking = EventBooking.LoadById(eventBookingId);

            foreach (EventBooking eventBooking in EventBookingCollection.LoadByEvent(selectedBooking.Event))
            {
                int capacity = GetCapacityCount(eventBooking.Id);
                bool isFull = CheckCapacityStatus(capacity, eventBooking.Event.Capacity);
                bool isSelected = false;

                if(eventBooking.Id == selectedBooking.Id)
                {
                    isSelected = true;
                }

                result.Add(new EventBookingViewModel()
                {
                    Id = eventBooking.Id,
                    IsSelected = isSelected,
                    Bookings = eventBooking.Booking,
                    Date = eventBooking.Date,
                    Capacity = eventBooking.Event.Capacity,
                    CapacityCount = capacity,
                    IsFull = isFull,
                    HasCapacity = eventBooking.Event.hasCapacity
                });
            }

            return result;
        }

        public int GetCapacityCount(int eventBookingId)
        {
            int count = EventRegistrationCollection.CountByEventBooking(eventBookingId, RegistrationStatus.confirmed);

            return count;
        }

        public bool CheckCapacityStatus(int count, int capacity)
        {
            if(count >= capacity)
            {
                return true;
            }

            return false;
        }

        public bool GetIsSelected(int bookingId, BookingCollection bookings)
        {
            var result = false;

            foreach(Booking booking in bookings)
            {
                if(booking.Id == bookingId)
                {
                    return true;
                }
            }

            return result;
        }

        public EventViewModel GetClassPricing(int eventId)
        {
            var result = new EventViewModel();

            return result;
        }

        public EventViewModel GetClassRegistrations(int eventBookingId)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            EventViewModel model = new EventViewModel()
            {
                EventBooking = eventBooking,
                Registrations = EventRegistrationCollection.LoadByEventBooking(eventBooking)
            };

            return model;
        }

        public EventViewModel GetClassWaitlists(int eventBookingId)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            EventViewModel model = new EventViewModel()
            {
                EventBooking = eventBooking,
                Waitlists = EventWaitlistCollection.LoadByEventBooking(eventBooking)
            };

            return model;
        }

        public SmsViewModel GetSmsRecipients(int eventBookingId, bool isRegistrations)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            var result = new SmsViewModel()
            {
                Title = eventBooking.Event.Title,
                Recipients = new List<RecipientViewModel>(),
                Reference = eventBookingId,
                IsRegistrations = isRegistrations
            };

            if (isRegistrations)
            {
                foreach (EventRegistration registration in EventRegistrationCollection.LoadByEventBookingId(eventBookingId, RegistrationStatus.confirmed))
                {
                    result.Recipients.Add(new RecipientViewModel()
                    {
                        CustomerId = registration.Customer.Id,
                        Customer = registration.Customer,
                        Destination = GetSmsDestination(registration.Customer.User.PhoneNumber),
                        IsSelected = true
                    });
                }
            }
            else
            {
                foreach (EventWaitlist waitlist in EventWaitlistCollection.LoadByEventBooking(eventBooking))
                {
                    result.Recipients.Add(new RecipientViewModel()
                    {
                        CustomerId = waitlist.Customer.Id,
                        Customer = waitlist.Customer,
                        Destination = GetSmsDestination(waitlist.Customer.User.PhoneNumber),
                        IsSelected = true
                    });
                }
            }


            return result;
        }

        public string GetSmsDestination(string phoneNumber)
        {
            var result = "";

            PhoneNumber number = new PhoneNumber();

            try
            {
                number = PhoneNumberUtil.Instance.Parse(phoneNumber, "AU");
            }
            catch (NumberParseException e)
            {
                throw;
            }

            if (number.IsValidNumber)
            {
                result = number.Format(PhoneNumberUtil.PhoneNumberFormat.E164);
            }

            //if we got this far, phone number is invalid
            return result;
        }

        public int[] GetSelectedCoaches(CoachCollection coaches)
        {
            var result = new List<int>();

            foreach(Coach coach in coaches)
            {
                result.Add(coach.Id);
            }

            return result.ToArray();
        }

        public int[] GetSelectedPlayerLevels(PlayerLevelCollection levels)
        {
            var result = new List<int>();

            foreach (PlayerLevel level in levels)
            {
                result.Add(level.Id);
            }

            return result.ToArray();
        }

        public int[] GetSelectedGenderAges(GenderAgeCollection genderAges)
        {
            var result = new List<int>();

            foreach (GenderAge genderAge in genderAges)
            {
                result.Add(genderAge.Id);
            }

            return result.ToArray();
        }

        public int[] GetSelectedCourts(CourtCollection courts)
        {
            var result = new List<int>();

            foreach (Court court in courts)
            {
                result.Add(court.Id);
            }

            return result.ToArray();
        }

        public int[] GetSelectedCourtBookings(BookingCollection bookings)
        {
            var result = new List<int>();

            foreach(Booking booking in bookings)
            {
                result.Add(booking.Court.Id);
            }

            return result.ToArray();
        }

        public int[] GetSelectedDays(DaysOfWeekCollection days)
        {
            var result = new List<int>();

            foreach (DaysOfWeek day in days)
            {
                result.Add(day.Id);
            }

            return result.ToArray();
        }

        public int[] GetClassTypeIds(Club club)
        {
            var result = new List<int>();

            IEnumerable<BookingType> classTypes = BookingTypeCollection.LoadByClub(club).Where(x => x.BookingCategory == BookingCategory.Class);

            foreach (BookingType type in classTypes)
            {
                result.Add(type.Id);
            }

            return result.ToArray();
        }

        public RegistrationResult RegisterCustomer(int eventBookingId, int customerId)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            bool isRegistered = false;

            Customer customer = Customer.LoadById(customerId);

            decimal fee = 0;

            if (customer.isMember == false)
            {
                MembershipType type = MembershipType.LoadDefaultMembershipType(club.Id, true);

                EventFee eventFee = EventFee.LoadOneByEventAndMemberShipType(eventBooking.Event.Id, type.Id);

                fee = eventFee.DailyFee;
            }
            else
            {
                Member member = Member.LoadByActiveCustomer(customer.Id, true);

                EventFee eventFee = EventFee.LoadOneByEventAndMemberShipType(eventBooking.Event.Id, member.MembershipType.Id);

                fee = eventFee.DailyFee;
            }

            EventRegistration registration = new EventRegistration()
            {
                CustomerId = customer.Id,
                EventBookingId = eventBooking.Id,
                isAttending = true,
                isPaid = false,
                DateRegistered = DateTime.UtcNow,
                Fee = fee
            };

            isRegistered = registration.Save();

            RegistrationResult result = new RegistrationResult()
            {
                EventBookingId = eventBookingId,
                EventRegistrationId = registration.Id,
                Result = isRegistered
            };

            //var orderStatus = new OrderStatus();

            //if (fee >= 0)
            //{
            //    orderStatus = OrderStatus.created;
            //}
            //else
            //{
            //    orderStatus = OrderStatus.paid;
            //}

            //Order order = new Order()
            //{
            //    OrderType = OrderType.registration,
            //    Quantity = 1,
            //    OrderStatus = orderStatus,
            //    CustomerId = customer.Id,
            //    RegistrationId = registration.Id,
            //    CreationDateUTC = DateTime.UtcNow,
            //    Amount = fee
            //};
            //order.Save();

            InsertOrder(registration);

            return result;
        }  

        public WaitlistResult WaitlistCustomer(int eventBookingId, int customerId)
        {
            bool isWaitlisted = false;

            EventWaitlist waitlist = new EventWaitlist()
            {
                CustomerId = customerId,
                EventBookingId = eventBookingId,
                isWaiting = true
            };
            isWaitlisted = waitlist.Save();

            WaitlistResult result = new WaitlistResult()
            {
                EventBookingId = eventBookingId,
                EventWaitlistId = waitlist.Id,
                Result = isWaitlisted
            };

            return result;
        }

        public EventViewModel GetEventBookingDetails(int eventBookingId)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            EventViewModel model = new EventViewModel()
            {
                EventBooking = eventBooking,
                SelectedCoaches = GetSelectedCoaches(eventBooking.Coaches),
                SelectedPlayerLevels = GetSelectedPlayerLevels(eventBooking.Levels),
                SelectedCourts = GetSelectedCourtBookings(eventBooking.Booking)
            };

            return model;
        }

        public EventBookingCancellationViewModel GetCancellationDetails(int eventBookingId)
        {
            var result = new EventBookingCancellationViewModel()
            {
                EventBooking = EventBooking.LoadById(eventBookingId),
                RegistrationCount = EventRegistrationCollection.CountByPaidConfirmed(eventBookingId, true, RegistrationStatus.confirmed)
            };

            return result;
        }

        public void InsertOrder(EventRegistration registration)
        {
            OrderItemStatus status = (registration.Fee > 0) ? OrderItemStatus.created : OrderItemStatus.paid;

            OrderItem item = new OrderItem()
            {
                CreationDateUTC = DateTime.UtcNow,
                OrderItemStatus = status,
                OrderItemType = OrderItemType.registration,
                Amount = registration.Fee,
                RegistrationId = registration.Id,
                Quantity = 1,
                CustomerId = registration.Customer.Id,
                Name = registration.EventBooking.Event.Title
            };
            item.Save();

            registration.OrderItemId = item.Id;
            registration.Save();
        }
    }
}