﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using TennisManager.Models;

namespace TennisManager.Services
{
    public class BlobService
    {
        public void CreateContainer()
        { 
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mjentaimages");
            container.CreateIfNotExists();
            container.SetPermissions(
                    new BlobContainerPermissions
                    {
                        PublicAccess =
                    BlobContainerPublicAccessType.Blob
                    });
        }

        public void UploadImage(FileStorageModel model)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            ConfigurationManager.AppSettings["StorageConnectionString"]);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get container name
            
            CloudBlobContainer container = blobClient.GetContainerReference(model.Container);
            container.CreateIfNotExists();

            foreach (HttpPostedFileBase image in model.Files)
            {
            //    string blobReference = model.SourceType + "/" + model.Object + "/" + model.ObjectId + "/" + image.FileName;
            //    CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobReference);
            //    blockBlob.UploadFromStream(image.InputStream);

            //create product image instance


            }
        }

        public FileStorageModel GetImages(FileStorageModel model)
        {


            return model;
        }
    }
}