﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Products;
using System.Web.Mvc;
using TennisManager.Models;
using Clubs;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;

namespace TennisManager.Services
{
    public class ProductService
    {
        //private ProductService productService;

        //public ProductService()
        //{
        //    this.productService = new ProductService();
        //}
        public ProductViewModel Read(ProductViewModel model)
        {
            if (string.IsNullOrEmpty(model.Search) == false)
            {
                return GetSearchedProducts(model);

            }
            if (model.ProductFilters != null)
            {
                return GetFilteredProducts(model);
            }

            return GetAllProducts(model);
        }

        public void Insert(ProductViewModel model)
        {
            Product product = new Product();

            bool hasImage = false;
            if (model.FileStorage != null)
            {
                hasImage = true;
            }

            InventoryOption productInventoryOption = new InventoryOption();

            if (model.InventoryOptionId == 0)
            {
                productInventoryOption = InventoryOption.track;
            }

            else
            {
                productInventoryOption = InventoryOption.do_not_track;
            }

            if (model.HasVariant == false)
            {
                product = new Product()
                {
                    Title = model.Title,
                    Description = model.Description,
                    ProductTypeId = model.ProductTypeId,
                    ProductClassId = model.ProductClassId,
                    ProductVendorId = model.ProductVendorId,
                    ProductBrandId = model.ProductBrandId,
                    hasImages = hasImage,
                    Reference = Guid.NewGuid().ToString(),
                    Quantity = model.Quantity,
                    InventoryOption = productInventoryOption,
                    hasVariant = false,
                    hasMultiple = false,
                    hasMemberPrice = model.HasMemberPrice,
                    ClubId = model.Club.Id,
                    VariantCount = 1
                };
                product.Save();

                ProductVariant productVariant = new ProductVariant()
                {
                    Barcode = model.Barcode,
                    InventoryOption = productInventoryOption,
                    isDefault = true,
                    RetailPrice = System.Convert.ToDecimal(model.RetailPrice),
                    MemberPrice = System.Convert.ToDecimal(model.MembersPrice),
                    Quantity = model.Quantity,
                    Sequence = 1,
                    ProductId = product.Id

                };
                productVariant.Save();
            }

            else
            {
                InventoryOption productVariantInventoryOption = new InventoryOption();

                if (model.VariantInventoryOptionId == 0)
                {
                    productVariantInventoryOption = InventoryOption.track;
                }

                else
                {
                    productVariantInventoryOption = InventoryOption.do_not_track;
                }


                product = new Product()
                {
                    Title = model.Title,
                    Description = model.Description,
                    ProductTypeId = model.ProductTypeId,
                    ProductClassId = model.ProductClassId,
                    ProductVendorId = model.ProductVendorId,    
                    ProductBrandId = model.ProductBrandId,
                    ProductOptionId = model.ProductOptionId,
                    hasImages = hasImage,
                    Reference = Guid.NewGuid().ToString(),
                    InventoryOption = productVariantInventoryOption,
                    hasVariant = true,
                    hasMultiple = true,
                    hasMemberPrice = model.HasMemberPrice,
                    ClubId = model.Club.Id,
                    VariantCount = model.ProductOptionValues.Count()
                };
                product.Save();

                int sequence = 0;

                foreach (ProductOptionValueViewModel value in model.ProductOptionValues)
                {
                    sequence = sequence + 1;

                    ProductVariant productVariant = new ProductVariant()
                    {
                        Title = value.Name,
                        Barcode = value.Barcode,
                        ProductId = product.Id,
                        ProductOptionValue = value.Name,
                        Quantity = value.Quantity,
                        RetailPrice = System.Convert.ToDecimal(value.RetailPrice),
                        MemberPrice = System.Convert.ToDecimal(value.MemberPrice),
                        Sequence = sequence,
                        isDefault = false,
                        InventoryOption = productVariantInventoryOption
                    };
                    productVariant.Save();
                }
            }

            if (model.FileStorage != null)
            {
                model.FileStorage.Container = product.Reference;
                model.FileStorage.ObjectId = product.Id;
                UploadImage(model.FileStorage);
            }
        }

        public void Update(ProductViewModel model)
        {
            Product product = Product.LoadById(model.Id);

            InventoryOption productInventoryOption = new InventoryOption();

            if (model.InventoryOptionId == 0)
            {
                productInventoryOption = InventoryOption.track;
            }
            else
            {
                productInventoryOption = InventoryOption.do_not_track;
            }

            if (model.HasVariant == false)
            {
                //check if current product instance has variant
                if (product.hasVariant == false)
                {
                    ProductVariant productVariant = ProductVariant.LoadOneByProductId(product.Id, true);
                    productVariant.RetailPrice = System.Convert.ToDecimal(model.RetailPrice);
                    productVariant.MemberPrice = System.Convert.ToDecimal(model.MembersPrice);
                    productVariant.Barcode = model.Barcode;
                    productVariant.InventoryOption = productInventoryOption;
                    productVariant.Quantity = model.Quantity;
                    productVariant.Save();
                }
                if (product.hasVariant == true)
                {
                    foreach (ProductVariant variant in ProductVariantCollection.LoadByProduct(product))
                    {
                        ProductVariant.Delete(variant);
                    }

                    ProductVariant newVariant = new ProductVariant()
                    {
                        Barcode = model.Barcode,
                        InventoryOption = productInventoryOption,
                        isDefault = true,
                        RetailPrice = System.Convert.ToDecimal(model.RetailPrice),
                        MemberPrice = System.Convert.ToDecimal(model.MembersPrice),
                        Quantity = model.Quantity,
                        Sequence = 1,
                        ProductId = product.Id
                    };
                    newVariant.Save();
                }

                product.Title = model.Title;
                product.Description = model.Description;
                product.ProductBrandId = model.ProductBrandId;
                product.ProductTypeId = model.ProductTypeId;
                product.ProductClassId = model.ProductClassId;
                product.ProductVendorId = model.ProductVendorId;
                product.hasVariant = false;
                product.hasMultiple = false;
                product.hasMemberPrice = model.HasMemberPrice;
                product.InventoryOption = productInventoryOption;
                product.VariantCount = 1;
                product.Save();

            
            }

            if (model.HasVariant == true)
            {
                int variantCount = 0;
                int productOptionId = 0;

                InventoryOption productVariantInventoryOption = new InventoryOption();

                if (model.VariantInventoryOptionId == 0)
                {
                    productVariantInventoryOption = InventoryOption.track;
                }

                else
                {
                    productVariantInventoryOption = InventoryOption.do_not_track;
                }


                if (product.hasVariant == true)
                {
                    
                    //save updated values for variant instances
                    variantCount = model.ProductVariants.Count();

                    foreach (ProductVariantViewModel variant in model.ProductVariants)
                    {
                        ProductVariant productVariant = ProductVariant.LoadById(variant.Id);
                        productVariant.Barcode = variant.Barcode;
                        productVariant.RetailPrice = variant.RetailPrice;
                        productVariant.MemberPrice = variant.MemberPrice;
                        productVariant.Quantity = variant.Quantity;
                        productVariant.ProductOptionValue = variant.ProductOptionValue;
                        productVariant.Save();
                    }
                }
                if (product.hasVariant == false)
                {
                    variantCount = model.ProductOptionValues.Count();

                    ProductVariant oldVariant = ProductVariant.LoadOneByProductId(product.Id, true);
                    ProductVariant.Delete(oldVariant);

                    int sequence = 0;

                    foreach (ProductOptionValueViewModel value in model.ProductOptionValues)
                    {
                        sequence = sequence + 1;

                        ProductVariant productVariant = new ProductVariant()
                        {
                            Title = value.Name,
                            Barcode = value.Barcode,
                            ProductId = product.Id,
                            ProductOptionValue = value.Name,
                            Quantity = value.Quantity,
                            RetailPrice = System.Convert.ToDecimal(value.RetailPrice),
                            MemberPrice = System.Convert.ToDecimal(value.MemberPrice),
                            Sequence = sequence,
                            isDefault = false,
                            InventoryOption = productVariantInventoryOption
                        };
                        productVariant.Save();
                    }
                }

                product.Title = model.Title;
                product.Description = model.Description;
                product.ProductBrandId = model.ProductBrandId;
                product.ProductTypeId = model.ProductTypeId;
                product.ProductClassId = model.ProductClassId;
                product.ProductVendorId = model.ProductVendorId;
                product.ProductOptionId = model.ProductOptionId;
                product.hasVariant = true;
                product.hasMultiple = true;
                product.hasMemberPrice = model.HasMemberPrice;
                product.InventoryOption = productVariantInventoryOption;
                product.VariantCount = variantCount;
                product.Save();
            }
        }

        public void Delete(ProductViewModel model)
        {

        }

        public ProductViewModel GetAllProducts(ProductViewModel model)
        {
            model.Records = ProductCollection.CountByClub(model.Club.Id);

            model.PageCount = GetPageCount(model);

            IEnumerable<Product> products = ProductCollection.PageLoadByClub(model.PageIndex, model.PageSize, model.Club.Id);

            model.Products = products;


            List<ProductDisplayViewModel> productList = new List<ProductDisplayViewModel>();

            foreach (Product p in products)
            {
                productList.Add(new ProductDisplayViewModel()
                {
                    Id = p.Id,
                    Title = p.Title,
                    ProductType = p.ProductType,
                    ProductClass = p.ProductClass,
                    ProductBrand = p.ProductBrand,
                    InventoryCount = GetInventoryCount(p),
                    RetailPrice = GetProductRetailPrice(p),
                    MemberPrice = GetProductMemberPrice(p),
                    PhotoUrl = p.PhotoUrl,
                    hasImage = p.hasImages,
                    hasVariant = p.hasVariant,
                    hasMemberPrice = p.hasMemberPrice,
                    InventoryOption = p.InventoryOption,
                    VariantCount = p.VariantCount
                });
            }
            model.ProductList = productList;

            return model;
        }

        public ProductViewModel GetFilteredProducts(ProductViewModel model)
        {
            model.Search = "";

            // 0 - 0 - 0 - 0
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                return GetAllProducts(model);
            }

            // 1 - 0 - 0 - 0
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                ProductType productType = ProductType.LoadById(model.ProductFilters.ProductTypeId);

                model.Records = ProductCollection.CountByProductTypeId(productType.Id);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductType(model.PageIndex, model.PageSize, null, productType);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 1 - 0 - 0
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                ProductClass productClass = ProductClass.LoadById(model.ProductFilters.ProductClassId);

                model.Records = ProductCollection.CountByProductTypeId(productClass.Id);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductClass(model.PageIndex, model.PageSize, null, productClass);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 0 - 1 - 0
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                ProductVendor productVendor = ProductVendor.LoadById(model.ProductFilters.ProductVendorId);

                model.Records = ProductCollection.CountByProductVendorId(productVendor.Id);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductVendor(model.PageIndex, model.PageSize, null, productVendor);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 0 - 0 - 1
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                ProductBrand productBrand = ProductBrand.LoadById(model.ProductFilters.ProductBrandId);

                model.Records = ProductCollection.CountByProductBrandId(productBrand.Id);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductBrand(model.PageIndex, model.PageSize, null, productBrand);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }



            // 1 - 1 - 0 - 0
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productClassId = model.ProductFilters.ProductClassId;

                model.Records = ProductCollection.CountByProductTypeProductClass(productTypeId, productClassId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductClass(model.PageIndex, model.PageSize, null, productTypeId, productClassId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 1 - 1 - 0
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productClassId = model.ProductFilters.ProductClassId;
                int productVendorId = model.ProductFilters.ProductVendorId;

                model.Records = ProductCollection.CountByProductTypeProductClassProductVendor(productTypeId, productClassId, productVendorId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductClassProductVendor(model.PageIndex, model.PageSize, null, productTypeId, productClassId, productVendorId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 1 - 1 - 1
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productClassId = model.ProductFilters.ProductClassId;
                int productVendorId = model.ProductFilters.ProductVendorId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByAllFilters(productTypeId, productVendorId, productBrandId, productClassId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByAllFilters(model.PageIndex, model.PageSize, null, productTypeId, productVendorId, productBrandId, productClassId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 0 - 1 - 0
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productVendorId = model.ProductFilters.ProductVendorId;

                model.Records = ProductCollection.CountByProductTypeProductVendor(productTypeId, productVendorId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductVendor(model.PageIndex, model.PageSize, null, productTypeId, productVendorId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 0 - 0 - 1
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductTypeProductBrand(productTypeId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductBrand(model.PageIndex, model.PageSize, null, productTypeId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 0 - 1 - 1
            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productVendorId = model.ProductFilters.ProductVendorId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductTypeProductVendorProductBrand(productTypeId, productVendorId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductVendorProductBrand(model.PageIndex, model.PageSize, null, productTypeId, productVendorId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 1 - 1 - 0
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId == 0))
            {
                int productClassId = model.ProductFilters.ProductClassId;
                int productVendorId = model.ProductFilters.ProductVendorId;

                model.Records = ProductCollection.CountByProductClassProductVendor(productClassId, productVendorId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductClassProductVendor(model.PageIndex, model.PageSize, null, productClassId, productVendorId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 0 - 1 - 1
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId == 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productVendorId = model.ProductFilters.ProductVendorId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductVendorProductBrand(productVendorId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductVendorProductBrand(model.PageIndex, model.PageSize, null, productVendorId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 1 - 1 - 1
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId != 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productClassId = model.ProductFilters.ProductClassId;
                int productVendorId = model.ProductFilters.ProductVendorId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductClassProductVendorProductBrand(productClassId, productVendorId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductClassProductVendorProductBrand(model.PageIndex, model.PageSize, null, productClassId, productVendorId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 0 - 1 - 0 - 1
            if ((model.ProductFilters.ProductTypeId == 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productClassId = model.ProductFilters.ProductClassId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductClassProductBrand(productClassId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductClassProductBrand(model.PageIndex, model.PageSize, null, productClassId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }

            // 1 - 1 - 0 - 1

            if ((model.ProductFilters.ProductTypeId != 0) && (model.ProductFilters.ProductClassId != 0) && (model.ProductFilters.ProductVendorId == 0) && (model.ProductFilters.ProductBrandId != 0))
            {
                int productTypeId = model.ProductFilters.ProductTypeId;
                int productClassId = model.ProductFilters.ProductClassId;
                int productBrandId = model.ProductFilters.ProductBrandId;

                model.Records = ProductCollection.CountByProductTypeProductClassProductBrand(productTypeId, productClassId, productBrandId);

                model.PageCount = GetPageCount(model);

                IEnumerable<Product> products = ProductCollection.PageLoadByProductTypeProductClassProductBrand(model.PageIndex, model.PageSize, null, productTypeId, productClassId, productBrandId);

                model.ProductList = GetProductDisplayList(products);

                return model;
            }


            return model;
        }

        public ProductViewModel GetSearchedProducts(ProductViewModel model)
        {
            List<ProductDisplayViewModel> productList = new List<ProductDisplayViewModel>();

            //IEnumerable<Product> productTest = ProductCollection.SearchByTitle("%" + model.Search + "%", model.Club.Id);

            foreach (Product p in ProductCollection.SearchByTitle("%" + model.Search + "%", model.Club.Id))
            {
                productList.Add(new ProductDisplayViewModel() {
                    Id = p.Id,
                    Title = p.Title,
                    ProductType = p.ProductType,
                    ProductClass = p.ProductClass,
                    ProductBrand = p.ProductBrand,
                    InventoryCount = GetInventoryCount(p),
                    RetailPrice = GetProductRetailPrice(p),
                    MemberPrice = GetProductMemberPrice(p),
                    PhotoUrl = p.PhotoUrl,
                    hasImage = p.hasImages,
                    hasVariant = p.hasVariant,
                    InventoryOption = p.InventoryOption,
                    VariantCount = p.VariantCount

                });
            }

            foreach (ProductVariant p in ProductVariantCollection.SearchByBarcode("%" + model.Search + "%", model.Club.Id))
            {
                productList.Add(new ProductDisplayViewModel() {
                    Id = p.Id,
                    Title = p.Product.Title,
                    ProductType = p.Product.ProductType,
                    ProductClass = p.Product.ProductClass,
                    ProductBrand = p.Product.ProductBrand,
                    InventoryCount = GetInventoryCount(p.Product),
                    RetailPrice = GetProductRetailPrice(p.Product),
                    MemberPrice = GetProductMemberPrice(p.Product),
                    PhotoUrl = p.Product.PhotoUrl,
                    hasImage = p.Product.hasImages,
                    hasVariant = p.Product.hasVariant,
                    InventoryOption = p.Product.InventoryOption,
                    VariantCount = p.Product.VariantCount
                });
            }

            model.ProductList = productList;

            model.ProductFilters = null;

            return model;
        }

        public int GetPageCount(ProductViewModel model)
        {
            int records = model.Records;
            int recordsPerPage = model.PageSize;
            int pageCount = (records + recordsPerPage - 1) / recordsPerPage;

            return pageCount;
        }

        public void UploadImage(FileStorageModel model)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            ConfigurationManager.AppSettings["StorageConnectionString"]);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(model.Container);
            if (container.CreateIfNotExists())
            {
                container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            }

            int count = ProductImageCollection.CountProductImages(model.ObjectId);

            //upload default image
            if (count == 0)
            {
                HttpPostedFileBase defaultImage = model.Files.FirstOrDefault();

                foreach (HttpPostedFileBase image in model.Files)
                {
                    bool isDefault = false;

                    if (defaultImage == image)
                    {
                        isDefault = true;
                    }

                    Guid reference = Guid.NewGuid();

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(reference.ToString());
                    blockBlob.UploadFromStream(image.InputStream);


                    ProductImage productImage = new ProductImage()
                    {
                        Name = image.FileName,
                        ProductId = model.ObjectId,
                        Reference = reference.ToString(),
                        Url = blockBlob.Uri.ToString(),
                        isDefault = isDefault
                    };
                    productImage.Save();

                    if (defaultImage == image)
                    {
                        Product product = Product.LoadById(model.ObjectId);
                        product.PhotoUrl = blockBlob.Uri.ToString();
                        product.hasImages = true;
                        product.Save();
                    }
                }
            }
            //upload additional images
            else
            {
                foreach (HttpPostedFileBase image in model.Files)
                {

                    Guid reference = Guid.NewGuid();

                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(reference.ToString());
                    blockBlob.UploadFromStream(image.InputStream);

                    ProductImage productImage = new ProductImage()
                    {
                        Name = image.FileName,
                        ProductId = model.ObjectId,
                        Reference = reference.ToString(),
                        Url = blockBlob.Uri.ToString(),
                        isDefault = false
                    };
                    productImage.Save();
                }
            }
        }

        public IEnumerable<ProductImage> GetProductImage (int productId)
        {
            Product product = Product.LoadById(productId);
            IEnumerable<ProductImage> images = ProductImageCollection.LoadByProduct(product);

            return images;
        }

        public void DeleteImage(FileStorageModel model)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            ConfigurationManager.AppSettings["StorageConnectionString"]);

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(model.Container);

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(model.FileReference);

            blockBlob.Delete();

            ProductImage productImage = ProductImage.LoadById(model.ImageId);

            ProductImage.Delete(productImage);

            Product product = Product.LoadById(model.ObjectId);

            int count = ProductImageCollection.CountProductImages(model.ObjectId);

            if (count == 0)
            {
                product.hasImages = false;
                product.PhotoUrl = "";
                product.Save();
            }
            if (count == 1)
            {

                IEnumerable<ProductImage> images = ProductImageCollection.LoadByProduct(product);
                ProductImage image = images.FirstOrDefault();
                image.isDefault = true;
                image.Save();

                product.PhotoUrl = image.Url;
                product.Save();
            }

        }

        public int GetInventoryCount(Product product)
        {
            int count = 0;

            foreach (ProductVariant p in ProductVariantCollection.LoadByProduct(product))
            {
                count = count + p.Quantity;
            }

            return count;
        }

        public decimal GetProductRetailPrice(Product product)
        {
            decimal price = 0;
            if (product.hasVariant == true)
            {
                List<ProductVariant> variants = ProductVariantCollection.LoadByProduct(product).OrderBy(v => v.RetailPrice).ToList();
                price = variants[0].RetailPrice;
            }
            if(product.hasVariant == false)
            {
                ProductVariant variant = ProductVariant.LoadOneByProductId(product.Id, true);
                price = variant.RetailPrice;
            }

            return price;
        }

        public decimal GetProductMemberPrice(Product product)
        {
            decimal price = 0;
            if (product.hasVariant == true)
            {
                List<ProductVariant> variants = ProductVariantCollection.LoadByProduct(product).OrderBy(v => v.MemberPrice).ToList();
                price = variants[0].MemberPrice;
            }
            if (product.hasVariant == false)
            {
                ProductVariant variant = ProductVariant.LoadOneByProductId(product.Id, true);
                price = variant.MemberPrice;
            }

            return price;
        }

        public ProductViewModel GetProductDetails(Product product)
        {
            int inventoryOptionId = 0;

            if (product.InventoryOption == InventoryOption.do_not_track)
            {
                inventoryOptionId = 1;
            }

            int VariantHasMemberPrice = 0;

            if (product.hasMemberPrice == true)
            {
                VariantHasMemberPrice = 1;
            }




            ProductViewModel model = new ProductViewModel()
            {
                Title = product.Title,
                Description = product.Description,
                ProductBrandId = product.ProductBrandId,
                ProductTypeId = product.ProductTypeId,
                ProductClassId = product.ProductClassId,
                ProductVendorId = product.ProductVendorId,
                HasVariant = product.hasVariant,
                HasMemberPrice = product.hasMemberPrice,
                ProductOptionId = product.ProductOptionId,
                VariantHasMemberPrice = VariantHasMemberPrice
            };

            if (product.hasVariant == false)
            {
                ProductVariant defaultVariant = ProductVariant.LoadOneByProductId(product.Id, true);
                model.Barcode = defaultVariant.Barcode;
                model.RetailPrice = System.Convert.ToDouble(defaultVariant.RetailPrice);
                model.MembersPrice = System.Convert.ToDouble(defaultVariant.MemberPrice);
                model.Quantity = defaultVariant.Quantity;
                model.InventoryOptionId = inventoryOptionId;

            }

            if (product.hasVariant == true)
            {

                model.VariantInventoryOptionId = inventoryOptionId;

                List<ProductVariantViewModel> variants = new List<ProductVariantViewModel>();

                foreach (ProductVariant variant in ProductVariantCollection.LoadByProduct(product))
                {
                    variants.Add(new ProductVariantViewModel()
                    {
                        Id = variant.Id,
                        Barcode = variant.Barcode,
                        ProductOption = product.ProductOption,
                        ProductOptionValue = variant.ProductOptionValue,
                        Quantity = variant.Quantity,
                        RetailPrice = variant.RetailPrice,
                        MemberPrice = variant.MemberPrice 
                    });
                }
                model.ProductVariants = variants;
            }

            return model;
        }

        public DeleteVariantResult DeleteProductVariant(int productVariantId)
        {
            DeleteVariantResult result = new DeleteVariantResult();

            ProductVariant variant = ProductVariant.LoadById(productVariantId);

            int variantCount = ProductVariantCollection.CountByProduct(variant.Product.Id);

            bool hasMultiple = false;

            Product product = Product.LoadById(variant.Product.Id);

            if (variantCount == 1)
            {
                product.hasVariant = false;
                product.VariantCount = 0;
                product.hasMultiple = false;
                product.Save();

                variant.isDefault = true;
                variant.Save();

                hasMultiple = false;
            }


            if (variantCount > 1)
            {
                ProductVariant.Delete(variant);
                result.Success = true;
                hasMultiple = true;

                variantCount = ProductVariantCollection.CountByProduct(variant.Product.Id);

                product.VariantCount = variantCount;
                product.Save();
            }

            result.HasMultiple = hasMultiple;

            return result;
        }


        public List<ProductDisplayViewModel> GetProductDisplayList(IEnumerable<Product> products)
        {
            List<ProductDisplayViewModel> productList = new List<ProductDisplayViewModel>();

            foreach (Product p in products)
            {
                productList.Add(new ProductDisplayViewModel()
                {
                    Id = p.Id,
                    Title = p.Title,
                    ProductType = p.ProductType,
                    ProductClass = p.ProductClass,
                    ProductBrand = p.ProductBrand,
                    InventoryCount = GetInventoryCount(p),
                    RetailPrice = GetProductRetailPrice(p),
                    MemberPrice = GetProductMemberPrice(p),
                    PhotoUrl = p.PhotoUrl,
                    hasImage = p.hasImages,
                    hasVariant = p.hasVariant,
                    InventoryOption = p.InventoryOption,
                    VariantCount = p.VariantCount
                });
            }

            return productList;
        }
    }
}