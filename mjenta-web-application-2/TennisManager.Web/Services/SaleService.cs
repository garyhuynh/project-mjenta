﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using TennisManager.Models;
using Sales;
using Microsoft.AspNet.Identity;
using Users;
using Clubs;
using Orders;
using Bookings;
using Events;

namespace TennisManager.Services
{
    public class SaleService
    {

        public List<CustomerViewModel> GetCustomerSearch(string search, int clubId)
        {
            List<CustomerViewModel> CustomerList = new List<CustomerViewModel>();

            foreach (Customer customer in CustomerCollection.SearchCustomer("%" + search + "%", clubId))
            {
                CustomerList.Add(new CustomerViewModel()
                {
                    Id = customer.Id,
                    User = customer.User,
                    IsMember = customer.isMember
                });
            }

                return CustomerList;
        }

        public decimal GetOrderPrice(OrderItem orderItem)
        {
            if(orderItem.OrderItemType == OrderItemType.booking)
            {
                Booking booking = Booking.LoadById(orderItem.Booking.Id);
                return booking.Price;
            }
            else if (orderItem.OrderItemType == OrderItemType.credit)
            {

            }
            else if (orderItem.OrderItemType == OrderItemType.membership)
            {

            }
            else if (orderItem.OrderItemType == OrderItemType.product)
            {

            }
            else if (orderItem.OrderItemType == OrderItemType.registration)
            {
                EventRegistration registration = EventRegistration.LoadById(orderItem.Registration.Id);
                return registration.Fee;
            }

            return 0;
        }

        public Order CreateOrder(SaleViewModel model)
        {
            var order = new Order();

            ////Get User Identity
            //string userId = HttpContext.Current.User.Identity.GetUserId();
            //int id = System.Convert.ToInt32(userId);
            //User user = User.LoadOneById(id);

            ////Get Admin and Club Identity
            //ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            //Club club = Club.LoadOneById(admin.ClubId);

            //Customer customer = Customer.LoadById(model.CustomerId);

            //order = new Order()
            //{
            //    CustomerId = model.CustomerId,
            //    Subtotal = model.SubTotal,
            //    Discount = model.Discount,
            //    Tax = model.Tax,
            //    OrderDateUTC = DateTime.UtcNow,
            //    Total = model.TotalAmount,
            //    ClubId = club.Id

            //};
            //order.Save();

            //foreach (var item in model.SaleItems)
            //{
            //    OrderItemType orderItemType = (OrderItemType)Enum.ToObject(typeof(OrderItemType), item.OrderItemTypeId);

            //    if (orderItemType == OrderItemType.product)
            //    {
            //        OrderItemStatus status = (item.SalePrice > 0) ? OrderItemStatus.created : OrderItemStatus.paid;

            //        OrderItem orderItem = new OrderItem()
            //        {
            //            CreationDateUTC = DateTime.UtcNow,
            //            OrderItemStatus = OrderItemStatus.paid,
            //            OrderItemType = OrderItemType.product,
            //            Amount = item.SalePrice,
            //            ProductVariantId = item.MetaDataId,
            //            Quantity = item.Quantity,
            //            Note = item.Note,
            //            CustomerId = model.CustomerId,
            //            AdjustType = (AdjustType)Enum.ToObject(typeof(AdjustType), item.ItemAdjustTypeId),
            //            AdjustAmount = item.ItemAdjust,
            //            Name = item.ItemName,
            //            OrderId = order.Id
            //        };
            //        orderItem.Save();
            //        item.OrderItemId = orderItem.Id;

            //    }
            //    else if (orderItemType == OrderItemType.booking || orderItemType == OrderItemType.registration)
            //    {
            //        OrderItem orderItem = OrderItem.LoadById(item.OrderItemId);
            //        orderItem.Amount = item.SalePrice;
            //        orderItem.OrderItemStatus = OrderItemStatus.paid;
            //        orderItem.OrderId = order.Id;
            //        orderItem.AdjustAmount = item.ItemAdjust;
            //        orderItem.AdjustType = (AdjustType)Enum.ToObject(typeof(AdjustType), item.ItemAdjustTypeId);
            //        orderItem.Note = item.Note;
            //        orderItem.Quantity = item.Quantity;
            //        orderItem.Save();
            //    }

            //    if (item.DetailList.Count() > 0)
            //    {
            //        int sequence = 0;

            //        foreach (var detail in item.DetailList)
            //        {
            //            sequence += sequence + 1;

            //            OrderItemDetail detailLine = new OrderItemDetail()
            //            {
            //                Name = detail,
            //                Sequence = sequence,
            //                OrderItemId = item.OrderItemId
            //            };
            //            detailLine.Save();
            //        }
            //    }
            //}

            return order;
        }

    }
}