﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TennisManager.Models;
using Microsoft.AspNet.Identity;
using Invoices;
using Orders;
using Users;
using Clubs;

namespace TennisManager.Services
{
    public class InvoiceService
    {
        public Invoice Insert(SaleViewModel model)
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            var invoice = new Invoice()
            {
                AmountPaid = model.AmountPaid,
                Balance = model.Balance,
                Change = model.Change,
                CreationDateUTC = DateTime.UtcNow,
                CustomerId = model.CustomerId,
                ClubId = club.Id,
                OrderId = model.Order.Id
            };

            return invoice;
        }

    }
}