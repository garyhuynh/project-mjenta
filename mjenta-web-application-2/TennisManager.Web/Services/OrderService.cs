﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orders;
using Bookings;
using Classification;
using Events;
using Users;

namespace TennisManager.Services
{
    public class OrderService
    {
        public List<string> GetBookingDetails(int metaDataId, OrderItemType orderItemType)
        {
            var result = new List<string>();


            if (orderItemType == OrderItemType.booking)
            {
                Booking booking = Booking.LoadById(metaDataId);

                if (booking.BookingType.BookingOption == BookingOption.Booking)
                {
                    result.Add(booking.Start.ToString("D"));
                    result.Add(booking.Court.Facility.FullName);
                    result.Add(booking.Court.Name);
                    result.Add(booking.Start.ToShortTimeString() + " - " + booking.End.ToShortTimeString());
                }
                if (booking.BookingType.BookingOption == BookingOption.Lesson)
                {
                    result.Add(booking.Start.ToString("D"));
                    result.Add(booking.Court.Facility.FullName);
                    result.Add(booking.Court.Name);
                    result.Add(booking.Coach.User.FirstName + " - " + booking.Coach.User.LastName);
                    result.Add(booking.Start.ToShortTimeString() + " - " + booking.End.ToShortTimeString());
                }
            }
            if (orderItemType == OrderItemType.registration)
            {

                EventRegistration registration = EventRegistration.LoadById(metaDataId);

                result.Add(registration.EventBooking.Date.ToString("D"));

                List<string> coaches = new List<string>();

                foreach(Coach coach in registration.EventBooking.Coaches)
                {
                    coaches.Add(coach.User.FirstName + " " + coach.User.LastName);
                }

                string strCoaches = string.Join(", ", coaches);

                result.Add(strCoaches);

                List<string> levels = new List<string>();

                foreach (PlayerLevel level in registration.EventBooking.Levels)
                {
                    levels.Add(level.Name);
                }

                string strLevels = string.Join(", ", levels);

                result.Add(strLevels);

                List<string> courts = new List<string>();

                foreach (Booking booking in registration.EventBooking.Booking)
                {
                    courts.Add(booking.Court.Name);
                }

                string strCourts = string.Join(", ", courts);

                result.Add(strCourts);

                BookingCollection bookings = registration.EventBooking.Booking;
                bookings.OrderBy(x => x.Start);
                string start = bookings.FirstOrDefault().Start.ToShortTimeString();

                bookings.OrderBy(x => x.End);
                string end = bookings[bookings.Count() - 1].End.ToShortTimeString();

                result.Add(start + " - " + end);

            }

            return result;
        }

        public string GetDetails(OrderItem orderItem)
        {
            string result, date, facility, coach, court, start, end, time = string.Empty;
            
            // Get detail for casual and recurring bookings
            if (orderItem.OrderItemType == OrderItemType.booking)
            {
                if(orderItem.Booking.BookingType.BookingOption == BookingOption.Booking)
                { 
                    date = orderItem.Booking.Start.ToString("D");
                    court = orderItem.Booking.Court.Name;
                    facility = orderItem.Booking.Court.Facility.FullName;
                    time = orderItem.Booking.Start.ToShortTimeString() + " - " + orderItem.Booking.End.ToShortTimeString();

                    result = "<div>" + date + "<br />" + facility + "<br />" + court + "<br />" + time + "</div>";
                }
                // Get details for private lessons
                else
                {
                    court = orderItem.Booking.Court.Name;
                    date = orderItem.Booking.Start.ToString("D");
                    facility = orderItem.Booking.Court.Facility.FullName;
                    time = orderItem.Booking.Start.ToShortTimeString() + " - " + orderItem.Booking.End.ToShortTimeString();
                    coach = orderItem.Booking.Coach.User.FirstName + " " + orderItem.Booking.Coach.User.LastName;
                    result = "<div>" + date + "<br />" + facility + "<br />" + coach + "<br />" + court + "<br />" + time + "</div>";
                }
            }
            // Get details for registrations
            else
            {
                date = orderItem.Registration.EventBooking.Date.ToString("D");
                facility = orderItem.Registration.EventBooking.Booking[0].Court.Facility.FullName;
                BookingCollection bookings = orderItem.Registration.EventBooking.Booking;

                List<string> courtNames = new List<string>();

                foreach (Booking booking in bookings)
                {
                    courtNames.Add(booking.Court.Name);
                }

                court = string.Join(", ", courtNames);

                CoachCollection coaches = orderItem.Registration.EventBooking.Coaches;

                List<string> coachNames = new List<string>();

                foreach(Coach c in coaches)
                {
                    coachNames.Add(c.User.FirstName + " " + c.User.LastName);
                }

                coach = string.Join(", ", coachNames);

                bookings.OrderBy(x => x.Start);
                start = bookings.FirstOrDefault().Start.ToShortTimeString();

                bookings.OrderBy(x => x.End);
                end = bookings[bookings.Count() - 1].End.ToShortTimeString();

                time = start + " - " + end;

                result = "<div>" + date + "<br />" + facility + "<br />" + coach + "<br />" + court + "<br />" + time + "</div>";

            }

            return result;
        }

        





    }
}