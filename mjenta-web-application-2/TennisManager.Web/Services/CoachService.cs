﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TennisManager.Models;
using TennisManager.Web.Security;
using Clubs;
using Microsoft.WindowsAzure;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Owin.Security;
using System.Security.Principal;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using Users;
using Members;
using Bookings;


namespace TennisManager.Services
{
    public class CoachService
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CoachService(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public IEnumerable<CoachDisplayViewModel> Read()
        {
            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            List<CoachDisplayViewModel> coachList = new List<CoachDisplayViewModel>();
            
            foreach(Coach coach in CoachCollection.LoadByClub(club))
            {
                coachList.Add(new CoachDisplayViewModel()
                {
                    Id = coach.Id,
                    Name = coach.User.FirstName + " " + coach.User.LastName,
                    Phone = coach.User.PhoneNumber,
                    Email = coach.User.Email,
                    Pin = coach.User.Pin,
                    ClassCount = 0,
                    LessonCount = 0
                });
            }

            return coachList;
        }

        public async Task<IdentityResult> Create(CoachViewModel model)
        {

            //Get User Identity
            string userId = HttpContext.Current.User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            User user = User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.ClubId = club.Id;

            User newUser = new User()
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.Mobile,
                FirstName = model.FirstName,
                LastName = model.LastName,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                Suburb = model.Suburb,
                Postcode = model.Postcode,
                State = model.State,
                DateOfBirth = DateTime.UtcNow,
                Pin = "5555",
                CreationDateUTC = DateTime.UtcNow,
                LastPasswordChangeDate = DateTime.UtcNow,
                AccessFailedWindowStart = DateTime.UtcNow,
                LockoutEnabled = false,
                LastProfileUpdateDate = DateTime.UtcNow,
                TwoFactorEnabled = false,
                GenderId = model.GenderId,
                Country = "AU",
                TennisId = model.TennisID
            };

            var result = await UserManager.CreateAsync(newUser);

            UserManager.AddToRole(newUser.Id.ToString(), "Coach");

            model.User = newUser;

            if (result.Succeeded)
            {
                Coach coach = new Coach()
                {
                    CreationDateUTC = DateTime.UtcNow,
                    UserId = newUser.Id,
                    ClubId = club.Id,
                    isActive = true
                };
                coach.Save();
            }
            return result;
        }

    }
}