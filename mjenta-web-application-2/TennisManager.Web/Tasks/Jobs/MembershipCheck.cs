﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Members;
using Users;

namespace TennisManager.Tasks.Jobs
{
    public class MembershipCheck : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            DateTime date = DateTime.UtcNow;

            foreach (var member in MemberCollection.LoadAll())
            {
                if ((member.EndDateUTC <= date) && (member.isActive == true))
                {
                    member.isActive = false;
                    member.Save();

                    Customer customer = Customer.LoadById(member.Customer.Id);
                    customer.isMember = false;
                    customer.Save();

                    //revert back to default

                    MembershipType membershipType = MembershipType.LoadDefaultMembershipType(customer.Club.Id, true);

                    Member newMember = new Member()
                    {
                        CustomerId = customer.Id,
                        MembershipTypeId = membershipType.Id,
                        StartDateUTC = DateTime.UtcNow,
                        EndDateUTC = new DateTime(9999, 12, 31),
                        isActive = true
                    };
                    newMember.Save();

                }
            }   
        }
    }
}