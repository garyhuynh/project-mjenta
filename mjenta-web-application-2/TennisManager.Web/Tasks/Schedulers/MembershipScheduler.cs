﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TennisManager.Tasks.Jobs;
using Quartz;
using Quartz.Impl;

namespace TennisManager.Tasks.Schedulers
{
    public class MembershipScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<MembershipCheck>().Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (s =>
                    s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
                )
                .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}