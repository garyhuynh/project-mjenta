﻿    function btnAddVariant_click() {

        $("#btnAddVariant").addClass("hidden");

        $("#btnCancelVariant").removeClass("hidden");

        $("#OptionContainer").removeClass("hidden");

        $("#btnAddOption").removeClass("hidden");

        $("#rowOptionOne").removeClass("hidden");

        $("#hiddenHasVariant").val(true);

        $("#single-variant-container").fadeOut();

        resetHasMemberPrice();

    
    }

    function btnCancelVariant_click() {

        $("#btnCancelVariant").addClass("hidden");

        $("#btnAddVariant").removeClass("hidden");

        $("#OptionContainer").addClass("hidden");

        $("#VariantContainer").addClass("hidden");

        resetTagsInput();

        $("#hiddenHasVariant").val(false);

        $("#single-variant-container").fadeIn();

        resetHasMemberPrice();

    }

    function btnAddOption_Click() {
    
        var rowOneState = $("#rowOptionOne").hasClass("hidden");
        var rowTwoState = $("#rowOptionTwo").hasClass("hidden");

        if (rowOneState == true) {
            $("#rowOptionOne").removeClass("hidden");
            checkBtnAddOptionVisibility()
            return;
        }

        if (rowTwoState == true) {
            $("#rowOptionTwo").removeClass("hidden");
            checkBtnAddOptionVisibility()
            return;
        }

        if (rowTwoState == false) {
            $("#rowOptionThree").removeClass("hidden");
            checkBtnAddOptionVisibility()
            return;
        }


    }

    function btnRemoveOption_click(row) {
        $("#rowOption" + row).addClass("hidden");

        var rowOneState = $("#rowOptionOne").hasClass("hidden");
        var rowTwoState = $("#rowOptionTwo").hasClass("hidden");
        var rowThreeState = $("#rowOptionThree").hasClass("hidden");

        var rowOneValue = rowOneState ? 1 : 0;
        var rowTwoValue = rowTwoState ? 1 : 0;
        var rowThreeValue = rowThreeState ? 1 : 0;

        var counter = rowOneValue + rowTwoValue + rowThreeValue;

        if (counter == 3)
        {
            btnCancelVariant_click();
        }
        if (counter != 0) {
            $("#btnAddOption").removeClass("hidden");
        }
    
    }

    function checkBtnAddOptionVisibility()
    {
        var rowOneState = $("#rowOptionOne").hasClass("hidden");
        var rowTwoState = $("#rowOptionTwo").hasClass("hidden");
        var rowThreeState = $("#rowOptionThree").hasClass("hidden");

        var rowOneValue = rowOneState ? 1 : 0;
        var rowTwoValue = rowTwoState ? 1 : 0;
        var rowThreeValue = rowThreeState ? 1 : 0;

        var counter = rowOneValue + rowTwoValue + rowThreeValue;

        if (counter == 0) {
            $("#btnAddOption").addClass("hidden");
        }
    }

    function btnAddSpecificationItem_click(item) {

        $('#specificationModal').modal('show');

        $("#SpecificationContainer").empty();

        if (item == 'brand') {
            $.ajax({
                type: "POST",
                load: $("#SpecificationContainer").load("/Product/RenderPartial_AddProductBrand")
            });
        }

        if (item == 'type'){
            $.ajax({
                type: "POST",
                load: $("#SpecificationContainer").load("/Product/RenderPartial_AddProductType")
            });
        }

        if (item == 'class') {
            $.ajax({
                type: "POST",
                load: $("#SpecificationContainer").load("/Product/RenderPartial_AddProductClass")
            });
        }

        if (item == 'vendor') {
            $.ajax({
                type: "POST",
                load: $("#SpecificationContainer").load("/Product/RenderPartial_AddProductVendor")
            });
        }

        if (item == 'option') {
            $.ajax({
                type: "POST",
                load: $("#SpecificationContainer").load("/Product/RenderPartial_AddProductOption")
            });
        }



    }

    function btn_ProductTypeAdd() {

        var productType = $("#txtProductType").val(); 

        $.ajax({
            url: '/Product/AddProductType',
            type: 'GET',
            dataType: 'json',
            data: { productType: productType },
            cache: false,
            async: false,
            success: function (newProductType) {

                if (newProductType == false)
                {
                    alert("product type already exists");
                    return;
                }
                $('#specificationModal').modal('hide');
                var ddlProductType = $("#ProductTypeId").data("kendoDropDownList");
                ddlProductType.dataSource.add({ Id: newProductType.Id, Name: newProductType.Name });
                ddlProductType.value(newProductType.Id);
            
            },
        });

    }

    function btn_ProductClassAdd() {

        var productClassName = $("#txtProductClass").val();

        $.ajax({
            url: '/Product/AddProductClass',
            type: 'GET',
            dataType: 'json',
            data: { productClassName: productClassName },
            cache: false,
            async: false,
            success: function (newProductClass) {

                if (newProductClass == false) {
                    alert("product collection already exists");
                    return;
                }
                $('#specificationModal').modal('hide');
                var ddlProductClass = $("#ProductClassId").data("kendoDropDownList");
                ddlProductClass.dataSource.add({ Id: newProductClass.Id, Name: newProductClass.Name });
                ddlProductClass.value(newProductClass.Id);
            },
        });

    }

    function btn_ProductBrandAdd() {

        var productBrandName = $("#txtProductBrand").val();

        $.ajax({
            url: '/Product/AddProductBrand',
            type: 'GET',
            dataType: 'json',
            data: { productBrandName: productBrandName },
            cache: false,
            async: false,
            success: function (newProductBrand) {

                if (newProductBrand == false) {
                    alert("product collection already exists");
                    return;
                }
                $('#specificationModal').modal('hide');
                var ddlProductBrand = $("#ProductBrandId").data("kendoDropDownList");
                ddlProductBrand.dataSource.add({ Id: newProductBrand.Id, Name: newProductBrand.Name });
                ddlProductBrand.value(newProductBrand.Id);

            },
        });

    }

    function btn_ProductVendorAdd() {

        var productVendorName = $("#txtProductVendor").val();

        $.ajax({
            url: '/Product/AddProductVendor',
            type: 'GET',
            dataType: 'json',
            data: { productVendorName: productVendorName },
            cache: false,
            async: false,
            success: function (newProductVendor) {

                if (newProductVendor == false) {
                    alert("product vendor already exists");
                    return;
                }
                $('#specificationModal').modal('hide');
                var ddlProductVendor = $("#ProductVendorId").data("kendoDropDownList");
                ddlProductVendor.dataSource.add({ Id: newProductVendor.Id, Name: newProductVendor.Name });
                ddlProductVendor.value(newProductVendor.Id);

            },
        });

    }

    function btn_ProductOptionAdd() {

        var productOptionName = $("#txtProductOption").val();

        $.ajax({
            url: '/Product/AddProductOption',
            type: 'GET',
            dataType: 'json',
            data: { productOptionName: productOptionName },
            cache: false,
            async: false,
            success: function (newOption) {

                if (newOption == false) {
                    alert("product option already exists");
                    return;
                }
                $('#specificationModal').modal('hide');
                var ddlProductOption = $("#ProductOptionId").data("kendoDropDownList");
                ddlProductOption.dataSource.add({ Id: newOption.Id, Name: newOption.Name });
                ddlProductOption.value(newOption.Id);
            },
        });

    }

    function ddlInventoryOption_change(e) {

        var dataItem = e.sender.dataItem();

        if (dataItem.Value == 0) {
            $("#QuantityContainer").removeClass("hidden");
        }
        else {
            $("#QuantityContainer").addClass("hidden");
        }
    }

    function ddlVariantInventoryOption_change(e) {

        console.log("variant inventory option changed");

        refreshVariantGrid();
    }

    $("#listOptionOneValue").on('itemAdded', function (event) {

        //refreshVariantGrid();

    });


    $("#listOptionOneValue").on('itemRemoved', function (event) {



        refreshVariantGrid();

        //check number of items
        var currentCount = $("#listOptionOneValue").val().length;

        if (currentCount == 0) {

            $("#VariantContainer").addClass("hidden");
            var ddlVariantInventoryOption = $("#VariantInventoryOptionId").data("kendoDropDownList");
            ddlVariantInventoryOption.value(1);
        }
    });
    
    function ddlProductOption_change(e) {


        resetTagsInput();

        var dataItem = e.sender.dataItem();
        var id = dataItem.Id;
        console.log(id);


        $("#VariantContainer").addClass("hidden");


    }

    function refreshVariantTableInventory() {
        $("#tblVariants > thead").html("");
        $("#tblVariants > tbody").html("");

        var options = $("#listOptionOneValue").tagsinput('items');



        var th1 = '<th class="col-md-3" style="text-align:center"><i class="fa fa-cube"></i>&nbsp;&nbsp;&nbsp;Variant</th>';
        var th2 = ' <th class="col-md-3" style="text-align:center"><i class="fa fa-barcode"></i>&nbsp;&nbsp;&nbsp;Barcode</th>';
        var th3 = '   <th class="col-md-3" style="text-align:center"><i class="fa fa-cubes"></i>&nbsp;&nbsp;&nbsp;Quantity</th>';
        var th4 = '   <th class="col-md-3" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Retail Price</th>';

        $("#tblVariants thead").append(
         '<tr class="gray-bg">' + th1 + th2 + th3 + th4 + '</tr>'
         );

        for (var i = 0; i < options.length; i++) {
            var td1 = '<td><input id="lblOptionValueName' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Name" value="' + options[i] + '" readonly style="text-align:center;" /></td>';
            var td2 = '<td><input id="txtOptionValueBarcode' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Barcode" /></td>';
            var td3 = '<td><input id="txtOptionValueQuantity' + i + '" type="text" name="ProductOptionValues[' + i + '].Quantity"  type="number" style="width: 100%;" /></td>';
            var td4 = '<td> <input id="txtOptionValueRetailPrice' + i + '" name="ProductOptionValues[' + i + '].RetailPrice" type="number" style="width: 100%;" /> </td>';

            $("#tblVariants tbody").append(
            '<tr id="rowOptionValue' + i + '">' + td1 + td2 + td3 + td4 + '</tr>'
            );

            $("#txtOptionValueRetailPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });

            $("#txtOptionValueQuantity" + i).kendoNumericTextBox({
                format: "#",
                value: 0,
                decimals: 0,
            });
            //$("#txtOptionValueBarcode" + i).kendoTextBox();
        }
    }

    function refreshVariantTable() {

        $("#tblVariants > thead").html("");
        $("#tblVariants > tbody").html("");

        var options = $("#listOptionOneValue").tagsinput('items');

        var th1 = '<th class="col-md-3" style="text-align:center"><i class="fa fa-cube"></i>&nbsp;&nbsp;&nbsp;Variant </th>';
        var th2 = ' <th class="col-md-3" style="text-align:center"><i class="fa fa-barcode"></i>&nbsp;&nbsp;&nbsp;Barcode </th>';
        var th3 = '   <th class="col-md-3" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Retail Price </th>';

        $("#tblVariants thead").append(
         '<tr class="gray-bg">' + th1 + th2 + th3 + '</tr>'
         );
        

        for (var i = 0; i < options.length; i++) {
            var td1 = '<td><input id="lblOptionValueName' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Name" value="' + options[i] + '" readonly style="text-align:center;" /></td>';
            var td2 = '<td><input id="txtOptionValueBarcode' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Barcode" /></td>';
            var td3 = '<td> <input id="txtOptionValueRetailPrice' + i + '" name="ProductOptionValues[' + i + '].RetailPrice" type="number" style="width: 100%;" /> </td>';

            $("#tblVariants tbody").append(
            '<tr id="rowOptionValue' + i + '">' + td1 + td2 + td3 + '</tr>'
            );

            $("#txtOptionValueRetailPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });



        }
    }
    
    function refreshVariantTableHasInventoryMemberPrice() {
        $("#tblVariants > thead").html("");
        $("#tblVariants > tbody").html("");

        var options = $("#listOptionOneValue").tagsinput('items');


        var th1 = '<th class="col-md-2" style="text-align:center"><i class="fa fa-cube"></i>&nbsp;&nbsp;&nbsp;Variant</th>';
        var th2 = ' <th class="col-md-2" style="text-align:center"><i class="fa fa-barcode"></i>&nbsp;&nbsp;&nbsp;Barcode</th>';
        var th3 = '   <th class="col-md-2" style="text-align:center"><i class="fa fa-cubes"></i>&nbsp;&nbsp;&nbsp;Quantity</th>';
        var th4 = '   <th class="col-md-2" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Retail Price</th>';
        var th5 = '   <th class="col-md-2" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Member Price</th>';


        $("#tblVariants thead").append(
         '<tr class="gray-bg">' + th1 + th2 + th3 + th4 + th5 +'</tr>'
         );

        for (var i = 0; i < options.length; i++) {
            var td1 = '<td><input id="lblOptionValueName' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Name" value="' + options[i] + '" readonly style="text-align:center;" /></td>';
            var td2 = '<td><input id="txtOptionValueBarcode' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Barcode" /></td>';
            var td3 = '<td><input id="txtOptionValueQuantity' + i + '" type="text" name="ProductOptionValues[' + i + '].Quantity"  type="number" style="width: 100%;" /></td>';
            var td4 = '<td> <input id="txtOptionValueRetailPrice' + i + '" name="ProductOptionValues[' + i + '].RetailPrice" type="number" style="width: 100%;" /> </td>';
            var td5 = '<td> <input id="txtOptionValueMemberPrice' + i + '" name="ProductOptionValues[' + i + '].MemberPrice" type="number" style="width: 100%;" /> </td>';



            $("#tblVariants tbody").append(
            '<tr id="rowOptionValue' + i + '">' + td1 + td2 + td3 + td4 + td5 + '</tr>'
            );

            $("#txtOptionValueRetailPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });

            $("#txtOptionValueMemberPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });

            $("#txtOptionValueQuantity" + i).kendoNumericTextBox({
                format: "#",
                value: 0,
                decimals: 0,
            });
        }
    }

    function refreshVariantTableHasMemberPrice() {
        $("#tblVariants > thead").html("");
        $("#tblVariants > tbody").html("");

        var options = $("#listOptionOneValue").tagsinput('items');


        var th1 = '<th class="col-md-3" style="text-align:center"><i class="fa fa-cube"></i>&nbsp;&nbsp;&nbsp;Variant</th>';
        var th2 = ' <th class="col-md-3" style="text-align:center"><i class="fa fa-barcode"></i>&nbsp;&nbsp;&nbsp;Barcode</th>';
        var th3 = '   <th class="col-md-3" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Retail Price</th>';
        var th4 = '   <th class="col-md-3" style="text-align:center"><i class="fa fa-money"></i>&nbsp;&nbsp;&nbsp;Member Price</th>';


        $("#tblVariants thead").append(
         '<tr class="gray-bg">' + th1 + th2 + th3 + th4 + '</tr>'
         );

        for (var i = 0; i < options.length; i++) {
            var td1 = '<td><input id="lblOptionValueName' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Name" value="' + options[i] + '" readonly style="text-align:center;" /></td>';
            var td2 = '<td><input id="txtOptionValueBarcode' + i + '" type="text" class="input form-control" name="ProductOptionValues[' + i + '].Barcode" /></td>';
            var td3 = '<td> <input id="txtOptionValueRetailPrice' + i + '" name="ProductOptionValues[' + i + '].RetailPrice" type="number" style="width: 100%;" /> </td>';
            var td4 = '<td> <input id="txtOptionValueMemberPrice' + i + '" name="ProductOptionValues[' + i + '].MemberPrice" type="number" style="width: 100%;" /> </td>';



            $("#tblVariants tbody").append(
            '<tr id="rowOptionValue' + i + '">' + td1 + td2 + td3 + td4 + '</tr>'
            );

            $("#txtOptionValueRetailPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });

            $("#txtOptionValueMemberPrice" + i).kendoNumericTextBox({
                format: "c",
                value: 0,
                decimals: 2
            });

        }
    }

    function ddlProductOption_databound(e) {
        if (this.dataSource.data().length == 0) {
            this.element.removeAttr("required");
        }
    }

    function resetTagsInput() {
        $("#listOptionOneValue").tagsinput('removeAll');
        $("#listOptionOneValue").tagsinput('refresh');
    }

    function optionValueAdded(event) {
        console.log("option value added");
        $("#VariantContainer").removeClass("hidden");
        console.log('item added : ' + event.item);
        console.log(event);
        var optionValue = event.item;

        refreshVariantGrid();
    
    }


    function HasMemberPrice_toggle(state) {
        
        var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

        if (state == "enable") {
            $("#HasMemberEnable").removeClass("btn btn-default");
            $("#HasMemberEnable").addClass("btn btn-primary");

            $("#HasMemberDisable").removeClass("btn btn-primary");
            $("#HasMemberDisable").addClass("btn btn-default");

            txtMemberPrice.value(0);
            txtMemberPrice.enable();
            $("#hiddenHasMemberPrice").val(true);

            console.log("toggle state:" + $("#hiddenHasMemberPrice").val());

        }
        if (state == "disable") {
            resetHasMemberPrice();
        }


    }

    function ddlVariantHasMemberPrice_change(e) {

        refreshVariantGrid();

        var HasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList").value();

        if (HasMemberPrice == 0){
            $("#hiddenHasMemberPrice").val(false);

        }
        if (HasMemberPrice == 1) {
            $("#hiddenHasMemberPrice").val(true);

        }
    }
    

    function resetHasMemberPrice() {

        var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

        $("#HasMemberDisable").removeClass("btn btn-default");
        $("#HasMemberDisable").addClass("btn btn-primary");

        $("#HasMemberEnable").removeClass("btn btn-primary");
        $("#HasMemberEnable").addClass("btn btn-default");

        txtMemberPrice.value(0);
        txtMemberPrice.enable(false);
        $("#hiddenHasMemberPrice").val(false);

        var ddlHasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList");
        ddlHasMemberPrice.value(0);


    }

    function refreshVariantGrid() {

        var VariantInventoryOption = $("#VariantInventoryOptionId").data("kendoDropDownList").value();

        var HasMemberPrice = $("#VariantHasMemberPrice").data("kendoDropDownList").value();

        if ((VariantInventoryOption == 0) && (HasMemberPrice == 0)){
            refreshVariantTableInventory();
        }

        if ((VariantInventoryOption == 0) && (HasMemberPrice == 1)) {
            
            refreshVariantTableHasInventoryMemberPrice();
        }

        if ((VariantInventoryOption == 1) && (HasMemberPrice == 0)) {
            refreshVariantTable();
        }

        if ((VariantInventoryOption == 1) && (HasMemberPrice == 1)) {
            refreshVariantTableHasMemberPrice();
        }

    }

    $(document).ready(function () {

        var hiddenHasMemberPrice = $("#hiddenHasMemberPrice").val(); 

        if (($("#hiddenHasVariant").val() == "False") || ($("#hiddenHasVariant").val() == false)) {

            console.log("no variant");

        if ((hiddenHasMemberPrice == "False") || (hiddenHasMemberPrice == "false")) {

            var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

            txtMemberPrice.enable(false);

            $("#HasMemberDisable").removeClass("btn btn-default");
            $("#HasMemberDisable").addClass("btn btn-primary");

            $("#HasMemberEnable").removeClass("btn btn-primary");
            $("#HasMemberEnable").addClass("btn btn-default");
        }
    
        if ((hiddenHasMemberPrice == "True") || (hiddenHasMemberPrice == "true")) {


            var txtMemberPrice = $("#MembersPrice").data("kendoNumericTextBox");

            txtMemberPrice.enable();

            $("#HasMemberEnable").removeClass("btn btn-default");
            $("#HasMemberEnable").addClass("btn btn-primary");

            $("#HasMemberDisable").removeClass("btn btn-primary");
            $("#HasMemberDisable").addClass("btn btn-default");
            }
        }
    });