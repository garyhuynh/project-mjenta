﻿function iconExpand_click(index, id) {
   
    $('#iconExpand_' + id).addClass("hidden");
    $('#iconCollapse_' + id).removeClass("hidden");

    var table = document.getElementById("tblProduct");

    var row = table.insertRow(index + 1);
    row.id = "tblRowProduct_" + id;

    var cell = row.insertCell(0);
    cell.colSpan = 8;
    cell.id = "tblCellProduct_" + id;

    var cellHtml = '<div id="ProductDetailContainer_' + id + '"> </div>';
    cell.innerHTML = cellHtml;

    $('#tblRowProduct_' + id).addClass("hidden");

    var model = {
        Id: id
    };

    $.ajax({
        type: "POST",
        load: $('#ProductDetailContainer_' + id).load("/Product/RenderPartial_ViewProductVariant", model),
        success: function () {

            $('#tblRowProduct_' + id).removeClass("hidden");

        }
    });
}

function iconCollapse_click(index, id) {

    $('#iconExpand_' + id).removeClass("hidden");
    $('#iconCollapse_' + id).addClass("hidden");

    var table = document.getElementById("tblProduct");
    var row = table.deleteRow(index + 1);
}

function ProductFilter_changed(e) {
    
    $("#formProductSearch").submit();

}

function pagination_clicked(pageIndex) {
    console.log(pageIndex);
    console.log($("#PageIndex").val());
    console.log($("#PageIndex").val(pageIndex-1));
    $("#formProductSearch").submit();
}

$(document).ready(function () {
    $("[rel=tooltip]").tooltip({ placement: 'right' });
});