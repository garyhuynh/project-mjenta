﻿function scheduler_databound(e) {
    {
        //remove date row
        $(".k-scheduler-layout tr:first .k-scheduler-table")
          .not(".k-scheduler-header-date")
          .find("tr:eq(1)")
          .hide();
    }
}

function combobox_databound(e) {
    {
        var value = $('#Member').data('kendoComboBox').text();
        if (value == 0) {
            $("#Member").data().kendoComboBox.text('');
        }
    }
}

function plusbutton_click(e) {
    var endTime = $('#end_timepicker').data('kendoTimePicker');

    var newTime = new Date(endTime.value().getTime() + 30 * 60000);

    endTime.value(newTime);
    endTime.trigger("change");


}

function minusbutton_click(e) {

    var startTime = $('#start_timepicker').data('kendoTimePicker');
    var startDateTime = new Date(startTime.value().getTime());

    var endTime = $('#end_timepicker').data('kendoTimePicker');
    var endDateTime = new Date(endTime.value().getTime());

    var newTime = new Date(endDateTime - 30 * 60000);

    var strStartDateTime = startDateTime.toString();
    var strEndDateTime = endDateTime.toString();
    var strNewDateTime = newTime.toString();

    if (strNewDateTime != strStartDateTime) {
        endTime.value(newTime);
        endTime.trigger("change");
    }
}

function browser_resize() {

    var h = window.innerHeight;

    var scheduler = $("#Scheduler").data("kendoScheduler");
    scheduler.element.height(h - 125);
    scheduler.refresh();
}

$(document).ready(function () {

    var h = window.innerHeight;
    var scheduler = $("#Scheduler").data("kendoScheduler");
    scheduler.element.height(h - 125);
    scheduler.refresh();

});

function scheduler_sync(e) {
    var scheduler = $("#Scheduler").data("kendoScheduler");
    scheduler.dataSource.read();
}

function scheduler_navigate(e) {
    setTimeout(function () {
        var scheduler = $("#Scheduler").data("kendoScheduler");
        scheduler.refresh();
    }, 500);
    clearTimeout(setTimeout);
}

function scheduler_change(e) {
    $("#Scheduler").removeClass('k-state-selected');

}

function scheduler_edit(e) {
    {
        if (e.event.isNew()) {

            //get court from dropdown
            var court = $('#court_dropdown').data('kendoDropDownList').value();

            //set court in multi select
            var multiselect = $("#court_multiselect").data("kendoMultiSelect");
            multiselect.value(court);
            multiselect.trigger("change");

            //set default number of players
            var multiselect = $("#numberofplayers").data("kendoDropDownList");
            multiselect.value(2);

            var startTimePicker = $('#start_timepicker').data('kendoTimePicker');
            startTimePicker.readonly();

            //set end time to one hour interval
            var endTime = $('#end_timepicker').data('kendoTimePicker');

            var newTime = new Date(endTime.value().getTime() + 30 * 60000);
            endTime.value(newTime);
            endTime.trigger("change");
        }
        if (!e.event.isNew()) {
       

            GetUserDetails(e.event.User);
            GetMembership(e.event.User);
            GetBookingDetails(e.event.Id);

        }

    }
}

function scheduler_move(e) {
    if (roomIsOccupied(e.start, e.end, e.event, e.resources)) {
        this.wrapper.find(".k-event-drag-hint").addClass("invalid-slot");
    }
}

function scheduler_moveEnd(e) {

    if (!checkAvailability(e.start, e.end, e.event, e.resources)) {
        e.preventDefault();
    }
}

function scheduler_resize(e) {
    if (roomIsOccupied(e.start, e.end, e.event, e.resources)) {
        this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
        e.preventDefault();
    }
}

function scheduler_resizeEnd(e) {

    if (!checkAvailability(e.start, e.end, e.events)) {
        e.preventDefault();
    }
}

function scheduler_save(e) {

    if (e.event.isNew()) {

        var multiselect = $("#court_multiselect").data("kendoMultiSelect").value();
        var count = multiselect.length;
        
        if (count > 1) {
            
            var scheduler = $("#Scheduler").data("kendoScheduler");
            var events = scheduler.occurrencesInRange(e.event.start, e.event.end);
            var idx = events.indexOf(e.event);
            if (idx > -1) {
                events.splice(idx, 1);
            }
            var eventCount = events.length;
            
            if (events.length > 1) {

                for (var i = 0; i < events.length; i++)
                {
                    var test = events[i].Court;
                    if (multiselect.indexOf(test) > -1) {
                        alert("This court is not available in this time....");
                        e.preventDefault();
                        return;
                    }
                }
           
            }
        }
        else {
            if (!checkAvailability(e.event.start, e.event.end, e.event)) {
                e.preventDefault();
            }
        }
    } 
}

function scheduler_add(e) {
    if (!checkAvailability(e.event.start, e.event.end, e.event)) {
        e.preventDefault();
    }
}

function occurrencesInRangeByResource(start, end, resourceFieldName, event, resources) {

    //get all bookings



    var scheduler = $("#Scheduler").getKendoScheduler();
    
    var occurrences = scheduler.occurrencesInRange(start, end);

    var idx = occurrences.indexOf(event);
    if (idx > -1) {
        occurrences.splice(idx, 1);
    }

    event = $.extend({}, event, resources);

    return filterByResource(occurrences, resourceFieldName, event[resourceFieldName]);
}

function filterByResource(occurrences, resourceFieldName, value) {
    var result = [];
    var occurrence;

    for (var idx = 0, length = occurrences.length; idx < length; idx++) {
        occurrence = occurrences[idx];
        var resourceValue = occurrence[resourceFieldName];

        if (resourceValue === value) {
            result.push(occurrence);
        } else if (resourceValue instanceof kendo.data.ObservableArray) {
            if (value) {
                for (var i = 0; i < value.length; i++) {
                    if (resourceValue.indexOf(value[i]) != -1) {
                        result.push(occurrence);
                        break;
                    }
                }
            }
        }
    }
    return result;
}

function roomIsOccupied(start, end, event, resources) {
    var occurrences = occurrencesInRangeByResource(start, end, "Court", event, resources);
    if (occurrences.length > 0) {
        return true;
    }
    return false;
}

function checkAvailability(start, end, event, resources) {
    
    if (roomIsOccupied(start, end, event, resources)) {
        setTimeout(function () {
            alert("This court is not available in this time.");
        }, 0);

        return false;
    }

    return true;
}

function checkCourtAvailability(start, end, event, resources) {

    console.log("check court availability");
    console.log(start, end, event, resources);

    if (courtIsOccupied(start, end, event, resources)) {
        setTimeout(function () {
            alert("This court is not available in this time.........");
        }, 0);

        return false;
    }

    return true;
}

function courtIsOccupied(start, end, event, resources) {

    console.log("check court is occupied");

    var occurrences = occurrencesInRangeByResource(start, end, "Courts", event, resources);
    if (occurrences.length > 0) {
        console.log(occurrences);
        return true;
    }
    return false;
}

function GetCurrentBookings(start) {

    var date = new Date(parseInt(start.substr(6)));

    alert(date);
}