﻿$(document).ready(function () {

    $("#txtSearchCustomer").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#txtSearchCustomer").blur();
            GetCustomerSearch();
        }
    });

    if (location.hash) {

        console.log("location has hash!");

        $('a[href=' + location.hash + ']').tab('show');

        if (location.hash == "#Bookings") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerBookingContainer").load("/Customer/RenderPartial_CustomerBooking", { customerId: customerId })
            });
        }

        if (location.hash == "#Registrations") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerRegistrationContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }

        if (location.hash == "#Accounts") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerAccountContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }

        if (location.hash == "#Memberships") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerMembershipsContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }

        if (location.hash == "#Invoices") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerInvoicesContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }

        if (location.hash == "#Communication") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerCommunicationContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }

        if (location.hash == "#Notes") {
            var customerId = $("#hiddenCustomerId").val();

            $.ajax({
                type: "GET",
                load: $("#CustomerNotesContainer").load("/Customer/RenderPartial_CustomerBookings", { customerId: customerId })
            });
        }


    }

    $(document.body).on("click", "a[data-toggle]", function (event) {
        location.hash = this.getAttribute("href");
    });

});

$(window).on('popstate', function () {
    var anchor = location.hash || $("a[data-toggle=tab]").first().attr("href");
    $('a[href=' + anchor + ']').tab('show');
});

$(function () {

    $(".tab1 a").click(function (e) {


        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab2 a").click(function (e) {

        console.log("booking tab clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerBookingContainer").load("/Customer/RenderPartial_CustomerBooking", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab3 a").click(function (e) {

        console.log("booking registration clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerRegistrationContainer").load("/Customer/RenderPartial_CustomerRegistration", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab4 a").click(function (e) {

        console.log("booking accounts clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerAccountContainer").load("/Customer/RenderPartial_", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab5 a").click(function (e) {

        console.log("booking memberships clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerMembershipsContainer").load("/Customer/RenderPartial_", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab6 a").click(function (e) {

        console.log("booking invoices clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerInvoicesContainer").load("/Customer/RenderPartial_", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab7 a").click(function (e) {

        console.log("booking communication clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerCommunicationContainer").load("/Customer/RenderPartial_", { customerId: customerId })
        });
        history.pushState(null, null, $(this).attr('href'));
    });

    $(".tab8 a").click(function (e) {

        console.log("booking notes clicked");
        var customerId = $("#hiddenCustomerId").val();

        $.ajax({
            type: "GET",
            load: $("#CustomerNotesContainer").load("/Customer/RenderPartial_", { customerId: customerId })
        });

        history.pushState(null, null, $(this).attr('href'));
    });


});


function btnCustomerSearch_click() {
    GetCustomerSearch();
}

function GetCustomerSearch() {
    var searchValue = $("#txtSearchCustomer").val();
    
    $("#CustomerListContainer").empty();
    $.ajax({
        type: "GET",
        load: $("#CustomerListContainer").load("/Customer/RenderPartial_CustomerList", { searchValue: searchValue })
    });

}

function btnCustomerDetail_click(customerId) {
    console.log(customerId);

    $("#rowCustomerIcon").hide();

    $.ajax({
        type: "GET",
        load: $("#CustomerDetailContainer").load("/Customer/RenderPartial_CustomerDetail", { customerId: customerId })
    });
}

function submit_click() {
    $("#btnSubmit").addClass("disabled");
}

function pagination_clicked(pageNumber) {

    var pageIndex = parseInt(pageNumber) - 1;

    var searchValue = $("#txtSearchCustomer").val();


    console.log("New Page Index: " + pageIndex);

    $("#CustomerListContainer").empty();
    $.ajax({
        type: "GET",
        load: $("#CustomerListContainer").load("/Customer/RenderPartial_CustomerList", { pageIndex: pageIndex, searchValue: searchValue })
    });
}

function CustomerDetailFormSubmitSuccess(data) {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
}

function BookingType_change(e) {

    var dataItem = e.sender.dataItem();

    var bookingTypeId = dataItem.Id;

    if (bookingTypeId == '') {
        bookingTypeId = 0;
    }

    var customerId = $("#hiddenCustomerId").val();

    $.ajax({
        type: "GET",
        load: $("#CustomerBookingListContainer").load("/Customer/RenderPartial_CustomerBookingList", { customerId: customerId, bookingTypeId: bookingTypeId })
    });


}