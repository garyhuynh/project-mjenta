﻿
function addcourt_clicked(eventBookingId, eventDate) {

    $("#ViewClassModal").modal("hide");

    $("#hiddenAddCourt").val(1);
    $("#hiddenEventBookingId").val(eventBookingId);



    $("#ViewClassContainer").empty();

    $.ajax({
        type: "POST",
        load: $("#add-court-container").load("/Booking/RenderPartial_Alert_AddCourt", { eventBookingId: eventBookingId })
    });

    console.log("change to date: " + new Date(eventDate));

    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    scheduler.date(new Date(eventDate));

    var h = window.innerHeight;
    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    scheduler.element.height(h - 195);
    scheduler.refresh();

}

function addcourt_cancel() {

    $("#hiddenAddCourt").val(0);
    var h = window.innerHeight;
    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    scheduler.element.height(h - 125);
    scheduler.refresh();
};

function addcourt_save() {

    console.log("save added court");

    $("#add-court-save").addClass("disabled");

    var start = parseInt($("#hiddenStart").val());
    var end = parseInt($("#hiddenEnd").val());
    var strCourts = $('#SelectedCourts').data('kendoMultiSelect').value().toString();
    var eventBookingId = $("#hiddenEventBookingId").val();
    var courts = strCourts.split(',');

    var startTime = new Date(start);
    var endTime = new Date(end);

    var startUTC = convertDateToUTC(startTime);
    var endUTC = convertDateToUTC(endTime);

    var available = true;

    if (checkCourtAvailability(startUTC, endUTC, courts) == true) {

        available = false;

        setTimeout(function () {
            alert("Selected court(s) is not available in this time...");
        }, 0);

        $("#add-court-save").removeClass("disabled");
    }

    if (available) {

        var model = {
            EventBookingId: eventBookingId,
            longStart: start,
            longEnd: end,
            strCourts: strCourts
        };

        console.log(model);

        var result = false;
        var hubStart = {};
        var bookingIds = new Array();

        $.connection.bookingHub.server.addCourt(model).done(function (bookings) {
            $.each(bookings, function () {
                bookingIds.push(this.Id);
            });

            //var scheduler = $("#BookingScheduler").data("kendoScheduler");
            //scheduler.dataSource.read();

            $("#AddCourtModal").modal("hide");
            $("#hiddenAddCourt").val(0);
            $("#add-court-container").empty();

            setTimeout(function () {
                var h = window.innerHeight;
                var scheduler = $("#BookingScheduler").data("kendoScheduler");
                scheduler.element.height(h - 125);
                scheduler.dataSource.read();
            }, 500);
            clearTimeout(setTimeout);

        });

        hubStart = $.connection.hub.start();



    }
}