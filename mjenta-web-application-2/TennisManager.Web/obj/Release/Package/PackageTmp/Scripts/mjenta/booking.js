﻿$(function () {
    $('#BookingModal').on('hidden.bs.modal', function (e) {
        $("#BookingContainer").empty();
    });

    $('#ViewBookingModal').on('hidden.bs.modal', function (e) {
        $("#ViewBookingContainer").empty();
        $("#BalancesContainer").empty();
        $("#CancelContainer").empty();
    });

    $('#modalCustomerSearch').on('show.bs.modal', function (e) {
        console.log("register customer popup shown!");
    });

});

$(window).resize(function () {

    console.log("resizing browser");

    var addCourtStatus = $("#hiddenAddCourt").val();

    if (addCourtStatus == "1") {
        setTimeout(function () {
            var h = window.innerHeight;
            var scheduler = $("#BookingScheduler").data("kendoScheduler");
            scheduler.element.height(h - 195);
            scheduler.dataSource.read();
        }, 500);
        clearTimeout(setTimeout);
    }
    else {
        setTimeout(function () {
            var h = window.innerHeight;
            var scheduler = $("#BookingScheduler").data("kendoScheduler");
            scheduler.element.height(h - 125);
            scheduler.dataSource.read();
        }, 500);
        clearTimeout(setTimeout);
    }






});


function btnBookingType_click(bookingType) {

    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    scheduler.dataSource.read();

    if (bookingType == 'Booking') {

        $("#hiddenSearchOption").val("casual_booking");

        $.ajax({
            type: "POST",
            load: $("#BookingContainer").load("/Booking/RenderPartial_Booking_1")
        });
    }

}

$(document).on('keyup', '#txtCustomerSearch', function (e) {
    if (e.keyCode == 13) {
        $("#txtCustomerSearch").blur();
        customerSearch_click();
    }

});

$(document).on('keyup', '#message-text-area', function (e) {

    var maxLength = 160;
    var textLength = $('#message-text-area').val().length;
    var remainingText = maxLength - textLength;
    $('#text-counter').html(remainingText + ' characters remaining');
});


function customerSearch_click() {

    var search = $("#txtCustomerSearch").val();

    $("#UserIconContainer").hide();

    $.ajax({
        type: "POST",
        load: $("#CustomerListContainer").load("/Booking/RenderPartial_CustomerList", { search: search })
    });
}

function btnCustomer_click(customerId) {

    var searchOption = $("#hiddenSearchOption").val();

    if (searchOption == "class_register") {

        //this is empty!
        var eventBookingId = $("#hiddenEventBookingId").val();

        RegisterCustomerToClass(customerId, eventBookingId);

        return;
    }
    if (searchOption == "class_waitlist") {

        var eventBookingId = $("#hiddenEventBookingId").val();

        WaitlistCustomerToClass(customerId, eventBookingId);
    }
    if (searchOption == "casual_booking")
        {
        $("#hiddenCustomerId").val(customerId);

        var courtId = $("#hiddenCourtId").val();
        var start = $("#hiddenStart").val();
        var offset = $("#hiddenOffset").val();

        var model = {
            CourtId: courtId,
            CustomerId: customerId,
            longStart: start,
            Offset: offset
        };

        $.ajax({
            type: "POST",
            load: $("#BookingContainer").load("/Booking/RenderPartial_Booking_2", model)
        });
    }
}

function BookingType_change(e) {

    var dataItem = e.sender.dataItem();
    var BookingType = dataItem.Name;

    var txtPrice = $("#Price").data("kendoNumericTextBox");
    var txtQuantity = $("#NumberOfPlayers").data("kendoDropDownList");



    if (BookingType == 'Casual Booking') {
        $("#RecurrenceGroup").hide();
        $("#CoachGroup").hide();
        txtQuantity.value(2);
        txtPrice.enable(false);
        CalculateBookingPrice();
    }
    if (BookingType == 'Recurring Booking') {
        $("#RecurrenceGroup").show();
        $("#CoachGroup").hide();
        txtQuantity.value(2);
        txtPrice.value(0);
        txtPrice.enable(false);
        CalculateBookingPrice();
    }
    if (BookingType == 'Private Lesson') {
        $("#RecurrenceGroup").hide();
        $("#CoachGroup").show();

        txtPrice.enable(true);
        txtPrice.value(0);
        txtQuantity.value(1);
    }

}

function endtime_changed() {

    var start = parseInt($("#hiddenStart").val());
    var startTime = new Date(start);

    var endTime = this.value();
    var endTimeUTC = Date.UTC(startTime.getFullYear(), startTime.getMonth(), startTime.getDate(), endTime.getHours(), endTime.getMinutes(), 0, 0);
    $("#hiddenEnd").val(endTimeUTC);

    CalculateBookingPrice();
}

function courts_changed() {

    var courtCount = this.value().length;

    if (courtCount == 0) {
        this.value($("#hiddenCourtId").val());
    }
    CalculateBookingPrice();
}

function occurences_changed() {

    if (this.value() == null){
        var txtOccurences = $("#Occurrences").data("kendoNumericTextBox");
        txtOccurences.value(1);
    }
    console.log($("#BookingTypeId").data("kendoDropDownList").text());

    CalculateBookingPrice();
}


function CalculateBookingPrice() {

    var bookingType = $("#BookingTypeId").data("kendoDropDownList").text();

    if (bookingType == 'Casual Booking') {

        CalculateCasualBookingPrice();
    }

    if (bookingType == 'Recurring Booking') {

        CalculateRecurringBookingPrice();
    }
}


function CalculateCasualBookingPrice() {

    var start = parseInt($("#hiddenStart").val());
    var end = parseInt($("#hiddenEnd").val());
    var customerId = $("#hiddenCustomerId").val();
    var strCourts = $('#CourtIds').data('kendoMultiSelect').value().toString();


    var model = {
        CustomerId: customerId,
        strCourts: strCourts,
        longStart: start,
        longEnd: end
    };

    console.log(model);

    $.ajax({
        url: '/Booking/CalculateCasualBookingPrice',
        type: 'GET',
        dataType: 'json',
        data: model,
        cache: false,
        async: false,
        success: function (price) {

            var txtPrice = $("#Price").data("kendoNumericTextBox").value(price);

        },
    });
}

function CalculateRecurringBookingPrice() {

    var start = parseInt($("#hiddenStart").val());
    var end = parseInt($("#hiddenEnd").val());
    var customerId = $("#hiddenCustomerId").val();
    var strCourts = $('#CourtIds').data('kendoMultiSelect').value().toString();
    var occurences = $('#Occurrences').data('kendoNumericTextBox').value()

    var model = {
        CustomerId: customerId,
        strCourts: strCourts,
        longStart: start,
        longEnd: end,
        Occurrences: occurences
    };

    console.log(model);

    $.ajax({
        url: '/Booking/CalculateRecurringBookingPrice',
        type: 'GET',
        dataType: 'json',
        data: model,
        cache: false,
        async: false,
        success: function (price) {

            var txtPrice = $("#Price").data("kendoNumericTextBox").value(price);

        },
    });

}

function btnBookNow_click() {

    $("#btnBookNow").addClass("disabled");

    CreateBooking();
}

function CreateBooking() {

    var bookingTypeId = $("#BookingTypeId").data("kendoDropDownList").value();
    var start = parseInt($("#hiddenStart").val());
    var end = parseInt($("#hiddenEnd").val());
    var customerId = $("#hiddenCustomerId").val();
    var strCourts = $('#CourtIds').data('kendoMultiSelect').value().toString();
    var occurences = $('#Occurrences').data('kendoNumericTextBox').value()
    var coachId = $("#CoachId").data("kendoDropDownList").value();
    var numberOfPlayers = $("#NumberOfPlayers").data("kendoDropDownList").value();
    var price = $("#Price").data("kendoNumericTextBox").value();


    var courts = strCourts.split(',');
    var startTime = new Date(start);
    var endTime = new Date(end);

    var startUTC = convertDateToUTC(startTime);
    var endUTC = convertDateToUTC(endTime);

    var addDays = 0;

    var availabilityStatus = true;

    if (occurences > 1) {
        if (checkRecurrenceAvailability(startUTC, endUTC, occurences, courts) == true) {

            availabilityStatus = false;

            setTimeout(function () {
                alert("This court is not available in the following occurrence...");
            }, 0);

            $("#btnBookNow").removeClass("disabled");
        }
    }

    if (occurences == 1) {
        if (checkCourtAvailability(startUTC, endUTC, courts) == true) {

            availabilityStatus = false;

            setTimeout(function () {
                alert("This court is not available in this time...");
            }, 0);

            $("#btnBookNow").removeClass("disabled");
        }
    }

    if (availabilityStatus == true) {

        var booking = {
            BookingTypeId: bookingTypeId,
            longStart: start,
            longEnd: end,
            CustomerId: customerId,
            strCourts: strCourts,
            Occurrences: occurences,
            CoachId: coachId,
            NumberOfPlayers: numberOfPlayers,
            Price: price
        };

        var result = false;
        var hubStart = {};
        var bookingIds = new Array();

        $.connection.bookingHub.server.create(booking).done(function (bookings) {
            $.each(bookings, function () {
                bookingIds.push(this.Id);
            });

            console.log(bookingIds);

            var model = {
                strBookingIds: bookingIds.join(),
                CustomerId: customerId
            };

            $.ajax({
                type: "POST",
                load: $("#BookingContainer").load("/Booking/RenderPartial_Booking_3", model)
            });
        });

        hubStart = $.connection.hub.start();

        var scheduler = $("#BookingScheduler").data("kendoScheduler");
        scheduler.dataSource.read();
    }

}


function redirectToSale(data) {


    if (data.RedirectUrl) {

        console.log(data.RedirectUrl);

        window.location.href = data.RedirectUrl;

    }
}

function convertDateToUTC(date) {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
}

function addDaystoDate(date, days) {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + days, date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
}


function showOutstandingBalances(customerId, bookingId) {

    $.ajax({
        type: "POST",
        load: $("#BalancesContainer").load("/Booking/RenderPartial_ViewOutstandingBalance", { customerId: customerId, bookingId: bookingId })
    });
    $("#ViewBookingContainer").hide();
    $("#BalancesContainer").show();

}

function CancelBooking(bookingId) {

    $.ajax({
        type: "POST",
        load: $("#CancelContainer").load("/Booking/RenderPartial_CancelBookng", { bookingId: bookingId })
    });
    $("#ViewBookingContainer").hide();
    $("#CancelContainer").show();

}

function OutstandingBalance_back() {
    $("#BalancesContainer").hide();
    $("#CancelContainer").hide();
    $("#ViewBookingContainer").show();
}

function CancelBooking_back() {
    $("#BalancesContainer").hide();
    $("#CancelContainer").hide();
    $("#ViewBookingContainer").show();
}

function ProcessClassDetailForm_success(data) {
    console.log("process class step 1");
}

function ProcessClassScheduleForm_success(data) {
    console.log("process class step 2");
}

function classdate_selected(e) {

    var dataValue = (this.value().getFullYear() + "/" + this.value().getMonth() + "/" + this.value().getDate()).toString();

    var state = $('a[data-value="' + dataValue + '"]').closest("td[role=gridcell]").hasClass('k-class-date-selected');

    console.log(state);
    if (state == true){
        $('a[data-value="' + dataValue + '"]').closest("td[role=gridcell]").removeClass('k-class-date-selected k-state-selected');
    }
    else {
        $('a[data-value="' + dataValue + '"]').closest("td[role=gridcell]").addClass('k-class-date-selected');
    }
}



function classdate_navigate() {
    console.log("calendar navigated");
}

//$(document).on('click', '.k-link', function () {


//    console.log("Date has been clicked: " + $(this).attr('data-value'));
//});

function RegisterCustomerToClass(customerId, eventBookingId) {

    console.log(customerId, eventBookingId);

    $.ajax({
        url: '/Booking/RegisterCustomerToClass',
        type: 'GET',
        dataType: 'json',
        data: { eventBookingId: eventBookingId, customerId: customerId },
        cache: false,
        async: false,
        success: function (result) {

            console.log(result);

        },
    });

    $("#class-booking-container").empty();

    $.ajax({
        type: "POST",
        load: $("#class-booking-container").load("/Booking/RenderPartial_ClassBookings", { eventBookingId: eventBookingId }, function () {
            $("li[class='list-group-item active']").ScrollTo();
        })
    });

    $("#class-registration-container").empty();

    $.ajax({
        type: "POST",
        load: $("#class-registration-container").load("/Booking/RenderPartial_ClassAttendees", { eventBookingId: eventBookingId })
    });

}


function WaitlistCustomerToClass(customerId, eventBookingId) {

    $.ajax({
        url: '/Booking/WaitlistCustomerToClass',
        type: 'GET',
        dataType: 'json',
        data: { eventBookingId: eventBookingId, customerId: customerId },
        cache: false,
        async: false,
        success: function (result) {
            console.log(result);
        },
    });

    $("#class-booking-container").empty();

    $.ajax({
        type: "POST",
        load: $("#class-booking-container").load("/Booking/RenderPartial_ClassBookings", { eventBookingId: eventBookingId }, function () {
            $("li[class='list-group-item active']").ScrollTo();
        })
    });

    $("#class-registration-container").empty();

    $.ajax({
        type: "POST",
        load: $("#class-registration-container").load("/Booking/RenderPartial_ClassAttendees", { eventBookingId: eventBookingId }, function () {

            $("a[href='#ClassWaitlist']").tab('show');

            $.ajax({
                type: "POST",
                load: $("#ClassWaitlistContainer").load("/Booking/RenderPartial_ClassWaitList", { eventBookingId: eventBookingId })
            });

        })
    });


}