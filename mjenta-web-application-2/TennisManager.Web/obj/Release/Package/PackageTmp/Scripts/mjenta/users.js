﻿function GetUserDetails(userId) {


    $.ajax({
        url: 'Booking/GetUser',
        type: 'GET',
        dataType: 'json',
        data: { userId: userId },
        cache: false,
        async: false,
        success: function (user) {
            
            document.getElementById('fullname_lbl').innerHTML = user.FullName;
            document.getElementById('pin_lbl').innerHTML = user.Pin;
            document.getElementById('email_lbl').innerHTML = user.Email;
            document.getElementById('phone_lbl').innerHTML = user.PhoneNumber;
        },
    });
    
}

function GetMembership(userId)
{
    
    $.ajax({
        url: 'Booking/GetMembership',
        type: 'GET',
        dataType: 'json',
        data: { userId: userId },
        cache: false,
        async: false,
        success: function (membership) {

            var date = new Date(parseInt(membership.End.substr(6)));
            var day = date.getDay();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            
            document.getElementById('membershipstatus_lbl').innerHTML = "Valid until " + day + "/" + month + "/" + year;

            GetMembershipType(membership.MembershipTypeId);
        },
    });
}

function GetMembershipType(membershipTypeId) {

    $.ajax({
        url: 'Booking/GetMembershipType',
        type: 'GET',
        dataType: 'json',
        data: { membershipTypeId: membershipTypeId },
        cache: false,
        async: false,
        success: function (membershipType) {

            document.getElementById('membership_lbl').innerHTML = membershipType.Name;
 
        },
    });
}

