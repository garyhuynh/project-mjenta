﻿
function GetBookingDetails(bookingId) {


    $.ajax({
        url: 'GetBooking',
        type: 'GET',
        dataType: 'json',
        data: { bookingId: bookingId },
        cache: false,
        async: false,
        success: function (booking) {

            //get booking date
            var date = new Date(parseInt(booking.Start.substr(6)));

            var day = date.getDay();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();

            //get start time
            var start = new Date(parseInt(booking.Start.substr(6)));
            var startHours = start.getHours();
            var am = true;
            if (startHours > 12) {
                am = false;
                startHours -= 12;
            }
            if (startHours == 12) {
                am = false;
            }
            if (startHours == 0) {
                startHours = 12;
            }
            var startMinutes = start.getMinutes();
            if (startMinutes == 0) {
                startMinutes = "00";
            }

            var startDisplay = startHours + ":" + startMinutes + (am ? "am" : "pm");

            //get end time
            var end = new Date(parseInt(booking.End.substr(6)));
            var endHours = end.getHours();
            var am = true;
            if (endHours > 12) {
                am = false;
                endHours -= 12;
            }
            if (endHours == 12) {
                am = false;
            }
            if (endHours == 0) {
                endHours = 12;
            }
            var endMinutes = end.getMinutes();

            if (endMinutes == 0) {
                endMinutes = "00";
            }

            var endDisplay = endHours + ":" + endMinutes + (am ? "am" : "pm");

            document.getElementById('date_lbl').innerHTML = day + "/" + month + "/" + year;
            document.getElementById('time_lbl').innerHTML = startDisplay + " - " + endDisplay;
            document.getElementById('price_lbl').innerHTML = "$" + (booking.Price).toFixed(2);
            document.getElementById('status_lbl').innerHTML = booking.isPaid;
            document.getElementById('players_lbl').innerHTML = booking.NumberOfPlayers;

            GetCourt(booking.CourtId);
            GetBookingStatus(booking.isPaid);
            GetBookingType(booking.BookingTypeId);
            GetFacility(booking.CourtId);
        },
    });

}

function GetCourt(courtId) {

    $.ajax({
        url: 'GetCourt',
        type: 'GET',
        dataType: 'json',
        data: { courtId: courtId },
        cache: false,
        async: false,
        success: function (court) {

            document.getElementById('court_lbl').innerHTML = court.Name;

        },
    });

}

function GetBookingType(bookingTypeId) {

    $.ajax({
        url: 'GetBookingType',
        type: 'GET',
        dataType: 'json',
        data: { bookingTypeId: bookingTypeId },
        cache: false,
        async: false,
        success: function (bookingType) {

            document.getElementById('bookingtype_lbl').innerHTML = bookingType.Name;

        },
    });

}

function GetFacility(courtId) {


    $.ajax({
        url: 'GetFacility',
        type: 'GET',
        dataType: 'json',
        data: { courtId: courtId },
        cache: false,
        async: false,
        success: function (facility) {

            document.getElementById('facility_lbl').innerHTML = facility.FullName;
        },
    });

}

function GetBookingStatus(isPaid) {

    if (isPaid == true) {
        document.getElementById('status_lbl').innerHTML = "Paid";
    }

    if (isPaid == false) {
        document.getElementById('status_lbl').innerHTML = "Not Paid";
    }
}

