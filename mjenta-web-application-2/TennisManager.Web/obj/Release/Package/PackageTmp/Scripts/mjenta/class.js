﻿$(function () {

    $('#modalClassDetails').on('hidden.bs.modal', function (e) {
        $("#DetailsContainer").empty();

        //remove list instances
        $("#BookingTypeId-list").remove();
        $("#SelectedCoaches-list").remove();
        $("#SelectedGenderAgeGroups-list").remove();
        $("#SelectedPlayerLevels-list").remove();
        $("#hasWaitlist-list").remove();
        $("#isOnline-list").remove();
        $("#SelectedDays-list").remove();
        $("#SelectedCourts-list").remove();
        $("#StartTime_timeview").remove();
        $("#EndTime_timeview").remove();

        //return active tab to default
    });

    $('#modalClassBookings').on('hidden.bs.modal', function (e) {
        $("#ClassBookingContainer").empty();


    });
    
});



function pricingoption_changed(e) {

    var pricingOptionId = $("#PricingOptionId").data("kendoDropDownList").value();
    RenderPricingPartial(pricingOptionId);

}

function RenderPricingPartial(pricingOptionId) {

    $("#PricingStructureContainer").empty();

    var model = {
        PricingOptionId: pricingOptionId,
    };

    $.ajax({
        type: "POST",
        load: $("#PricingContainer").load("/Class/RenderPartial_PricingOption", model)
    });



}

function isUnlimitedCapacity_changed(element){
    var txtCapacity = $("#Capacity").data("kendoNumericTextBox");

    if (element.checked) {
        txtCapacity.value(0);
        txtCapacity.enable(false);
    }
    else {
        txtCapacity.value(0);
        txtCapacity.enable(true);
    }
}


function GetFacilityName(color) {
    var colour = color.split(', ')[0];
    return colour;
}

function GetFacilityColour(color) {
    var colour = color.split(', ')[1];
    return colour;
}


function showClassBookings(eventId, eventName) {

    console.log("show bookings for event id: " + eventId);

    $.ajax({
            type: "POST",
            load: $("#BookingsContainer").load("/Class/RenderPartial_ClassBookings", { eventId: eventId })
        });

    $(".modal-header h4").html(eventName);

    $('#modalClassBookings').modal('show');

}



function showClassDetails(eventId) {

    console.log("show class details for event id: " + eventId);

    $.ajax({
        type: "POST",
        load: $("#DetailsContainer").load("/Class/RenderPartial_ClassDetails", { eventId: eventId })
    });

    $('#modalClassDetails').modal('show');

}

function EditClass_success(data) {

    console.log("save success");

    $('html, body').animate({ scrollTop: 0 }, 'fast');

    $("#BookingTypeId-list").remove();
    $("#SelectedCoaches-list").remove();
    $("#SelectedGenderAgeGroups-list").remove();
    $("#SelectedPlayerLevels-list").remove();
    $("#hasWaitlist-list").remove();
    $("#isOnline-list").remove();
    $("#SelectedDays-list").remove();
    $("#SelectedCourts-list").remove();
    $("#StartTime_timeview").remove();
    $("#EndTime_timeview").remove();



    filterList($("#hiddenPageIndex").val());

}

function EditClass_complete(data) {

    console.log("save complete");

}

function classtype_changed(e) {

    $("#search").val('');
    filterList(0);
}

function coach_changed(e) {

    $("#search").val('');
    filterList(0);
}

function day_changed(e) {

    $("#search").val('');
    filterList(0);
}

function level_changed(e) {

    $("#search").val('');
    filterList(0);
}

function filterList(pageIndex) {


    var typeId = parseInt($("#ClassTypeId").data("kendoDropDownList").value());
    var coachId = parseInt($("#CoachId").data("kendoDropDownList").value());
    var dayId = parseInt($("#DayId").data("kendoDropDownList").value());
    var levelId = parseInt($("#PlayerLevelId").data("kendoDropDownList").value());
    var searchValue = $("#search").val();


    if(isNaN(typeId)) {
        typeId = 0;
    }
    if (isNaN(coachId)) {
        coachId = 0;
    }
    if (isNaN(dayId)) {
        dayId = 0;
    }
    if (isNaN(levelId)) {
        levelId = 0;
    }



    var model = {
        BookingTypeId: typeId,
        CoachId: coachId,
        DayId: dayId,
        LevelId: levelId,
        SearchValue: searchValue,
        PageIndex: parseInt(pageIndex)
    };
    
    console.log(model);

    $.ajax({
        type: "GET",
        load: $(".ClassListContainer").load("/Class/RenderPartial_ClassList", model)
    });
}

$(document).ready(function () {
    $("#search").keyup(function (e) {
        if (e.keyCode == 13) {
            $("#search").blur();
            var typeId = parseInt($("#ClassTypeId").data("kendoDropDownList").value(0));
            var coachId = parseInt($("#CoachId").data("kendoDropDownList").value(0));
            var dayId = parseInt($("#DayId").data("kendoDropDownList").value(0));
            var levelId = parseInt($("#PlayerLevelId").data("kendoDropDownList").value(0));

            filterList(0);
        }
    });
});

function pagination_clicked(pageIndex) {
    console.log("new page number: " + pageIndex);

    $("#hiddenPageIndex").val(pageIndex - 1)

    console.log("new page index: " + $("#hiddenPageIndex").val());

    filterList($("#hiddenPageIndex").val());
}