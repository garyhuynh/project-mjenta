﻿function checkRecurrenceAvailability(start, end, occurrences, courts) {
    console.log("checking recurrence availability");
    var scheduler = $("#BookingScheduler").data("kendoScheduler");

    var epocDayRange = occurrences * 7 * 86400000;

    var recurrenceStart = new Date(start);
    recurrenceStart.setHours(0);
    recurrenceStart.setMinutes(0);
    var epochStartDate = recurrenceStart.getTime();

    var recurrenceEnd = new Date(end);
    recurrenceEnd.setHours(0);
    recurrenceEnd.setMinutes(0);
    var epochEndDate = recurrenceStart.getTime() + epocDayRange;


    var startDate = new Date(epochStartDate);
    var endDate = new Date(epochEndDate);

    var bookingsInRange = scheduler.dataSource.expand(startDate, endDate);

    var recurrenceStartTime = new Date(start);

    var occurenceStartDates = [];

    for (var i = 0; i < occurrences; i++) {

        var addDays = i * 7 * 86400000;

       occurenceStartDates.push(new Date(recurrenceStartTime.getTime() + addDays).toString());
    }

    for (var i = 0; i < bookingsInRange.length; i++) {

            var bookingIndex = occurenceStartDates.indexOf(bookingsInRange[i].start.toString());
            var courtIndex = courts.indexOf(bookingsInRange[i].CourtId.toString());

            if ((bookingIndex > -1) && (courtIndex > -1)){
                return true;
            };
    }
    return false;
}

function checkCourtAvailability(start, end, courts) {

    var scheduler = $("#BookingScheduler").getKendoScheduler();

    console.log(start, end);


    var occurrences = scheduler.occurrencesInRange(start, end);

    for (var i = 0; i < occurrences.length; i++) {
        var index = courts.indexOf(occurrences[i].CourtId.toString());

        if (index > -1) {
            return true;
        }
    }
    return false;
}

function checkAvailability(start, end, event, resources) {

    if (roomIsOccupied(start, end, event, resources)) {

        setTimeout(function () {
            alert("This court is not available in this time.");
        }, 0);

        return false;
    }

    return true;
}

function roomIsOccupied(start, end, event, resources) {
    var occurrences = occurrencesInRangeByResource(start, end, "CourtId", event, resources);
    if (occurrences.length > 0) {
        return true;
    }
    return false;
}



function occurrencesInRangeByResource(start, end, resourceFieldName, event, resources) {

    var scheduler = $("#BookingScheduler").getKendoScheduler();

    var occurrences = scheduler.occurrencesInRange(start, end);

    var idx = occurrences.indexOf(event);
    if (idx > -1) {
        occurrences.splice(idx, 1);
    }

    event = $.extend({}, event, resources);

    return filterByResource(occurrences, resourceFieldName, event[resourceFieldName]);
}

function filterByResource(occurrences, resourceFieldName, value) {

    var result = [];
    var occurrence;

    for (var idx = 0, length = occurrences.length; idx < length; idx++) {

        occurrence = occurrences[idx];
        var resourceValue = occurrence[resourceFieldName];

        if (resourceValue === value) {
            result.push(occurrence);
        } else if (resourceValue instanceof kendo.data.ObservableArray) {
            if (value) {
                for (var i = 0; i < value.length; i++) {
                    if (resourceValue.indexOf(value[i]) != -1) {
                        result.push(occurrence);
                        break;
                    }
                }
            }
        }
    }
    return result;
}

