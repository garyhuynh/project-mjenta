﻿
function GetFacilityShortName(courtId) {
    var result = null;

    $.ajax({
        url: 'GetFacility',
        type: 'GET',
        dataType: 'json',
        data: { courtId: courtId },
        cache: false,
        async: false,
        success: function (facility) {
            result = facility.ShortName;
        },
    });
    return result;
}


function GetFacilityName(color) {
    var colour = color.split(', ')[0];
    return colour;
}

function GetFacilityColour(color) {
    var colour = color.split(', ')[1];
    return colour;
}