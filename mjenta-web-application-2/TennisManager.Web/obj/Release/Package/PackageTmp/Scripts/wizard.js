﻿$(function () {
    $('#BookingModal').on('hidden.bs.modal', function (e) {
        $("#BookingContainer").empty();
        

    });
});


$(document).on('keyup', "#txtSearchPlayer", function (e) {

    if (event.keyCode == 13) {
        $("#btnSearchPlayer").click();
    }
});

function btnPlayersearch_click(model) {

    var search = $("#txtSearchPlayer").val();

    model.Search = search;


    $.ajax({
        type: "POST",
        load: $("#usersDataTable").load("/Booking/RenderPartial_CasualBooking1_1", model)
    });
}

function btnPlayerSelect_click(model, id)
{
    model.CustomerId = id;

    $.ajax({
        type: "POST",
        load: $("#BookingContainer").load("/Booking/RenderPartial_CasualBooking2", model)
    });


}

function btnBookingDetailsNext_click(model) {
   

    model.longEnd = $('#EndTimePicker').data('kendoTimePicker').value().getTime();
    model.NumberOfPlayers = $("#NumberOfPlayers").data("kendoDropDownList").value();
    model.strCourts = $('#CourtsMultiSelect').data('kendoMultiSelect').value().toString();

    var numberOfCourts = $('#CourtsMultiSelect').data('kendoMultiSelect').value().length;

    if (numberOfCourts > 1) {
        model.isMultiCourt = true;
    }
    else {
        model.isMultiCourt = false;
    }


    $.ajax({
        type: "POST",
        load: $("#BookingContainer").load("/Booking/RenderPartial_CasualBooking3", model)
    });

}

function btnBookingConfirm_click(model) {

    var start = new Date(model.longStart);
    var startHours = start.getHours() * 60;
    var startMinutes = start.getMinutes();

    var endTime = new Date(model.longEnd);
    var endHours = endTime.getHours() * 60;
    var endMinutes = endTime.getMinutes();

    var duration = ((endHours + endMinutes) - (startHours + startMinutes));

    var endUnix = model.longStart + duration * 60000;

    var end = new Date(endUnix);

    var courts = model.strCourts.split(',');


    if (checkCourtAvailability(start, end, courts) == true) {
        setTimeout(function () {
            alert("This court is not available in this time.");
        }, 0);
    }
    else {
        var booking = {
            CustomerId: model.CustomerId,
            longStart: model.longStart,
            longEnd: model.longEnd,
            Offset: model.Offset,
            Duration: duration,
            strCourts: model.strCourts,
            NumberOfPlayers: model.NumberOfPlayers,
            Price: model.Price
        }
        var result = false;
        var hubStart = {};
        var bookingIds = new Array();


        $.connection.bookingHub.server.create(booking).done(function (bookings) {
            $.each(bookings, function () {
                bookingIds.push(this.Id);
            });

            console.log(bookingIds);

            model.strBookingIds = bookingIds.join();

            console.log(model.strBookingIds);

            $.ajax({
                type: "POST",
                load: $("#BookingContainer").load("/Booking/RenderPartial_CasualBooking4", model)
            });
        });

        hubStart = $.connection.hub.start();

        var scheduler = $("#BookingScheduler").data("kendoScheduler");
        scheduler.dataSource.read();
    }
}


function btnDeleteBooking1_click(model) {

    var e = {
        Id: model.Id,
        isRecurrenceHead: false
    }

    var scheduler = $("#BookingScheduler").data("kendoScheduler");
    var booking = scheduler.dataSource.get(model.Id);

    scheduler.dataSource.remove(booking);

    $.connection.bookingHub.server.destroy(booking);

}


function checkCourtAvailability(start, end, courts) {

    var scheduler = $("#BookingScheduler").getKendoScheduler();

    var occurrences = scheduler.occurrencesInRange(start, end);

    console.log(occurrences);

    for (var i = 0; i < occurrences.length; i++) {
        var index = courts.indexOf(occurrences[i].CourtId.toString());

        if (index > -1) {
            return true;
        }
    }
    return false;
}


