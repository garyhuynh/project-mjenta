﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.Identity;
using TennisManager.Models;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Clubs;
using Bookings;
namespace TennisManager.Hubs
{

    public class BookingHub : Hub
    {
        private BookingService bookingService;

        public BookingHub()
        {
            this.bookingService = new BookingService();
        }

        public IEnumerable<BookingViewModel> Read()
        {
            return bookingService.Read();
        }

        public void Update(BookingViewModel model)
        {

            bookingService.Update(model);
            Clients.Others.update(model);

        }

        public IEnumerable<BookingViewModel> Create(BookingViewModel model)
        {

            var result = new List<BookingViewModel>();

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longEnd);
            List<int> courtIds = new List<int>(model.strCourts.Split(',').Select(int.Parse));
            model.Start = startTime;
            model.End = endTime;
            model.CourtIds = courtIds;

            result = bookingService.Insert(model);
           
            foreach(BookingViewModel m in result)
            {
                Clients.Others.create(m);
            }

            return result;
        }

        public IEnumerable<BookingViewModel> AddCourt(BookingViewModel model)
        {
            var result = new List<BookingViewModel>();

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longEnd);
            List<int> courtIds = new List<int>(model.strCourts.Split(',').Select(int.Parse));
            model.Start = startTime;
            model.End = endTime;
            model.CourtIds = courtIds;

            result = bookingService.AddCourt(model);

            foreach (BookingViewModel m in result)
            {
                Clients.Others.addCourt(m);
            }


            return result;
        }



        public void Destroy (Booking booking)
        {
            bookingService.Delete(booking);
            Clients.Others.destroy(booking);
        }

        public void LockRecord(int id)
        {
            Clients.Others.lockRecord(new
            {
                id = id
            });
        }

        public void UnlockRecord(int id)
        {
            Clients.Others.unlockRecord(new
            {
                id = id
            });
        }

    }
}