﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Members;

namespace TennisManager.Controllers
{
    public class MemberController : ApiController
    {
        public IEnumerable<Member> Get()
        {
            IEnumerable<Member> members = MemberCollection.LoadAll();

            return members;
        }

        // GET api/member/29
        public Member Get(int id)
        {
            Member member = Member.LoadById(id);
            if (member == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return member;
        }

    }
}
