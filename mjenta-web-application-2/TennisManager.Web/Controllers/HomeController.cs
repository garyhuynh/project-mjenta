﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Clubs;
using Users;
using Members;

namespace TennisManager.Controllers
{
    public class HomeController : Controller
    {
       
        

        [Authorize]
        public ActionResult Index()
        {

            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);
            ViewData["SubTitle"] = club.FullName;

            ViewData["Message"] = "Hello " + User.Identity.Name;

            return View();
        }
        [Authorize]
        public ActionResult Minor()
        {
            ViewData["SubTitle"] = "Simple example of second view";
            ViewData["Message"] = "Data are passing to view by ViewData from controller";


          
            return View();
        }
    }
}