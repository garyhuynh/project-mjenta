﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net; 
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Bookings;
using Orders;
using Users;
using Clubs;
using Rates;
using Events;
using Members;
using Communications;
using TennisManager.Models;
using Classification;
using TennisManager.Services;


namespace TennisManager.Controllers
{
    public class BookingController : Controller
    {

        private BookingService bookingService;
        private ClassService classService;
        private ContactService contactService;
        private CustomerService customerService;
        private SaleService saleService;

        public BookingController()
        {
            this.bookingService = new BookingService();
            this.classService = new ClassService();
            this.contactService = new ContactService();
            this.saleService = new SaleService();
            this.customerService = new CustomerService(_userManager, _signInManager);
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        [Authorize]
        public ActionResult Index()
        {

            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            RoleCollection roles = user.Roles;

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            List<Court> courtList = CourtCollection.LoadByClub(club).ToList();
            List<BookingType> bookingTypeList = BookingTypeCollection.LoadAll().ToList();

            ViewBag.User = user;
            ViewBag.Club = club;
            ViewBag.Courts = courtList;


            return View();
        }
       


        [Authorize]
        public virtual ActionResult RenderPartial_BookingType()
        {
                 
            return PartialView("_BookingType");
        }

        public virtual ActionResult RenderPartial_Booking_1()
        {
            return PartialView("_Booking_1");
        }

        public virtual ActionResult RenderPartial_Booking_2(BookingViewModel model)
        {
            Customer customer = Customer.LoadById(model.CustomerId);

            model.Customer = customer;

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = startTime.AddHours(1);

            model.Price = bookingService.CalculateBookingFee(model.CustomerId, model.CourtId, startTime, endTime);

            return PartialView("_Booking_2", model);
        }

        public virtual ActionResult RenderPartial_Booking_3(BookingViewModel model)
        {
            //str booking ids passed in model

            //model = bookingService.GetUnpaidBookings(model);

            //model.OrderList = bookingService.GetOutstandingBalances(model.CustomerId, 0, 0, model.strBookingIds);

            return PartialView("_Booking_3");
        }

        public virtual ActionResult RenderPartial_CustomerList(string search)
        {
            BookingViewModel model = new BookingViewModel();

            model.CustomerCollection = bookingService.GetCustomerList(search);

            return PartialView("_CustomerList", model);
        }

        public virtual ActionResult RenderPartial_ClassList(string search)
        {
            BookingViewModel model = new BookingViewModel();

            model.EventCollection = bookingService.GetClassList(search);

            return PartialView("_ClassList", model);
        }

        //public virtual ActionResult RenderPartial_ViewOutstandingBalance(int customerId, int bookingId)
        //{
        //    BookingViewModel model = new BookingViewModel()
        //    {
        //        OrderList = bookingService.GetOutstandingBalances(customerId, bookingId, 0, null)
        //    };

        //    return PartialView("_ViewOutstandingBalance", model);
        //}

        public virtual ActionResult RenderPartial_CancelBookng(int bookingId)
        {
            return PartialView("_CancelBooking");
        }

        public virtual ActionResult RenderPartial_ViewBooking(int bookingId)
        {
            BookingViewModel model = bookingService.GetBookingDetails(bookingId);

            return PartialView("_ViewBooking", model);
        }
        
        public virtual ActionResult RenderPartial_DeleteCasualBooking(BookingViewModel model)
        {
            Booking booking = Booking.Load(model.Id);
            model.User = booking.User;
            model.BookingType = booking.BookingType;
            model.Start = DateTime.SpecifyKind(booking.Start, DateTimeKind.Utc);
            model.End = DateTime.SpecifyKind(booking.End, DateTimeKind.Utc);
            model.NumberOfPlayers = booking.NumberOfPlayers;
            model.Price = booking.Price;
            model.isMultiCourt = booking.isMultiCourt;

            return PartialView("_DeleteCasualBooking", model);
        }

        public virtual ActionResult RenderPartial_Class_1(BookingViewModel model)
        {
            return PartialView("_Class_1", model);
        }

        public virtual ActionResult RenderPartial_Class_2(BookingViewModel model)
        {
            model.Event = classService.GetClassDetails(model.EventId);

            return PartialView("_Class_2", model);
        }

        public virtual ActionResult RenderPartial_ViewClass(int bookingId, int eventId)
        {
            BookingViewModel model = bookingService.GetClassBookingDetails(bookingId, eventId);

            return PartialView("_ViewClass", model);
        }

        public virtual ActionResult RenderPartial_ClassBookings(int eventBookingId)
        {
            EventViewModel model = new EventViewModel()
            {
                EventBookings = classService.GetClassBookings(eventBookingId)
            };
                     
            return PartialView("_ClassBookings", model);
        }

        public virtual ActionResult RenderPartial_Alert_AddCourt(int eventBookingId)
        {
            BookingViewModel model = new BookingViewModel()
            {
                EventBooking = EventBooking.LoadById(eventBookingId)
            };

            return PartialView("_Alert_AddCourt", model);
        }

        public virtual ActionResult RenderPartial_AddCourt(BookingViewModel model)
        {

            model.EventBooking = EventBooking.LoadById(model.EventBookingId);
            model.SelectedCourts = new int[] { model.CourtId };


            return PartialView("_AddCourt", model);
        }

        public virtual ActionResult RenderPartial_BreadCrumb(BreadCrumbViewModel model)
        {

            return PartialView("_BreadCrumb", model);
        }

        public virtual ActionResult RenderPartial_ClassDetails(BookingViewModel model)
        {

            return PartialView("_ClassDetails", model);
        }


        public virtual ActionResult RenderPartial_ClassWaitList(int eventBookingId)
        {

            EventViewModel model = classService.GetClassWaitlists(eventBookingId);

            return PartialView("_ClassWaitlist", model);
        }

        public PartialViewResult RenderPartial_ClassAttendees(int eventBookingId)
        {
            EventViewModel model = classService.GetClassRegistrations(eventBookingId);

            return PartialView("_ClassAttendees", model);
        }

        public PartialViewResult RenderPartial_ClassRegistration(EventViewModel model)
        {

            return PartialView("_ClassRegistration", model);
        }

        public PartialViewResult RenderPartial_RegisterCustomer(int eventBookingId)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            EventViewModel model = new EventViewModel()
            {
                EventBooking = eventBooking
            };

            return PartialView("_RegisterCustomer", model);
        }

        public PartialViewResult RenderPartial_WaitlistCustomer(int eventBookingId)
        {
            EventBooking eventBooking = EventBooking.LoadById(eventBookingId);

            EventViewModel model = new EventViewModel()
            {
                EventBooking = eventBooking
            };

            return PartialView("_WaitlistCustomer", model);
        }

        public PartialViewResult RenderPartial_SmsCustomer(int eventBookingId, bool isRegistrations)
        {
            SmsViewModel model = classService.GetSmsRecipients(eventBookingId, isRegistrations);

            return PartialView("_SmsCustomer", model);
        }

        public PartialViewResult RenderPartial_EditClassDetails(int eventBookingId)
        {
            EventViewModel model = classService.GetEventBookingDetails(eventBookingId);

            return PartialView("_ClassDetails_Edit", model);
        }

        public PartialViewResult RenderPartial_ClassBooking_Delete(int eventBookingId)
        {

            var result = classService.GetCancellationDetails(eventBookingId);

            return PartialView("_ClassBooking_Delete", result);
        }

        public PartialViewResult RenderPartial_CustomerProfile(int customerId, bool isRegistration, int eventBookingId)
        {
            EventViewModel model = new EventViewModel()
            {
                Customer = customerService.GetCustomerDetail(customerId),
                IsRegistration = isRegistration,
                EventBooking = EventBooking.LoadById(eventBookingId)
                
            };

            return PartialView("_CustomerProfile", model);
        }

        public PartialViewResult RenderPartial_OutstandingBalance(int customerId, int orderItemId, int eventBookingId)
        {

            var result = new OutstandingBalanceViewModel()
            {
                Customer = Customer.LoadById(customerId),
                RedirectId = eventBookingId,
                CustomerId = customerId,
                OrderItems = bookingService.GetOutstandingOrderItems(customerId, orderItemId)
            };

            return PartialView("_OutstandingBalances", result);
        }

        public virtual JsonResult Courts_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<CourtViewModel>();

            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<Court> courts = CourtCollection.LoadByClub(club);

            foreach (Court c in courts)
            {
                results.Add(new CourtViewModel()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Colour = c.Colour,
                    Text = c.Text,
                    Club = c.Club
                });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult Coaches_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<CoachDisplayViewModel>();

            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            foreach (Coach coach in CoachCollection.LoadByClub(club))
            {
                results.Add(new CoachDisplayViewModel()
                {
                    Id = coach.Id,
                    Name = coach.User.FirstName + " " + coach.User.LastName
                });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult BookingType_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<BookingType>();

            IEnumerable<BookingType> bookingTypes = BookingTypeCollection.LoadAll();

            foreach (BookingType b in bookingTypes)
            {
                results.Add(new BookingType()
                {
                    Id = b.Id,
                    Name = b.Name,
                    Colour = b.Colour
                });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult CancellationType_Read([DataSourceRequest] DataSourceRequest request)
        {

            IEnumerable<CancellationType> result = CancellationTypeCollection.LoadAll().OrderBy(x => x.Sequence);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ClassType_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<BookingType>();

            IEnumerable<BookingType> classTypes = BookingTypeCollection.LoadAll().Where(x => x.BookingCategory == BookingCategory.Class);

            return Json(classTypes, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GenderAge_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<GenderAge>();

            IEnumerable<GenderAge> genderAgeGroups = GenderAgeCollection.LoadAll();

            foreach (GenderAge genderAge in genderAgeGroups)
            {
                results.Add(new GenderAge()
                {
                    Id = genderAge.Id,
                    Name = genderAge.Name
                });
            }


            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult PlayerLevel_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<PlayerLevel>();

            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<PlayerLevel> playerLevels = PlayerLevelCollection.LoadByClub(club);

            foreach (PlayerLevel level in playerLevels)
            {
                results.Add(new PlayerLevel()
                {
                    Id = level.Id,
                    Name = level.Name
                });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult BookingStatus_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<BookingStatus>();

            IEnumerable<BookingStatus> bookingStatuses = BookingStatusCollection.LoadAll();

            foreach (BookingStatus b in bookingStatuses)
            {
                results.Add(new BookingStatus()
                {
                    Id = b.Id,
                    Name = b.Name,
                    Colour = b.Colour
                });
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult Terms_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<BookingStatus>();

            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<Term> terms = TermCollection.LoadByClub(club);

            //foreach (Term b in terms)
            //{
            //    results.Add(new BookingStatus()
            //    {
            //        Id = b.Id,
            //        Name = b.Name,
            //        Colour = b.Colour
            //    });
            //}

            return Json(terms, JsonRequestBehavior.AllowGet);
        }
        
        [Authorize]
        public JsonResult CalculateCasualBookingPrice(BookingViewModel model)
        {
            List<int> courtIds = new List<int>(model.strCourts.Split(',').Select(int.Parse));

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longEnd);

            decimal price = 0;

            foreach(int courtId in courtIds)
            {
               price += bookingService.CalculateBookingFee(model.CustomerId, courtId, startTime, endTime);
            }


            return Json(price, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult CalculateRecurringBookingPrice(BookingViewModel model)
        {
            List<int> courtIds = new List<int>(model.strCourts.Split(',').Select(int.Parse));

            DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longStart);
            DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(model.longEnd);

            decimal singleBookingPrice = 0;

            foreach (int courtId in courtIds)
            {
                singleBookingPrice += bookingService.CalculateBookingFee(model.CustomerId, courtId, startTime, endTime);
            }

            decimal totalPrice = singleBookingPrice * model.Occurrences;

            return Json(totalPrice, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult BookingPayment(BookingViewModel booking)
        {
            SaleViewModel model = new SaleViewModel();

            List<SaleItemViewModel> saleItems = new List<SaleItemViewModel>();

            decimal price = 0;

            foreach (OrderViewModel o in booking.OrderList)
            {
                if (o.IsSelected == true)
                {
                    OrderItem orderItem = OrderItem.LoadById(o.Id);

                    saleItems.Add(new SaleItemViewModel()
                    {
                        OrderItem = orderItem
                    });
                }
            }

            Customer customer = Customer.LoadById(booking.CustomerId);

            model.SaleItems = saleItems;
            model.TotalAmount = price;
            model.Customer = customer;

            TempData["SaleModel"] = model;

            return Json(new { RedirectUrl = Url.Action("Index", "Sale") });
        }

        [Authorize]
        [HttpPost]
        public ActionResult PayBooking(BookingViewModel booking)
        {
            SaleViewModel model = new SaleViewModel();

            List<SaleItemViewModel> saleItems = new List<SaleItemViewModel>();

            decimal price = 0;

            foreach (OrderViewModel o in booking.OrderList)
            {
                if (o.IsSelected == true)
                {
                    OrderItem orderItem = OrderItem.LoadById(o.Id);

                    saleItems.Add(new SaleItemViewModel()
                    {
                        OrderItem = orderItem
                    });
                }
            }

            Customer customer = Customer.LoadById(booking.CustomerId);

            model.SaleItems = saleItems;
            model.TotalAmount = price;
            model.Customer = customer;

            TempData["SaleModel"] = model;

            return Json(new { RedirectUrl = Url.Action("Index", "Sale") });
        }
        [Authorize]
        [HttpPost]
        public ActionResult ProcessClassDetails(BookingViewModel model)
        {
            if (ModelState.IsValid)
            {

            }

            return PartialView("_Class_2", model);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ProcessClassSchedule(BookingViewModel model)
        {
            if (ModelState.IsValid)
            {

            }

            return PartialView("_Class_3", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ProcessClassPrice(BookingViewModel model)
        {
            if (ModelState.IsValid)
            {

            }

            return PartialView("_Class_3", model);
        }

        public RegistrationResult RegisterCustomerToClass(int eventBookingId, int customerId)
        {
            var result = classService.RegisterCustomer(eventBookingId, customerId);

            return result;
        }

        public WaitlistResult WaitlistCustomerToClass(int eventBookingId, int customerId)
        {
            var result = classService.WaitlistCustomer(eventBookingId, customerId);

            return result;
        }

        [Authorize]
        [HttpPost]
        public ActionResult SendSms(SmsViewModel model)
        {
            if (ModelState.IsValid)
            {
                SmsResponseViewModel response = contactService.Send(model);
                response.Reference = model.Reference;
                response.IsRegistration = model.IsRegistrations;

                return PartialView("_SmsCustomer_Response", response);
            }

            model = classService.GetSmsRecipients(model.Reference, model.IsRegistrations);
            ViewBag.Result = "Failed";

            return PartialView("_SmsCustomer", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveClassDetails(EventViewModel model)
        {
            if (ModelState.IsValid)
            {

                classService.UpdateClassBooking(model);

                ViewBag.Result = "Success";

                model = classService.GetEventBookingDetails(model.EventBooking.Id);

                return PartialView("_ClassDetails_Edit", model);
            }

            if (model.Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Events.Event e = Events.Event.LoadById(model.Id);

            if (e == null)
            {
                return HttpNotFound();
            }

            model = classService.GetEventBookingDetails(model.EventBooking.Id);

            ViewBag.Result = "Failed";

            return PartialView("_ClassDetails_Edit", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult OutstandingBalances_Pay(OutstandingBalanceViewModel model)
         {
            if (ModelState.IsValid)
            {

                List<SaleItemViewModel> saleItems = new List<SaleItemViewModel>();

                decimal price = 0;

                foreach (OrderItemViewModel item in model.OrderItems)
                {
                    if(item.IsSelected)
                    {
                        OrderItem orderItem = OrderItem.LoadById(item.Id);

                        saleItems.Add(new SaleItemViewModel()
                        {
                            OrderItem = orderItem
                        });
                    }
                }

                var saleModel = new SaleViewModel()
                {
                    SaleItems = saleItems,
                    Customer = Customer.LoadById(model.CustomerId),
                    TotalAmount = price
                };

                TempData["SaleModel"] = saleModel;

                return Json(new { RedirectUrl = Url.Action("Index", "Sale") });

            }

            ViewBag.Result = "Failed";

            var result = new OutstandingBalanceViewModel()
            {
                Customer = Customer.LoadById(model.CustomerId),
                RedirectId = model.RedirectId,
                //OutstandingBalances = bookingService.GetUnpaidOrders(model.CustomerId, 0, model.RegistrationId, null),
                CustomerId = model.CustomerId,
                RegistrationId = model.RegistrationId
            };

            return PartialView("_OutstandingBalances", result);
        }

    }
}