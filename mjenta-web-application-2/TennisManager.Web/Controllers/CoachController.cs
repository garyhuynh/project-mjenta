﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security;
using Bookings;
using Users;
using Members;
using Clubs;
using TennisManager.Models;
using System.Globalization;
using TennisManager.Services;

namespace TennisManager.Controllers
{
    public class CoachController : Controller
    {

        private CoachService coachService;

        public CoachController()
        {
            this.coachService = new CoachService(_userManager, _signInManager);
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CoachController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [Authorize]
        // GET: Coach
        public ActionResult Index()
        {
            CoachViewModel model = new CoachViewModel();
            model.CoachList = coachService.Read();

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Create(CoachViewModel model)
        {
            if (ModelState.IsValid)
            {
                var task = coachService.Create(model);

                IdentityResult result = await task;

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Coach");
                }
                AddErrors(result);
            }
            // If we got this far, something failed, redisplay form

            return View(model);
        }





        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

    }
}