﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Bookings;
using Users;
using Members;
using Rates;
using Clubs;
using TennisManager.Models;



namespace TennisManager.Controllers
{
    public class SettingController : Controller
    {


        // GET: Setting
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Spreadsheet()
        {
            return View();
        }

        [Authorize]
        public ActionResult CourtHireRates()
        {

            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            Clubs.ClubAdmin admin = Clubs.ClubAdmin.LoadOneByUser(id);
            Clubs.Club club = Clubs.Club.LoadOneById(admin.ClubId);

            IEnumerable<MembershipType> membershipTypes = MembershipTypeCollection.LoadAllByClub(club.Id);
            ViewBag.MembershipTypes = membershipTypes;

            IEnumerable<TimePeriod> timePeriods = TimePeriodCollection.LoadByClub(club);
            ViewBag.TimePeriods = timePeriods;

            IEnumerable<Facility> facilities = FacilityCollection.LoadAllByClub(club.Id);

            List<Zone> zoneList = new List<Zone>();

            foreach (Facility f in facilities)
            {
                IEnumerable<Zone> zones = ZoneCollection.LoadAllByFacility(f.Id);

                foreach (Zone z in zones)
                {
                    zoneList.Add(z);
                }

            }

            ViewBag.Zones = zoneList;

            //TimePeriod newPeriod = new TimePeriod()
            //{
            //    Name = "timetimetime",
            //    Start = DateTime.Parse("01/01/1900 6:00:00 AM"),
            //    End = DateTime.Parse("01/01/1900 11:00:00 PM")
            //};
            //newPeriod.Save();

            return View();
        }

        [Authorize]
        public ActionResult TimePeriodWeekday_Read([DataSourceRequest]DataSourceRequest request)
        {


            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.Load(id);


            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<MembershipTypeTimePeriod> membershipTypeTimePeriodCollection = MembershipTypeTimePeriodCollection.LoadByClubAndIsWeekday(club.Id, true);
            List<object> rowInstances = new List<object>();

            

            foreach(MembershipTypeTimePeriod m in membershipTypeTimePeriodCollection)
            {
                TimePeriodGroup group = TimePeriodGroup.LoadById(m.TimePeriodGroupId);

                foreach (TimePeriod period in TimePeriodCollection.LoadByTimePeriodGroup(group))
                {

                }
            }


            //Get Time Periods
           // IEnumerable<TimePeriod> timePeriods = TimePeriodCollection.LoadAll(club.Id);

            
            

            //Get Time Zone

            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.Now);

            //Define Datasource Result
            DataSourceResult result = new DataSourceResult();

            if (Request.IsLocal)
            {
                result = membershipTypeTimePeriodCollection.ToDataSourceResult(request, collection => new TimePeriodViewModel
                {
                    //Id = instance.Id,
                    //MembershipType = instance.MembershipTypeId,
                    //Start = .Start.Add(offset),
                    //End = .End.Add(offset)
                });
            }
            //else 
            //{
            //    result = timePeriods.ToDataSourceResult(request, timePeriod => new TimePeriodViewModel
            //    {

            //        Id = timePeriod.Id,
            //        MembershipType = timePeriod.MembershipTypeId,
            //        Start = timePeriod.Start,
            //        End = timePeriod.End

            //    });
            //}


            return Json(result);
        }

        [Authorize]
        public ActionResult TimePeriodGroup_Read([DataSourceRequest]DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<TimePeriodGroup> timePeriodGroups = TimePeriodGroupCollection.LoadByClub(club);

            DataSourceResult result = timePeriodGroups.ToDataSourceResult(request, group => new TimePeriodGroupViewModel
            {
                Id = group.Id,
                Name = group.Name,
                IsDefault = group.isDefault,
                Description = group.Description,
                NumberOfTimePeriod = group.NumberOfTimePeriods
            });

            return Json(result);

        }
        
        [Authorize]
        public ActionResult TimePeriodGroupDetail_Read([DataSourceRequest]DataSourceRequest request, int timePeriodGroupId)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            //Get Time Period Group Object
            TimePeriodGroup timePeriodGroup = TimePeriodGroup.LoadById(timePeriodGroupId);

            //Get Time Periods
            IEnumerable<TimePeriod> timePeriods = TimePeriodCollection.LoadByTimePeriodGroup(timePeriodGroup);

            //Get Time Zone

            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.Now);

            TimeSpan info = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            double offset1 = info.TotalHours;

            //Define Datasource Result
            DataSourceResult result = new DataSourceResult();

                result = timePeriods.ToDataSourceResult(request, timePeriod => new TimePeriodViewModel
                {

                    Id = timePeriod.Id,
                    Name = timePeriod.Name,
                    strTimePeriodStart = GetTimePeriodStartTime(timePeriod.Id),
                    strTimePeriodEnd = GetTimePeriodEndTime(timePeriod.Id)

                });

            return Json(result);
        }

        public ActionResult TimePeriod_Update([DataSourceRequest] DataSourceRequest request, TimePeriodViewModel model)
        {
            if (model != null && ModelState.IsValid)
            {
                TimePeriod period = TimePeriod.LoadById(model.Id);
                period.Name = model.Name;
                period.Start = model.Start;
                period.End = model.End;
                period.Save();
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }


        [Authorize]
        public ActionResult CourtHireRates_Read([DataSourceRequest]DataSourceRequest request, bool isWeekday)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);


            IEnumerable<CourtHireRate> rates = CourtHireRateCollection.LoadByClubAndIsWeekday(club.Id, isWeekday);

            //Get Time Zone

            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.UtcNow);

            TimeSpan info = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            double offset1 = info.TotalHours;

            //Define Datasource Result
            DataSourceResult result = new DataSourceResult();
     
                result = rates.ToDataSourceResult(request, rate => new CourtHireRateViewModel
                {
                    Id = rate.Id,
                    Club = rate.ClubId,
                    MembershipType = rate.MembershipType,
                    MembershipTypeId = rate.MembershipTypeId,
                    Zone = rate.Zone,
                    ZoneId = rate.ZoneId,
                    Rate = rate.Rate,
                    TimePeriod = rate.TimePeriodId,
                    strTimePeriodStart = GetTimePeriodStartTime(rate.TimePeriodId),
                    strTimePeriodEnd = GetTimePeriodEndTime(rate.TimePeriodId)
                });
          

            return Json(result);
        }

        [Authorize]
        public ActionResult CourtHireRates_Update([DataSourceRequest] DataSourceRequest request, CourtHireRateViewModel model)
        {
            if (model != null && model.Rate != null)
            {
                CourtHireRate rate = CourtHireRate.LoadById(model.Id);
                rate.Rate = model.Rate;
                rate.Save();             
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        [Authorize]
        public ActionResult MembershipTypeTimePeriod_Read([DataSourceRequest] DataSourceRequest request, MembershipTypeTimePeriodViewModel model)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<MembershipType> membershipTypeCollection = MembershipTypeCollection.LoadByClub(club);
            ViewBag.MembershipTypeCollection = membershipTypeCollection;
           


            IEnumerable<TimePeriodGroup> timePeriodGroups = TimePeriodGroupCollection.LoadByClub(club);
            ViewBag.TimePeriodGroups = timePeriodGroups;
           


            IEnumerable<MembershipTypeTimePeriod> collection = MembershipTypeTimePeriodCollection.LoadByClub(club);

            DataSourceResult result = collection.ToDataSourceResult(request, item => new MembershipTypeTimePeriodViewModel
            {
                Id = item.Id,
                IsWeekday = item.isWeekday,
                //MembershipType = item.MembershipType,
                MembershipTypeId = item.MembershipTypeId,
               // TimePeriodGroup = item.TimePeriodGroup,
                TimePeriodGroupId = item.TimePeriodGroupId,
                DayOfWeek = GetDayOfWeek(item.isWeekday)
            });

            return Json(result);

        }
       
        [Authorize]
        public ActionResult MembershipTypeTimePeriodViewModel_Update([DataSourceRequest] DataSourceRequest request, MembershipTypeTimePeriodViewModel model)
        {

            if (model != null && ModelState.IsValid)
            {
                //Get current Time Period Group of current model
                TimePeriodGroup group = TimePeriodGroup.LoadById(model.TimePeriodGroupId);
                MembershipTypeTimePeriod current = MembershipTypeTimePeriod.LoadById(model.Id);
                 //if same, dont update
                if (model.TimePeriodGroupId != current.TimePeriodGroupId)
                {

                //Get User Identity
                string userId = User.Identity.GetUserId();
                int id = System.Convert.ToInt32(userId);
                Users.User user = Users.User.LoadOneById(id);

                //Get Admin and Club Identity
                ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
                Club club = Club.LoadOneById(admin.ClubId);

                //Update MembershipTypeTimePeriod
                MembershipTypeTimePeriod instance = MembershipTypeTimePeriod.LoadById(model.Id);
                instance.TimePeriodGroupId = model.TimePeriodGroupId;
                instance.Save();

                //Update Court Hire Rates

                //Get all current rates with reference: membership type, time period and isWeekday
                //List<CourtHireRate> ratesList = new List<CourtHireRate>();

                //Remove all current rates
                //TimePeriodGroup periodGroup = TimePeriodGroup.LoadById(model.TimePeriodGroupId);
                foreach (TimePeriod t in TimePeriodCollection.LoadByTimePeriodGroup(current.TimePeriodGroup))
                {
                    IEnumerable<CourtHireRate> rateCollection = CourtHireRateCollection.LoadByMembershipTypeIsWeekdayTimePeriod(model.MembershipTypeId, t.Id, model.IsWeekday);
                    foreach(CourtHireRate c in rateCollection)
                    {
                        CourtHireRate.Delete(c);
           
                    }
                }

                

                

                //Create new rates
                    TimePeriodGroup timePeriodGroup = TimePeriodGroup.LoadById(model.TimePeriodGroupId);
                    
                    foreach (TimePeriod t in TimePeriodCollection.LoadByTimePeriodGroup(timePeriodGroup))
                    {
                        foreach (Zone z in ZoneCollection.LoadByClub(club))
                        {
                            CourtHireRate newRate = new CourtHireRate();
                            newRate.Rate = 0m;
                            newRate.ZoneId = z.Id;
                            newRate.TimePeriodId = t.Id;
                            newRate.MembershipTypeId = model.MembershipTypeId;
                            newRate.IsWeekday = model.IsWeekday;
                            newRate.ClubId = club.Id;
                            newRate.Save();
                        }
                    }
                       


                }
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));

        }


        [Authorize]
        public virtual ActionResult RenderPartial_TimePeriodSettings()
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<MembershipType> membershipTypeCollection = MembershipTypeCollection.LoadByClub(club);
            ViewBag.MembershipTypeCollection = membershipTypeCollection;

            IEnumerable<TimePeriodGroup> timePeriodGroups = TimePeriodGroupCollection.LoadByClub(club);
            ViewBag.TimePeriodGroups = timePeriodGroups;

            return PartialView("_TimePeriodSettings");
        }

        [Authorize]
        public virtual ActionResult RenderPartial_SpecialRates()
        {
            return PartialView("_SpecialRates");
        }

        [Authorize]
        public virtual ActionResult RenderPartial_WeekendRates()
        {
            return PartialView("_WeekendRates");
        }

        [Authorize]
        public string GetTimePeriodStartTime(int timePeriodId)
        {
            TimePeriod period = TimePeriod.LoadById(timePeriodId);
            DateTime startTime = period.StartTime;
           
            return startTime.ToShortTimeString();
        }

        [Authorize]
        public string GetTimePeriodEndTime(int timePeriodId)
        {
            TimePeriod period = TimePeriod.LoadById(timePeriodId);
            DateTime endTime = period.EndTime;

            return endTime.ToShortTimeString();
        }

        [Authorize]
        public string GetDayOfWeek(bool? isWeekday)
        {
            string dayOfWeek = "";

            if (isWeekday == true)
            {
                dayOfWeek = "Weekdays";
            }

            if (isWeekday == false)
            {
                dayOfWeek = "Weekends & Public Holidays";
            }

            return dayOfWeek;
        }
        
        [Authorize]
        public int GetNumberOfTimePeriods(int id)
        {
            TimePeriodGroup group = TimePeriodGroup.LoadById(id);

            return group.NumberOfTimePeriods;
        }

        [HttpGet]
        public virtual PartialViewResult RenderPartial_Test(int id)
        {
            return PartialView("_Test");
        }


        //public TimeSpan GetOffset(DateTime time)
        //{

        //    //return 10;
        //}
    }
}