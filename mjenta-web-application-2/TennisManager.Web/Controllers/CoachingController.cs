﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TennisManager.Controllers
{
    public class CoachingController : Controller
    {
        // GET: Coaching
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrivateLessons()
        {
            return View();
        }

        public ActionResult GroupClasses()
        {
            return View();
        }

        public ActionResult CardioTennis()
        {
            return View();
        }

        public ActionResult HotShots()
        {
            return View();
        }

        // GET: Coaching/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Coaching/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Coaching/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Coaching/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Coaching/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Coaching/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Coaching/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
