﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security;
using Bookings;
using Users;
using Classification;
using Members;
using Clubs;
using TennisManager.Models;
using System.Globalization;
using TennisManager.Services;

namespace TennisManager.Controllers
{
    public class ClassController : Controller
    {

        private ClassService classService;

        public ClassController()
        {
            this.classService = new ClassService();

        }



        [Authorize]
        public ActionResult Index(EventViewModel model)
        {

            return View(classService.Read(model));
        }

        [Authorize]
        public ActionResult Create()
        {
            EventViewModel model = new EventViewModel();

            return View(model);
        }


        [Authorize]
        [HttpPost]
        public ActionResult Create(EventViewModel model)
        {
            if (ModelState.IsValid)
            {
                classService.Insert(model);

                return RedirectToAction("Index", "Class");
            }

            model.MembershipTypes = classService.GetMembershipTypes();

            return View(model);
        }

        public virtual JsonResult Days_Read([DataSourceRequest] DataSourceRequest request)
        {
            var results = new List<DaysOfWeek>();

            IEnumerable<DaysOfWeek> days = DaysOfWeekCollection.LoadAll();

            return Json(days, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult RenderPartial_PricingOption(EventViewModel model)
        {
            model.Fees = classService.GetFeeStructure();

            return PartialView("_PricingOption", model);
        }


        public PartialViewResult RenderPartial_ClassList(EventViewModel model)
        {
            model = classService.Read(model);

            return PartialView("_ViewClassList", model);
        }

        public PartialViewResult RenderPartial_ClassDetails(int eventId)
        {
            EventViewModel model = classService.GetClassDetails(eventId);

            return PartialView("_ClassDetails", model);
        }

        public PartialViewResult RenderPartial_ClassBookings(int eventId)
        {
            EventViewModel model = new EventViewModel()
            {
                Id = eventId
            };

            return PartialView("_ClassBookings", model);
        }

        //public PartialViewResult RenderPartial_ClassBookingList(int eventId)
        //{

        //    EventViewModel model = classService.GetClassBookings(eventId);

        //    return PartialView("_ClassBookingList", model);
        //}


        public PartialViewResult RenderPartial_ClassRegistrations(EventViewModel model)
        {

            return PartialView("_ClassBookings", model);
        }

        public PartialViewResult RenderPartial_ClassPricing(int eventId)
        {
            EventViewModel model = classService.GetClassPricing(eventId);

            return PartialView("_ClassPricing", model);
        }

        public PartialViewResult RenderPartial_ClassRegistrations(int eventId)
        {
            //EventViewModel model = classService.GetClassRegistrations(eventId);

            //return PartialView("_ClassRegistrations", model);
            return PartialView("_ClassRegistrations");
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveClassDetails(EventViewModel model)
        {
            if (ModelState.IsValid)
            {

                classService.Update(model);

                ViewBag.Result = "Success";

                //update model

                Events.Event ee = Events.Event.LoadById(model.Id);

                model = classService.GetClassDetails(ee.Id);

                return PartialView("_ClassDetails", model);
            }

            if (model.Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Events.Event e = Events.Event.LoadById(model.Id);

            if (e == null)
            {
                return HttpNotFound();
            }

            model = classService.GetClassDetails(e.Id);

            ViewBag.Result = "Failed";

            return PartialView("_ClassDetails", model);
        }


    }
}