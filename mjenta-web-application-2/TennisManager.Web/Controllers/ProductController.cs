﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Users;
using Clubs;
using Microsoft.AspNet.Identity;
using Products;
using TennisManager.Models;
using TennisManager.Services;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.Routing;
using ImageProcessor.Web;

namespace TennisManager.Controllers
{
    public class ProductController : Controller
    {
        private ProductService productService;

        public ProductController()
        {
            this.productService = new ProductService();
        }

        [Authorize]
        public ActionResult Index(ProductViewModel model)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.Club = club;
            model.PageSize = 15;
            model = productService.Read(model);

            ViewBag.Search = model.Search;

            return View(model);
        }

        [Authorize]
        public ActionResult Inventory()
        {
            return View();
        }

        [Authorize]
        public ActionResult Passes()
        {
            return View();
        }

        [Authorize]
        public ActionResult Discount()
        {
            return View();
        }

        [Authorize]
        public ActionResult Settings()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Search(ProductViewModel model)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            model.Club = club;

            model = productService.Read(model);

            ViewBag.Search = model.Search;


            return Index(model);
        }

        [Authorize]
        public ActionResult Create()
        {
            ProductViewModel model = new ProductViewModel();

            model.HasVariant = false;
            model.HasMemberPrice = false;

            return View(model);
        }

        [Authorize]
        public ActionResult Details(int? id)
        {
            int productId = id.GetValueOrDefault();


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Product product = Product.LoadById(productId);

            if (product == null)
            {
                return HttpNotFound();
            }

            ProductViewModel model = productService.GetProductDetails(product);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(ProductViewModel model, IEnumerable<HttpPostedFileBase> ProductImage)
        {
            if (ModelState.IsValid)
            {

                string userId = User.Identity.GetUserId();
                int id = System.Convert.ToInt32(userId);
                Users.User user = Users.User.LoadOneById(id);

                //Get Admin and Club Identity
                ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
                Club club = Club.LoadOneById(admin.ClubId);

                model.Club = club;


                if (ProductImage != null)
                {
                    FileStorageModel fileModel = new FileStorageModel();
                    fileModel.Files = ProductImage;
                    model.FileStorage = fileModel;
                }

                productService.Insert(model);

                return RedirectToAction("Index", "Product");
            }

            return View();
        }

        public virtual JsonResult ProductType_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<ProductType> productTypes = ProductTypeCollection.LoadByClub(club);

            return Json(productTypes, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ProductClass_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<ProductClass> productClasses = ProductClassCollection.LoadByClub(club);

            return Json(productClasses, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ProductVendor_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<ProductVendor> productVendors = ProductVendorCollection.LoadByClub(club);

            return Json(productVendors, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ProductOption_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<ProductOption> productOptions = ProductOptionCollection.LoadByClub(club);

            List<ProductOptionViewModel> productOptionsList = new List<ProductOptionViewModel>();

            foreach (ProductOption p in productOptions)
            {
                productOptionsList.Add(new ProductOptionViewModel()
                {
                    Id = p.Id,
                    Name = p.Name
                });
            }

            return Json(productOptionsList, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult ProductBrand_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<ProductBrand> productBrands = ProductBrandCollection.LoadByClub(club);

            return Json(productBrands, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult RenderPartial_AddProductBrand()
        {
            return PartialView("_AddProductBrand");
        }

        public virtual ActionResult RenderPartial_AddProductType()
        {
            return PartialView("_AddProductType");
        }

        public virtual ActionResult RenderPartial_AddProductClass()
        {
            return PartialView("_AddProductClass");
        }

        public virtual ActionResult RenderPartial_AddProductVendor()
        {
            return PartialView("_AddProductVendor");
        }

        public virtual ActionResult RenderPartial_AddProductOption()
        {
            return PartialView("_AddProductOption");
        }

        public virtual ActionResult RenderPartial_ViewProductVariant(ProductViewModel model)
        {
            List<ProductVariantViewModel> variantList = new List<ProductVariantViewModel>();

            Product product = Product.LoadById(model.Id);

            model.InventoryOption = product.InventoryOption;
            model.HasMemberPrice = product.hasMemberPrice;

            IEnumerable<ProductVariant> variants = ProductVariantCollection.LoadByProduct(product);

            foreach (ProductVariant variant in variants)
            {
                variantList.Add(new ProductVariantViewModel()
                {
                    Id = variant.Id,
                    Barcode = variant.Barcode,
                    ProductOption = product.ProductOption,
                    ProductOptionValue = variant.ProductOptionValue,
                    Quantity = variant.Quantity,
                    RetailPrice = variant.RetailPrice,
                    MemberPrice = variant.MemberPrice
                });
            }

            model.ProductVariants = variantList;

            return PartialView("_ViewProductVariant", model);
        }

        public virtual ActionResult RenderPartial_ViewProductInfo(int productId)
        {
            Product product = Product.LoadById(productId);

            int inventoryOptionId = 0;

            if (product.InventoryOption == InventoryOption.do_not_track)
            {
                inventoryOptionId = 1;
            }


            ProductViewModel model = new ProductViewModel()
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                ProductBrandId = product.ProductBrandId,
                ProductTypeId = product.ProductTypeId,
                ProductClassId = product.ProductClassId,
                ProductVendorId = product.ProductVendorId,
                HasVariant = product.hasVariant,
                HasMemberPrice = product.hasMemberPrice,
                InventoryOptionId = inventoryOptionId

            };

            if (product.hasVariant == true)
            {
                List<ProductVariantViewModel> variants = new List<ProductVariantViewModel>();

                foreach (ProductVariant variant in ProductVariantCollection.LoadByProduct(product))
                {
                    variants.Add(new ProductVariantViewModel()
                    {
                        Id = variant.Id,
                        Barcode = variant.Barcode,
                        ProductOption = product.ProductOption,
                        ProductOptionValue = variant.ProductOptionValue,
                        Quantity = variant.Quantity,
                        RetailPrice = variant.RetailPrice,
                        MemberPrice = variant.MemberPrice
                    });
                }
                model.ProductVariants = variants;
            }

            return PartialView("_ViewProductInfo", model);
        }

        public virtual ActionResult RenderPartial_ViewProductDiscount(int productId)
        {
            return PartialView("_ViewProductDiscount");
        }

        public virtual ActionResult RenderPartial_ViewProductImage(int productId)
        {
            Product product = Product.LoadById(productId);

            ProductViewModel model = new ProductViewModel()
            {
                Id = productId,
                HasImages = false
            };

            if (product.hasImages == true)
            {
                model.HasImages = true;
                model.Images = productService.GetProductImage(productId);
            }


            return PartialView("_ViewProductImage", model);
        }

        public virtual ActionResult RenderPartial_DeleteProductVariant(int productVariantId)
        {
            ProductVariant variant = ProductVariant.LoadById(productVariantId);

            ProductVariantViewModel model = new ProductVariantViewModel()
            {
                Id = variant.Id,
                Barcode = variant.Barcode,
                RetailPrice = variant.RetailPrice,
                ProductOption = variant.Product.ProductOption,
                ProductOptionValue = variant.ProductOptionValue,
                Product = variant.Product
            };



            return PartialView("_DeleteProductVariant", model);
        }

        public virtual ActionResult RenderPartial_AddProductVariant(int productId)
        {
            Product product = Product.LoadById(productId);

            ProductVariantViewModel model = new ProductVariantViewModel()
            {
                Product = product,
                ProductOption = product.ProductOption
            };

            return PartialView("_AddProductVariant", model);
        }

        public JsonResult AddProductType(string productType)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            ProductType productTypeCheck = ProductType.LoadOneByNameClub(productType, club.Id);

            if (productTypeCheck != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }



            ProductType newProductType = new ProductType()
            {
                Name = productType,
                Club = club
            };
            newProductType.Save();

            return Json(newProductType, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddProductClass(string productClassName)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            ProductClass productClassCheck = ProductClass.LoadOneByNameClub(productClassName, club.Id);

            if (productClassCheck != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            ProductClass newProductClass = new ProductClass()
            {
                Name = productClassName,
                Club = club
            };
            newProductClass.Save();

            return Json(newProductClass, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddProductVendor(string productVendorName)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            ProductVendor productVendorCheck = ProductVendor.LoadOneByNameClub(productVendorName, club.Id);

            if (productVendorCheck != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            ProductVendor newProductVendor = new ProductVendor()
            {
                Name = productVendorName,
                Club = club
            };
            newProductVendor.Save();

            return Json(newProductVendor, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddProductBrand(string productBrandName)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            ProductBrand productBrandCheck = ProductBrand.LoadOneByNameClub(productBrandName, club.Id);

            if (productBrandCheck != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            ProductBrand newProductBrand = new ProductBrand()
            {
                Name = productBrandName,
                Club = club
            };
            newProductBrand.Save();

            return Json(newProductBrand, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddProductOption(string productOptionName)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            ProductOption productOptionCheck = ProductOption.LoadOneByNameClub(productOptionName, club.Id);

            if (productOptionCheck != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            ProductOption newProductOption = new ProductOption()
            {
                Name = productOptionName,
                Club = club
            };
            newProductOption.Save();

            return Json(newProductOption, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddProductVariant(ProductVariantViewModel model)
        {

            //incomplete

            int duplicates = ProductVariantCollection.CountDuplicates(model.ProductId, model.ProductOptionValue);

            if (duplicates != 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            ProductVariant variant = new ProductVariant()
            {
                ProductId = model.ProductId,
                Barcode = model.Barcode,
                Quantity = model.Quantity,
                ProductOptionValue = model.ProductOptionValue,
                RetailPrice = model.RetailPrice
            };
            variant.Save();

            int variantCount = ProductVariantCollection.CountByProduct(model.ProductId);
            Product product = Product.LoadById(model.ProductId);
            product.VariantCount = variantCount;
            product.Save();


            return Json(variant, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteProductVariant(int productVariantId)
        {
            DeleteVariantResult result = productService.DeleteProductVariant(productVariantId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadImage(ProductViewModel model, IEnumerable<HttpPostedFileBase> ProductImage)
        {

            if (ProductImage != null)
            {
                Product product = Product.LoadById(model.Id);

                FileStorageModel fileModel = new FileStorageModel()
                {
                    Container = product.Reference,
                    ObjectId = model.Id,
                    Files = ProductImage
                };

                productService.UploadImage(fileModel);

            }

            string url = "~/Product/Details/" + model.Id + "#product-images";

            return Redirect(url);
        }

        [Authorize]
        [HttpPost]
        public ActionResult DeleteImage(ProductViewModel model)
        {

            Product product = Product.LoadById(model.Id);
            ProductImage productImage = ProductImage.LoadById(model.ImageDeleteId);

            FileStorageModel fileModel = new FileStorageModel()
            {
                Container = product.Reference,
                ImageId = model.ImageDeleteId,
                FileReference = productImage.Reference,
                ObjectId = product.Id
            };

            productService.DeleteImage(fileModel);

            string url = "~/Product/Details/" + model.Id + "#product-images";

            return Redirect(url);
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveProductInfo(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                int id = System.Convert.ToInt32(userId);
                Users.User user = Users.User.LoadOneById(id);

                //Get Admin and Club Identity
                ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
                Club club = Club.LoadOneById(admin.ClubId);

                model.Club = club;

                productService.Update(model);

                ViewBag.Result = "Success";

                //update model

                Product p = Product.LoadById(model.Id);

                model = productService.GetProductDetails(p);

                return PartialView("_ViewProductInfo", model);
            }

            if (model.Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Product product = Product.LoadById(model.Id);

            if (product == null)
            {
                return HttpNotFound();
            }

            model = productService.GetProductDetails(product);

            ViewBag.Result = "Failed";

            return PartialView("_ViewProductInfo", model);
        }
    }
}
