﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Security.Claims;
using Kendo.Mvc.UI;
using Kendo.Mvc.Resources;
using Kendo.Mvc.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security;
using Bookings;
using Users;
using Members;
using Clubs;
using TennisManager.Models;
using System.Globalization;
using TennisManager.Services;

namespace TennisManager.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerService customerService;

        public CustomerController()
        {
            this.customerService = new CustomerService(_userManager, _signInManager);
        }


        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CustomerController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Customer
        [Authorize]
        public ActionResult Index()
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Search = "";

            return View(model);
        }

        [Authorize]
        public ActionResult Create()
        {

            CustomerViewModel model = new CustomerViewModel();
            model.MembershipTypeList = customerService.GetMembershipTypes();

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Create(CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var task = customerService.CreateUser(model);

                IdentityResult result = await task;

                if (result.Succeeded)
                {

                    return RedirectToAction("Index", "Customer");
                }
                AddErrors(result);
            }
            // If we got this far, something failed, redisplay form

            model.MembershipTypeList = customerService.GetMembershipTypes();

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = Customer.LoadById(id.GetValueOrDefault());

            //RoleCollection roles = customer.User.Roles;

            if (customer == null)
            {
                return HttpNotFound();
            }

            CustomerViewModel model = new CustomerViewModel()
            {
                Id = customer.Id,
                FirstName = customer.User.FirstName,
                LastName = customer.User.LastName
                
            };

            return View(model);
        }

        #region Render Parial Views

        public PartialViewResult RenderPartial_CustomerList(int? pageIndex, string searchValue)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Search = searchValue;

            Pagination pagination = new Pagination()
            {
                PageIndex = System.Convert.ToInt32(pageIndex),
                PageSize = 15
            };

            model.Pagination = pagination;

            model.CustomerCollection = customerService.Read(model);

            return PartialView("_CustomerList", model);
        }

        public PartialViewResult RenderPartial_CustomerDetail(int? customerId)
        {
            CustomerViewModel model = customerService.GetCustomerDetail(customerId.GetValueOrDefault());

            return PartialView("_CustomerDetail", model);
        }

        public PartialViewResult RenderPartial_CustomerBooking(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerBooking", model);
        }

        public PartialViewResult RenderPartial_CustomerBookingList(int customerId, int bookingTypeId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.CustomerBookings = customerService.GetCustomerBookings(customerId, bookingTypeId);

            return PartialView("_CustomerBookingList", model);
        }

        public PartialViewResult RenderPartial_CustomerRegistration(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerRegistration", model);
        }

        public PartialViewResult RenderPartial_CustomerAcount(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerAccount", model);
        }

        public PartialViewResult RenderPartial_CustomerMembership(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerMembership", model);
        }

        public PartialViewResult RenderPartial_CustomerInvoice(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerInvoice", model);
        }

        public PartialViewResult RenderPartial_CustomerCommunication(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerCommunication", model);
        }

        public PartialViewResult RenderPartial_CustomerNote(int customerId)
        {
            CustomerViewModel model = new CustomerViewModel();

            model.Id = customerId;

            return PartialView("_CustomerNote", model);
        }

        #endregion

        #region Dropdown List Binding

        public virtual JsonResult BookingType_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            BookingCategory booking = BookingCategory.Booking;

            IEnumerable<BookingType> bookingTypes = BookingTypeCollection.LoadAll().Where(x => x.BookingCategory == booking);

            return Json(bookingTypes, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult RegistrationType_Read([DataSourceRequest] DataSourceRequest request)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            BookingCategory enumClass = BookingCategory.Class;
            BookingCategory enumComp = BookingCategory.Competition;

            IEnumerable<BookingType> registrationTypes = BookingTypeCollection.LoadAll().Where((x => x.BookingCategory == enumClass || x.BookingCategory == enumComp));

            return Json(registrationTypes, JsonRequestBehavior.AllowGet);
        }

        

        public virtual JsonResult Dob_Day_Read([DataSourceRequest] DataSourceRequest request)
        {
            List<Day> days = new List<Day>();
       
            for (int i = 1; i <= 31; i++)
            {
                days.Add(new Day()
                {
                    Value = i,
                    Text = i.ToString()
                });
            }
            
            return Json(days, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult Dob_Month_Read([DataSourceRequest] DataSourceRequest request)
        {
            List<Month> months = new List<Month>()
            {
                new Month(){ Value = 1, Text = "January" },
                new Month(){ Value = 2, Text = "February" },
                new Month(){ Value = 3, Text = "March" },
                new Month(){ Value = 4, Text = "April" },
                new Month(){ Value = 5, Text = "May" },
                new Month(){ Value = 6, Text = "June" },
                new Month(){ Value = 7, Text = "July" },
                new Month(){ Value = 8, Text = "August" },
                new Month(){ Value = 9, Text = "September" },
                new Month(){ Value = 10, Text = "Obtober" },
                new Month(){ Value = 11, Text = "November" },
                new Month(){ Value = 12, Text = "December" }
            };

     

            return Json(months, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult Dob_Year_Read([DataSourceRequest] DataSourceRequest request)
        {
            int currentYear = DateTime.UtcNow.Year;

            List<Day> years = new List<Day>();

            for (int i = 1900; i <= currentYear; i++)
            {
                years.Add(new Day()
                {
                    Value = i,
                    Text = i.ToString()
                });
            }

            return Json(years, JsonRequestBehavior.AllowGet);
        }


        #endregion

        [Authorize]
        [HttpPost]
        public ActionResult SaveCustomerDetail(CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                customerService.Update(model);

                ViewBag.Result = "Success";

                model = customerService.GetCustomerDetail(model.Id);

                return PartialView("_CustomerDetail", model);
            }

            if (model.Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Customer product = Customer.LoadById(model.Id);

            if (product == null)
            {
                return HttpNotFound();
            }

            model = customerService.GetCustomerDetail(model.Id);

            ViewBag.Result = "Failed";

            return PartialView("_CustomerDetail", model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

    }
}