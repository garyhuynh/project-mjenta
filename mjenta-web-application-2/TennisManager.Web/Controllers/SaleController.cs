﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TennisManager.Models;
using Orders;
using System.Net;
using Users;
using Microsoft.AspNet.Identity;
using Clubs;
using Products;
using TennisManager.Services;
using Newtonsoft.Json;
using Accounts;
using Rotativa;
using Invoices;


namespace TennisManager.Controllers
{
    public class SaleController : Controller
    {
        private SaleService saleService;
        private BookingService bookingService;
        private OrderService orderService;
        private InvoiceService invoiceService;
        private PaymentService paymentService;

        public SaleController()
        {
            this.saleService = new SaleService();
            this.bookingService = new BookingService();
            this.orderService = new OrderService();
            this.invoiceService = new InvoiceService();
            this.paymentService = new PaymentService();
        }

        // GET: Sale
        [Authorize]
        public ActionResult Index(SaleViewModel model)
        {
            var newModel = TempData["SaleModel"] as SaleViewModel;

            if (newModel != null)
            {
                return View(newModel);
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Process(SaleViewModel model)
        {
            return View("Index");
        }

        [ChildActionOnly]
        public PartialViewResult RenderPartial_ProductLookup()
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            List<ProductLookupViewModel> productList = new List<ProductLookupViewModel>();

            foreach (ProductVariant variant in ProductVariantCollection.LoadByClubId(club.Id))
            {
                string variantValue = "";

                if (variant.Product.hasVariant == true)
                {
                    variantValue = variant.ProductOptionValue;
                }

                productList.Add(new ProductLookupViewModel()
                {
                    ProductVariantId = variant.Id,
                    DisplayName = variant.Product.Title,
                    RetailPrice = variant.RetailPrice,
                    MemberPrice = variant.MemberPrice,
                    ThumbnailUrl = variant.Product.PhotoUrl,
                    HasVariant = variant.Product.hasVariant,
                    Variant = variantValue,
                    HasMemberPrice = variant.Product.hasMemberPrice
                });
            }


            return PartialView("_ProductLookup", productList);
        }

        [Authorize]
        public JsonResult GetProductDetails(int variantId, bool isMember)
        {
            ProductVariant variant = ProductVariant.LoadById(variantId);

            // get display name for variant and non variant
            string displayName = "";
            if (variant.Product.hasVariant == true)
            {
                displayName = variant.Product.Title + "&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;" + variant.ProductOptionValue;
            }
            else
            {
                displayName = variant.Product.Title;
            }


            // get item price
            decimal salePrice = 0;

            if (isMember == true)
            {
                if (variant.Product.hasMemberPrice == true)
                {
                    salePrice = variant.MemberPrice;
                }
                if (variant.Product.hasMemberPrice == false)
                {
                    salePrice = variant.RetailPrice;
                }
            }
            else
            {
                salePrice = variant.RetailPrice;
            }

            SaleItemViewModel model = new SaleItemViewModel()
            {
                ProductVariantId = variant.Id,
                DisplayName = displayName,
                SalePrice = salePrice,
                HasVariant = variant.Product.hasVariant,
                Variant = variant.ProductOptionValue,
                ThumbnailUrl = variant.Product.PhotoUrl,
                Taxable = variant.Product.Taxable
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult GetProductPrices(int productVariantId)
        {
            ProductVariant variant = ProductVariant.LoadById(productVariantId);

            // get item price
            decimal salePrice = 0;

            if (variant.Product.hasMemberPrice == true)
            {
                salePrice = variant.MemberPrice;
            }
            if (variant.Product.hasMemberPrice == false)
            {
                salePrice = variant.RetailPrice;
            }

            SaleItemViewModel model = new SaleItemViewModel()
            {
                HasMemberPrice = variant.Product.hasMemberPrice,
                RetailPrice = variant.RetailPrice,
                MemberPrice = variant.MemberPrice
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult GetCustomerDetails(int customerId)
        {
            Customer customer = Customer.LoadById(customerId);

            CustomerViewModel model = new CustomerViewModel()
            {
                Id = customer.Id,
                FirstName = customer.User.FirstName,
                LastName = customer.User.LastName,
                IsMember = customer.isMember
            };
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public virtual ActionResult RenderPartial_CustomerSearch(string search)
        {
            //Get User Identity
            string userId = User.Identity.GetUserId();
            int id = System.Convert.ToInt32(userId);
            Users.User user = Users.User.LoadOneById(id);

            //Get Admin and Club Identity
            ClubAdmin admin = ClubAdmin.LoadOneByUser(id);
            Club club = Club.LoadOneById(admin.ClubId);

            IEnumerable<CustomerViewModel> CustomerList = saleService.GetCustomerSearch(search, club.Id);

            return PartialView("_CustomerTable", CustomerList);
        }

        public PartialViewResult RenderPartial_SaleItem(SaleItemViewModel model)
        {
            model.Details = orderService.GetDetails(model.OrderItem);

            return PartialView("_SaleItem", model);
        }

        public PartialViewResult RenderPartial_AddItem(int productVariantId, bool isMember)
        {
            ProductVariant variant = ProductVariant.LoadById(productVariantId);

            OrderItem orderItem = new OrderItem();
            orderItem.OrderItemType = OrderItemType.product;
            orderItem.ProductVariant = variant;

            var result = new SaleItemViewModel()
            {
                OrderItem = orderItem,
                isMember = isMember
            };

            return PartialView("_SaleItem", result);
        }

        [Authorize]
        public virtual ActionResult RenderPartial_SaleSummary(string data)
        {
            SaleViewModel model = JsonConvert.DeserializeObject<SaleViewModel>(data);

            if (model.CustomerId != 0)
            {
                model.Customer = Customer.LoadById(model.CustomerId);
            }

            foreach (var item in model.SaleItems)
            {

                item.OrderItemType = (OrderItemType)Enum.ToObject(typeof(OrderItemType), item.OrderItemTypeId);


                if ((item.OrderItemType == OrderItemType.booking) || (item.OrderItemType == OrderItemType.registration))
                {
                    item.DetailList = orderService.GetBookingDetails(item.MetaDataId, item.OrderItemType);
                }
            }

            return PartialView("_SaleSummary", model);
        }

        public ActionResult RenderPartial_PaymentType(SaleViewModel model)
        {
            return PartialView("_PaymentTypes", model);
        }

        public ActionResult RenderPartial_SaleComplete(string data)
        {
            SaleViewModel model = JsonConvert.DeserializeObject<SaleViewModel>(data);

            model.Order = saleService.CreateOrder(model);

            model.Invoice = invoiceService.Insert(model);

            paymentService.Insert(model);

            return PartialView("_SaleComplete");
        }

        [Authorize]
        [HttpPost]
        public ActionResult ProcessPayment(SaleViewModel model)
        {
            if (ModelState.IsValid)
            {
                return PartialView("", model);
            }

            if (model.Id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView("", model);
        }

        public ActionResult PrintInvoice(int invoiceId)
        {

            var model = new InvoiceViewModel()
            {
                Note = "this is a note..."
            };

            return new PartialViewAsPdf("_Invoice", model);
        }

        public virtual ActionResult RenderPartial_Invoice(int invoiceId)
        {
            var model = new InvoiceViewModel();
            model.Id = invoiceId;
            model.Note = "This is a note";

            return PartialView("_Invoice", model);
        }

        public virtual ActionResult RenderPartial_PaymentLine(decimal amount, int paymentTypeId)
        {

            PaymentViewModel model = new PaymentViewModel()
            {
                Amount = amount,
                SourceType = (SourceType)Enum.ToObject(typeof(SourceType), paymentTypeId)    
            };

            return PartialView("_PaymentLine", model);
        }


        [Authorize]
        [HttpPost]
        public ActionResult Insert(SaleViewModel model)
        {
            return PartialView("_SaleComplete");
        }
    }
}